function add(id,o)
{
    alert(id);
     alert(o);
}

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
}

function Tarea(){
  this.tarea_id = $('#tarea_id').val();
  this.orden_id = $('#orden_id').val(); 
}

function Movimiento(){
  this.articulo_id = $('#articulo_id').val();
  this.tipo = $('#tipo').val();
  this.activo_id = $('#activo_id').val();
  this.orden_id = $('#orden_id').val();
  this.cantidad = $('#cantidad').val();
  this.razon = $('#razon').val();
}

function Completar(){
  this.activo_id = $('#activo_id').val();
  this.orden_id = $('#orden_id').val();
  this.termina = $('#termina').val();
  this.horas = $('#horas').val();
  this.kilometros = $('#kilometros').val();
}

function removeStylemovimiento(){
  $('#field-cantidad').removeClass("has-error");
  $('#field-cantidad .msj-error').html("");

  $('#field-razon').removeClass("has-error");
  $('#field-razon .msj-error').html("");
}

function removeStylecompletar(){

  $('#field-termina').removeClass("has-error");
  $('#field-termina .msj-error').html("");
  $('#field-horas').removeClass("has-error");
  $('#field-horas .msj-error').html("");
  $('#field-kilometros').removeClass("has-error");
  $('#field-kilometros .msj-error').html("");
}

function cambiarDisplay(div1,div2) {
  if (!document.getElementById) return false;
  fila = document.getElementById(div1);
  fila2 = document.getElementById(div2);
  $('#tarea_id').val('').trigger('change');
  $('#articulo_id').val('').trigger('change');
  $('#cantidad').val('');
  $('#razon').val('');
  if (fila.style.display != "none") {
    fila.style.display = "none"; //ocultar fila 
    fila2.style.display = ""; //mostrar fila  
  } else {
    fila.style.display = ""; //mostrar fila 
    fila2.style.display = "none"; //ocultar fila 
  }
}


$.getJSON(ruta+"/ordenes/"+ $('#activo_id').val() +"/getTareas", function(taras){

    taras = $.map(taras, function(item) {
            return { id: item.id, text: item.nombre }; 
        });

    $("#tarea_id").select2({
    placeholder: "Seleccione",
    tags:true,
    data: taras,
    createTag: function (params) {
      return {
        id: params.term,
        text: params.term,
        newOption: true }
      },
    templateResult: function (data) {
      var $result = $("<span></span>");
      $result.text(data.text);
      if (data.newOption) {
        $result.append(" <em>(Nuevo)</em>");
      }
      return $result;
    },
    width:'92%',
    language: "es",
    })
});

$.getJSON(ruta+"/ordenes/getArticulos", function(insumos){

    insumos = $.map(insumos, function(item) {
            return { id: item.id, text: item.full }; 
        });

    $("#articulo_id").select2({
    placeholder: "Seleccione",
    tags:true,
    data: insumos,
    createTag: function (params) {
      return {
        id: params.term,
        text: params.term,
        newOption: true }
      },
    templateResult: function (data) {
      var $result = $("<span></span>");
      $result.text(data.text);
      if (data.newOption) {
        $result.append(" <em>(Nuevo)</em>");
      }
      return $result;
    },
    width:'92%',
    language: "es",
    })
});


// --------------------------------------------------ELIMINAR ACTIVO---------------------------------------
var RUTA_ORDENES = ruta+'/ordenes';

function delete_Orden(id){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ORDENES+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
              showConfirmButton: false,
              timer: 2000
            })
            setTimeout("location.href= RUTA_ORDENES", 2050);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar la orden. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'la orden se ha eliminado exitosamente',
      'success'
      );
  });
}

function delete_Tarea(orden,tarea){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ORDENES+"/"+orden+"/borrar_tarea/"+tarea;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
            })
            $("#tareas").load(location.href + " #tareas");
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al activo. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'La tarea se ha eliminado exitosamente',
      'success'
      );
  });
}

function delete_Movimiento(movimiento){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ORDENES+"/borrar_movimiento/"+movimiento;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
            })
            $("#articulos").load(location.href + " #articulos");
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al activo. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'La tarea se ha eliminado exitosamente',
      'success'
      );
  });
}

$('#guardar-tarea').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Tarea();
  type = 'POST';
  route = RUTA_ORDENES+"/agregar_tarea";
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        );
      $('#tarea_id').val('').trigger('change');
      fila = document.getElementById('div1');
      fila2 = document.getElementById('div2');
      fila.style.display = "none"; //ocultar fila 
      fila2.style.display = ""; //mostrar fila
      $("#tareas").load(location.href + " #tareas");
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de guardar los datos. La tarea ya puede estar relacionada o se requieren campos. Status: '+jqXHR.status,
          'error'
          )
      }
  });
});

$('#guardar-movimiento').click(function(){
  var btn = this
  starLoad(btn)
  var data = new Movimiento();  
  $.ajax({
    url: ruta+'/movimientos',
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: 'POST',
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStylemovimiento();
      sweetAlert(
        'Exito!',
        'Se ha realizado el moviemiento de articulo de forma exitosa! ',
        'success'
        )
      $('#articulo_id').val('').trigger('change');
      $('#cantidad').val('');
      $('#razon').val('');
      fila = document.getElementById('div3');
      fila2 = document.getElementById('div4');
      fila.style.display = "none"; //ocultar fila 
      fila2.style.display = ""; //mostrar fila
      $("#articulos").load(location.href + " #articulos");
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylemovimiento()       
          if(jqXHR.responseJSON.cantidad){         
            $('#field-cantidad').addClass("has-error");
            $('#field-cantidad .msj-error').html(jqXHR.responseJSON.cantidad);
          }
          if(jqXHR.responseJSON.razon){         
            $('#field-razon').addClass("has-error");
            $('#field-razon .msj-error').html(jqXHR.responseJSON.razon);
          }
        }
      else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de realizar el movimiento. Posible error: No hay suficiente en stock para realizar el movimientos. Status: '+jqXHR.status,
          'error'
          )
        }   
      }
  });
});

$('#guardar-completar').click(function(){
  var type = 'PUT';
  var route = RUTA_ORDENES+"/completar/"+$('#orden_id').val();
  var btn = this
  starLoad(btn)
  var data = new Completar();
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn);
      removeStylecompletar();
      $("#detalles").load(location.href + " #detalles");
      $("#tareas").load(location.href + " #tareas");
      $("#div2").load(location.href + " #div2");
      $("#articulos").load(location.href + " #articulos");
      $("#div4").load(location.href + " #div4");
      $("#botones").load(location.href + " #botones");
      $('#modal-compleado').modal('hide');
      sweetAlert(
        'Exito!',
        'Se completado la orden de forma exitosa! ',
        'success'
        );      
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylecompletar()
          if(jqXHR.responseJSON.termina){         
            $('#field-termina').addClass("has-error");
            $('#field-termina .msj-error').html(jqXHR.responseJSON.termina);
          }
          if(jqXHR.responseJSON.horas){         
            $('#field-horas').addClass("has-error");
            $('#field-horas .msj-error').html(jqXHR.responseJSON.horas);
          } 
          if(jqXHR.responseJSON.kilometros){         
            $('#field-kilometros').addClass("has-error");
            $('#field-kilometros .msj-error').html(jqXHR.responseJSON.kilometros);
          }
        }
      else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
          'error'
          )
        }      }
  });
});

function cambiarEstado(orden,estado){
  swal({
    title: '¿Estás seguro que quiere cambiar el estado de la orden?',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, estoy seguro',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ORDENES+"/"+orden+"/cambiar/"+estado;
        $.ajax({
          url: route,
          type: 'GET',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function(res){
            $("#detalles").load(location.href + " #detalles");
            $("#botones").load(location.href + " #botones");
            $("#tareas").load(location.href + " #tareas");
            $("#div2").load(location.href + " #div2");
            $("#articulos").load(location.href + " #articulos");
            $("#div4").load(location.href + " #div4"); 
            sweetAlert({
              title: 'Exito!',
              text: 'Se ha actualizado el estado de forma exitosa! ',
              type: 'success',
            })
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de actualizar el estado. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'La tarea se ha eliminado exitosamente',
      'success'
      );
  });
}



$('#modal-compleado').on('hidden.bs.modal', function (e) 
{
  $('#termina').val('');
  removeStylecompletar();
});