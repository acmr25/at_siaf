
var ordenes = ruta+'/activo-orden';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ---------------------------------------------------tabla de ordenes de un activo en particular----------------------------------------------------------------------

var tabla_ordenes = $("#tabla-ordenes").DataTable({
  processing: true,
  serverSide: true,
  ajax: ordenes+'/'+id+'/listar',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'inicia', name: 'inicia','className': 'text-center'},
    { data: 'termina', name: 'termina','className': 'text-center',
      render: function ( data, type, full, meta ) {
      var text ='';
      if (full.termina != null) {
        text = data;
      }
      return text;
      }
    },
    { data: 'tarea_nombre', name: 'tarea_nombre', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text ='';
      if (full.tareas.length > 0) {
        var a = 1;
        for (var i = 0; i < full.tareas.length; i++) {
          text += a+'. '+full.tareas[i].nombre+'.<br /> ';
          a++;
        }
      }
      return text;
      }
    },
    { data: 'empleado', name: 'empleado',
        render: function ( data, type, full, meta ) {
        var text = full.empleado.nombre;
        return text;
      }
    },
    { data: 'reporte.id', name: 'reporte.id','className': 'text-center',
      render: function ( data, type, full, meta ) {
      var text = '';
      if (full.reporte != null) {
        text = full.reporte.id;
      }
      return text;
      }
    }
  ],

  //createdRow: function ( row, full, index ) {
    //if (full.estado == 'Sin asignar') {
    // $(row).addClass('danger')
    //}
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 //},

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-ordenes').click(function(){
  tabla_ordenes.ajax.reload();
})

$('#tabla-ordenes tbody').on('click', 'tr', function () {
var data = tabla_ordenes.row(this).data().id;
window.location = ruta+'/ordenes/'+data;
} );