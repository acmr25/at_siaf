
var RUTA_ARTICULOS = ruta+'/articulos';
var movimientos = ruta+'/movimientos';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ---------------------------------------------------INDEX----------------------------------------------------------------------

var tabla_movimientos = $("#movimientos").DataTable({
  processing: true,
  serverSide: true,
  ajax: movimientos+'/'+id+'/movimientos',
  columns: [

  
  { data: 'cantidad', name: 'cantidad',
    render: function(data, type, full, meta){
      if(full.tipo == 'ENTRADA' ){
        var text = '<span class="label label-info"><i class="fa fa-plus"></i></span> ' +data;
      }
      else {
        var text = '<span class="label label-danger"><i class="fa fa-minus"></i></span> '+data;
      }
      return text;
    }
  },
  { data: 'fecha', name: 'fecha'},
  { data: 'razon', name: 'razon',
    render: function(data, type, full, meta){
      if (full.razon.length > 160) {
        var text= '<p style="font-size: 12px">'+full.razon+'</p>';
      }
      else{
        var text = '<p >'+data+'</p>';
      }      
      return '<div style="text-align: justify;">'+text+'</div>';
    }
  },
  { data: 'creador.nombre', name: 'creador.nombre'},
  { data: 'equipo', name: 'equipo',
  render: function(data, type, full){
      if(full.activo != null){
        text='<a href="'+ruta+'/activos/'+full.activo.id+'" >'+data+'</a>' ;
        return text;
      }else{
        return "";
      }
    }
  },
  { data: 'orden_id', name: 'orden_id', 'className': 'text-center',
  render: function(data, type, full){
      if(full.orden_id != null){
        var text= '<a href="'+ruta+'/ordenes/'+full.orden_id+'" >'+data+'</a>'
        return text;
      }else{
        return "";
      }
    }
  },
  { data: 'id', name: 'id',"visible": false},
  { data: 'tipo', name: 'tipo', "visible": false},
  ],
  createdRow: function ( row, full, index ) {
    if (full.tipo == 'salida' ) {
     $(row).addClass('danger')
    }
    if (full.tipo == 'entrada' ) {
      $(row).addClass('info')
    }
 },

  order: [[ 6 , "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  
});

$('#actualizar-movimientos').click(function(){
  tabla_movimientos.ajax.reload();
})

// --------------------------------------------------NI IDEA----------------------------------------------------------------------

function Articulo(){
  this.id = $('#id_articulo').val();
  this.nro_activo = $('#nro_activo-articulo').val();
  this.nombre = $('#nombre-articulo').val();
  this.categoria = $('#categoria-articulo').val();
  this.descripcion = $('#descripcion-articulo').val();
  this.fabricante = $('#fabricante-articulo').val();
  this.unidad = $('#unidad-articulo').val();
  this.lowstock = $('#lowstock-articulo').val();
  this.estado = $('#estado-articulo').val();
}

function Movimiento(){
  this.id = $('#id_movimiento').val();
  this.articulo_id = $('#articulo_id').val();
  this.tipo = $('#tipo').val();
  this.activo_id = $('#activo_id').val();
  this.cantidad = $('#cantidad').val();
  this.razon = $('#razon').val();
}

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 


// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStylearticulo(){
  $('#field-nro_activo-articulo').removeClass("has-error");
  $('#field-nro_activo-articulo .msj-error').html("");

  $('#field-nombre-articulo').removeClass("has-error");
  $('#field-nombre-articulo .msj-error').html("");

  $('#field-fabricante-articulo').removeClass("has-error");
  $('#field-fabricante-articulo .msj-error').html("");

  $('#field-categoria-articulo').removeClass("has-error");
  $('#field-categoria-articulo .msj-error').html("");

  $('#field-unidad-articulo').removeClass("has-error");
  $('#field-unidad-articulo .msj-error').html("");

  $('#field-lowstock-articulo').removeClass("has-error");
  $('#field-lowstock-articulo .msj-error').html("");

  $('#field-descripcion-articulo').removeClass("has-error");
  $('#field-descripcion-articulo .msj-error').html("");
}

function removeStylemovimiento(){
  $('#field-activo').removeClass("has-error");
  $('#field-activo .msj-error').html("");

  $('#field-cantidad').removeClass("has-error");
  $('#field-cantidad .msj-error').html("");

  $('#field-razon').removeClass("has-error");
  $('#field-razon .msj-error').html("");
}

// --------------------------------------------------GUARDAR NUEVO O ACTUALIZACION----------------------------------------------------------------------

$('#guardar-articulo').click(function(){
  var btn = this
  starLoad(btn)
  var data = new Articulo();
  $.ajax({
    url: RUTA_ARTICULOS+'/'+data.id,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: 'PUT',
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStylearticulo();
      $('#modal-edit').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success')
      $("#caja").load(location.href + " #caja");
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylearticulo()
          if(jqXHR.responseJSON.nro_activo){         
            $('#field-nro_activo-articulo').addClass("has-error");
            $('#field-nro_activo-articulo .msj-error').html(jqXHR.responseJSON.nro_activo);
          }          
          if(jqXHR.responseJSON.nombre){         
            $('#field-nombre-articulo').addClass("has-error");
            $('#field-nombre-articulo .msj-error').html(jqXHR.responseJSON.nombre);
          }
          if(jqXHR.responseJSON.categoria){         
            $('#field-categoria-articulo').addClass("has-error");
            $('#field-categoria-articulo .msj-error').html(jqXHR.responseJSON.categoria);
          }
          if(jqXHR.responseJSON.descripcion){         
            $('#field-descripcion-articulo').addClass("has-error");
            $('#field-descripcion-articulo .msj-error').html(jqXHR.responseJSON.descripcion);
          }
          if(jqXHR.responseJSON.fabricante){         
            $('#field-fabricante-articulo').addClass("has-error");
            $('#field-fabricante-articulo .msj-error').html(jqXHR.responseJSON.fabricante);
          }
          if(jqXHR.responseJSON.unidad){         
             $('#field-unidad-articulo').addClass("has-error");
            $('#field-unidad-articulo .msj-error').html(jqXHR.responseJSON.unidad);
          }
          if(jqXHR.responseJSON.lowstock){         
             $('#field-lowstock-articulo').addClass("has-error");
            $('#field-lowstock-articulo .msj-error').html(jqXHR.responseJSON.lowstock);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});


$('#guardar-movimiento').click(function(){
  var btn = this
  starLoad(btn)
  var data = new Movimiento();  
  $.ajax({
    url: movimientos,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: 'POST',
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStylemovimiento();
      $('#modal-movimiento').modal('hide');
      sweetAlert(
        'Exito!',
        'Se ha realizado el moviemiento de articulo de forma exitosa! ',
        'success'
        )  
      $("#caja").load(location.href + " #caja");
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylemovimiento()
          if(jqXHR.responseJSON.activo_id){         
            $('#field-activo').addClass("has-error");
            $('#field-activo .msj-error').html(jqXHR.responseJSON.activo_id);
          }          
          if(jqXHR.responseJSON.cantidad){         
            $('#field-cantidad').addClass("has-error");
            $('#field-cantidad .msj-error').html(jqXHR.responseJSON.cantidad);
          }
          if(jqXHR.responseJSON.razon){         
            $('#field-razon').addClass("has-error");
            $('#field-razon .msj-error').html(jqXHR.responseJSON.razon);
          }
        }
      else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de realizar el movimiento. Posible error: No hay suficiente en stock para realizar el movimientos. Status: '+jqXHR.status,
          'error'
          )
        }
      }
  });
});
// --------------------------------------------------ELIMINAR Articulo----------------------------------------------------------------------

function deleteArticulo(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ARTICULOS+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
              showConfirmButton: false,
              timer: 2000
            })
            setTimeout("location.href= RUTA_ARTICULOS", 2050);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar los datos. El articulo debe estar relacionado en alguna operación. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El empleado se ha eliminado exitosamente',
      'success'
      );
  });
}

// -------------------------------------------------- NI IDEA DE MODALS----------------------------------------------------------------------

$('#modal-articulo').on('hidden.bs.modal', function (e) 
{
  tabla_movimientos.ajax.reload();
  $('#form-articulo')[0].reset();
  $('#id_articulo').val('');
  removeStylearticulo();
});

$('#modal-movimiento').on('hidden.bs.modal', function (e) 
{
  tabla_movimientos.ajax.reload();
  $('#form-movimiento')[0].reset();
  $('#id_movimiento').val('');
  $('#tipo').val('').trigger('change');
  $('#activo').val('').trigger('change');
  removeStylemovimiento();
});    

// --------------------------------------------------SHOW EMPLEADO----------------------------------------------------------------------

$('.select2').select2({
  width:'100%',
  placeholder: "Seleccione",
  tags:true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true }
  },
  templateResult: function (data) {
    var $result = $("<span></span>");
    $result.text(data.text);
    if (data.newOption) {
      $result.append(" <em>(Nuevo)</em>");
    }
    return $result;
  },
  language: "es"
})

$("#activo_id").select2({
  placeholder: "Seleccione",
  width:'100%',
  language: "es",
})