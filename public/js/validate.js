
var extresiones = {
	numeros: { 
		valor: /^[0-9]+$/,
		mensaje: "Solo ingrese valores numericos"
	},
	letras:{
		valor: /^[a-zA-Z]+$/,
		mensaje: "Solo ingrese valores alfabeticos"
	},
	letras_latinas: {
		valor: /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/,
		mensaje: "Solo ingrese valores alfabeticos"
	},
	email:{
		valor: /^[a-zA-Z0-9\._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/,
		mensaje: "El Email debe de tener un formato ejemplo@ejemplo.com"
	},
	password: {
		valor: /^([a-z]+[0-9]+)|([0-9]+[a-z]+)/i,
		mensaje: "Combinación no valida"
	},
	url: {
		valor: /^(ht|f)tps?:\/\/\w+([\.\-\w]+)?\.([a-z]{2,6})?([\.\-\w\/_]+)$/i,
		mensaje: "URL no valida"
	},
	phoneNumber: {
		valor: /^[0-9-()+]{3,20}/,
		mensaje: "Numero de telefono no valido"
	}
};

function validar(campo, extresion){

	var v = $(campo+' input').val().match(extresion.valor);
	if(v==null){
		$(campo).addClass("has-error");
		$(campo+' .msj').fadeIn(60).html(extresion.mensaje);
	}else{
		$(campo).removeClass("has-error");
		$(campo+' .msj').fadeOut(60).html("");
	}
	
}

function isUrlFacebook(input) {
	url = input;
	console.log(url)
    //Declaramos la expresión regular que se usará para validar la url pasada por parámetro
    var regexp = /http(?:s)?:\/\/(?:www\.)?facebook\.com\//
    //Retorna true en caso de que la url sea valida o false en caso contrario
    if(url != ""){
    	if(regexp.test(url)){
    		$('#field-facebook').removeClass('has-error');
    		$('#field-facebook .msj-error').html('');
    	}else{
    		$('#field-facebook').addClass('has-error');
    		$('#field-facebook .msj-error').html('Esta URL no es compatible con la de facebook');
    	}
    	return regexp.test(url);
    }else{
    	$('#field-facebook').removeClass('has-error');
    	$('#field-facebook .msj-error').html('');
    	return true;
    }
    
}

function isUrlTwitter(input) {
	url = input;
	console.log(url)
    //Declaramos la expresión regular que se usará para validar la url pasada por parámetro
    var regexp = /http(?:s)?:\/\/(?:www\.)?twitter\.com\//
    //Retorna true en caso de que la url sea valida o false en caso contrario
    if(url != ""){
    	if(regexp.test(url)){
    		$('#field-twitter').removeClass('has-error');
    		$('#field-twitter .msj-error').html('');
    	}else{
    		$('#field-twitter').addClass('has-error');
    		$('#field-twitter .msj-error').html('Esta URL no es compatible con la de twitter');
    	}
    	return regexp.test(url);
    }else{
    	$('#field-twitter').removeClass('has-error');
    	$('#field-twitter .msj-error').html('');
    	return true;
    }

}

function isUrlInstagram(input) {
	url = input;
	console.log(url)
    //Declaramos la expresión regular que se usará para validar la url pasada por parámetro
    var regexp = /http(?:s)?:\/\/(?:www\.)?instagram\.com\//
    //Retorna true en caso de que la url sea valida o false en caso contrario
    if(url != ""){
    	if(regexp.test(url)){
    		$('#field-instagram').removeClass('has-error');
    		$('#field-instagram .msj-error').html('');
    	}else{
    		$('#field-instagram').addClass('has-error');
    		$('#field-instagram .msj-error').html('Esta URL no es compatible con la de instagram');
    	}
    	return regexp.test(url);
    }else{
    	$('#field-instagram').removeClass('has-error');
    	$('#field-instagram .msj-error').html('');
    	return true;
    }

}

function soloLetrasURL(e){
	console.log(e.keyCode)
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = "/:abcdefghijklmnopqrstuvwxyz?.=1234567890";
	especiales = "8-37-39-46";

	tecla_especial = false
	for(var i in especiales){
		if(key == especiales[i]){
			tecla_especial = true;
			break;
		}
	}

	if(letras.indexOf(tecla)==-1 && !tecla_especial){
		return false;
	}
}

function soloLetras(e){
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	especiales = "8-37-39-46";

	tecla_especial = false
	for(var i in especiales){
		if(key == especiales[i]){
			tecla_especial = true;
			break;
		}
	}

	if(letras.indexOf(tecla)==-1 && !tecla_especial){
		return false;
	}
}

function soloNumeros(e){
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = "1234567890";
	especiales = "8-37-39-46";

	tecla_especial = false
	for(var i in especiales){
		if(key == especiales[i]){
			tecla_especial = true;
			break;
		}
	}

	if(letras.indexOf(tecla)==-1 && !tecla_especial){
		return false;
	}
}