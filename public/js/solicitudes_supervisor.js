var RUTA_SOLICITUDES = ruta+'/solicitudes';
// ---------------------------------------------------INDEX----------------------------------------------------------------------
var tabla_solicitudes_supervisor = $("#tabla-solicitudes-supervisor").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_SOLICITUDES+'/listar/2',
  search: { "caseInsensitive": true },
  columns: [
    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'origen', name: 'origen',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'destino', name: 'destino',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'solicitante.nombre', name: 'solicitante.nombre',
      render: function ( data, type, full, meta ) {
        var text ='<b>Solicitante: </b>'+data+' '+full.solicitante.cargo+' de '+full.solicitante.departamento.nombre+'</br>';
        text +='<b>Motivo: </b>'+full.motivo+'</br>';
        text +='<b>Fecha y hora de salida:</b></br>'+full.fecha_salida+' '+full.hora_salida;
        if (full.fecha_regreso != null) {
          text +='</br><b>Fecha y hora de regreso:</b></br>'+full.fecha_regreso+' '+full.hora_regreso+'</br>';
        }
        if (full.dt_aprobado != null) {
          text +='</br><b>Aprobado el:</b></br>'+full.dt_aprobado;
        }
        return '<p">'+text+'</p>'
      }
    },
    { data: 'estado', name: 'estado','className': 'text-center'},
    { data: 'calificacion', name: 'calificacion','className': 'text-center', "width": "11%"},
    { data: 'motivo', name: 'motivo', "visible": false},
    { data: 'fecha_salida', name: 'fecha_salida', "visible": false},
    { data: 'fecha_regreso', name: 'fecha_regreso', "visible": false},

  ],
  language: leng,
  order: [[ 0, "desc" ]],
  "bAutoWidth": false,
  "scrollX": true,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-solicitudes-supervisor').click(function(){
  tabla_solicitudes_supervisor.ajax.reload();
})

$('#tabla-solicitudes-supervisor tbody').on('click', 'tr', function () {
var data = tabla_solicitudes_supervisor.row(this).data().id;
window.location = ruta+'/solicitudes/'+data;
} );