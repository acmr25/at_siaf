
var ordenes = ruta+'/ordenes';

// ---------------------------------------------------tabla de ordenes de un activo en particular----------------------------------------------------------------------

var tabla_ordenes = $("#tabla-ordenes").DataTable({
  processing: true,
  serverSide: true,
  ajax: ordenes+'/listar',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'inicia', name: 'inicia','className': 'text-center'},
    { data: 'termina', name: 'termina','className': 'text-center',
      render: function ( data, type, full, meta ) {
      var text ='';
      if (full.termina != null) {
        text = data;
      }
      return text;
      }
    },
    { data: 'empleado', name: 'empleado',
        render: function ( data, type, full, meta ) {
        var text = full.empleado.nombre;
        return text;
      }
    },
    { data: 'unidad', name: 'unidad'    },
    { data: 'estado_orden', name: 'estado_orden','className': 'text-center',
      render: function ( data, type, full, meta ) {
      var text = '';
      if (full.estado_orden == 'abierta') {
        text = '<span class="label label-info">Abierta</span>';
      }
      if (full.estado_orden == 'en progreso') {
        text = '<span class="label label-warning">En progreso</span>';
      }
      if (full.estado_orden == 'detenida') {
        text = '<span class="label label-danger">Detenida</span>';
      }
      if (full.estado_orden == 'completada') {
        text = '<span class="label label-primary">Completada</span>';
      }
      return text;
      }
    },

  ],

  //createdRow: function ( row, full, index ) {
    //if (full.estado == 'Sin asignar') {
    // $(row).addClass('danger')
    //}
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 //},

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-ordenes').click(function(){
  tabla_ordenes.ajax.reload();
})

$('#tabla-ordenes tbody').on('click', 'tr', function () {
var data = tabla_ordenes.row(this).data().id;
window.location = ruta+'/ordenes/'+data;
} );
