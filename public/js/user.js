

var tabla_usuarios = $("#tabla-usuarios").DataTable({
  processing: true,
  serverSide: true,
  ajax: ruta+'/usuarios/listar',
  search: { "caseInsensitive": true },
  columns: [
  { data: 'id', name: 'id'},
  { data: 'cedula', name: 'cedula'},
  { data: 'nombre', name: 'nombre'},
  { data: 'cargo', name: 'cargo'},
  { data: 'departamento.nombre', name: 'departamento.nombre'},
  { data: 'email', name: 'email'},
  { data: 'usuario', name: 'usuario'},
  { data: 'estado', name: 'estado'},
  { data: 'rol.nombre', name: 'rol.nombre'},
  { data: 'id',
    'orderable': false,
    render: function ( data, type, full, meta ) {
      var text = '<div class="text-center ">'+
      '<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="UsuarioActual('+data+')"><i class="fa fa-edit"></i></button>'+
      '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteUsuario(' + data + ')"><i class="fa fa-remove"></i></button>'+
      '</div>'; 
      return text;
    }
  }],
  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-user').click(function(){
  tabla_usuarios.ajax.reload();
})


function deleteUsuario(id){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta+'/usuarios/'+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            tabla_usuarios.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al empleado. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El Usuario se ha eliminado exitosamente',
      'success'
      );
  });
}

function Usuario(){
  this.user_id = $('#user_id').val();
  this.cedula = $('#cedula').val();
  this.nombre = $('#nombre').val();
  this.cargo = $('#cargo').val();
  this.departamento_id = $('#departamento_id').val();
  this.email = $('#email').val();
  this.password = $('#password').val();
  this.password_confirmation = $('#password_confirmation').val();
  this.rol_id = $('#rol_id').val();
  this.estado = $('#estado').val();
  this.usuario = $('#usuario').val();
}

function starLoad_cargarsito(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad_descargansito(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleUsuario(){
  $('#field-cedula').removeClass("has-error");
  $('#field-cedula .msj-error').html("");

  $('#field-nombre').removeClass("has-error");
  $('#field-nombre .msj-error').html("");

  $('#field-usuario').removeClass("has-error");
  $('#field-usuario .msj-error').html("");

  $('#field-cargo').removeClass("has-error");
  $('#field-cargo .msj-error').html("");

  $('#field-departamento_id').removeClass("has-error");
  $('#field-departamento_id .msj-error').html("");

  $('#field-email').removeClass("has-error");
  $('#field-email .msj-error').html("");

  $('#field-password').removeClass("has-error");
  $('#field-password .msj-error').html("");

  $('#field-password_confirmation').removeClass("has-error");
  $('#field-password_confirmation .msj-error').html("");

  $('#CAMPO_ROL').removeClass("has-error");
  $('#CAMPO_ROL .msj-error').html("");

  $('#CAMPO_ESTADO').removeClass("has-error");
  $('#CAMPO_ESTADO .msj-error').html("");

}

function UsuarioActual(id){
  $.ajax({
    url: ruta+'/usuarios/'+id,
    type: 'GET',
    success: function(res){ 
      $('#user_id').val(res.id);
      $('#nombre').val(res.nombre); 
      $('#cedula').val(res.cedula);    
      $('#cargo').val(res.cargo);
      $('#departamento_id').val(res.departamento_id);
      $('#email').val(res.email);
      $('#rol_id').val(res.rol_id);
      $('#estado').val(res.estado);
      $('#usuario').val(res.usuario);
      $('#modal-user').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

$('#guardar-usuario').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad_cargarsito(btn)
  var data = new Usuario();
  if(data.user_id == "" || data.user_id == null || data.user_id == undefined){
    type = 'POST';
    route = ruta+'/usuarios';
  }else{
    type = 'PUT';
    route = ruta+'/usuarios/'+data.user_id
  }
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad_descargansito(btn)
      removeStyleUsuario();
      $('#modal-user').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad_descargansito(btn)
      if(jqXHR.status == 422){
        removeStyleUsuario()
          if(jqXHR.responseJSON.cedula){         
            $('#field-cedula').addClass("has-error");
            $('#field-cedula .msj-error').html(jqXHR.responseJSON.cedula);
          }          
          if(jqXHR.responseJSON.nombre){         
            $('#field-nombre').addClass("has-error");
            $('#field-nombre .msj-error').html(jqXHR.responseJSON.nombre);
          }
          if(jqXHR.responseJSON.usuario){         
            $('#field-usuario').addClass("has-error");
            $('#field-usuario .msj-error').html(jqXHR.responseJSON.usuario);
          }
          if(jqXHR.responseJSON.cargo){         
            $('#field-cargo').addClass("has-error");
            $('#field-cargo .msj-error').html(jqXHR.responseJSON.cargo);
          }
          if(jqXHR.responseJSON.departamento_id){         
            $('#field-departamento_id').addClass("has-error");
            $('#field-departamento_id .msj-error').html(jqXHR.responseJSON.departamento_id);
          }
          if(jqXHR.responseJSON.email){         
            $('#field-email').addClass("has-error");
            $('#field-email .msj-error').html(jqXHR.responseJSON.email);
          }
          if(jqXHR.responseJSON.password){         
             $('#field-password').addClass("has-error");
            $('#field-password .msj-error').html(jqXHR.responseJSON.password);
          }
          if(jqXHR.responseJSON.password_confirmation){         
            $('#field-password_confirmation').addClass("has-error");
            $('#field-password_confirmation .msj-error').html(jqXHR.responseJSON.password_confirmation);
          }
          if(jqXHR.responseJSON.rol_id){         
            $('#CAMPO_ROL').addClass("has-error");
            $('#CAMPO_ROL .msj-error').html(jqXHR.responseJSON.rol_id);
          }
          if(jqXHR.responseJSON.estado){         
            $('#CAMPO_ESTADO').addClass("has-error");
            $('#CAMPO_ESTADO .msj-error').html(jqXHR.responseJSON.estado);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});


$('#modal-user').on('hidden.bs.modal', function (e) 
{ 
  $('#form-user')[0].reset();
  $('#user_id').val('');
  removeStyleUsuario();
  tabla_usuarios.ajax.reload();
});