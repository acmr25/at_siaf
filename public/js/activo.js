var RUTA_ACTIVOS = ruta+'/activos';
// ---------------------------------------------------INDEX----------------------------------------------------------------------
var tabla_activos = $("#activos").DataTable({
  dom: 
    "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
  processing: true,
  serverSide: true,
  ajax: RUTA_ACTIVOS+'/listar',
  columns: [
    { data: 'mantenimiento' , name: 'mantenimiento', "orderable": false, 
    render: function(data, type, full, meta){
        var text;
        if(full.mantenimiento != null){
          if(full.mantenimiento == 'PENDIENTE'){
          text= '<span class="label label-danger"><i class="fa fa-fw fa-wrench"></i></span>';
          }
          if(full.mantenimiento == 'ACERCANDOSE'){
          text= '<span class="label label-warning"><i class="fa fa-fw fa-tachometer"></i></span>';
          }
          if(full.mantenimiento == 'PENDIENTE Y ACERCANDOSE'){
          text= '<span class="label label-danger"><i class="fa fa-fw fa-wrench"></i></span><span class="label label-warning"><i class="fa fa-fw fa-tachometer"></i></span>';}          
        }
        else{
            text = '';
          }
      return text;
      }
    },
    { data: 'descripcion', name: 'descripcion',"width": "35%"},//1
    { data: 'clasificacion', name: 'clasificacion','className': 'text-center'},//2
    { data: 'estatus', name: 'estatus','className': 'text-center',//3
      render: function(data){
        if(data != 'Inoperativo'){
          var text = '<b><span style="color:green">'+data+' </span></b>';
        }
        else { var text = '<div class="text-muted"> '+data+' </div>';}
        return text;
      }
    },
    { data: 'ubicacion', name: 'ubicacion','className': 'text-center'},//4
    { data: 'tipo', name: 'tipo','className': 'text-center', "visible": false, "title": "Tipo"}, //5
    { data: 'placa', name: 'placa','className': 'text-center', "visible": false, "title": "Placa"},//6
    { data: 'marca', name: 'marca','className': 'text-center', "visible": false, "title": "Marca"},//7
    { data: 'modelo', name: 'modelo','className': 'text-center', "visible": false, "title": "Modelo"},//8
    { data: 'nro_activo', name: 'nro_activo','className': 'text-center', "visible": false, "title": "Serial(Profit)"},   //9
    { data: 'id', name: 'id', "visible": false},
  ],
  initComplete: function () {
    this.api().columns([2,3,4]).every( function () {
        var column = this;
        var select = $('<select class="form-control" placeholder="Filtro"><option value=""></option></select>')
            .appendTo( $(column.footer()).empty() )
            .on( 'change', function () {
                var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                );
                column
                    .search( val ? '^'+val+'$' : '', true, false )
                    .draw();
            } );
        column.data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
  },
  createdRow: function ( row, full, index ) {
    if (full.mantenimiento == 'PENDIENTE' || full.mantenimiento == 'PENDIENTE Y ACERCANDOSE') {
     $(row).addClass('danger')
    }
    if (full.mantenimiento == 'ACERCANDOSE' ) {
      $(row).addClass('warning')
    }
  },
  order: [[ 8, "desc" ]],
  scrollY: true,
  "scrollX": true,
  language: leng,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
  buttons: [
        {
          extend: 'excelHtml5',
          exportOptions: {
              columns: [5,6,7,8,2,3,4,9],
              search: 'applied',
              order: 'applied' //column id visible in PDF 
            } 
        },
    ],
});

$('#activos tbody').on('click', 'tr', function () {
var data = tabla_activos.row(this).data().id;
window.location = ruta+'/activos/'+data;
} );

$('#actualizar-activo').click(function(){
  tabla_activos.ajax.reload();
})

