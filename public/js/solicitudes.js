var RUTA_SOLICITUDES = ruta+'/solicitudes';
  $.fn.dataTable.ext.errMode = 'throw';
// ---------------------------------------------------INDEX----------------------------------------------------------------------
var tabla_solicitudes = $("#tabla-solicitudes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_SOLICITUDES+'/listar/1',
  search: { "caseInsensitive": true },
  columns: [
    { data: 'id', name: 'id','className': 'text-center', "width": "10%"},
    { data: 'origen', name: 'origen',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'destino', name: 'destino',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'motivo', name: 'motivo', 
      render: function ( data, type, full, meta ) {
        var text ='<b>Motivo: </b>'+data+'</br>';
        if (full.solicitante_id!=full.sup_id) {
          text +='<b>Supervisor: </b>'+full.supervisor.nombre+' '+full.supervisor.cargo+' de '+full.supervisor.departamento.nombre+'</br>';
        }
        text +='<b>Fecha y hora de salida:</b></br>'+full.fecha_salida+' '+full.hora_salida;
        if (full.fecha_regreso != null) {
          text +='</br><b>Fecha y hora de regreso:</b></br>'+full.fecha_regreso+' '+full.hora_regreso;
        }
        if (full.dt_aprobado != null) {
          text +='</br><b>Aprobado el:</b></br>'+full.dt_aprobado;
        }
        return text
      }
    },
    { data: 'estado', name: 'estado','className': 'text-center'},
    { data: 'calificacion', name: 'calificacion','className': 'text-center'},
    { data: 'supervisor.nombre', name: 'supervisor.nombre', "visible": false},
    { data: 'fecha_salida', name: 'fecha_salida', "visible": false},
    { data: 'fecha_regreso', name: 'fecha_regreso', "visible": false},
    
  ],

  language: leng,
  order: [[ 0, "desc" ]],
  "bAutoWidth": false,
  "scrollX": true,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});


$('#actualizar-solicitudes').click(function(){
  tabla_solicitudes.ajax.reload();
})

$('#tabla-solicitudes tbody').on('click', 'tr', function () {
var data = tabla_solicitudes.row(this).data().id;
window.location = ruta+'/solicitudes/'+data;
} );