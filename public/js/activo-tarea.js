
var tareas = ruta+'/activo-tarea';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ---------------------------------------------------tabla de tareas de mantenimiento de un activo en particular----------------------------------------------------------------------

var tabla_tareas = $("#tabla-tareas").DataTable({
  processing: true,
  serverSide: true,
  ajax: tareas+'/'+id+'/listar',
  columns: [
    { data: 'pivot.estado', name: 'pivot.estado', "orderable": false, 'className': 'text-center',
    render: function(data, type, full, meta){
        var text ;
        if(full.pivot.estado == 'PENDIENTE'){
          text = '<span class="label label-danger"><i class="fa fa-fw fa-wrench"></i></span></div>';
        }
        if(full.pivot.estado == 'ACERCANDOSE'){
          text = '<span class="label label-warning"><i class="fa fa-fw fa-tachometer"></i></span>'; 
          }
        if(full.pivot.estado == null ){
          text = ''; 
          }
        return text;
        }  
    },
    { data: 'nombre', name: 'nombre', 
      render: function(data, type, full, meta){
          var text='<p style="text-align: justify;">'+data+'.</p>';
          if (full.pivot.orden_id != null) {
            text += '<p style="font-size: 12px;">Asignado a la orden nro.'+full.pivot.orden_id+'</p>';
          }
          return text;
          }
    },
    { data: 'pivot.prioridad', name: 'pivot.prioridad','className': 'text-center'},
    { data: "Realizar cada", defaultContent : "",'className': 'text-center'},
    { data: "Preaviso", defaultContent : "", 'className': 'text-center'},
    { data: "Ultima Realización", defaultContent : "", 'className': 'text-center'},
    { data: 'pivot.id', name: 'pivot.id', "orderable": false, 'className': 'text-center',
      render: function ( data, type, full, meta ) {
        var text = '<div class="text-center ">'+
        '<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="ShowTarea('+data+')"><i class="fa fa-edit"></i></button>'+
        '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="DeleteTarea('+full.pivot.activo_id +','+full.id+')"><i class="fa fa-remove"></i></button>'+
        '</div>'; 
        return text;
      }
    },
    { data: 'pivot.id', name: 'pivot.id',"visible": false}  
  ],

  columnDefs: [
    {
      "render": function ( data, type, full, meta) {
        if (full.pivot.horas != null && full.pivot.kilometros != null) {
          var text ='<div>'+full.pivot.horas+' hr / '+full.pivot.kilometros+' km </div>';
        }
        if (full.pivot.horas != null && full.pivot.kilometros == null) {
          var text ='<div>'+full.pivot.horas+' hr </div>';
        }
        if (full.pivot.horas  == null && full.pivot.kilometros != null) {
          var text ='<div>'+full.pivot.kilometros+' km </div>';
        }                    
         return '<div>'+text+'</div>';
      },
      "targets": 3
    },
    {
      "render": function ( data, type, full, meta) {
        if (full.pivot.pre_horas != null && full.pivot.pre_kilometros != null) {
          var text ='<div>'+full.pivot.pre_horas+' hr / '+full.pivot.pre_kilometros+' km </div>';
        }
        if (full.pivot.pre_horas != null && full.pivot.pre_kilometros == null) {
          var text ='<div>'+full.pivot.pre_horas+' hr </div>';
        }
        if (full.pivot.pre_horas  == null && full.pivot.pre_kilometros != null) {
          var text ='<div>'+full.pivot.pre_kilometros+' km </div>';
        }                    
         return '<div>'+text+'</div>';
      },
      "targets": 4
    },
    {
      "render": function ( data, type, full, meta) {
        if (full.pivot.realizacion_horas != null && full.pivot.realizacion_kilometros != null) {
          var text ='<div>'+full.pivot.realizacion_horas+' hr / '+full.pivot.realizacion_kilometros+' km </div>';
        }
        if (full.pivot.realizacion_horas != null && full.pivot.realizacion_kilometros == null) {
          var text ='<div>'+full.pivot.realizacion_horas+' hr </div>';
        }
        if (full.pivot.realizacion_horas  == null && full.pivot.realizacion_kilometros != null) {
          var text ='<div>'+full.pivot.realizacion_kilometros+' km </div>';
        }                    
         return text;
      },
      "targets": 5
    }
  ],

  createdRow: function ( row, full, index ) {
    if (full.pivot.estado == 'PENDIENTE' ) {
     $(row).addClass('danger')
    }
    if (full.pivot.estado == 'ACERCANDOSE' ) {
      $(row).addClass('warning')
    }
 },

  order: [[ 7, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
  
});

//$('#tabla-tareas tbody').on('click', 'tr', function () {
//var data = tabla_tareas.row(this).data().pivot.id;
//window.location = tareas+'/'+data;
//} );

$('#actualizar-tareas').click(function(){
  tabla_tareas.ajax.reload();
})

// ------------------------------------------- modelo y botones----------------------------------------------------------------------

function Tarea(){
  this.id = $('#id').val();
  this.tarea_id = $('#tarea_id').val();
  this.activo_id = $('#activo_id').val();

  this.horas = $('#horas_aviso').val();
  this.pre_horas = $('#horas-preaviso').val();
  this.realizacion_horas = $('#horas-ultima').val();

  this.kilometros = $('#kilometros_aviso').val();
  this.pre_kilometros = $('#kilometros-preaviso').val();
  this.realizacion_kilometros = $('#kilometros-ultima').val();

  this.prioridad = $('#prioridad').val();
  
}

function removeStyletarea(){

  $('#field-tarea-nombre').removeClass("has-error");
  $('#field-tarea-nombre .msj-error').html("");

  $('#field-horas').removeClass("has-error");
  $('#field-horas .msj-error').html("");

  $('#field-pre_horas').removeClass("has-error");
  $('#field-pre_horas .msj-error').html("");

  $('#field-realizacion_horas').removeClass("has-error");
  $('#field-realizacion_horas .msj-error').html("");

  $('#field-kilometros').removeClass("has-error");
  $('#field-kilometros .msj-error').html("");

  $('#field-pre_kilometros').removeClass("has-error");
  $('#field-pre_kilometros .msj-error').html("");

  $('#field-realizacion_kilometros').removeClass("has-error");
  $('#field-realizacion_kilometros .msj-error').html("");

  $('#field-tarea-prioridad').removeClass("has-error");
  $('#field-tarea-prioridad .msj-error').html("");

}

function ShowTarea(id){
  $.ajax({
    url: tareas+'/'+id,
    type: 'GET',
    success: function(res){ 
      $('#id').val(res[0].id);
      $('#tarea_id').val(res[0].tarea_id).trigger('change').prop('disabled',true);
      $('#horas_aviso').val(res[0].horas);     
      $('#horas-preaviso').val(res[0].pre_horas);
      $('#horas-ultima').val(res[0].realizacion_horas);
      $('#kilometros_aviso').val(res[0].kilometros);
      $('#kilometros-preaviso').val(res[0].pre_kilometros);
      $('#kilometros-ultima').val(res[0].realizacion_kilometros);
      $('#prioridad').val(res[0].prioridad).trigger('change');
      $('#modal-tarea').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos del empleado. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}


$('#guardar-tarea').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Tarea();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = tareas;
  }else{
    type = 'PUT';
    route = tareas+'/'+data.id
  }
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyletarea();
      $('#modal-tarea').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyletarea()
          if(jqXHR.responseJSON.tarea_id){         
            $('#field-tarea-nombre').addClass("has-error");
            $('#field-tarea-nombre .msj-error').html(jqXHR.responseJSON.tarea_id);
          }
          if(jqXHR.responseJSON.horas){         
            $('#field-horas').addClass("has-error");
            $('#field-horas .msj-error').html(jqXHR.responseJSON.horas);
          } 
          if(jqXHR.responseJSON.pre_horas){         
            $('#field-pre_horas').addClass("has-error");
            $('#field-pre_horas .msj-error').html(jqXHR.responseJSON.pre_horas);
          } 
          if(jqXHR.responseJSON.realizacion_horas){         
            $('#field-realizacion_horas').addClass("has-error");
            $('#field-realizacion_horas .msj-error').html(jqXHR.responseJSON.realizacion_horas);
          } 
          if(jqXHR.responseJSON.kilometros){         
            $('#field-kilometros').addClass("has-error");
            $('#field-kilometros .msj-error').html(jqXHR.responseJSON.kilometros);
          } 
          if(jqXHR.responseJSON.pre_kilometros){         
            $('#field-pre_kilometros').addClass("has-error");
            $('#field-pre_kilometros .msj-error').html(jqXHR.responseJSON.pre_kilometros);
          } 
          if(jqXHR.responseJSON.realizacion_kilometros){         
            $('#field-realizacion_kilometros').addClass("has-error");
            $('#field-realizacion_kilometros .msj-error').html(jqXHR.responseJSON.realizacion_kilometros);
          }
          if(jqXHR.responseJSON.prioridad){         
            $('#field-tarea-prioridad').addClass("has-error");
            $('#field-tarea-prioridad .msj-error').html(jqXHR.responseJSON.prioridad);
          } 
        }
      else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de guardar los datos. La tarea ya puede estar relacionada o se requieren campos. Status: '+jqXHR.status,
          'error'
          )
        }
      }
  });
});

$('#modal-tarea').on('hidden.bs.modal', function (e) 
{
  tabla_tareas.ajax.reload();
  $('#form-tarea')[0].reset();
  $('#id').val('');
  $('#tarea_id').val('').trigger('change').prop('disabled',false );
  $('#prioridad').val('').trigger('change');
  removeStyletarea();
});    

function DeleteTarea(activo,tarea){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = tareas+"/"+activo+"/"+tarea;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            $('#modal-tarea').modal('hide');
            tabla_tareas.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar la tarea. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'La tarea  se ha eliminado exitosamente',
      'success'
      );
  });
}

// ---------------------------------------------------select2----------------------------------------------------------------------

$('.select2').select2({
      placeholder: "Seleccione",
      tags:true,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true }
        },
      templateResult: function (data) {
        var $result = $("<span></span>");
        $result.text(data.text);
        if (data.newOption) {
          $result.append(" <em>(Nuevo)</em>");
        }
        return $result;
      },
      language: "es",
      width:'100%'
    }) 