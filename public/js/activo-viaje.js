
var RUTA_VIAJES = ruta+'/viajes';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ------------------------------------------------------------------------------------------------------------------------

var tabla_viajes = $("#tabla-viajes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/'+id+'/por_unidad',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'chofer.nombre', name: 'chofer.nombre',
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.chofer ==null) {
          text+='Sin Asignar';
        }
        else {
          text+=data;
        }
        return text;
        }
    },
    { data: 'estado', name: 'estado'},    
    { data: 'origen', name: 'origen'},    
    { data: 'destino', name: 'destino'},    
    { data: 'salida', name: 'salida',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_salida+' '+full.hora_salida;
        return text;
      }
    },
    { data: 'llegada', name: 'llegada',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_llegada+' '+full.hora_llegada;
        return text;
      }
    },
    { data: 'solicitudes_asociados', name: 'solicitudes_asociados', "width": "25%",
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.solicitudes.length > 0) {
          for (var i = 0; i < full.solicitudes.length; i++) {            
            text +='<b>ID.</b>'+full.solicitudes[i].id;
            if (full.solicitante) { text += ' - <b>Solicitante: </b>'+full.solicitante.nombre+'</a><br />'} ;
          }
        }
        return text
      }
    }, 
  ],

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-viajes').click(function(){
  tabla_viajes.ajax.reload();
})

