var RUTA_PROVEEDORES = ruta+'/proveedores';

// ---------------------------------------------------INDEX----------------------------------------------------------------------
var tabla_proveedores = $("#proveedores").DataTable({

  processing: true,
  serverSide: true,
  ajax: RUTA_PROVEEDORES+'/listar',
  search: { "caseInsensitive": true },
  columns: [

  { data: 'id', name: 'id'},
  { data: 'cedula', name: 'cedula'},
  { data: 'nombre', name: 'nombre'},
  { data: 'contrato', name: 'contrato'},
  { data: 'telefono', name: 'telefono'},
  { data: 'condicion', name: 'condicion',
    render: function(data){
      if(data == 'Activo'){
        var text = '<b class="text-success"> '+data+' </b>';
      }
      else { var text = '<div class="text-muted"> '+data+' </div>';}
      return text;
    }
  },
  { data: 'id',
    'orderable': false,
    render: function ( data, type, full, meta ) {
      var text = '<div class="text-center ">'+
      '<div class="btn-group"><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showProveedor('+data+')"><i class="fa fa-edit"></i></button>'+
      '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" OnClick="deleteProveedor(' + data + ')"><i class="fa fa-remove"></i></button>'+
      '</div></div>'; 
      return text;
    }
  }],
  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],

});

$('#actualizar-proveedor').click(function(){
  tabla_proveedores.ajax.reload();
})

// --------------------------------------------------ELIMINAR EMPLEADO----------------------------------------------------------------------

function deleteProveedor(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_PROVEEDORES+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            tabla_proveedores.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al proveedor. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El proveedor se ha eliminado exitosamente',
      'success'
      );
  });
}
// --------------------------------------------------NI IDEA----------------------------------------------------------------------

function Proveedor(){
  this.id = $('#id_proveedor').val();
  this.cedula = $('#rif-proveedor').val();
  this.nombre = $('#nombre-proveedor').val();
  this.tipo = $('#tipo-proveedor').val();
  this.condicion = $('#condicion-proveedor').val();
  this.telefono = $('#telefono-proveedor').val();
  this.contrato = $('#direccion-proveedor').val();
}


function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 


// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStyleProveedor(){
  $('#field-rif-proveedor').removeClass("has-error");
  $('#field-rif-proveedor .msj-error').html("");

  $('#field-nombre-proveedor').removeClass("has-error");
  $('#field-nombre-proveedor .msj-error').html("");

  $('#field-tipo-proveedor').removeClass("has-error");
  $('#field-tipo-proveedor .msj-error').html("");

  $('#field-condicion-proveedor').removeClass("has-error");
  $('#field-condicion-proveedor .msj-error').html("");

  $('#field-telefono-proveedor').removeClass("has-error");
  $('#field-telefono-proveedor .msj-error').html("");

  $('#field-direccion-proveedor').removeClass("has-error");
  $('#field-direccion-proveedor .msj-error').html("");
}

// --------------------------------------------------GUARDAR NUEVO O ACTUALIZACION DE EMPLEADO----------------------------------------------------------------------

$('#guardar-proveedor').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Proveedor();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = RUTA_PROVEEDORES
  }else{
    type = 'PUT';
    route = RUTA_PROVEEDORES+'/'+data.id
  }

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyleProveedor();
      $('#modal-proveedores').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleProveedor()
          if(jqXHR.responseJSON.cedula){         
            $('#field-rif-proveedor').addClass("has-error");
            $('#field-rif-proveedor .msj-error').html(jqXHR.responseJSON.cedula);
          }          
          if(jqXHR.responseJSON.nombre){         
            $('#field-nombre-proveedor').addClass("has-error");
            $('#field-nombre-proveedor .msj-error').html(jqXHR.responseJSON.nombre);
          }
          if(jqXHR.responseJSON.tipo){         
            $('#field-tipo-proveedor').addClass("has-error");
            $('#field-tipo-proveedor .msj-error').html(jqXHR.responseJSON.tipo);
          }
          if(jqXHR.responseJSON.condicion){         
            $('#field-condicion-proveedor').addClass("has-error");
            $('#field-condicion-proveedor .msj-error').html(jqXHR.responseJSON.condicion);
          }
          if(jqXHR.responseJSON.telefono){         
             $('#field-telefono-proveedor').addClass("has-error");
            $('#field-telefono-proveedor .msj-error').html(jqXHR.responseJSON.telefono);
          }
          if(jqXHR.responseJSON.contrato){         
             $('#field-direccion-proveedor').addClass("has-error");
            $('#field-direccion-proveedor .msj-error').html(jqXHR.responseJSON.contrato);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

// -------------------------------------------------- NI IDEA DE MODALS----------------------------------------------------------------------

$('#modal-proveedores').on('hidden.bs.modal', function (e) 
{
  tabla_proveedores.ajax.reload();
  $('#form-proveedor')[0].reset();
  $('#id_proveedor').val('');
  removeStyleProveedor();
}); 

// --------------------------------------------------SHOW EMPLEADO----------------------------------------------------------------------


function showProveedor(id){
  $.ajax({
    url: RUTA_PROVEEDORES+'/'+id,
    type: 'GET',
    success: function(res){ 
      $('#id_proveedor').val(res.id);
      $('#rif-proveedor').val(res.cedula);
      $('#nombre-proveedor').val(res.nombre);     
      $('#direccion-proveedor').val(res.contrato);     
      $('#telefono-proveedor').val(res.telefono);
      $('#tipo-proveedor').val(res.tipo);
      $('#condicion-proveedor').val(res.condicion);
      $('#modal-proveedores').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos del proveedor. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

// --------------------------------------------------PLUGINS----------------------------------------------------------------------

//INPUTMASK PARA TELEFONO
$("[data-mask]").inputmask();


