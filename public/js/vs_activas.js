
var RUTA_VIAJES = ruta+'/viajes';

// -----------------------------------------------------------------------------------------------------------------------
var tabla_solicitudes = $("#tabla-solicitudes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/solicitudes_listar/1',
  search: { "caseInsensitive": true },
  columns: [
    {
      'targets': 0,
      'searchable': false,
      'orderable': false,
      'className': 'text-center',
      'render': function (data, type, full, meta){
        return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
      }
    },
    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'origen', name: 'origen',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'destino', name: 'destino',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'solicitante.nombre', name: 'solicitante.nombre',
        render: function ( data, type, full, meta ) {
        var text =data+' '+full.solicitante.cargo+' de '+full.solicitante.departamento.nombre+'</br>';
        return '<p">'+text+'</p>'
      }
    },
    { data: 'supervisor.nombre', name: 'supervisor.nombre',
      render: function ( data, type, full, meta ) {
        var text='';
        if (full.supervisor.id != full.solicitante.id) {
          text =full.supervisor.nombre+' '+full.supervisor.cargo+' de '+full.supervisor.departamento.nombre+'</br>';}
        return '<p">'+text+'</p>'
      }
    },
    { data: 'motivo', name: 'motivo', "width": "20%",
      render: function ( data, type, full, meta ) {
        var text ='<b>Motivo: </b>'+full.motivo+'</br>';
        text +='<b>Nro. de Pasajeros: </b>'+full.nro_pasajeros+'</br>';
        text +='<b>Fecha y hora de salida:</b></br>'+full.fecha_salida+' '+full.hora_salida;
        if (full.fecha_regreso != null) {
          text +='</br><b>Fecha y hora de regreso:</b></br>'+full.fecha_regreso+' '+full.hora_regreso;
        }
        if (full.dt_aprobado != null) {
          text +='</br><b>Aprobado el:</b></br>'+full.dt_aprobado;
        }
        return '<p">'+text+'</p>'
      }
    },
    { data: 'fecha_salida', name: 'fecha_salida', "visible": false},
    { data: 'fecha_regreso', name: 'fecha_regreso', "visible": false},

  ],
  language: leng,
  order: [[ 1, "desc" ]],
  "bAutoWidth": false,
  "scrollX": true,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-solicitudes').click(function(){
  tabla_solicitudes.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-solicitudes').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_solicitudes.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
}); 

$('#btn-tomar-select').click(function(){
  var data = {
    ids: tabla_solicitudes.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  var _token = $('input[name="_token"]').val();
  data._token= _token;
  if(data.ids.length > 0){
    swal({
      text: "¿Estás seguro que desea crear un viaje para estas solicitudes?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      preConfirm: function() {
        return new Promise(function(resolve, reject) {          
          $.ajax({
            url: RUTA_VIAJES+'/nuevo',
            type: 'POST',
            data: data,
            success: function(res){ 
              if (res.puestos <= 4) {
                sweetAlert({
                  title:'Exito!',
                  text: 'Espere un momento.',
                  type: 'success',
                  showConfirmButton: false,
                })
                window.location.href = RUTA_VIAJES+'/'+res.id+'/edit';
              }
              else {
                swal(
                  'Error',
                  'El número de pasajeros excede la cantidad permitida por vehículo.',
                  'error'
                  )
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    })
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar al menos  una solicitud para porder realizar esta acción',
      'warning'
      );
  }
});

$('#btn-asignar-select').click(function(){
  $.getJSON(RUTA_VIAJES+"/opciones", function(viajes){

    var data = {
      ids: tabla_solicitudes.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
    };
    var _token = $('input[name="_token"]').val();
    data._token= _token;
    
    var opciones = {};  
    $.map(viajes,function(viajes) {
      opciones[viajes.id] = viajes.nombre;
    });  
    console.log(opciones)

    if(viajes.length > 0){
      if(data.ids.length > 0){
        swal({
          type: 'info',
          text: 'Seleccione el ID del viaje a asignar para las solicitudes seleccionadas.',
          input: 'select',
          inputPlaceholder: 'Motivo del rechazo',
          inputOptions: opciones,
          inputPlaceholder: '-SELECCIONE-',
          inputAttributes: {
            autocapitalize: 'off',
          },  
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cancelar',
          showLoaderOnConfirm: true,
          inputValidator: function(inputValue) {
            return new Promise(function(resolve, reject) {
                if (inputValue==="" || inputValue===null || inputValue===undefined ) {
                    reject('Por favor seleccione el ID del viaje para asignar.');
                } else {
                    resolve();
                }
            });
          },
          preConfirm: function(inputValue) {
            return new Promise(function(resolve, reject) {          
              data.viaje_id= inputValue;
              $.ajax({
                url: RUTA_VIAJES+'/asignar',
                type: 'PUT',
                data: data,
                success: function(res){
                  if (res <= 4) {
                    swal(
                      'Asignados!',
                      'Las solicitudes seleccionados se han asignado exitosamente',
                      'success'
                    );
                    tabla_solicitudes.ajax.reload();
                    $("#recargar_solicitudes").load(location.href + " #recargar_solicitudes");
                  }
                  else {
                    swal(
                      'Error',
                      'El número de pasajeros excede la cantidad de puestos disponibles ',
                      'error'
                      )
                  }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  swal(
                    'Error',
                    'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                    'error'
                    )
                }
              })
            });
          },
          allowOutsideClick: false
        })
      }
      else{
        swal(
        'Atención!',
        'Debe de seleccionar al menos una solicitud para porder realizar esta acción.',
        'warning'
        );
      }
    }
    else{
      swal(
        'Atención!',
        'No hay viajes activos para asignar.',
        'warning'
        );
    }
  });
});

$('#btn-rechazar-select').click(function(){
  var data = {
    ids: tabla_solicitudes.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  var _token = $('input[name="_token"]').val();
  data._token= _token;

  if(data.ids.length > 0){
    swal({
      type: 'info',
      title: '¿Estás seguro que desea rechazar estas solicitudes?',
      input: 'textarea',
      inputPlaceholder: 'Motivo del rechazo',
      inputAttributes: {
        autocapitalize: 'off',
      },  
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      inputValidator: function(inputValue) {
        return new Promise(function(resolve, reject) {
            if (inputValue==="" || inputValue===null || inputValue===undefined ) {
                reject('Por favor ingrese el motivo de rechazo.');
            } else {
                resolve();
            }
        });
      },
      preConfirm: function(inputValue) {
        return new Promise(function(resolve, reject) {
          console.log(data);          
          data.motivo_rechazo= inputValue;
          $.ajax({
            url: RUTA_VIAJES+'/solicitudes_rechazar',
            type: 'PUT',
            data: data,
            success: function(res){ 
              resolve()
              tabla_solicitudes.ajax.reload();
              $("#recargar_solicitudes").load(location.href + " #recargar_solicitudes");

            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    }).then(function() {
      swal(
        'Rechazados!',
        'Las solicitudes seleccionadas se han rechazado exitosamente',
        'success'
        );
    });
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar al menos  una solicitud para porder realizar esta acción',
      'warning'
      );
  }
});

// -----------------------------------------------------------------------------------------------------------------------
var tabla_pendientes = $("#tabla-pendientes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/solicitudes_listar/2',
  search: { "caseInsensitive": true },
  columns: [
    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'origen', name: 'origen',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'destino', name: 'destino',
      render: function ( data, type, full, meta ) {
        var text ='<p>'+data+'</p>'
        return text;
      }      
    },
    { data: 'solicitante.nombre', name: 'solicitante.nombre',
        render: function ( data, type, full, meta ) {
        var text =data+' '+full.solicitante.cargo+' de '+full.solicitante.departamento.nombre+'</br>';
        return '<p">'+text+'</p>'
      }
    },
    { data: 'supervisor.nombre', name: 'supervisor.nombre',
      render: function ( data, type, full, meta ) {
        var text='';
        if (full.supervisor.id != full.solicitante.id) {
          text =full.supervisor.nombre+' '+full.supervisor.cargo+' de '+full.supervisor.departamento.nombre+'</br>';}
        return '<p">'+text+'</p>'
      }
    },
    { data: 'motivo', name: 'motivo', "width": "20%",
      render: function ( data, type, full, meta ) {
        var text ='<b>Motivo: </b>'+full.motivo+'</br>';
        text +='<b>Nro. de Pasajeros: </b>'+full.nro_pasajeros+'</br>';
        text +='<b>Fecha y hora de salida:</b></br>'+full.fecha_salida+' '+full.hora_salida;
        if (full.fecha_regreso != null) {
          text +='</br><b>Fecha y hora de regreso:</b></br>'+full.fecha_regreso+' '+full.hora_regreso;
        }
        if (full.dt_aprobado != null) {
          text +='</br><b>Aprobado el:</b></br>'+full.dt_aprobado;
        }
        if (full.viajes.length > 0) {
          text += '</br><b>Viajes Asociados:</b></br>'
          for (var i = 0; i < full.viajes.length; i++) {            
            text +='ID.'+full.viajes[i].id;
            if (full.chofer) { text += ' - Chofer:'+full.chofer.nombre+'</a><br />'} ;
          }
        }
        return '<p">'+text+'</p>'
      }
    },
    { data: 'estado', name: 'estado'},
    { data: 'fecha_salida', name: 'fecha_salida', "visible": false},
    { data: 'fecha_regreso', name: 'fecha_regreso', "visible": false},

  ],
  language: leng,
  order: [[ 1, "desc" ]],
  "bAutoWidth": false,
  "scrollX": true,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-pendientes').click(function(){
  tabla_pendientes.ajax.reload();
})
// ------------------------------------------------------------------------------------------------------------------------

var tabla_viajes = $("#tabla-viajes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/listar/1',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'activos_list', name: 'activos_list', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text ='Sin Asignar';
      if (full.activos.length > 0) {
        for (var i = 0; i < full.activos.length; i++) {
          text ='<a href="'+ruta+'/activos/'+full.activos[i].id+'">'+full.activos[i].unidad+' '+full.activos[i].placa+' '+full.activos[i].marca+''+full.activos[i].modelo+'</a><br />';
        }
      }
      return text;
      }
    },
    { data: 'chofer.nombre', name: 'chofer.nombre',
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.chofer ==null) {
          text+='Sin Asignar';
        }
        else {
          text+=data;
        }
        return text;
        }
    },
    { data: 'origen', name: 'origen'},    
    { data: 'salida', name: 'salida',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_salida+' '+full.hora_salida;
        return text;
      }
    }, 
    { data: 'estado', name: 'estado'},       
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text = '<a href="'+RUTA_VIAJES+'/'+data+'" title="Ver" class="btn btn-info btn-xs"><i class="fa  fa-eye"></i></a> ';
        if (full.estado == 'Sin asignar') {
          text += '<a href="'+RUTA_VIAJES+'/'+data+'/edit" title="Asignar" class="btn btn-danger btn-xs"><i class="fa fa-exclamation-triangle"></i></a> '
        }        
        if (full.estado == 'En proceso' || full.estado == 'Programado') {
          text += '<a href="'+RUTA_VIAJES+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> '
        }        
        if (full.estado == 'En proceso') {
          text += '<a href="'+RUTA_VIAJES+'/'+data+'/cerrar" title="Cerrar" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>';
        }
        return text;
      }
    }
  ],

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-viajes').click(function(){
  tabla_viajes.ajax.reload();
})

