
var RUTA_ARTICULOS = ruta+'/articulos';

// ---------------------------------------------------INDEX----------------------------------------------------------------------
var tabla_articulos = $("#articulos").DataTable({

  dom: 
  "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
  "<'row'<'col-sm-12'tr>>" +
  "<'row'<'col-sm-5'i><'col-sm-7'p>>",

  processing: true,
  serverSide: true,
  ajax: RUTA_ARTICULOS+'/listar',
  columns: [
  
  { data: 'estado' , name: 'estado', "orderable": false, 'className': 'text-center', 
    render: function(data, type, full, meta){
      var text ;
      if(full.cantidad <= full.lowstock){
       text= '<span class="label label-danger"><i class="fa fa-warning"></i></span>';      
      }
      else{
       text='';      
      }
    return text;     
    }
  },
  { data: 'nro_activo', name: 'nro_activo', 'className': 'text-center'},
  { data: 'nombre', name: 'nombre', 'className': 'text-center'},
  { data: 'cantidad', name: 'cantidad','className': 'text-center',
    render: function(data, type, full, meta){
      var text = +data+' | '+full.unidad;
      return '<div>'+text+'</div>';     
    }
  },
  { data: 'categoria', name: 'categoria','className': 'text-center'},
  { data: 'fabricante' , name: 'fabricante','className': 'text-center'},
  { data: 'id', name: 'id',"visible": false},
  { data: 'unidad' , name: 'unidad', "visible": false},  
  { data: 'estado' , name: 'estado', "visible": false} 
  ],
  createdRow: function ( row, full, index ) {
   if (full.cantidad <= full.lowstock) {
      $(row).addClass('danger')
    }
  },

  order: [[ 6, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
  
  buttons: [
          {
          extend: 'excelHtml5',
          exportOptions: {
              columns: [1,2,3,4,5],
              search: 'applied',
              order: 'applied' //column id visible in PDF 
            } 
        },
            /*
            {
                extend: 'pdfHtml5',
                title: 'Lista de Articulos',
                pageSize: 'LETTER',
                orientation: 'portrait',
                customize: function (doc) {
                  doc.pageMargins = [50,85,50,50];//[izq,arriba,derecha,abajo]
                  doc.content[1].alignment = 'center';
                  doc.content[1].table.widths = 
                      Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  var objLayout = {};
                  // Horizontal line thickness
                  objLayout['hLineWidth'] = function(i) { return .5; };
                  // Vertikal line thickness
                  objLayout['vLineWidth'] = function(i) { return .5; };
                  // Horizontal line color
                  objLayout['hLineColor'] = function(i) { return '#aaa'; };
                  // Vertical line color
                  objLayout['vLineColor'] = function(i) { return '#aaa'; };
                  // Left padding of the cell
                  objLayout['paddingLeft'] = function(i) { return 4; };
                  // Right padding of the cell
                  objLayout['paddingRight'] = function(i) { return 4; };

                  objLayout['paddingTop'] = function(i) { return 10; };

                  objLayout['paddingBottom'] = function(i) { return 10; };

                  // Inject the object in the document
                  doc.content[1].layout = objLayout;


                  doc['header']=(function() {
                    return {
                      columns: [
                        {
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB0sAAAHkCAYAAABfSMfmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAuIwAALiMAcz2uy8AAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxNzowODoyMiAwODo1ODo0NwlViroAAEOZSURBVHhe7d1JkhxHliDQDEqfAIvuU6UIV0Xuu49V2KN6BZE+VXHBE6Qgmka4ZTgcPtigw/+q74l4ZiBIuqt9nUz1uxre3t/f/wE9/Y//96YRTuDT1y+XnwBy+ePzb2+XHwEAAACAwfxy+X8AAG5IlAIAAADA2CRL6cqp0nn8+evvl58AAAAAAABikCwFALjDqVIAAAAAGJ9kKd04VQoAAAAAAEBPkqVAMx7FC2ThVCkAAAAAzEGylC6cKgUAAAAAAKA3yVKgKadLgeicKgUAAACAeUiWAgAAAAAAAFOSLKU5j+DF6VIgKqdKAQAAAGAukqUAAAAAAADAlCRLacqpUgCicqoUAAAAAOYjWQp04VG8AAAAAABAb5KlNONUKQBROVUKAAAAAHOSLAW6cboUiECiFAAAAADmJVkKAAAAAAAATEmylCY8gheAiJwqBQAAAIC5SZYCXXkULwAAAAAA0ItkKdU5VQpARE6VAgAAAACSpUB3TpcCAAAAAAA9SJYCANNxqhQAAAAAWEiWUpVH8LKV06UAAAAAAEBrkqUAwFScKgUAAAAAVpKlVONUKQAAAAAAAJFJlgJheBQvUJtTpQAAAADANclSqnCqFAAAAAAAgOgkS4FQnC4FanGqFAAAAAC4JVkKAAAAAAAATEmylOI8gheAaJwqBQAAAADukSwFwvEoXgAAAAAAoAXJUopyqhSAaJwqBQAAAAAekSwFQnK6FChBohQAAAAAeEaylGKcKgUAAAAAACATyVIAYEhOlQIAAAAAr0iWAmF5FC8AAAAAAFCTZClFeAQvAJE4VQoAAAAAbCFZCoTmdCkAAAAAAFCLZCmnOVUKQCROlQIAAAAAW0mWAuE5XQoAAAAAANTw9v7uUCDnOFlKC5++frn8BPCYU6UAADC+//m//6vKXpT1BAAjMV9uJ1nKKRKltCJZCmxhcwMAAMZQa4P3DOsNACIxV5YjWcopkqW0JGEKPGPjAgAAcoq42buVdQgALZgr65Is5TCJUlqTLAWesUlBloWDtvoh82LvntHrdrT6WuiPPzKOxjZiH+TDbO165PZsbnnMPNNOxFjrGz/TJ3hlxPkyanv65fL/AOH9+evvl58AfuTGHYhg5I1fADhrmSfX1+VXQ5rlOgEo73oOGXUeiXp9kqUc4lQpABBJpkXEqAseIDfjKFDD0l/X1+VXU5n9+rNSX7SQqZ3pE/XNOles1x3h2iVLAYDUnCoFqG/GhTsAx0Ta+IxETHJRV0Bt67xgvPmudywkS4FUPIoXgFsZFxYWQ+NSt2RkHAVKWPqlvvnaGiexgnll7P/GrHLMAc/1io9kKbt5BC8AUThVCgAAfdn0PU7sYlM3QEnG/H1ax0uyFEjH6VIAgHYs6AG4x6ZvOWIJMDZj/HGt5kjJUnZxqhSAKJwqZZF5wWGxNC51SybGUWCvpe/pf3WIazzqhNIytyn9Yb8lZuJWRu04SpYCAOlIlAIAQHs2fOuzsR6P+gCOMHaUV3OOlCxlM6dKicSjeAEYYeFh8QT0ZBwFtlr6mv7WlpjDeEbo08al14zf9dWIr2QpAJCKU6VABqMsji3yATAX9CX+MagHYAtjRTulYy1ZCqTldCkAAADUsWxC2vSNQT3EoB6AZ4wR7ZWMuWQpm3gELwAROFXKaqRFiAUV0INxFHhGv4pnqRP1Anm59xqbmPRTKvaSpUBqTpcCAFFlXzBb8APMyfgfm/rpS/yBW8aF/krUgWQpLzlVCkAETpWyGnEhYnEFtGQcBR7Rl3JQT32JP3u59xqXOMRxti4kSwEAAABgYssGow3fXNQXQF/G4XjO1IlkKZCeR/HC+JwqZWUxAm3oa+NSt8At40Je6q4fsWcrbWVM6jWuo3UjWcpTHsELANCGxdaY1Cu0o7/BfvpNfuqwH7FndvoAUR1pm5KlwBCcLoVxOVUKAADl2eQeh7oEaMeYOybJUh5yqhQAiGSGBYlFFxFoh+MyjgIrfWU86rQPcecZ917j0efz2FtXkqUAQFhOlQIjsKAGAFpwz9GHuAPkJ1nKXU6VkpFH8QKMa6YNCJstQA3GUWClj4xN/UIM7r3GY3zNZ0+dSZYCACE5VQrQng0AgLEZ5+egntsTcyCyZY/tyOvyn09BshQYitOlAEBENtAA6M1cNBf13Z6Yw7iy9e9SSc9S79PT1rp7e383hvMjj+Alu09fv1x+ArLKegNGHbNuOszQD2ar2wx1qk7GZBwdw6z1OIva7XX09nMmfiPHJto4OMM4FiXmEWM92rz8zKxz9sh1nKFOW8c/UzvfEhsnSwGAUGZaQAEAAPss64Xb1+UfHVLyvaKZNWHTk5gDrfWavzLNm1vGZidL+YFTpYzC6VLIa7QNCs6ZfbNh9P4wY/1GrlP1MSbjqPuKWWRv6zXb6gjjQK++PMoYGmUsnGVOihDviLGeZU6epZ0/MmI9R65T4/t2r2LlZCkAEMYsiycAAMrJvjFd8x54hNj0XCOsn9+zDOQze7IMaCPS3DTCXClZCgzpz19/v/wEQFY2GQDOMY4yA4nSMUXcdM28EWw+aE/M56TeaSXqfBR5nnzVPyVL+TeP4AWgp6wbD1CTxfZ4otaptsaotO2xZa/f2ve/WeMTfV2Qdd1iPARaMNbUZ/+sDslSYFhOlwIAAIxJonQ8S0yyxCVTWelH0gjy04/3yzo/SpbyN6dKAejJRgO3LEg+iAVwhLHjg1iMJ3udtrj3zRajrOuBbOU2HrYn5vNQ1x/Eop4s807GeV2yFAAAYGI2M4BMso9ZWZOCNWWPiToFgBye3UdKluJUKUPzKF6Iz+YCtyRuaK31ODR7Gzfu12ccZVQSpdtkitMoc0Km6zBHtCfm41PH8LNsc7xkKQAA3Ih2U2/xDWRjHKW07HU4SlKwpNFioo55xjxEbe694Jy393dtdnZOljKDf/3zvesNgxuEfixYIZ8IY+YydkQbu0ccz6LEuEd9R6nPHnUQqX+N2K8WEeIbqZ5Xo9b3DKK1pb1atr0ssRq5P6qDx7L35bNmHwv0+7qW+Ear9xHqXEzPyTQWOVk6OYlSACCSSDfS0RYhERcZHDdrfY6wYRKdcfQx42hO2evNuPez0WOiznnEPDQm916PafPlZYtppjlRshQAAK7Y4JqTeody9CdKkSgFRiN5RA3mm7kYR+qQLJ2YU6XMRHsHiM8NP7Slz41HnTKS7O25x8Z1hpjNsqGf4TrNGXCefkQv2l55/s7SiUkeMZuef2+pCawf366DPKKMlbfjRrQxfKRxLVqd9yhPz/rseb1R+3t2UeMapVyr0ep9RNHazF692lj0uM3W9zK049Z1kr1vl1Q79hFjPeIYECXOt7GNVv+Z6z76uDViv+rFydJJSZQyI+0eIK6oi0zmov7rEt+6jKOMIvqm5Cu9+kD2uI3IeMgz+mx+7r2IwFhSjmQpAAA8EW3xaTEEZGMcZavsdWPD+rFZYxP9uo2HMCb3XnNZ4ivG50mWAgAAdNJrUWsxDUSTfVySKAX2cj8G8WWa35cxxbhynGTphDyKlJlp/wDxRLmZt8kJ9ehfdRlHyS77xp62DxwlsZGTey8iW9qnsWU/yVIAAHgh2iLUwqceGw5Qh3GUR7LXRYS2HT2Gs8+t0a/feAhjcu/FEnNx306ydDJO1YF+ABCJG3do3w/0u7GoTzLL3n5nTwICZZjLc1FfZLO02fV1+RV3SJYCADC9jJudFjpkIZkwB+Moe2WPv7FtG3H6ThyA0tx7tTPSGL7Uwfq6/IoLyVJgSk6XArCXTa55qGuoQ99iJVEK8COJC2owX/GMxOmPJEsnIjkEAEQS5YY88wLSooa9tJmxGEfP0yfayx7zaO1dG6aEGdtRxLlLf44vSh2596K0pV7W1+VX05EshQb+9c/3tBMYAPAh86KU2GwaMAvj6Nyyj3XaLwDZmLvKmCmO14nTmdapkqWTcKq0H4nSuPQLgH5muuGGmd3bVND/yxBHssneZm027ydmPxKPeCLWifk9LnXDrJa2v74uvxqSZCkAANMaYdPKor2OETc0tRVqMI6yRfYYS3LBuCRMyca9Vz/uB77X3fXr8ushSJZOwOm5fq5PlTphGpP+AdBe9htqCyRqGW2xST3GUTLRXgHIzlzGSix/tPSN69fl1ylJlgIAQHLZFyWMyUYCmRhH68geV+MYzCFiXzcvMTptfExLva6vy6/SkCwFAGBKNkB5ZaQ2YjOCGoyjPJN93NG+AYjG3BSDetgmW+JUsnRwHjHaz73H7noUb0z6CUA7oyRsLI6oZZQ+Qj3GUTLI3k6ztM/IcdbH85ntHuT2eiO22dnqJKpR6sG4XJZ47rP0o+vX5dehSJYCAMAARlnEA/RiHC0jexxtflKT9hWbhCm05Z5hXkvdr6/Lr7qTLB2Y03IxOV0ak/4CUF+km+ASixoLo3FkX6RHpH/UYRwluuzjqTYJwDX3XlDf0s/W1+VXXUiWQgUSogBAD9k3qSPqsaFQuh61C9hOfzkue+xsIAOLiGPBkfHVmEYW7h+4trSH9XX5VTOSpYNySg72028A5mAxA3COcZRbNjoB6so+znKOeSo29VNH68SpZCl04OQpALMZdXFvUQQ/0y/qMI4SVfa2qQ0Ct4wLLNx7sccSV7GtZ+mPtfukZCkUJhEKAPQ06qK+px6L3lL1qD3AfvrNdtljZVMTeCTi+GB+YlQjtW33FnUtbaVWe5EsHZBHieYgqRqT/gNQXqSFj4ULkJFxlIiyb2xqywA84t6LM9RZfUsfLd1PJUuhIAlQAJhPxIVQ9g1sYC7G0Xyyx8cmJrCF+YlRadv1LTF2v1Hf0m5KtR3J0sE4FQfn6UcA5VjMQz89+p8NgfKMo0STvU0ap4A9JJXmI76U5L6jjaXfnu27kqXQkZOoANBOzUWKBdD4etSxjRqiMY6SfVzSzgDIxL3XGJZYi3cbZ+5VJUuhEIlPACASiTaAc4yjP5IoBWYVcfwwRzGi0du1pGkbR9uRZOlAPDoUytGfAM6zgId+evQ/C//yjKNEkb0tGp8A2MK9Fy24L6lv6ct7+7NkKXTmRCoA1NdiMRJxwWOxX1aPOlaHRGEcnVf2GERsV0A+5ihac+81tiX26+vyKyrY054kSwfhFFxfEp5j0q8AjrNwBzjHOEoE2duhDUigJGPK2Nx70YukaV1b+7ZkKQQg2QoA1GLRPx91DmXN2qeyX7dNR+CVEcZ3932MaNZ2LWnal2TpAJx+AwB4rOViw8KGGjJsFmj7YzOOzkeiFOC+iOOLhOl43HvNbamT9XX5FSdtGSclS+Ekp0LH5ssIAPtZrDMyC1ZaMI7Sk0QpALNx70VUEqflvOrnkqUQhKQrAJTXY0ERcRFj8Q8cZRz9bpZxVKI0l8jX697jvshxma3/nGGeoqYe7UubzmGpp/V1+RU7PWtXkqXJOfXWlwTnHPQzgO0saKCvHn3QYr0s4yi9ZG97xiKgJWPOONx7kdGaNDUWlSNZCgAAE7AJUE6PBan6g/5G7ocSpQD5uV9kNNr0NteJU/dErz1qV5KliTntNh4nVePS3wBei7aQ6blIsEABjjCOfjCOtpN9I1JbAXox/uTn3uuD9jyOpS7X1+VX3LjX9yVL4SCJTQAAIsme8ADakygFOCfaOOR+ELh2nTh13/Tc2/u78TMjp9z6q5ksVb8xnalzN6v9uBGAdox1OUQaFyO1mb1x6VX2Z+XsUaYj7SlzvddmHM0hWrs5Knt7G6Uezopej+rpg7qKGYMS1x31uqKVK+J4EL1f8p25pJ7Z+8B123KyFGAjSWyAxywymY0FO6UZR2kpe3szBgORGJNycu8F38ev69fl11OSLIUDPIIXAMjKpgDAOdnHUYnSsYgHJWhH50WL4TLWq1dGYQ3bzjJuXL8uv56CZGlCTreNTzI2Lv0P4GcWLtBfj35oA64c4yitZG9rxh0ASnDvBduMnji9HgskSwEAAJKwsQMcJVFKD+at78RhDtHGKe0OKOk6cTrifZlkaTJOtfXn1CcAkJ2NkzJGXCAC22QbR7OP+8ZbIAvjFdRhDRvPmjQdZdyTLIWgJGXj8qUFgA8WLADnGEepTaJ0fNFjNPs4F/369TFm494LyluTppnnlLf3d2NDJpI0fbVOYKrvuPa2BTdi/Vj4QV3Gt9x6j5GR2s/RWPS4htuyRijDHiPUe0nG0dwitKFnsrev6PGNJHpdz1yX6uZDxFjUun7z+8+ijAPqJrcW7ShaG8k8h2bpb0uMnSxNROJsPk6XxqU/AlhkwqLHwlXfG4e6pKbs7SvzxiCAMSwm917QVqaxULIUAAAAYCASpUQza4JCYgaA2WW5r5MsTcIptv6c8gSADzZ+xqAeOUISowz9bwwR6zF72zLGHCNu8WToi9pNXeIbi3uvMajHnDKMh5KlEJwkbVy+xAAA9GazALgmUUpk5ixmZFwD+C76eChZmoCEDAAAtdi4PK/Hok+9QRxR+qNEKRliOMv8leE69Tkgq9pjbLTxcZa5szfJUtjA6U4e8WUGYEZu1GFeNlbLMI5SWvY2ZWwBRmaM68+9F8QQeTyULIUEJGsBgJpsHuSk3iCOnv1RopRsRp+/zM/cY6yDsoy1lCZZGpxTaxCffgrMxIIE7rMBxlbGUUrK3p6MneVliemoY2GW69L3mIl7L0agHdcnWQovRDnV6XQpAAAAK4lSshtt49dGNq8Y9wDikiwNzGk1AABascHHKzb44LmW46hEKc9kiu8o9x+ZrkP/60v8oZyaY6++Oh/JUnjCaU628uUGYAajbGYB9GIcpYTs7cjmI7eyt2ljO8SlfzIS7bkuydKgJF64R/IWAKjJ4uscCQCg9jiafZw2TraTLdZZ23a2cuuDMagHKGe2Nexs19vCOiZLlgIU4ksOwMjckMPcbOqdZxzlrOxtyDjCK0sbz9LOM5WVmIyJ9emjEE/kfilZCg84xQkAAEAE2Td8JQX6yBr36O09a3/UDwH2iTpu+iJAHZKlATmdxjOSuAC05kZ8Lur7HBuR3KNfzaV0fWdvP8ZFjljafbS2H7FMW+mHMamXerLPnewzY31nvOboZZYsBSjIlx0AAIBSsm/+SQT0l70Olj7Qsx+sn5+9LxKXcRI4KtPcFLWs12Pw2/u7uT4SiZYYMpze1Fbiutd+LKz6sfCA84xhc2oxfkZqWyWvd7Q+U7otjFrvzxhH51SifWk786g9Ho3UlsRqn1Zz3RYRYxshPrOM9S1jbf6cU602lqE9RRrrb0WO33XcnCwFKEwiGxiJRSbAOcZRgLKWcfX2dflHh5R8r2gib57zQT2VNVo/hi0izmHZ5lXJUgAAgIJseAEQzehz07ohe+R1eQvoyv0jxJWpf0aY27LMr7f16jG8gTiNFkOGR/CutJm4btuRBVg/FhxwXOSxa4S+nXHxUFqkGJS+1lHm/hptYOR6v2UcrWuGcXSUsYTXWvVJbWoeEcf5iO0vUpxG758tYh05hhH75F4Z2mitOGfun7P3vUdu4+JkKUAFEtkA9YywyFyMch1APsZRmJd+Mwf1nJN6G5d7r/wyX/uSyLx+XX592O37lXjP1u7Vp2RpEBIrMWQ6VbrIVl4AIJeMi54obHYBC+MoEZmjxqZ+c1N/cI57r9fWBOfR1+VthiNZClCJL0EAmUW9AR5t88BmCIzLONqGcRTggzGRmbn3amPmccYYO4ZH9ShZGoCECgAAEI3NAIBxGePHo07HoS4B2pMshYusj7T1KN7YfBkCyGjkx6qwn/YA++k3XNMeiEpCZhzqcjzqdB9zLddqtgd9M7dn9SdZCgBACqMuSiy2xqVuicY4CtzSf/JThxCXe68xGXdzelVvkqWdOXUGAETiG7nco13MxwbAcfoL92gXRGbMz0vdjU39bmOO5Z7a7UL/zGVLfUmWwl+yP8rWo3hj86UIAF6x0AI4xzgK5yx9SD/KRX3NQT0TlbbJaCRLO5JAAQDYxkKMrLRdotAWgS2MFTmoJ4hPPx2fOs5haz1JlsIgnC6N7c9ff7/8BBCXxxfxjPYBr+knPKN9kIXN39jUz3zU+WPmVp5p0T70z9j21I9kaSdOlcYhyQgARGCRRRTaIllpu1CO/hTPUifqZV7qnoi0y+/EIZ4jc6ZkKQAAob+Ra+ERh29uw2PGUbYwjpLJkY1G6lAP8DP3XmzRqp2o8ziO1oVkKVMb7VSpU7KxeRQvAK9YYI1JvUI7+huUp1/1Jf6stAUi0i4/iEV/Z+pAsrQDj+AFANjGYiMep6LGps+NR53GYxwlo2UsMZ60Jebco03Ep47iaXnvZezu52zcJUsBGnK6FIjIpi3AOcZRYBY2gOuz0Q6vufciOuN4O6XmTcnSxpwqjWPUR9Z6FC8A5GZRNSb1Cu3ob1BXqU1JfiaubKGdEI02eZ/5sq7S8ZUsBQCYWORv5FpUxOWb3PDBOMoRxlFGUHqTcmZiyV4ztxf3XhzRs91oF2XVmjMlS2FATpfG5lG8ALxiMUUP2h0j0Z6hnVqbljMQO2AUxrLnjPfn1Y6hZGlDHsEbh2QiADjVwjnaD+gHnKP9MBobwduscRIrzpqxDZk7OSNC+zH+79cqZpKlAB04XQrwnMUDI9KuaUl7A3pZNzWNQz8SE2rQpuJQF+xhTnitdYwkSxtxqpTWnJ4FgNwsnGhJe2NE2jX0t250ztofZ79+YC7Guv3MEz/qGQ/JUqYjiUgUTpcCPUV+fJFFQh4eg8XMjKOUYBxlJj03QFua5TqJY5a25t6LEqK3o9nmj+tr7n3db+/v7strc6o0lpmSpdpefJ++frn8RC0z3WAAAABklPmLA9acANQ2yhfsIs+ZkqUNSFjFMeOpUu0vNsnS+ixcAQAA8om4MWx9CUAU0ROo2eZMydIGJKvikCwlIgnTuixmAQAAxlJrg9j6EYAR1E6kjjhfSpZWJlEVy6x/X6l2GJtkaV0WuwAAAAAAPPLL5f9heLMmSonvz19/v/wEAAAAAAC0JFlakdN8AAAAAAAAEJdkKUzAqVoAAAAAAICfSZZW4lRpLJKFROdRvAAAAAAA0J5kKQAAAAAAADAlyVKYhNO18TldCgAAAAAAbUmWVuARvLFIEgIAAAAAAHCPZClMROIYAAAAAADgg2RpYU6VxiI5SDYexQsAAAAAAO1IlgIAAAAAAABTkiyFyThtG5/TpQAAAAAA0IZkaUEewRuLpCAAAAAAAADPSJYCBOR0KQAAAAAA1CdZWohTpWTi1C0AAAAAAIBkKYOSDAQAAAAAAOAVydICnColIwnl+DyKFwAAAAAA6pIsBQAAAAAAAKYkWcpwnJhkJE6XAgAAAABAPZKlJ3kEL5lJLAMAAAAAADOTLGUokn8AAAAAAABsJVl6glOljECCOT6P4gUAAAAAgDokSwEAAAAAAIApvb2/Oxx5lJOlsTgheZy2nMOnr18uP7HHH59/MzYAAKnVuF+3fgKAD+ZagLlJlh4kuRSPG5BztOn4JEuPkSwFAKKLdi9ubQXAaMy1ADwjWXqQxFI8bjLO0aZzkDDdT7IUAIgm47239RYAmZhrAdhDsvQASaV43Eycp13nIFm6n2QpPGbsn0vP+yVtLT/32+eM2Ae0iW2i1v1o9SfOgLkWcsvSh/XLcf1y+X9gcgZ6AAAoY9nsuX5dfj2U0a8PgNiu56FR56LRrw8gEsnSnUxOQE9//vr75ScAAKKZdUPTRi4ArZhrYTyZ2rZ+OC7JUtJzIrIcsQQAgH3WzUsbJ2IBQB3mlw9iAVCHZClAMk6XAgD0Z6PyOfEB4CxzyXPiwwgytmH9bkySpTvoBPE4CQkAALRkY3If8QJgL3PHPuIFcJ5kKfADCWgAAPiZjchzxA+AV8wV54gfwHGSpRuZaIBIPIoXAKANG49liSUAt8y1ZYklWWRuq/rZeCRLScsJSAAAoBYbt/WILQAL80E9Yguwj2TpBiYWZiMRnYPTpQAAdVgDtiHOAPMyB7QhzkQ1QtvUv8YiWQoAAAB/WTY8bHq0Jd4AczHXtifeAK9JlpKSk4/1iTEAADOxkdiPjXOAORjr+zHXAjwnWfqCSQSIzKN4AQDOs+6LQT0AjMsYH4N6IIKR2qE+NQ7JUgAAAKZlgyMW9QEwHmN7LOoD4GeSpU+YOGLyeNh2xDoHp0sBAPZb1nvWfDGpF4AxmGvjUi/0MmLb05/GIFkKAADAVGxoxKeOAHIzjsenjgA+vL2/GxMfMWEAmXz6+uXyE9f++PybE9LwgHudufR8YoS2lt9ITxzRHnPJ3vaitrfRniIkzhCLuTYXYxWtjDw26Ef5OVn6gEkdAABgLNZ5+agzgFyM2/moMzhPP8pPshQAAIDh2cDIS90B5GC8zkvdAbOTLL3D5ABk9Oevv19+AgDgmjUeANRlrgWemWGMMA7mJlkKAADAsGxajEE9AsRljB6DegRmJll6w6QAZOZ0KQDAB+u7sahPgHiMzWNRn9QwU7vSh/KSLAUAAAAAAACmJFkKAADAcHyre0zqFSAOY/KY1Cswo7f3d2PfykQAjOLT1y+Xn/jj829vlx+BG+595vKvf753Gw+1tfx6tp8jRm5zR+titJhkaZNR456tT78iztCeufZn5lr42chjxTP6Tz5OlgIAADCMETcqr1+XX+92+z5n3guAuZlr77t9nzPvBUBbTpZezPoNB2BcTpd+52QpjCHqvZoNkG3UHy2NsLbr0TYzxi1DHzb+tSHO0Ja59hhzLbMZYaw4Q//JxclSAAAAhpB9Q2bZUOm1qdLzswHIw1x7nLkWIC7J0r/M/g0HAACA7DKv6yJtnmbayLWWB2jLXFuGuZYZaDtkI1kKMKg/f/398hMAABFF3izNsokLAM+Ya4FeJIxzkSwFAAAgtYwbERk2SG3iArAy19ZhrgWIYfpkqew+MDKnSwEA4sm0MRq9rNb0ANxjri3HXMte2swHscjDyVIAAADSyrYBkfEEScYyA1COubY+cy1AX1MnS2X1gRk4XQoAEEPmjVCbuABkYK6FvuRcyMrJUgAAAFLKtBkzwgZo1GuwKQdQj7m2LXMtnBetH+k/OUiWAgAAQEUjbN6uRroWAMZhrgXgjGmTpbL5wEw8ihcAGE2WNZ0NTwCyMtcCe0QaM6KNC/JR8TlZCgAAABWMunlrUxqAKMy1wDV9h6OmTJbK4gMzcroUABiFNR23tAmAsoyr3NImeEb7IDsnSwEAAKCw0b/V7lv7APRmrgWeidaHJJRjmy5ZqkECAABQk81NAKjLXAtxRMm5GBc4w8lSgIl4FC8AkJ0vwMZhQwpgTObaOMy1cE60PmR8jUuyFAAAAAqxqQkAdZlrASjt7f19nkS2rD3Ad5++frn8NL4/Pv9mEQUDiHofZ6NmG/VHSdHXddoV14x/bYgzlGWuBbaKMl48GxeijWnGsJicLAUAAIACbHwAQF3mWmCvaONG9C+kzGqak6UaIMAHJ0uBbKLey9ms2Ub9UUr0dZ02xS17EXMzJpCRuRbYKtJ48WpsiDa2GcvicbIUYEJ//vr75ScAAAAAgJwyJh59oS6eKZKlGh4AAAA1+XY4ANRlrgWOMn7wyhSP4ZUsBbhvhsfxegwvjCHq/ZwF1zbux3PI0J4jtyXjAfcY/+ZmXCAjcy2wRZSxYs+4EHF8M67F4TG8AAAAhCfpBAB1mWuBkUlM8szwyVKTPMBj/u5SAAAAAGArORdG5GQpAAAAnOBb6gBQl7kWuDXCuCDxHMfQyVINDQAAAAAA4LzsORdfvOARJ0sBJudRvAAAAAAA7Tn0F4NkKQAAAAAAACk4IUppwyZLZeMBtnO6FACIzPoOAOoy1wKvjDJOSLRyj5OlAAAAcJDNFgCoy1wLjM4XVvobMlmqYQEAAAAAAJwXKedS4gsUvoTBLSdLAfibR/ECAAAAALTnEGBfwyVLNSgAAAAAAICxOBFKLU6WAvBvTpcCAAAAAKtRD6hJvHJNshQAAAAAAAA68uTUfoZKlmpIAAAAAAAA50XKuTgJSk1OlgLwA4/iBQAAAABGFzEB61BgH8MkSzUgAAAAAACA8+RcmImTpQD8xOlSAAAAACCCmidAPd6XxRDJUt9wAAAAAAAAIDs5r/acLAXgLqdLAQAAAGA+knXMRrIUAAAAAACAcFo8Jjfio3glrNtKnyzVYAAAAAAAAM6Tc2FGTpYC8JBH8QIAAAAAtCdx3U7qZKmGAgAAAAAAMJ6Wj8eN+Che2nGyFICnnC4FAAAAgPE5oMasJEsBAADgIBtKAFCXuRbm1OOkZ8TTpcbANt7e33PGWQMBaOfT1y+Xn/L54/NvHqEBA4h67+cxPduoP0qIvAbUlnjE+NeGOEMZ5lqYW7QxoFe/jzgWGgPrc7IUgJc8ihcAAAAAoL3IX2YZRcpkqYYBAAAAAABwnlOlH5zinJOTpQBs4nQpAAAAAACjSfd3ljpVCtBPxr+71N9ZCmOIeg/oG6fbqD9KiLwW1JZ4xPjXhjhDGeZamFfk/s8HY2E9TpYCsJnTpQAAP7O5BAB1mWuhHv0LJEsBAABIwLeoAaAucy1AbBLb9aRKlmoIAAAAAAAA58m5wHdOlgKwi0fxAgD8zEYTANRlrgWglrf39xxzjMkQII5PX79cforvj8+/eYwQDCDqvaBHlW2j/igl+rpwxjYVqU4ixt/414Y4Qznm2njMtdQUvc9zn75YnpOlAOzmdCkA0INNAQCoy1wL85AohQ8pkqU6LQAAANHNtna1VgegNXMtgLGhBidLAQAAgNSchAKAusy1Y5Fsgx9JlgJwiEfxAgD8zMYTANRlrgWgtPDJUpMfAAAAK6caYrBWBxiXuTYGcy21aFtjUI9lOVkKwGFOlwIA/MzGBQDUZa4FoKTQyVKTHgAAALcynHgZeT0b7dqcgAIoz1zbl7kW2EIOrZy39/e4sVTRuZg0OUpfz+3T1y+Xn2L64/NvxiYYQNS5wv3PNuqPGjLcQ47axqLFPnKcjX9tiDPUYa7tx1xLLRn6Nfvon2V4DC9F6JAwL4/iBQC4b8TNKBtsAERirgVmZ8woI2yyVAXDPCTbAQDYK8s9pLVtXdYSAPWYa1mYa8ehr8BjTpYCcJrTpQAAj42yMWWDDYCozLUAnBEyWWpSyMW3iwAAgB4yrUWyr3Ot0wHmZK5tx1wLHGX8OM/JUiAESff8nC4FAHgu6yZG1HJbQwBwy1xblrl2HJJp8Nzb+3usPqLT5mPSpBT9P79PX79cforjj8+/GaNgADYPclN/1JbtPjJT24sc2wxxNP61Ic7HRYyd+Tkmc2095tpjjF/7ZOvDHJNl7IvYHp0s5RQ3sAAAAPtk2Kxayhi5nNaiADxjrj3PXDuODP0BepMsBcJwE5afR/ECAD1kvI+MvEFqQw2AW+bassy1ALGESpaaJAAAADgi6xfvIq2DI28qX/MlS4A+zLXnmWtpLUN7oxz1fZyTpRxm0gTucboUAGCfdeO01+ZGz88GgBbMtQA88/b+HmOMNlnkI1lKDcaCMXz6+uXyU39/fP7NWAUDiDo/uB/axvw+jgxtfqT2VjPeWeOUbdw1f7UhzsdFjN1o7XNE5tptzLV1Gb+2Gam/sl30fhyxXTpZCoRiUQQAwBkj3U8umwjXr8uvd7t9nzPvBQDm2p/dvs+Z94JStEPYzslSDpPUohbjwRiinC51shTGEHVucD+0jbl9HFnavDY3poxjrvmrDXE+LmLs3F/lYK4dU6b+Z/x6TT+dV/S+HLFthkiW6rT5uHGlJmPCGCRLgZKizg3uibYxt4/DBhq9ZB1vzV9tiPNxEWPn/ioPc+1YsvU949dzkfvnCON8hvEvcpwjxs9jeIFwLIzG8Oevv19+AgBozz0lANRlrgX2GmXcMP6Np3uy1DeQAAAAqMEmxhjUI0BcxugxqEcYj9zbPk6WspvJEwAAyML6JTf1BxCfsTo39TeeqEmy0dqavjOWrslSmW3gEZPNGDyKFwCIwL1lTuoNIA9jdk7qDeA7J0sBAAAYns1AAKjLXAv9OaDGNe1hO8lSdnHTA+zldCkAEIX1TB7qCiAn43ce6oqWRm1v+tE4uiVLZbSBV0w2AACU5h4zPnUEkJtxPD51NCY5F+7RLrZxshSA6pwuBQAisUEYl7oBGIPxPC51A2XpU2PokiyVyc5JpwcAAEZhfROPOgEYi3E9HnVCD9odGThZCoRmMgUAoBb3mnGoC4AxGd/jUBdjc0CNZ7SP15onS1UKwJw8ihcAiMjGYV9L/NUBwNiM832Za6E+fSw/J0vZRGcHAABGZROxDzEHmIe5tg8xn0PkA2raYBwOMj4nWQqEZ1Idh9OlAEBk7jvbEWuAORn/2xFraEufy61pslTmGgAAgMiWTQ4bHfWILwDmgrrElyi0w3jk6B5zspSXDGoAAMBsrIPKE1MArpkXyhPT+Uh+QRnNkqU6LXCGm71xeBQvAJDFcg/qPvQ8cQTgEXNEGeIIMeiHeTlZCgAAAE/YgDxO3ADYwlx7nLjNK/IBNe0yLgcb75Ms5SmDGlCD06UAQEY2crcTKwCOMH9sJ1YQk36ZU5NkqUw1UIKJBgCACGxOPiY2AJRgPnlMbFjIuXCG9vMzJ0sBAADggHWzcvYNS3EAoBZzzHfiQCbaKRlVT5bKUOdlUANq8iheAGAkM25gznjNAPRjrgWy0G/zcbIUSMVEAwBAZOum5qj3raNfH9CXsYUtRp+LRr8+zot8QE27zcNBxx+9vb/Xi4dg52ZgIypjy1g+ff1y+amOPz7/ZiwDAELIeB9rXQhsVWKMM+ZwlrkWgCMkS7nLJE1kxpaxSJYCADOLdG9rHQicIVlKVOZaAF6RLOUuEzfRGV/GUjNhKlkKAGRV457XWg+oRbKUjMy1ACyqJUslMnIzqROdMWYskqUAAAC5SZYCAFn9cvl/+Dc3pgAAAAC0ZD8KAOilSrLUiS+gNouosfz56++XnwAAAMjGXiAAkJmTpQAAAAAAAMCUiidLfZMsN6f1gF6cLgUAAJiT/SgAoCcnS4G0LKYAAACgLwcnAIDsJEsBAAAAgC58ERoA6K1ostQ3yQA4w6N4AQAA8rAXCACMwMlS/s03+chIuwUAAID2SiRKrekBgAiKJUt9kwyAEpwuBQAAiM0+IAAwEidLAQAAAIBNSiVKnSoFAKKQLOVvblABAAAAeGRJkjpRCgCMqEiy1I0S0JNk/3g8ihcAACCO0nt/1vEAQCROlgIAAAAAP1hPkkqUAgCjO50sdao0PzepQEROlwIAALRXI0G6sgcFAETkZCkwBAsuAAAA2G9NjtZMki6s2wGAqE4lS2veQAGA06UAAAB11E6OXpMoBQAic7J0cm5WAQAAAKjF3hMAEJ1kKTAMCzAAAACIYVmjW6cDABkcTpZ6BC8ALXgULwAAQC6SpABAJk6WTsyNKwAAAAClOE0KAGR0KFnqVCkQlUXZmJwuBQAAiEuSFADIzMlSAAAAAGCXNUEqSQoAZLc7WepU6RjcyAIAAACwhwQpADAiJ0uB4Vi0jcmjeAEAANqTIAUARidZCgAAAACTu06KXr8u/xgAYFhv7+/bn6rrEbxjcKPLDIxX4/r09cvlp23++PybMQ8AAAAAgLucLAWG5EsBAAAAAADAK5uTpU5pARCBv7sUAAAAAIBSnCydjNN2AAAAAAAA8J1kKTAsXw4AAAAAAACe2ZQs9QheACLxKF4AAAAAAEpwsnQiTtkBAAAAAADAh5fJUqdKgcx8SWBcTpcCAAAAAHCWk6UAAAAAAADAlJ4mS50qHYfTdQAAAAAAAPAjJ0uB4fmywLg8ihcAAAAAgDMkSwEAAAAAAIApPUyWegTvOJyqA0bmdCkAAAAAAEe9vb/LiQIAAAAAAADz8RheAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFN6e39/v/z43F//3rZ/sYO3v1x+LK7kdT8qZ+TYblWzDnisRfvM5Gg8ol57tvotVd6W9XG0zC3L+Ejmso/kaD0sMrejlfYEAAAAQHZPk6VnN9B6O7qBl/26e7NxWleL9pmlDmvFovf116zjGteWqby1ylojrreylb1WeUs5ct21r6lWXVwb4Rpqqh2fs7LHFwAAACCiu8nS6BtFR23ZYBr12luzmVde67YZtQ5Hj0OL6yt1Ta3q4mx5W5VzUSq217KVv2V5S9hyzT2uqURdrHrVSclrqK1XjI7KFFsAAACA6H5KlmbbLDri3gbTDNfdg828Mnq2zyh12LuP1o5Dj+s7c02ty3ukrD1iujoT20XPsi+yxbuEe9cc4ZqO1MUqSp2cuYbaosToqMixBQAAAMjih2Rp9g2jvdYNptmuuzUbeedEaJ896zBa/6wRi57XeOR6epV3T1l7xvRapvjes7X8kcp8xnq9Ea9na10sotbHnmtoIWqc9ooWVwAAAIBs/p0sHWXDiJhs5B0TqV/2qMOo41LpWPS8zr3X0rtOXpW3d/ke2RLnrGWPWu5RPauPDHXxqj21Mlq7jRJXAAAAgIx+Wf5ntA0jILdlTFpc/hjOpXhFylfqfY7q/fl7PStv5Gt5VbboZV9c/viDR7+nnux1EaGcWWK1x4jXBAAAANDK38lSqM0m3n7RYtaqPNGu+5mzZY1yrVvLEaW890Qu2+peGZffLS5/DC1LOWdwWxfZ6mYp7+LyRwoRUwAAAIBjfrGxAkSRcTwyhrZ3G/NMdXBd1kzlXmUv/0jW+Geuhx5lzxwvAAAAAOpwspRmbFBuFzVWNcsV9Zq3yFz2raJeY8bYL2VeXP6YTuayj2aEutCeyhJPAAAAgP0kS4HuRtjc3XsN0a75WXmilXUVtVwzEHtKatWetFsAAAAA7pEsBboaafN667VkuuaoZc0UQ+A1fRoAAACAXt6+ffu2a3Pqf/2f/3v5qY3//s//uPzU35Fr31v+1vE940jdvP3l8iNPHNk0btE+FyXrsPbm+LOY1BxbHsXo7PXuqeMSdduqvDXrYq9HZY5Uxkcyl30UW9p85PqIUP5H42cJZ8e0LGrGEAAAAGBE4ZOle9TewDty7XvLFDm+t47E2wbeNkc2dFu0z0XJOiy9cX2m/5QcP+7FqMS17rm+s3V7try1y1rK0TbTs8yrbGU/0z9LqHHdJa4pY/u/VfIa7o2fpRwZ1zK225oxBAAAABjRUMnSW6U3II9c+94yjB5fG3jbtNrQ7VmHR67xntJ9ptS4scap13Uerdse5S0V8z1KtZvWZY/a3rcoXfa9Sl5rjWsZoS5KXcM6fpZ2ZHzL2G5rxQ8AAABgVEP/naXLBlfvTS7gZyUScrX6d6n3Xa5xcfljCqXKG3ncLd1uSr/fMzU+J3PZe6hZ3zXfe1X7M2qX/4xs4zEAAAAA7QydLF1F3rzjtWWDc6/Lf8qgWvTpSOOGMayMmnGs/d6Z338ELWNU63Nalv/sZ0WZx1vFDAAAAIC+pkiWLmx45fF3tvPK5de7XP7TH1z+EZ2drYuWfTnCuJFt7Ipa3hblqvEZLeOZra210iMuy2eW/Nxe13CGeRsAAACAVqZJli56bBay3bIxurj8sbjL29t8TSzjhv9MosaqZblKfpb23t8I8eh5DdoTAAAAABlMlSxd2LiL55LDbJbEbPlZlDPjhn+m8SpqWXuUq8Rn9oxnpnZXU4Q4LGU4U44o1wAAAAAAkU2XLF3YuIujV+JSwrSPo3GfbcN/+axM41TUsvYsV6b6u6dk+bPHIqsR4l5qri71PgAAAACMacpkKTH03ry0eUpE2RIcEmHliWl/0epgb3mylz8C/RAAAABgHtMmS22CsZAwjS9SX61dlmzjUuTyRijbkTJEimm29ljKrNddm7gCAAAAENXbt2/fdiWLRtrs+u///I/LT9scufYWn9HL3mtbvP1l+f9oScq1XFEciU+L9rk4GqujdX60Tzy7tjP97EjMXqnR72uUc1W6vJFi+qgsNdrhPTU+52x9na2f0u3lqK3Xcaa8rz6jRV3U/IxebensHN1qTq3hSMzOxgsAAABgNs2SpUc3yJ4psZG1p1xHPm/vdbf4jJ6WDbyjSbOaom0sttrYbbkJe7Tea17X0TGkZJ87WoZXao0LNcpbuqy16rV2e6n5/mfq7Wz91GgzR0Soh1XEuo5S/nvOztGt5tQajsTsbLwAAAAAZpM6Wbo4u5lVe3Nw73W3+Iyelg28o0mz2iJtLrba2G25CRvxmo6OHyX63NHP3qr0uFCzvBHKurUMWd97caYOj9ZR7Wsq7WiM9pa55ue0inmrWC3OzM9H5p7F3uvr2W5vnYkXAAAAwIzS/52lkTangL72jgc9xo9lA/5okqG1taxZyrvIVNbWMsyXGef0I2WudZ3afxniCAAAADCX9MnSRcbN1Rk56TCvoyd7RpIp6ZiprCXsmUOOzDdbYnkk3pGSdLRRo18ebRPa0gexAAAAAMhtiGQpeUiasdVMyTqITn8sT4INAAAAAGKQLKWJM6dKl036vS9iOVL/SyLh9gVQi7mDEZy53wIAAACYlWQpkMa9BOryWv8Z/fWsB8mubVrV0Qz1YdyJzdMsAAAAANhCspTqnHKgtlETFst1Zbq2jPVwpMxH/huJ3PjW/vbolcG9cq8vtsncV91vAQAAABwjWUpVNu7Gs5zUOeLyn4dyZFO8VdIhW3JDMoaz7rWhpY/ee/WwlO/2dcaR6zjzmbdlX18ZXKaR3S7/eVX3YnjbXtdXTe63AAAAAI6TLKUaG3dEVnvj+owsCYxFpoQL57ToM9eJpWeft/Xfgxa2tset/x4AAAAAbQ2RLLXhFFOrUx2w19Exo3ZSMFviUZKUks7M5ZJP7R2J94hjRpR2654LAAAA4LipT5ba6Id5rJvSpTamS8qYJDV+ElHUPn4rQxlpp1S7lTAFAAAAOCZ1sjTLpijQ3jo+lBgnaiUHsyUds5WXfnrPzb0/v4ZIfW/E+C60WwAAAIA5vX379m3Xt9CPbuSc2eSrtXm0t0xHytHiM1o5Uoe3f2/pkVMPLeK+iPR3rEaOUxYl+1KJOF2Xp8W4cKbMZ8q62FveUu2wZZxKf1b0sq+2fuaZzyipVNsq6WhsolxL9vI/M0q7jXQ/AwAAAJBBs5OlywbU0VcNGTbtgP5KjRXL+6yvDDKVdVFrriC3aO3iaHmi9EX9rA1xBgAAAGhryr+zNFMCAOgjW7KwJGNkHc8SIEdiLqGyTZQ4Za+vM+U3pux3Jt5Hnk4BAAAAMLPpkqU27Ihg2ch85vKv0YlxglFJsPYh0cgR+isAAABAG1MlS2045rTU295XT5d851OXf5VKzm4wL//9+oLI9rTRVu05Yr/pWaYRxpEz19B7Tt5qhHoCAAAA4JhpkqVZNuvI6ZID/dvlVwzCBjqtHJ2nljb6qp1ma8frF19Kzt09YnD2M3vfu2xpW8/0Ln9rkdqt+xEAAACA7aZIls62WRfZ218uPw7h7+zoXy5/ZFA9kiyw15rYWtvr7Z+je5Rouv797T+L7Gzce19rlnbT26O2ef37238GAAAAQCxDJ0ttUFGTJOlcMiWdyKvUnJWpre6dq8/EqFVcso8VJco/+v3XiO0WAAAAYFZv375925XwybBhU2uD7si17y1L5Pgeieu9k6QRk4x7T7xGvIba7bNF2yzVd2/Lev2+Ja6jVDm3ql23pe0tb4myHq3Xo59d+/NKtNMa9sRryzWcqfvWdb5VibqrXcZnepe/Rdt/Vr6R2u3eexsAAACAWQ1xsnTZQLp+EVv2zbuIiVJ+9GxMePR7iGSE9nnd125f6z8fjURp/npd2+i91/rPs3C/AgAAALDNEMnSFqcQGNee5K2Nx7Ec3fQ25nBEpiRLbSViEa3/SpSO38ZHbLcAAAAA/KPdY3ifbQ6V3ACquVF3pJx7y9PiMyJYEpQREo97EqWLyMnS2m2nZD99pFdbjt7vso0Le8tboqxH2+fRz25ZJ0evrYae7eqe1vX+SIk66hnbSOVv0d57t+OW7XbvfQ4AAADAjEKcLF02f9bXWS022ThPopRIjow9xppYSswfUdW4tpHj1VqkROMR2cufkXgBAAAAxBLuMbwlNpAkMXjFSQsgk1LJleV9RkrU9L4WidL+dQAAAAAAZ4X8O0tLbLxJmALss3yJ4NblHxHAMjeemR+v/1tz5HlnY3i2Ps+SKAUAAACA70ImSxclNuBsBvPI8kjdxeWP3S1tVXvNR53Rw5pk2zJP7vl32abEeN27PkqMXdoUAAAAAKMImyxdlNiIk8yghDOJ1XVj/dmL/mz8k9HSbp+9Sir9fhmVGK97x3GEawAAAACAkkInSxc25Kip5unSlolQCVeAuiRKv5fffRkAAAAAowmfLC1BIolnaiZM99JWX1tidO91Von3IJ8j9a6txJGpLkZIlAIAAADAiFIkS0ts0Nnc5oy3v1x+3MzG8jlLn733goxmGw/u9d17ryzOllWiNIfrtvnsdVaJ9wAAAACgnLdv377tOlV3dIMnSsLzTDmOfP7ez2vxGdz3KiF65ARqq/rs2S9rad0XIsewVSzu9YEW7b5kDFvEqnXbXN1+7tH3PFL+xavPq/W+z0St72sl6v6M7OWvLUu7XRz5zFf3NgAAAAAkewzv6Bt2AMxrSaA8e92697tatsy/I87RZ2PcOybZy99ClmucoS4AAAAAepni7yy91nJzGY7Y20aPtmkbrx/EcG576j/7HJK9/NdqX8vZ988+Phjf6mjZB50qBQAAANgmXbK0xObdSJvFRy0baNcuv4bU9G0iJngkzZ6L2G9HSJSeuYbsbbYF8w0AAADAOFL9naXXemxkHvnMvZ/T4jMWtwnS0f4+zhIxudUqRqst11D7/Xs7en17ri1DDHv2gRbtvnQca7WbqO1x6/tHLf+qxefUjlULW64he/lbG6Xd3np1XwMAAADAd9M9hncVeSORcZzZFF7a6Pq69uj3fNgSm7MxjLjhzznP2sPRttKinWxt7y2cHfO2qH0trWJFHCO0WwAAAACOS5ssLbEBbeOKLJa2ur5KyJLoO7uBfRu32z8flSV+syrRblYl2ksLaznvlf36d3u1buuPypvxWno5EyOOedQ219+fqZOj7dapUgAAAIDtUp8slbCgpaMbj9ppDGc2q0uxeZ3H2QTH4kjfLzFelCj74mhZSl5D62sp8VnkFLHdAgAAANDGtI/hXdnMYjYlNoRbilbebPGblXpiRu5pzjHfAAAAAMwpfbK0xEaSzUVqi7LhmXXjdYT4OVXaXs92c+azI7T3s2WIcA2rrWVxL0AkZ/qQ+QYAAABgnyFOlkbalGVsZzYgtdNzesdP/TGLUm09Qp+Zqd9K9pah3QIAAADMZ5jH8J7dWLLJyMiW/jHC5muvazj7uU759NOjzZT4zKxt/Vav61j0/Gxyy9xuzTcAAAAA+03/d5bCXmc2IntswJb4zEibr61j2HPTnDKWOmxRj6U/p3Xbq/V5ra9j0eMzGYt2CwAAADCPoZKlZzeZnC6lhaWdttoQLfE5a6J0+f9S/n7jEzLGj/5atZuSWpR5+Yzan9PiMxZHP8f8H9vfE8dBl7c4pFW7XZT4nLPXCwAAADCr4U6Wnt1ssmHKFiU2JGtuwLbc4O2l5jXOEL9Z1ajX2u2l5vvXLPc9teMENdRutyXeX6IUAAAA4DiP4YWOSm2Srkq/X63N15LvW/Kas8SPc0rVc+n28krJz2td9mulP7vntUThi16PlRqH13ZWqq1ptwAAAABxvH379u398jMTud08fP/L5cdpHdlQrRW3rRvfNTdaj8Rjr97xW9SK4aP41brmEu6VOXJ5S4rQ547IWu57IvRbYno0nu5RayyLPN8AAAAAsI1k6aRuN9ZmSYg8c3SzccTYHY3FEbPFL/L13iv3iPUD5PJsTN1q1LGsRGwAAAAAZhf6Mbw2gMhAOz1ntPhlvZ5s5c4aZ6CPEccM4yAAAABAGWGTpesGkI2g8u7FVJxZaQvHbYmd+DIC7ZhWSra1kdrtSNcCAAAA0NsvNlvg/KbjKP2o13Usn7u4/DGl7OV/JOJ1rWWKWLYZiD+ZjdBu9T0AAACAskKeLL3dBLIpVI5Y1pM9ttrGcXtjFy3W0cqzx1L2xeWPKVyKnDLmt+XOeh3MLXO71ecAAAAAyvs7WZph48XmUH2zxrjkdWeM4VLmxeWPXV2KkiqG2cp7RKRrfFSWSGV8ZCnj4vLHdG3nUXmzXQd51Gxby3svLn9MIVt5AQAAALJ4e39/v/z4j3/89fPHHzp5thEUoXyZbd1kmynOW2NyRIY41rz+s2aIX4Rr3HMNvcu7pawRYnrPq7JHLfdiS9xXka+DXPa0u7Oit9uWsQAAAACYUahk6ZbNoOgbWlHt3WibIc57Y3JE1Di2uPZSIsawZPx6Xt/e68hS1p7lvGdr2bOW+1a06yCfo23vjKjttkcsAAAAAGbzQ7J01WPDaO9mUNRNrYiObrSNHOOjMTkqUixbX3spEWJYK3Y9ru3MtbQu79Gy9ojrtazlXhwt+7UI17FYriVKWXitRNs7I1K7vfwIAAAAQGV3k6WLVptFZzaDomxoRXZ2s61lO2j5WZcfm2t1jff0vO6SesSwRexaXVepa8lU3lZlXWWL8bVSZb/W4zoWt9fSqxxsV6P9HRWl3QIAAABQ38Nk6arWZlHJzaBeG1pHrddes9wl47uoVdZ75Wz5WT3Vus5r0a65pFHjV+u6al2L8n7IWOZFrXLfE+FaapeB/Vq2wSNatJnoMQAAAAAY2ctkKVBfqY3YmTdbS8TQZvVczraZHu2lRDtfRGjrI10L89BuAQAAAEbzj3/8f7eaU7kZ2K3YAAAAAElFTkSuQmCC',
                        width: 150
                        },

                        {
                          alignment: 'right',
                          fontSize: 10,
                          text: 'Departamento de Transporte y Servicios',
                          bold: true
                        }
                      ],
                      margin:  [ 21, 34, 25, 34 ]
                    }
                  });

                  var now = new Date();
                  var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear()+' '+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();

                  doc['footer']=(function(page, pages) {
                    return {
                      columns: [
                        {
                          alignment: 'left',
                          text: ['Fecha de creación: ', { text: jsDate.toString() }]
                        },
                        {
                          alignment: 'right',
                          text: ['pagina ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                        }
                      ],
                      margin: 20
                    }
                  });
                },
                exportOptions: {
                  columns: [1,2,3,4,5],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                },
                
            }*/
        ],
});

$('#articulos tbody').on('click', 'tr', function () {
var data = tabla_articulos.row(this).data().id;
window.location = ruta+'/articulos/'+data+'/edit';
} );

$('#actualizar-articulo').click(function(){
  tabla_articulos.ajax.reload();
})

// --------------------------------------------------NI IDEA----------------------------------------------------------------------

function Articulo(){
  this.id = $('#id_articulo').val();
  this.nro_activo = $('#nro_activo-articulo').val();
  this.nombre = $('#nombre-articulo').val();
  this.categoria = $('#categoria-articulo').val();
  this.descripcion = $('#descripcion-articulo').val();
  this.fabricante = $('#fabricante-articulo').val();
  this.unidad = $('#unidad-articulo').val();
  this.cantidad = $('#cantidad').val();
  this.lowstock = $('#lowstock-articulo').val();
  this.estado = $('#estado-articulo').val();
}


function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 


// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStylearticulo(){
  $('#field-nro_activo-articulo').removeClass("has-error");
  $('#field-nro_activo-articulo .msj-error').html("");

  $('#field-nombre-articulo').removeClass("has-error");
  $('#field-nombre-articulo .msj-error').html("");

  $('#field-fabricante-articulo').removeClass("has-error");
  $('#field-fabricante-articulo .msj-error').html("");

  $('#field-categoria-articulo').removeClass("has-error");
  $('#field-categoria-articulo .msj-error').html("");

  $('#field-unidad-articulo').removeClass("has-error");
  $('#field-unidad-articulo .msj-error').html("");

  $('#field-cantidad-articulo').removeClass("has-error");
  $('#field-cantidad-articulo .msj-error').html("");

  $('#field-lowstock-articulo').removeClass("has-error");
  $('#field-lowstock-articulo .msj-error').html("");

  $('#field-descripcion-articulo').removeClass("has-error");
  $('#field-descripcion-articulo .msj-error').html("");
}

// --------------------------------------------------GUARDAR NUEVO O ACTUALIZACION DE EMPLEADO----------------------------------------------------------------------

$('#guardar-articulo').click(function(){
  var type = 'POST';
  var route = RUTA_ARTICULOS
  var btn = this
  starLoad(btn)
  var data = new Articulo();

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStylearticulo();
      if(data.id == "" || data.id == null || data.id == undefined){
        $('#modal-articulo').modal('hide');
      }
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylearticulo()
          if(jqXHR.responseJSON.nro_activo){         
            $('#field-nro_activo-articulo').addClass("has-error");
            $('#field-nro_activo-articulo .msj-error').html(jqXHR.responseJSON.nro_activo);
          }          
          if(jqXHR.responseJSON.nombre){         
            $('#field-nombre-articulo').addClass("has-error");
            $('#field-nombre-articulo .msj-error').html(jqXHR.responseJSON.nombre);
          }
          if(jqXHR.responseJSON.categoria){         
            $('#field-categoria-articulo').addClass("has-error");
            $('#field-categoria-articulo .msj-error').html(jqXHR.responseJSON.categoria);
          }
          if(jqXHR.responseJSON.descripcion){         
            $('#field-descripcion-articulo').addClass("has-error");
            $('#field-descripcion-articulo .msj-error').html(jqXHR.responseJSON.descripcion);
          }
          if(jqXHR.responseJSON.fabricante){         
            $('#field-fabricante-articulo').addClass("has-error");
            $('#field-fabricante-articulo .msj-error').html(jqXHR.responseJSON.fabricante);
          }
          if(jqXHR.responseJSON.unidad){         
             $('#field-unidad-articulo').addClass("has-error");
            $('#field-unidad-articulo .msj-error').html(jqXHR.responseJSON.unidad);
          }
          if(jqXHR.responseJSON.cantidad){         
             $('#field-cantidad-articulo').addClass("has-error");
            $('#field-cantidad-articulo .msj-error').html(jqXHR.responseJSON.cantidad);
          }
          if(jqXHR.responseJSON.lowstock){         
             $('#field-lowstock-articulo').addClass("has-error");
            $('#field-lowstock-articulo .msj-error').html(jqXHR.responseJSON.lowstock);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

// -------------------------------------------------- NI IDEA DE MODALS----------------------------------------------------------------------

$('#modal-articulo').on('hidden.bs.modal', function (e) 
{
  tabla_articulos.ajax.reload();
  $('#form-articulo')[0].reset();
  $('#id_articulo').val('');
  removeStylearticulo();
});

$('.select2').select2({
  width:'100%',
  placeholder: "Seleccione",
  tags:true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true }
  },
  templateResult: function (data) {
    var $result = $("<span></span>");
    $result.text(data.text);
    if (data.newOption) {
      $result.append(" <em>(Nuevo)</em>");
    }
    return $result;
  },
  language: "es"
})
// -------------------------------------------------- movimientos ----------------------------------------------------------------------

var movimientos = ruta+'/movimientos';

// ---------------------------------------------------INDEX----------------------------------------------------------------------

var tabla_movimientos = $("#movimientos").DataTable({
  dom: 
  "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
  "<'row'<'col-sm-12'tr>>" +
  "<'row'<'col-sm-5'i><'col-sm-7'p>>",

  processing: true,
  serverSide: true,
  ajax: movimientos+'/listar',
  columns: [
 
  { data: 'articulo.nro_activo', name: 'articulo.nro_activo','className': 'text-center',},  
  { data: 'articulo.nombre', name: 'articulo.nombre','className': 'text-center',},
  { data: 'tipo', name: 'tipo', "visible": false,'className': 'text-center',},  
  { data: 'cantidad', name: 'cantidad', 'className': 'text-center','className': 'text-center', 
    render: function(data, type, full, meta){
      if(full.tipo == 'ENTRADA' ){
        var text = '<span class="label label-info"><i class="fa fa-plus"></i></span> ' +data;
      }
      else {
        var text = '<span class="label label-danger"><i class="fa fa-minus"></i></span> '+data;
      }
      return text;
    }
  },
  { data: 'fecha', name: 'fecha','className': 'text-center',},
  { data: 'creador.nombre', name: 'creador.nombre'},
  { data: 'razon', name: 'razon','className': 'text-center',
    render: function(data, type, full, meta){
      if (full.razon.length > 160) {
        var text= '<p style="font-size: 12px">'+full.razon+'</p>';
      }
      else{
        var text = '<p align="justify" >'+data+'</p>';
      }      
      return '<div style="width: 200px; word-break: break-all;">'+text+'</div>';
    }
  },
  { data: 'equipo', name: 'equipo','className': 'text-center',
  render: function(data, type, full){
      if(full.activo != null){
        text='<a href="'+ruta+'/activos/'+full.activo.id+'" >'+data+'</a>' ;
        return text;
      }else{
        return "";
      }
    }
  },
  { data: 'orden_id', name: 'orden_id','className': 'text-center',
  render: function(data, type, full){
      if(full.orden != null){
        var text= '<a href="'+ruta+'/ordenes/'+full.orden.id+'" >'+data+'</a>'
        return text
      }else{
        return "";
      }
    }
  },
  { data: 'id', name: 'id', "visible": false},
  
  ],
  
  createdRow: function ( row, full, index ) {
    if (full.tipo == 'salida' ) {
     $(row).addClass('danger')
    }
    if (full.tipo == 'entrada' ) {
      $(row).addClass('info')
    }
 },

  order: [[ 9 , "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
  buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0,1,2,3,4,5,6,7],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                } 
            },
          /*
            {   

                extend: 'pdfHtml5',
                filename: 'Lista de Movimientos de Articulos',
                title: 'Lista de Movimientos de Articulos',
                pageSize: 'LETTER',
                orientation: 'landscape',
                customize: function (doc) {
                  doc.pageMargins = [50,85,50,50];//[izq,arriba,derecha,abajo]
                  doc.content[1].alignment = 'center';
                  doc.content[1].table.widths = [71,75,35,41,55,200,100,69];

                  doc.styles['tableHeader'] = {
                      bold: true,
                      fontSize: 10,
                      color: 'white',
                      fillColor: '#01579B',
                      //alignment: 'center'
                  };

                  var objLayout = {};
                  // Horizontal line thickness
                  objLayout['hLineWidth'] = function(i) { return .5; };
                  // Vertikal line thickness
                  objLayout['vLineWidth'] = function(i) { return .5; };
                  // Horizontal line color
                  objLayout['hLineColor'] = function(i) { return '#aaa'; };
                  // Vertical line color
                  objLayout['vLineColor'] = function(i) { return '#aaa'; };
                  // Left padding of the cell
                  objLayout['paddingLeft'] = function(i) { return 4; };
                  // Right padding of the cell
                  objLayout['paddingRight'] = function(i) { return 4; };

                  objLayout['paddingTop'] = function(i) { return 10; };

                  objLayout['paddingBottom'] = function(i) { return 10; };
                  
                  // Inject the object in the document
                  doc.content[1].layout = objLayout;


                  
                  doc['header']=(function() {
                    return {
                      columns: [
                        {
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB0sAAAHkCAYAAABfSMfmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAuIwAALiMAcz2uy8AAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxNzowODoyMiAwODo1ODo0NwlViroAAEOZSURBVHhe7d1JkhxHliDQDEqfAIvuU6UIV0Xuu49V2KN6BZE+VXHBE6Qgmka4ZTgcPtigw/+q74l4ZiBIuqt9nUz1uxre3t/f/wE9/Y//96YRTuDT1y+XnwBy+ePzb2+XHwEAAACAwfxy+X8AAG5IlAIAAADA2CRL6cqp0nn8+evvl58AAAAAAABikCwFALjDqVIAAAAAGJ9kKd04VQoAAAAAAEBPkqVAMx7FC2ThVCkAAAAAzEGylC6cKgUAAAAAAKA3yVKgKadLgeicKgUAAACAeUiWAgAAAAAAAFOSLKU5j+DF6VIgKqdKAQAAAGAukqUAAAAAAADAlCRLacqpUgCicqoUAAAAAOYjWQp04VG8AAAAAABAb5KlNONUKQBROVUKAAAAAHOSLAW6cboUiECiFAAAAADmJVkKAAAAAAAATEmylCY8gheAiJwqBQAAAIC5SZYCXXkULwAAAAAA0ItkKdU5VQpARE6VAgAAAACSpUB3TpcCAAAAAAA9SJYCANNxqhQAAAAAWEiWUpVH8LKV06UAAAAAAEBrkqUAwFScKgUAAAAAVpKlVONUKQAAAAAAAJFJlgJheBQvUJtTpQAAAADANclSqnCqFAAAAAAAgOgkS4FQnC4FanGqFAAAAAC4JVkKAAAAAAAATEmylOI8gheAaJwqBQAAAADukSwFwvEoXgAAAAAAoAXJUopyqhSAaJwqBQAAAAAekSwFQnK6FChBohQAAAAAeEaylGKcKgUAAAAAACATyVIAYEhOlQIAAAAAr0iWAmF5FC8AAAAAAFCTZClFeAQvAJE4VQoAAAAAbCFZCoTmdCkAAAAAAFCLZCmnOVUKQCROlQIAAAAAW0mWAuE5XQoAAAAAANTw9v7uUCDnOFlKC5++frn8BPCYU6UAADC+//m//6vKXpT1BAAjMV9uJ1nKKRKltCJZCmxhcwMAAMZQa4P3DOsNACIxV5YjWcopkqW0JGEKPGPjAgAAcoq42buVdQgALZgr65Is5TCJUlqTLAWesUlBloWDtvoh82LvntHrdrT6WuiPPzKOxjZiH+TDbO165PZsbnnMPNNOxFjrGz/TJ3hlxPkyanv65fL/AOH9+evvl58AfuTGHYhg5I1fADhrmSfX1+VXQ5rlOgEo73oOGXUeiXp9kqUc4lQpABBJpkXEqAseIDfjKFDD0l/X1+VXU5n9+rNSX7SQqZ3pE/XNOles1x3h2iVLAYDUnCoFqG/GhTsAx0Ta+IxETHJRV0Bt67xgvPmudywkS4FUPIoXgFsZFxYWQ+NSt2RkHAVKWPqlvvnaGiexgnll7P/GrHLMAc/1io9kKbt5BC8AUThVCgAAfdn0PU7sYlM3QEnG/H1ax0uyFEjH6VIAgHYs6AG4x6ZvOWIJMDZj/HGt5kjJUnZxqhSAKJwqZZF5wWGxNC51SybGUWCvpe/pf3WIazzqhNIytyn9Yb8lZuJWRu04SpYCAOlIlAIAQHs2fOuzsR6P+gCOMHaUV3OOlCxlM6dKicSjeAEYYeFh8QT0ZBwFtlr6mv7WlpjDeEbo08al14zf9dWIr2QpAJCKU6VABqMsji3yATAX9CX+MagHYAtjRTulYy1ZCqTldCkAAADUsWxC2vSNQT3EoB6AZ4wR7ZWMuWQpm3gELwAROFXKaqRFiAUV0INxFHhGv4pnqRP1Anm59xqbmPRTKvaSpUBqTpcCAFFlXzBb8APMyfgfm/rpS/yBW8aF/krUgWQpLzlVCkAETpWyGnEhYnEFtGQcBR7Rl3JQT32JP3u59xqXOMRxti4kSwEAAABgYssGow3fXNQXQF/G4XjO1IlkKZCeR/HC+JwqZWUxAm3oa+NSt8At40Je6q4fsWcrbWVM6jWuo3UjWcpTHsELANCGxdaY1Cu0o7/BfvpNfuqwH7FndvoAUR1pm5KlwBCcLoVxOVUKAADl2eQeh7oEaMeYOybJUh5yqhQAiGSGBYlFFxFoh+MyjgIrfWU86rQPcecZ917j0efz2FtXkqUAQFhOlQIjsKAGAFpwz9GHuAPkJ1nKXU6VkpFH8QKMa6YNCJstQA3GUWClj4xN/UIM7r3GY3zNZ0+dSZYCACE5VQrQng0AgLEZ5+egntsTcyCyZY/tyOvyn09BshQYitOlAEBENtAA6M1cNBf13Z6Yw7iy9e9SSc9S79PT1rp7e383hvMjj+Alu09fv1x+ArLKegNGHbNuOszQD2ar2wx1qk7GZBwdw6z1OIva7XX09nMmfiPHJto4OMM4FiXmEWM92rz8zKxz9sh1nKFOW8c/UzvfEhsnSwGAUGZaQAEAAPss64Xb1+UfHVLyvaKZNWHTk5gDrfWavzLNm1vGZidL+YFTpYzC6VLIa7QNCs6ZfbNh9P4wY/1GrlP1MSbjqPuKWWRv6zXb6gjjQK++PMoYGmUsnGVOihDviLGeZU6epZ0/MmI9R65T4/t2r2LlZCkAEMYsiycAAMrJvjFd8x54hNj0XCOsn9+zDOQze7IMaCPS3DTCXClZCgzpz19/v/wEQFY2GQDOMY4yA4nSMUXcdM28EWw+aE/M56TeaSXqfBR5nnzVPyVL+TeP4AWgp6wbD1CTxfZ4otaptsaotO2xZa/f2ve/WeMTfV2Qdd1iPARaMNbUZ/+sDslSYFhOlwIAAIxJonQ8S0yyxCVTWelH0gjy04/3yzo/SpbyN6dKAejJRgO3LEg+iAVwhLHjg1iMJ3udtrj3zRajrOuBbOU2HrYn5vNQ1x/Eop4s807GeV2yFAAAYGI2M4BMso9ZWZOCNWWPiToFgBye3UdKluJUKUPzKF6Iz+YCtyRuaK31ODR7Gzfu12ccZVQSpdtkitMoc0Km6zBHtCfm41PH8LNsc7xkKQAA3Ih2U2/xDWRjHKW07HU4SlKwpNFioo55xjxEbe694Jy393dtdnZOljKDf/3zvesNgxuEfixYIZ8IY+YydkQbu0ccz6LEuEd9R6nPHnUQqX+N2K8WEeIbqZ5Xo9b3DKK1pb1atr0ssRq5P6qDx7L35bNmHwv0+7qW+Ear9xHqXEzPyTQWOVk6OYlSACCSSDfS0RYhERcZHDdrfY6wYRKdcfQx42hO2evNuPez0WOiznnEPDQm916PafPlZYtppjlRshQAAK7Y4JqTeody9CdKkSgFRiN5RA3mm7kYR+qQLJ2YU6XMRHsHiM8NP7Slz41HnTKS7O25x8Z1hpjNsqGf4TrNGXCefkQv2l55/s7SiUkeMZuef2+pCawf366DPKKMlbfjRrQxfKRxLVqd9yhPz/rseb1R+3t2UeMapVyr0ep9RNHazF692lj0uM3W9zK049Z1kr1vl1Q79hFjPeIYECXOt7GNVv+Z6z76uDViv+rFydJJSZQyI+0eIK6oi0zmov7rEt+6jKOMIvqm5Cu9+kD2uI3IeMgz+mx+7r2IwFhSjmQpAAA8EW3xaTEEZGMcZavsdWPD+rFZYxP9uo2HMCb3XnNZ4ivG50mWAgAAdNJrUWsxDUSTfVySKAX2cj8G8WWa35cxxbhynGTphDyKlJlp/wDxRLmZt8kJ9ehfdRlHyS77xp62DxwlsZGTey8iW9qnsWU/yVIAAHgh2iLUwqceGw5Qh3GUR7LXRYS2HT2Gs8+t0a/feAhjcu/FEnNx306ydDJO1YF+ABCJG3do3w/0u7GoTzLL3n5nTwICZZjLc1FfZLO02fV1+RV3SJYCADC9jJudFjpkIZkwB+Moe2WPv7FtG3H6ThyA0tx7tTPSGL7Uwfq6/IoLyVJgSk6XArCXTa55qGuoQ99iJVEK8COJC2owX/GMxOmPJEsnIjkEAEQS5YY88wLSooa9tJmxGEfP0yfayx7zaO1dG6aEGdtRxLlLf44vSh2596K0pV7W1+VX05EshQb+9c/3tBMYAPAh86KU2GwaMAvj6Nyyj3XaLwDZmLvKmCmO14nTmdapkqWTcKq0H4nSuPQLgH5muuGGmd3bVND/yxBHssneZm027ydmPxKPeCLWifk9LnXDrJa2v74uvxqSZCkAANMaYdPKor2OETc0tRVqMI6yRfYYS3LBuCRMyca9Vz/uB77X3fXr8ushSJZOwOm5fq5PlTphGpP+AdBe9htqCyRqGW2xST3GUTLRXgHIzlzGSix/tPSN69fl1ylJlgIAQHLZFyWMyUYCmRhH68geV+MYzCFiXzcvMTptfExLva6vy6/SkCwFAGBKNkB5ZaQ2YjOCGoyjPJN93NG+AYjG3BSDetgmW+JUsnRwHjHaz73H7noUb0z6CUA7oyRsLI6oZZQ+Qj3GUTLI3k6ztM/IcdbH85ntHuT2eiO22dnqJKpR6sG4XJZ47rP0o+vX5dehSJYCAMAARlnEA/RiHC0jexxtflKT9hWbhCm05Z5hXkvdr6/Lr7qTLB2Y03IxOV0ak/4CUF+km+ASixoLo3FkX6RHpH/UYRwluuzjqTYJwDX3XlDf0s/W1+VXXUiWQgUSogBAD9k3qSPqsaFQuh61C9hOfzkue+xsIAOLiGPBkfHVmEYW7h+4trSH9XX5VTOSpYNySg72028A5mAxA3COcZRbNjoB6so+znKOeSo29VNH68SpZCl04OQpALMZdXFvUQQ/0y/qMI4SVfa2qQ0Ct4wLLNx7sccSV7GtZ+mPtfukZCkUJhEKAPQ06qK+px6L3lL1qD3AfvrNdtljZVMTeCTi+GB+YlQjtW33FnUtbaVWe5EsHZBHieYgqRqT/gNQXqSFj4ULkJFxlIiyb2xqywA84t6LM9RZfUsfLd1PJUuhIAlQAJhPxIVQ9g1sYC7G0Xyyx8cmJrCF+YlRadv1LTF2v1Hf0m5KtR3J0sE4FQfn6UcA5VjMQz89+p8NgfKMo0STvU0ap4A9JJXmI76U5L6jjaXfnu27kqXQkZOoANBOzUWKBdD4etSxjRqiMY6SfVzSzgDIxL3XGJZYi3cbZ+5VJUuhEIlPACASiTaAc4yjP5IoBWYVcfwwRzGi0du1pGkbR9uRZOlAPDoUytGfAM6zgId+evQ/C//yjKNEkb0tGp8A2MK9Fy24L6lv6ct7+7NkKXTmRCoA1NdiMRJxwWOxX1aPOlaHRGEcnVf2GERsV0A+5ihac+81tiX26+vyKyrY054kSwfhFFxfEp5j0q8AjrNwBzjHOEoE2duhDUigJGPK2Nx70YukaV1b+7ZkKQQg2QoA1GLRPx91DmXN2qeyX7dNR+CVEcZ3932MaNZ2LWnal2TpAJx+AwB4rOViw8KGGjJsFmj7YzOOzkeiFOC+iOOLhOl43HvNbamT9XX5FSdtGSclS+Ekp0LH5ssIAPtZrDMyC1ZaMI7Sk0QpALNx70VUEqflvOrnkqUQhKQrAJTXY0ERcRFj8Q8cZRz9bpZxVKI0l8jX697jvshxma3/nGGeoqYe7UubzmGpp/V1+RU7PWtXkqXJOfXWlwTnHPQzgO0saKCvHn3QYr0s4yi9ZG97xiKgJWPOONx7kdGaNDUWlSNZCgAAE7AJUE6PBan6g/5G7ocSpQD5uV9kNNr0NteJU/dErz1qV5KliTntNh4nVePS3wBei7aQ6blIsEABjjCOfjCOtpN9I1JbAXox/uTn3uuD9jyOpS7X1+VX3LjX9yVL4SCJTQAAIsme8ADakygFOCfaOOR+ELh2nTh13/Tc2/u78TMjp9z6q5ksVb8xnalzN6v9uBGAdox1OUQaFyO1mb1x6VX2Z+XsUaYj7SlzvddmHM0hWrs5Knt7G6Uezopej+rpg7qKGYMS1x31uqKVK+J4EL1f8p25pJ7Z+8B123KyFGAjSWyAxywymY0FO6UZR2kpe3szBgORGJNycu8F38ev69fl11OSLIUDPIIXAMjKpgDAOdnHUYnSsYgHJWhH50WL4TLWq1dGYQ3bzjJuXL8uv56CZGlCTreNTzI2Lv0P4GcWLtBfj35oA64c4yitZG9rxh0ASnDvBduMnji9HgskSwEAAJKwsQMcJVFKD+at78RhDtHGKe0OKOk6cTrifZlkaTJOtfXn1CcAkJ2NkzJGXCAC22QbR7OP+8ZbIAvjFdRhDRvPmjQdZdyTLIWgJGXj8qUFgA8WLADnGEepTaJ0fNFjNPs4F/369TFm494LyluTppnnlLf3d2NDJpI0fbVOYKrvuPa2BTdi/Vj4QV3Gt9x6j5GR2s/RWPS4htuyRijDHiPUe0nG0dwitKFnsrev6PGNJHpdz1yX6uZDxFjUun7z+8+ijAPqJrcW7ShaG8k8h2bpb0uMnSxNROJsPk6XxqU/AlhkwqLHwlXfG4e6pKbs7SvzxiCAMSwm917QVqaxULIUAAAAYCASpUQza4JCYgaA2WW5r5MsTcIptv6c8gSADzZ+xqAeOUISowz9bwwR6zF72zLGHCNu8WToi9pNXeIbi3uvMajHnDKMh5KlEJwkbVy+xAAA9GazALgmUUpk5ixmZFwD+C76eChZmoCEDAAAtdi4PK/Hok+9QRxR+qNEKRliOMv8leE69Tkgq9pjbLTxcZa5szfJUtjA6U4e8WUGYEZu1GFeNlbLMI5SWvY2ZWwBRmaM68+9F8QQeTyULIUEJGsBgJpsHuSk3iCOnv1RopRsRp+/zM/cY6yDsoy1lCZZGpxTaxCffgrMxIIE7rMBxlbGUUrK3p6MneVliemoY2GW69L3mIl7L0agHdcnWQovRDnV6XQpAAAAK4lSshtt49dGNq8Y9wDikiwNzGk1AABascHHKzb44LmW46hEKc9kiu8o9x+ZrkP/60v8oZyaY6++Oh/JUnjCaU628uUGYAajbGYB9GIcpYTs7cjmI7eyt2ljO8SlfzIS7bkuydKgJF64R/IWAKjJ4uscCQCg9jiafZw2TraTLdZZ23a2cuuDMagHKGe2Nexs19vCOiZLlgIU4ksOwMjckMPcbOqdZxzlrOxtyDjCK0sbz9LOM5WVmIyJ9emjEE/kfilZCg84xQkAAEAE2Td8JQX6yBr36O09a3/UDwH2iTpu+iJAHZKlATmdxjOSuAC05kZ8Lur7HBuR3KNfzaV0fWdvP8ZFjljafbS2H7FMW+mHMamXerLPnewzY31nvOboZZYsBSjIlx0AAIBSsm/+SQT0l70Olj7Qsx+sn5+9LxKXcRI4KtPcFLWs12Pw2/u7uT4SiZYYMpze1Fbiutd+LKz6sfCA84xhc2oxfkZqWyWvd7Q+U7otjFrvzxhH51SifWk786g9Ho3UlsRqn1Zz3RYRYxshPrOM9S1jbf6cU602lqE9RRrrb0WO33XcnCwFKEwiGxiJRSbAOcZRgLKWcfX2dflHh5R8r2gib57zQT2VNVo/hi0izmHZ5lXJUgAAgIJseAEQzehz07ohe+R1eQvoyv0jxJWpf0aY27LMr7f16jG8gTiNFkOGR/CutJm4btuRBVg/FhxwXOSxa4S+nXHxUFqkGJS+1lHm/hptYOR6v2UcrWuGcXSUsYTXWvVJbWoeEcf5iO0vUpxG758tYh05hhH75F4Z2mitOGfun7P3vUdu4+JkKUAFEtkA9YywyFyMch1APsZRmJd+Mwf1nJN6G5d7r/wyX/uSyLx+XX592O37lXjP1u7Vp2RpEBIrMWQ6VbrIVl4AIJeMi54obHYBC+MoEZmjxqZ+c1N/cI57r9fWBOfR1+VthiNZClCJL0EAmUW9AR5t88BmCIzLONqGcRTggzGRmbn3amPmccYYO4ZH9ShZGoCECgAAEI3NAIBxGePHo07HoS4B2pMshYusj7T1KN7YfBkCyGjkx6qwn/YA++k3XNMeiEpCZhzqcjzqdB9zLddqtgd9M7dn9SdZCgBACqMuSiy2xqVuicY4CtzSf/JThxCXe68xGXdzelVvkqWdOXUGAETiG7nco13MxwbAcfoL92gXRGbMz0vdjU39bmOO5Z7a7UL/zGVLfUmWwl+yP8rWo3hj86UIAF6x0AI4xzgK5yx9SD/KRX3NQT0TlbbJaCRLO5JAAQDYxkKMrLRdotAWgS2MFTmoJ4hPPx2fOs5haz1JlsIgnC6N7c9ff7/8BBCXxxfxjPYBr+knPKN9kIXN39jUz3zU+WPmVp5p0T70z9j21I9kaSdOlcYhyQgARGCRRRTaIllpu1CO/hTPUifqZV7qnoi0y+/EIZ4jc6ZkKQAAob+Ra+ERh29uw2PGUbYwjpLJkY1G6lAP8DP3XmzRqp2o8ziO1oVkKVMb7VSpU7KxeRQvAK9YYI1JvUI7+huUp1/1Jf6stAUi0i4/iEV/Z+pAsrQDj+AFANjGYiMep6LGps+NR53GYxwlo2UsMZ60Jebco03Ep47iaXnvZezu52zcJUsBGnK6FIjIpi3AOcZRYBY2gOuz0Q6vufciOuN4O6XmTcnSxpwqjWPUR9Z6FC8A5GZRNSb1Cu3ob1BXqU1JfiaubKGdEI02eZ/5sq7S8ZUsBQCYWORv5FpUxOWb3PDBOMoRxlFGUHqTcmZiyV4ztxf3XhzRs91oF2XVmjMlS2FATpfG5lG8ALxiMUUP2h0j0Z6hnVqbljMQO2AUxrLnjPfn1Y6hZGlDHsEbh2QiADjVwjnaD+gHnKP9MBobwduscRIrzpqxDZk7OSNC+zH+79cqZpKlAB04XQrwnMUDI9KuaUl7A3pZNzWNQz8SE2rQpuJQF+xhTnitdYwkSxtxqpTWnJ4FgNwsnGhJe2NE2jX0t250ztofZ79+YC7Guv3MEz/qGQ/JUqYjiUgUTpcCPUV+fJFFQh4eg8XMjKOUYBxlJj03QFua5TqJY5a25t6LEqK3o9nmj+tr7n3db+/v7strc6o0lpmSpdpefJ++frn8RC0z3WAAAABklPmLA9acANQ2yhfsIs+ZkqUNSFjFMeOpUu0vNsnS+ixcAQAA8om4MWx9CUAU0ROo2eZMydIGJKvikCwlIgnTuixmAQAAxlJrg9j6EYAR1E6kjjhfSpZWJlEVy6x/X6l2GJtkaV0WuwAAAAAAPPLL5f9heLMmSonvz19/v/wEAAAAAAC0JFlakdN8AAAAAAAAEJdkKUzAqVoAAAAAAICfSZZW4lRpLJKFROdRvAAAAAAA0J5kKQAAAAAAADAlyVKYhNO18TldCgAAAAAAbUmWVuARvLFIEgIAAAAAAHCPZClMROIYAAAAAADgg2RpYU6VxiI5SDYexQsAAAAAAO1IlgIAAAAAAABTkiyFyThtG5/TpQAAAAAA0IZkaUEewRuLpCAAAAAAAADPSJYCBOR0KQAAAAAA1CdZWohTpWTi1C0AAAAAAIBkKYOSDAQAAAAAAOAVydICnColIwnl+DyKFwAAAAAA6pIsBQAAAAAAAKYkWcpwnJhkJE6XAgAAAABAPZKlJ3kEL5lJLAMAAAAAADOTLGUokn8AAAAAAABsJVl6glOljECCOT6P4gUAAAAAgDokSwEAAAAAAIApvb2/Oxx5lJOlsTgheZy2nMOnr18uP7HHH59/MzYAAKnVuF+3fgKAD+ZagLlJlh4kuRSPG5BztOn4JEuPkSwFAKKLdi9ubQXAaMy1ADwjWXqQxFI8bjLO0aZzkDDdT7IUAIgm47239RYAmZhrAdhDsvQASaV43Eycp13nIFm6n2QpPGbsn0vP+yVtLT/32+eM2Ae0iW2i1v1o9SfOgLkWcsvSh/XLcf1y+X9gcgZ6AAAoY9nsuX5dfj2U0a8PgNiu56FR56LRrw8gEsnSnUxOQE9//vr75ScAAKKZdUPTRi4ArZhrYTyZ2rZ+OC7JUtJzIrIcsQQAgH3WzUsbJ2IBQB3mlw9iAVCHZClAMk6XAgD0Z6PyOfEB4CxzyXPiwwgytmH9bkySpTvoBPE4CQkAALRkY3If8QJgL3PHPuIFcJ5kKfADCWgAAPiZjchzxA+AV8wV54gfwHGSpRuZaIBIPIoXAKANG49liSUAt8y1ZYklWWRuq/rZeCRLScsJSAAAoBYbt/WILQAL80E9Yguwj2TpBiYWZiMRnYPTpQAAdVgDtiHOAPMyB7QhzkQ1QtvUv8YiWQoAAAB/WTY8bHq0Jd4AczHXtifeAK9JlpKSk4/1iTEAADOxkdiPjXOAORjr+zHXAjwnWfqCSQSIzKN4AQDOs+6LQT0AjMsYH4N6IIKR2qE+NQ7JUgAAAKZlgyMW9QEwHmN7LOoD4GeSpU+YOGLyeNh2xDoHp0sBAPZb1nvWfDGpF4AxmGvjUi/0MmLb05/GIFkKAADAVGxoxKeOAHIzjsenjgA+vL2/GxMfMWEAmXz6+uXyE9f++PybE9LwgHudufR8YoS2lt9ITxzRHnPJ3vaitrfRniIkzhCLuTYXYxWtjDw26Ef5OVn6gEkdAABgLNZ5+agzgFyM2/moMzhPP8pPshQAAIDh2cDIS90B5GC8zkvdAbOTLL3D5ABk9Oevv19+AgDgmjUeANRlrgWemWGMMA7mJlkKAADAsGxajEE9AsRljB6DegRmJll6w6QAZOZ0KQDAB+u7sahPgHiMzWNRn9QwU7vSh/KSLAUAAAAAAACmJFkKAADAcHyre0zqFSAOY/KY1Cswo7f3d2PfykQAjOLT1y+Xn/jj829vlx+BG+595vKvf753Gw+1tfx6tp8jRm5zR+titJhkaZNR456tT78iztCeufZn5lr42chjxTP6Tz5OlgIAADCMETcqr1+XX+92+z5n3guAuZlr77t9nzPvBUBbTpZezPoNB2BcTpd+52QpjCHqvZoNkG3UHy2NsLbr0TYzxi1DHzb+tSHO0Ja59hhzLbMZYaw4Q//JxclSAAAAhpB9Q2bZUOm1qdLzswHIw1x7nLkWIC7J0r/M/g0HAACA7DKv6yJtnmbayLWWB2jLXFuGuZYZaDtkI1kKMKg/f/398hMAABFF3izNsokLAM+Ya4FeJIxzkSwFAAAgtYwbERk2SG3iArAy19ZhrgWIYfpkqew+MDKnSwEA4sm0MRq9rNb0ANxjri3HXMte2swHscjDyVIAAADSyrYBkfEEScYyA1COubY+cy1AX1MnS2X1gRk4XQoAEEPmjVCbuABkYK6FvuRcyMrJUgAAAFLKtBkzwgZo1GuwKQdQj7m2LXMtnBetH+k/OUiWAgAAQEUjbN6uRroWAMZhrgXgjGmTpbL5wEw8ihcAGE2WNZ0NTwCyMtcCe0QaM6KNC/JR8TlZCgAAABWMunlrUxqAKMy1wDV9h6OmTJbK4gMzcroUABiFNR23tAmAsoyr3NImeEb7IDsnSwEAAKCw0b/V7lv7APRmrgWeidaHJJRjmy5ZqkECAABQk81NAKjLXAtxRMm5GBc4w8lSgIl4FC8AkJ0vwMZhQwpgTObaOMy1cE60PmR8jUuyFAAAAAqxqQkAdZlrASjt7f19nkS2rD3Ad5++frn8NL4/Pv9mEQUDiHofZ6NmG/VHSdHXddoV14x/bYgzlGWuBbaKMl48GxeijWnGsJicLAUAAIACbHwAQF3mWmCvaONG9C+kzGqak6UaIMAHJ0uBbKLey9ms2Ub9UUr0dZ02xS17EXMzJpCRuRbYKtJ48WpsiDa2GcvicbIUYEJ//vr75ScAAAAAgJwyJh59oS6eKZKlGh4AAAA1+XY4ANRlrgWOMn7wyhSP4ZUsBbhvhsfxegwvjCHq/ZwF1zbux3PI0J4jtyXjAfcY/+ZmXCAjcy2wRZSxYs+4EHF8M67F4TG8AAAAhCfpBAB1mWuBkUlM8szwyVKTPMBj/u5SAAAAAGArORdG5GQpAAAAnOBb6gBQl7kWuDXCuCDxHMfQyVINDQAAAAAA4LzsORdfvOARJ0sBJudRvAAAAAAA7Tn0F4NkKQAAAAAAACk4IUppwyZLZeMBtnO6FACIzPoOAOoy1wKvjDJOSLRyj5OlAAAAcJDNFgCoy1wLjM4XVvobMlmqYQEAAAAAAJwXKedS4gsUvoTBLSdLAfibR/ECAAAAALTnEGBfwyVLNSgAAAAAAICxOBFKLU6WAvBvTpcCAAAAAKtRD6hJvHJNshQAAAAAAAA68uTUfoZKlmpIAAAAAAAA50XKuTgJSk1OlgLwA4/iBQAAAABGFzEB61BgH8MkSzUgAAAAAACA8+RcmImTpQD8xOlSAAAAACCCmidAPd6XxRDJUt9wAAAAAAAAIDs5r/acLAXgLqdLAQAAAGA+knXMRrIUAAAAAACAcFo8Jjfio3glrNtKnyzVYAAAAAAAAM6Tc2FGTpYC8JBH8QIAAAAAtCdx3U7qZKmGAgAAAAAAMJ6Wj8eN+Che2nGyFICnnC4FAAAAgPE5oMasJEsBAADgIBtKAFCXuRbm1OOkZ8TTpcbANt7e33PGWQMBaOfT1y+Xn/L54/NvHqEBA4h67+cxPduoP0qIvAbUlnjE+NeGOEMZ5lqYW7QxoFe/jzgWGgPrc7IUgJc8ihcAAAAAoL3IX2YZRcpkqYYBAAAAAABwnlOlH5zinJOTpQBs4nQpAAAAAACjSfd3ljpVCtBPxr+71N9ZCmOIeg/oG6fbqD9KiLwW1JZ4xPjXhjhDGeZamFfk/s8HY2E9TpYCsJnTpQAAP7O5BAB1mWuhHv0LJEsBAABIwLeoAaAucy1AbBLb9aRKlmoIAAAAAAAA58m5wHdOlgKwi0fxAgD8zEYTANRlrgWglrf39xxzjMkQII5PX79cforvj8+/eYwQDCDqvaBHlW2j/igl+rpwxjYVqU4ixt/414Y4Qznm2njMtdQUvc9zn75YnpOlAOzmdCkA0INNAQCoy1wL85AohQ8pkqU6LQAAANHNtna1VgegNXMtgLGhBidLAQAAgNSchAKAusy1Y5Fsgx9JlgJwiEfxAgD8zMYTANRlrgWgtPDJUpMfAAAAK6caYrBWBxiXuTYGcy21aFtjUI9lOVkKwGFOlwIA/MzGBQDUZa4FoKTQyVKTHgAAALcynHgZeT0b7dqcgAIoz1zbl7kW2EIOrZy39/e4sVTRuZg0OUpfz+3T1y+Xn2L64/NvxiYYQNS5wv3PNuqPGjLcQ47axqLFPnKcjX9tiDPUYa7tx1xLLRn6Nfvon2V4DC9F6JAwL4/iBQC4b8TNKBtsAERirgVmZ8woI2yyVAXDPCTbAQDYK8s9pLVtXdYSAPWYa1mYa8ehr8BjTpYCcJrTpQAAj42yMWWDDYCozLUAnBEyWWpSyMW3iwAAgB4yrUWyr3Ot0wHmZK5tx1wLHGX8OM/JUiAESff8nC4FAHgu6yZG1HJbQwBwy1xblrl2HJJp8Nzb+3usPqLT5mPSpBT9P79PX79cforjj8+/GaNgADYPclN/1JbtPjJT24sc2wxxNP61Ic7HRYyd+Tkmc2095tpjjF/7ZOvDHJNl7IvYHp0s5RQ3sAAAAPtk2Kxayhi5nNaiADxjrj3PXDuODP0BepMsBcJwE5afR/ECAD1kvI+MvEFqQw2AW+bassy1ALGESpaaJAAAADgi6xfvIq2DI28qX/MlS4A+zLXnmWtpLUN7oxz1fZyTpRxm0gTucboUAGCfdeO01+ZGz88GgBbMtQA88/b+HmOMNlnkI1lKDcaCMXz6+uXyU39/fP7NWAUDiDo/uB/axvw+jgxtfqT2VjPeWeOUbdw1f7UhzsdFjN1o7XNE5tptzLV1Gb+2Gam/sl30fhyxXTpZCoRiUQQAwBkj3U8umwjXr8uvd7t9nzPvBQDm2p/dvs+Z94JStEPYzslSDpPUohbjwRiinC51shTGEHVucD+0jbl9HFnavDY3poxjrvmrDXE+LmLs3F/lYK4dU6b+Z/x6TT+dV/S+HLFthkiW6rT5uHGlJmPCGCRLgZKizg3uibYxt4/DBhq9ZB1vzV9tiPNxEWPn/ioPc+1YsvU949dzkfvnCON8hvEvcpwjxs9jeIFwLIzG8Oevv19+AgBozz0lANRlrgX2GmXcMP6Np3uy1DeQAAAAqMEmxhjUI0BcxugxqEcYj9zbPk6WspvJEwAAyML6JTf1BxCfsTo39TeeqEmy0dqavjOWrslSmW3gEZPNGDyKFwCIwL1lTuoNIA9jdk7qDeA7J0sBAAAYns1AAKjLXAv9OaDGNe1hO8lSdnHTA+zldCkAEIX1TB7qCiAn43ce6oqWRm1v+tE4uiVLZbSBV0w2AACU5h4zPnUEkJtxPD51NCY5F+7RLrZxshSA6pwuBQAisUEYl7oBGIPxPC51A2XpU2PokiyVyc5JpwcAAEZhfROPOgEYi3E9HnVCD9odGThZCoRmMgUAoBb3mnGoC4AxGd/jUBdjc0CNZ7SP15onS1UKwJw8ihcAiMjGYV9L/NUBwNiM832Za6E+fSw/J0vZRGcHAABGZROxDzEHmIe5tg8xn0PkA2raYBwOMj4nWQqEZ1Idh9OlAEBk7jvbEWuAORn/2xFraEufy61pslTmGgAAgMiWTQ4bHfWILwDmgrrElyi0w3jk6B5zspSXDGoAAMBsrIPKE1MArpkXyhPT+Uh+QRnNkqU6LXCGm71xeBQvAJDFcg/qPvQ8cQTgEXNEGeIIMeiHeTlZCgAAAE/YgDxO3ADYwlx7nLjNK/IBNe0yLgcb75Ms5SmDGlCD06UAQEY2crcTKwCOMH9sJ1YQk36ZU5NkqUw1UIKJBgCACGxOPiY2AJRgPnlMbFjIuXCG9vMzJ0sBAADggHWzcvYNS3EAoBZzzHfiQCbaKRlVT5bKUOdlUANq8iheAGAkM25gznjNAPRjrgWy0G/zcbIUSMVEAwBAZOum5qj3raNfH9CXsYUtRp+LRr8+zot8QE27zcNBxx+9vb/Xi4dg52ZgIypjy1g+ff1y+amOPz7/ZiwDAELIeB9rXQhsVWKMM+ZwlrkWgCMkS7nLJE1kxpaxSJYCADOLdG9rHQicIVlKVOZaAF6RLOUuEzfRGV/GUjNhKlkKAGRV457XWg+oRbKUjMy1ACyqJUslMnIzqROdMWYskqUAAAC5SZYCAFn9cvl/+Dc3pgAAAAC0ZD8KAOilSrLUiS+gNouosfz56++XnwAAAMjGXiAAkJmTpQAAAAAAAMCUiidLfZMsN6f1gF6cLgUAAJiT/SgAoCcnS4G0LKYAAACgLwcnAIDsJEsBAAAAgC58ERoA6K1ostQ3yQA4w6N4AQAA8rAXCACMwMlS/s03+chIuwUAAID2SiRKrekBgAiKJUt9kwyAEpwuBQAAiM0+IAAwEidLAQAAAIBNSiVKnSoFAKKQLOVvblABAAAAeGRJkjpRCgCMqEiy1I0S0JNk/3g8ihcAACCO0nt/1vEAQCROlgIAAAAAP1hPkkqUAgCjO50sdao0PzepQEROlwIAALRXI0G6sgcFAETkZCkwBAsuAAAA2G9NjtZMki6s2wGAqE4lS2veQAGA06UAAAB11E6OXpMoBQAic7J0cm5WAQAAAKjF3hMAEJ1kKTAMCzAAAACIYVmjW6cDABkcTpZ6BC8ALXgULwAAQC6SpABAJk6WTsyNKwAAAAClOE0KAGR0KFnqVCkQlUXZmJwuBQAAiEuSFADIzMlSAAAAAGCXNUEqSQoAZLc7WepU6RjcyAIAAACwhwQpADAiJ0uB4Vi0jcmjeAEAANqTIAUARidZCgAAAACTu06KXr8u/xgAYFhv7+/bn6rrEbxjcKPLDIxX4/r09cvlp23++PybMQ8AAAAAgLucLAWG5EsBAAAAAADAK5uTpU5pARCBv7sUAAAAAIBSnCydjNN2AAAAAAAA8J1kKTAsXw4AAAAAAACe2ZQs9QheACLxKF4AAAAAAEpwsnQiTtkBAAAAAADAh5fJUqdKgcx8SWBcTpcCAAAAAHCWk6UAAAAAAADAlJ4mS50qHYfTdQAAAAAAAPAjJ0uB4fmywLg8ihcAAAAAgDMkSwEAAAAAAIApPUyWegTvOJyqA0bmdCkAAAAAAEe9vb/LiQIAAAAAAADz8RheAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFOSLAUAAAAAAACmJFkKAAAAAAAATEmyFAAAAAAAAJiSZCkAAAAAAAAwJclSAAAAAAAAYEqSpQAAAAAAAMCUJEsBAAAAAACAKUmWAgAAAAAAAFN6e39/v/z43F//3rZ/sYO3v1x+LK7kdT8qZ+TYblWzDnisRfvM5Gg8ol57tvotVd6W9XG0zC3L+Ejmso/kaD0sMrejlfYEAAAAQHZPk6VnN9B6O7qBl/26e7NxWleL9pmlDmvFovf116zjGteWqby1ylojrreylb1WeUs5ct21r6lWXVwb4Rpqqh2fs7LHFwAAACCiu8nS6BtFR23ZYBr12luzmVde67YZtQ5Hj0OL6yt1Ta3q4mx5W5VzUSq217KVv2V5S9hyzT2uqURdrHrVSclrqK1XjI7KFFsAAACA6H5KlmbbLDri3gbTDNfdg828Mnq2zyh12LuP1o5Dj+s7c02ty3ukrD1iujoT20XPsi+yxbuEe9cc4ZqO1MUqSp2cuYbaosToqMixBQAAAMjih2Rp9g2jvdYNptmuuzUbeedEaJ896zBa/6wRi57XeOR6epV3T1l7xvRapvjes7X8kcp8xnq9Ea9na10sotbHnmtoIWqc9ooWVwAAAIBs/p0sHWXDiJhs5B0TqV/2qMOo41LpWPS8zr3X0rtOXpW3d/ke2RLnrGWPWu5RPauPDHXxqj21Mlq7jRJXAAAAgIx+Wf5ntA0jILdlTFpc/hjOpXhFylfqfY7q/fl7PStv5Gt5VbboZV9c/viDR7+nnux1EaGcWWK1x4jXBAAAANDK38lSqM0m3n7RYtaqPNGu+5mzZY1yrVvLEaW890Qu2+peGZffLS5/DC1LOWdwWxfZ6mYp7+LyRwoRUwAAAIBjfrGxAkSRcTwyhrZ3G/NMdXBd1kzlXmUv/0jW+Geuhx5lzxwvAAAAAOpwspRmbFBuFzVWNcsV9Zq3yFz2raJeY8bYL2VeXP6YTuayj2aEutCeyhJPAAAAgP0kS4HuRtjc3XsN0a75WXmilXUVtVwzEHtKatWetFsAAAAA7pEsBboaafN667VkuuaoZc0UQ+A1fRoAAACAXt6+ffu2a3Pqf/2f/3v5qY3//s//uPzU35Fr31v+1vE940jdvP3l8iNPHNk0btE+FyXrsPbm+LOY1BxbHsXo7PXuqeMSdduqvDXrYq9HZY5Uxkcyl30UW9p85PqIUP5H42cJZ8e0LGrGEAAAAGBE4ZOle9TewDty7XvLFDm+t47E2wbeNkc2dFu0z0XJOiy9cX2m/5QcP+7FqMS17rm+s3V7try1y1rK0TbTs8yrbGU/0z9LqHHdJa4pY/u/VfIa7o2fpRwZ1zK225oxBAAAABjRUMnSW6U3II9c+94yjB5fG3jbtNrQ7VmHR67xntJ9ptS4scap13Uerdse5S0V8z1KtZvWZY/a3rcoXfa9Sl5rjWsZoS5KXcM6fpZ2ZHzL2G5rxQ8AAABgVEP/naXLBlfvTS7gZyUScrX6d6n3Xa5xcfljCqXKG3ncLd1uSr/fMzU+J3PZe6hZ3zXfe1X7M2qX/4xs4zEAAAAA7QydLF1F3rzjtWWDc6/Lf8qgWvTpSOOGMayMmnGs/d6Z338ELWNU63Nalv/sZ0WZx1vFDAAAAIC+pkiWLmx45fF3tvPK5de7XP7TH1z+EZ2drYuWfTnCuJFt7Ipa3hblqvEZLeOZra210iMuy2eW/Nxe13CGeRsAAACAVqZJli56bBay3bIxurj8sbjL29t8TSzjhv9MosaqZblKfpb23t8I8eh5DdoTAAAAABlMlSxd2LiL55LDbJbEbPlZlDPjhn+m8SpqWXuUq8Rn9oxnpnZXU4Q4LGU4U44o1wAAAAAAkU2XLF3YuIujV+JSwrSPo3GfbcN/+axM41TUsvYsV6b6u6dk+bPHIqsR4l5qri71PgAAAACMacpkKTH03ry0eUpE2RIcEmHliWl/0epgb3mylz8C/RAAAABgHtMmS22CsZAwjS9SX61dlmzjUuTyRijbkTJEimm29ljKrNddm7gCAAAAENXbt2/fdiWLRtrs+u///I/LT9scufYWn9HL3mtbvP1l+f9oScq1XFEciU+L9rk4GqujdX60Tzy7tjP97EjMXqnR72uUc1W6vJFi+qgsNdrhPTU+52x9na2f0u3lqK3Xcaa8rz6jRV3U/IxebensHN1qTq3hSMzOxgsAAABgNs2SpUc3yJ4psZG1p1xHPm/vdbf4jJ6WDbyjSbOaom0sttrYbbkJe7Tea17X0TGkZJ87WoZXao0LNcpbuqy16rV2e6n5/mfq7Wz91GgzR0Soh1XEuo5S/nvOztGt5tQajsTsbLwAAAAAZpM6Wbo4u5lVe3Nw73W3+Iyelg28o0mz2iJtLrba2G25CRvxmo6OHyX63NHP3qr0uFCzvBHKurUMWd97caYOj9ZR7Wsq7WiM9pa55ue0inmrWC3OzM9H5p7F3uvr2W5vnYkXAAAAwIzS/52lkTangL72jgc9xo9lA/5okqG1taxZyrvIVNbWMsyXGef0I2WudZ3afxniCAAAADCX9MnSRcbN1Rk56TCvoyd7RpIp6ZiprCXsmUOOzDdbYnkk3pGSdLRRo18ebRPa0gexAAAAAMhtiGQpeUiasdVMyTqITn8sT4INAAAAAGKQLKWJM6dKl036vS9iOVL/SyLh9gVQi7mDEZy53wIAAACYlWQpkMa9BOryWv8Z/fWsB8mubVrV0Qz1YdyJzdMsAAAAANhCspTqnHKgtlETFst1Zbq2jPVwpMxH/huJ3PjW/vbolcG9cq8vtsncV91vAQAAABwjWUpVNu7Gs5zUOeLyn4dyZFO8VdIhW3JDMoaz7rWhpY/ee/WwlO/2dcaR6zjzmbdlX18ZXKaR3S7/eVX3YnjbXtdXTe63AAAAAI6TLKUaG3dEVnvj+owsCYxFpoQL57ToM9eJpWeft/Xfgxa2tset/x4AAAAAbQ2RLLXhFFOrUx2w19Exo3ZSMFviUZKUks7M5ZJP7R2J94hjRpR2654LAAAA4LipT5ba6Id5rJvSpTamS8qYJDV+ElHUPn4rQxlpp1S7lTAFAAAAOCZ1sjTLpijQ3jo+lBgnaiUHsyUds5WXfnrPzb0/v4ZIfW/E+C60WwAAAIA5vX379m3Xt9CPbuSc2eSrtXm0t0xHytHiM1o5Uoe3f2/pkVMPLeK+iPR3rEaOUxYl+1KJOF2Xp8W4cKbMZ8q62FveUu2wZZxKf1b0sq+2fuaZzyipVNsq6WhsolxL9vI/M0q7jXQ/AwAAAJBBs5OlywbU0VcNGTbtgP5KjRXL+6yvDDKVdVFrriC3aO3iaHmi9EX9rA1xBgAAAGhryr+zNFMCAOgjW7KwJGNkHc8SIEdiLqGyTZQ4Za+vM+U3pux3Jt5Hnk4BAAAAMLPpkqU27Ihg2ch85vKv0YlxglFJsPYh0cgR+isAAABAG1MlS2045rTU295XT5d851OXf5VKzm4wL//9+oLI9rTRVu05Yr/pWaYRxpEz19B7Tt5qhHoCAAAA4JhpkqVZNuvI6ZID/dvlVwzCBjqtHJ2nljb6qp1ma8frF19Kzt09YnD2M3vfu2xpW8/0Ln9rkdqt+xEAAACA7aZIls62WRfZ218uPw7h7+zoXy5/ZFA9kiyw15rYWtvr7Z+je5Rouv797T+L7Gzce19rlnbT26O2ef37238GAAAAQCxDJ0ttUFGTJOlcMiWdyKvUnJWpre6dq8/EqFVcso8VJco/+v3XiO0WAAAAYFZv375925XwybBhU2uD7si17y1L5Pgeieu9k6QRk4x7T7xGvIba7bNF2yzVd2/Lev2+Ja6jVDm3ql23pe0tb4myHq3Xo59d+/NKtNMa9sRryzWcqfvWdb5VibqrXcZnepe/Rdt/Vr6R2u3eexsAAACAWQ1xsnTZQLp+EVv2zbuIiVJ+9GxMePR7iGSE9nnd125f6z8fjURp/npd2+i91/rPs3C/AgAAALDNEMnSFqcQGNee5K2Nx7Ec3fQ25nBEpiRLbSViEa3/SpSO38ZHbLcAAAAA/KPdY3ifbQ6V3ACquVF3pJx7y9PiMyJYEpQREo97EqWLyMnS2m2nZD99pFdbjt7vso0Le8tboqxH2+fRz25ZJ0evrYae7eqe1vX+SIk66hnbSOVv0d57t+OW7XbvfQ4AAADAjEKcLF02f9bXWS022ThPopRIjow9xppYSswfUdW4tpHj1VqkROMR2cufkXgBAAAAxBLuMbwlNpAkMXjFSQsgk1LJleV9RkrU9L4WidL+dQAAAAAAZ4X8O0tLbLxJmALss3yJ4NblHxHAMjeemR+v/1tz5HlnY3i2Ps+SKAUAAACA70ImSxclNuBsBvPI8kjdxeWP3S1tVXvNR53Rw5pk2zJP7vl32abEeN27PkqMXdoUAAAAAKMImyxdlNiIk8yghDOJ1XVj/dmL/mz8k9HSbp+9Sir9fhmVGK97x3GEawAAAACAkkInSxc25Kip5unSlolQCVeAuiRKv5fffRkAAAAAowmfLC1BIolnaiZM99JWX1tidO91Von3IJ8j9a6txJGpLkZIlAIAAADAiFIkS0ts0Nnc5oy3v1x+3MzG8jlLn733goxmGw/u9d17ryzOllWiNIfrtvnsdVaJ9wAAAACgnLdv377tOlV3dIMnSsLzTDmOfP7ez2vxGdz3KiF65ARqq/rs2S9rad0XIsewVSzu9YEW7b5kDFvEqnXbXN1+7tH3PFL+xavPq/W+z0St72sl6v6M7OWvLUu7XRz5zFf3NgAAAAAkewzv6Bt2AMxrSaA8e92697tatsy/I87RZ2PcOybZy99ClmucoS4AAAAAepni7yy91nJzGY7Y20aPtmkbrx/EcG576j/7HJK9/NdqX8vZ988+Phjf6mjZB50qBQAAANgmXbK0xObdSJvFRy0baNcuv4bU9G0iJngkzZ6L2G9HSJSeuYbsbbYF8w0AAADAOFL9naXXemxkHvnMvZ/T4jMWtwnS0f4+zhIxudUqRqst11D7/Xs7en17ri1DDHv2gRbtvnQca7WbqO1x6/tHLf+qxefUjlULW64he/lbG6Xd3np1XwMAAADAd9M9hncVeSORcZzZFF7a6Pq69uj3fNgSm7MxjLjhzznP2sPRttKinWxt7y2cHfO2qH0trWJFHCO0WwAAAACOS5ssLbEBbeOKLJa2ur5KyJLoO7uBfRu32z8flSV+syrRblYl2ksLaznvlf36d3u1buuPypvxWno5EyOOedQ219+fqZOj7dapUgAAAIDtUp8slbCgpaMbj9ppDGc2q0uxeZ3H2QTH4kjfLzFelCj74mhZSl5D62sp8VnkFLHdAgAAANDGtI/hXdnMYjYlNoRbilbebPGblXpiRu5pzjHfAAAAAMwpfbK0xEaSzUVqi7LhmXXjdYT4OVXaXs92c+azI7T3s2WIcA2rrWVxL0AkZ/qQ+QYAAABgnyFOlkbalGVsZzYgtdNzesdP/TGLUm09Qp+Zqd9K9pah3QIAAADMZ5jH8J7dWLLJyMiW/jHC5muvazj7uU759NOjzZT4zKxt/Vav61j0/Gxyy9xuzTcAAAAA+03/d5bCXmc2IntswJb4zEibr61j2HPTnDKWOmxRj6U/p3Xbq/V5ra9j0eMzGYt2CwAAADCPoZKlZzeZnC6lhaWdttoQLfE5a6J0+f9S/n7jEzLGj/5atZuSWpR5+Yzan9PiMxZHP8f8H9vfE8dBl7c4pFW7XZT4nLPXCwAAADCr4U6Wnt1ssmHKFiU2JGtuwLbc4O2l5jXOEL9Z1ajX2u2l5vvXLPc9teMENdRutyXeX6IUAAAA4DiP4YWOSm2Srkq/X63N15LvW/Kas8SPc0rVc+n28krJz2td9mulP7vntUThi16PlRqH13ZWqq1ptwAAAABxvH379u398jMTud08fP/L5cdpHdlQrRW3rRvfNTdaj8Rjr97xW9SK4aP41brmEu6VOXJ5S4rQ547IWu57IvRbYno0nu5RayyLPN8AAAAAsI1k6aRuN9ZmSYg8c3SzccTYHY3FEbPFL/L13iv3iPUD5PJsTN1q1LGsRGwAAAAAZhf6Mbw2gMhAOz1ntPhlvZ5s5c4aZ6CPEccM4yAAAABAGWGTpesGkI2g8u7FVJxZaQvHbYmd+DIC7ZhWSra1kdrtSNcCAAAA0NsvNlvg/KbjKP2o13Usn7u4/DGl7OV/JOJ1rWWKWLYZiD+ZjdBu9T0AAACAskKeLL3dBLIpVI5Y1pM9ttrGcXtjFy3W0cqzx1L2xeWPKVyKnDLmt+XOeh3MLXO71ecAAAAAyvs7WZph48XmUH2zxrjkdWeM4VLmxeWPXV2KkiqG2cp7RKRrfFSWSGV8ZCnj4vLHdG3nUXmzXQd51Gxby3svLn9MIVt5AQAAALJ4e39/v/z4j3/89fPHHzp5thEUoXyZbd1kmynOW2NyRIY41rz+s2aIX4Rr3HMNvcu7pawRYnrPq7JHLfdiS9xXka+DXPa0u7Oit9uWsQAAAACYUahk6ZbNoOgbWlHt3WibIc57Y3JE1Di2uPZSIsawZPx6Xt/e68hS1p7lvGdr2bOW+1a06yCfo23vjKjttkcsAAAAAGbzQ7J01WPDaO9mUNRNrYiObrSNHOOjMTkqUixbX3spEWJYK3Y9ru3MtbQu79Gy9ojrtazlXhwt+7UI17FYriVKWXitRNs7I1K7vfwIAAAAQGV3k6WLVptFZzaDomxoRXZ2s61lO2j5WZcfm2t1jff0vO6SesSwRexaXVepa8lU3lZlXWWL8bVSZb/W4zoWt9fSqxxsV6P9HRWl3QIAAABQ38Nk6arWZlHJzaBeG1pHrddes9wl47uoVdZ75Wz5WT3Vus5r0a65pFHjV+u6al2L8n7IWOZFrXLfE+FaapeB/Vq2wSNatJnoMQAAAAAY2ctkKVBfqY3YmTdbS8TQZvVczraZHu2lRDtfRGjrI10L89BuAQAAAEbzj3/8f7eaU7kZ2K3YAAAAAElFTkSuQmCC',
                        width: 150
                        },

                        {
                          alignment: 'right',
                          fontSize: 10,
                          text: 'Departamento de Transporte y Servicios',
                           bold: true
                        }
                      ],
                       margin:  [ 21, 34, 25, 34 ]
                    }
                  });

                  var now = new Date();
                  var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear()+' '+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
                  doc['footer']=(function(page, pages) {
                    return {
                      columns: [
                        {
                          alignment: 'left',
                          text: ['Fecha de creación: ', { text: jsDate.toString() }]
                        },
                        {
                          alignment: 'right',
                          text: ['pagina ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                        }
                      ],
                      margin: 20
                    }
                  });
                },
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7],
                  search: 'applied',
                  order: 'applied', //column id visible in PDF 
                },
               
            }*/
        ], 
});


$('#actualizar-movimientos').click(function(){
  tabla_movimientos.ajax.reload();
})

$(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
});

