var RUTA_DEPARTAMENTOS = ruta+'/departamentos';
var tabla_departamentos = $("#tabla-departamentos").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_DEPARTAMENTOS+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'nombre',name: 'nombre'},
	{ data: 'descripcion', name: 'descripcion'},
	{ data: 'padre', name: 'padre'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showDepartamento('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteDepartamento('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


loadCatalago();
function loadCatalago(){
	$.getJSON(RUTA_DEPARTAMENTOS+'/opciones',function(data){
		$("#departamento_padre").fadeIn(1000).html("");
		$("#departamento_padre").append('<option value="">Seleccione</option>');
		for (var i = 0; i < data.length ; i++) {
			$("#departamento_padre").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}
	});
}


$('#actualizar-departamentos').click(function(){
	tabla_departamentos.ajax.reload();
})

function Departamento(){
	this.id = $('#departamento_id').val();
	this.nombre = $('#departamento_nombre').val();
	this.descripcion = $('#departamento_descripcion').val();
	this.padre = $('#departamento_padre').val();
}

function removeStyleDepartamento(){
	$('#field-nombre-departamento').removeClass("has-error");
	$('#field-nombre-departamento .msj-error').html("");
	$('#field-descripcion-departamento').removeClass("has-error");
	$('#field-descripcion-departamento .msj-error').html("");
}

$('#guardar-departamento').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Departamento();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_DEPARTAMENTOS
	}else{
		type = 'PUT';
		route = RUTA_DEPARTAMENTOS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleDepartamento();
			$('#modal-departamentos').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleDepartamento()
				if(jqXHR.responseJSON.nombre){
					$('#field-nombre-departamento').addClass("has-error");
					$('#field-nombre-departamento .msj-error').html(jqXHR.responseJSON.nombre)
				}
				if(jqXHR.responseJSON.descripcion){
					$('#field-descripcion-departamento').addClass("has-error");
					$('#field-descripcion-departamento .msj-error').html(jqXHR.responseJSON.descripcion)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-departamentos').on('hidden.bs.modal', function (e) 
{
	tabla_departamentos.ajax.reload();
	$('#form-departamento')[0].reset();
	$('#departamento_id').val('');
	removeStyleDepartamento();
	loadCatalago();
}); 



function showDepartamento(id){
	$.ajax({
		url: RUTA_DEPARTAMENTOS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#departamento_id').val(res.id);
			$('#departamento_nombre').val(res.nombre);
			$('#departamento_descripcion').val(res.descripcion);
			$('#departamento_padre').val(res.padre);
			$('#modal-departamentos').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos del Departamento/Gerencia. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteDepartamento(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_DEPARTAMENTOS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_departamentos.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la categoria. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Departameno/Gerencia se ha eliminado exitosamente',
			'success'
			);
	});
}