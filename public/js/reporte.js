
var reportes = ruta+'/reportes';

// ---------------------------------------------------tabla de reportes de un activo en particular----------------------------------------------------------------------

var tabla_reportes = $("#tabla-reportes").DataTable({
  processing: true,
  serverSide: true,
  ajax: reportes+'/listar',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'unidad', name: 'unidad',
        render: function ( data, type, full, meta ) {
        var text = '<a href="'+ruta+'/activos/'+full.activo.id+'" style="font-size: 13px">'+data+'</a>';
        return text;
      }
    },
    { data: 'informador', name: 'informador','className': 'text-center'},
    { data: 'fecha', name: 'fecha','className': 'text-center'},
    { data: 'prioridad', name: 'prioridad','className': 'text-center',
    render: function(data){
        if(data == 'Alta'){
          var text = '<b><span style="color:red">'+data+' </span></b>';
        }
        if(data == 'Media'){
          var text = '<b><span style="color:#FF9900">'+data+' </span></b>';
        }
        if(data == 'Baja'){ var text = '<b><span style="color:grey">'+data+' </span></b>';}
        return text;
      }
    },    
    { data: 'estado', name: 'estado', 'className': 'text-center',
      render: function(data, type, full){
        var text;
        if (full.estado == 'Sin asignar') {
          text = '<span class="label badge-warning label-lg">'+full.estado+'<a href="'+ruta+'/activo-reporte/'+full.id+'/orden" class="btn btn-default btn-xs " title="Asignar" ><i class="fa fa-pencil-square-o"></i></a> </span>';
        }
        else{ //AQUI VA EL LINK PARA REDIRECCIONAR A LA ORDEN DE TRABAJO ASIGNADA.
          text='<a href="'+ruta+'/ordenes/'+full.orden.id+'"  style="font-size: 16px">OT Nro. '+ full.orden.id+'</a>';
        }
        return text;
      }
    },
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
          var text ='<button type="button" class="btn btn-info btn-xs data-toggle="tooltip" data-placement="top" title="Ver" OnClick="Mostrar('+data+', 2)"><i class="fa fa-eye"></i></button> ';
          return text;
        }
    }

  ],

  createdRow: function ( row, full, index ) {
    if (full.estado == 'Sin asignar') {
     $(row).addClass('danger')
    }
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 },

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-reportes').click(function(){
  tabla_reportes.ajax.reload();
})

// ------------------------------------------- BORRAR REPORTE----------------------------------------------------------------------

function DeleteReporte(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = reportes+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            $('#modal-archivos').modal('hide');
            tabla_reportes.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar el reporte. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El reporte se ha eliminado exitosamente',
      'success'
      );
  });
}

function Mostrar(id, option){
  $.ajax({
    url: reportes+'/'+id,
    type: 'GET',
    success: function(res){ 
      var titulo;
      var contenido;
      if(option == 1){
        if(res.archivos.length > 0){
          titulo = 'Archivos del Reporte Nro. '+res.id;
          contenido = '<table class="table table-hover table-bordered">';
          for (var i = 0; i < res.archivos.length; i++) {
            contenido+= '<tr><td>';
            contenido+= '<a href="'+ruta+'/image/archivos/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_externo+'</a>'
            contenido+= '</tr></td>';
          }
          contenido+= '<table>';
          var botones = '<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>';
          $('#titulo-modal-archivos').html(titulo);
          $('#contenido-modal-archivos').html(contenido);
          $('#botones').html(botones);
          $('#modal-archivos').modal('show');
        } 
      }
      if(option == 2){
        titulo = 'Reporte Nro. '+res.id;
        contenido= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="3" style="background: #344563; color: white; text-align: center;" >DETALLES DEL REPORTE</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Fecha</b></td>';
            contenido +='<td ><b>Prioridad</b></td>';
            contenido +='<td style="width: 70%"><b>Lugar</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.fecha+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.prioridad+'</td>';
            contenido +='<td style="vertical-align: middle;">'+res.lugar+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="width: 30%; text-align: center;"><b>Reportado por</b></th>';
            contenido +='<th style="text-align: center;"><b>C.I.</b> </th>';
            contenido +='<th style="width: 49%; text-align: center;"><b>Cargo</b></th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.informador+'</td>';
            contenido +='<td>'+res.ci_informador+'</td>';
            contenido +='<td>'+res.cargo_informador+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="5" style="background: #344563; color: white; text-align: center;" >DETALLES DEL EQUIPO O UNIDAD</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Unidad/Equipo</b></td>';
            contenido +='<td ><b>Placa</b></td>';
            contenido +='<td ><b>Marca</b></td>';
            contenido +='<td ><b>Modelo</b></td>';
            contenido +='<td ><b>Nro. de Activo</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.unidad+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.placa+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.marca+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.modelo+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.nro_activo+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido +='<table class="table table-bordered"  style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr >';
            contenido +='<th colspan="3" style="background: #344563; color: white; text-align: center;" >FALLAS QUE PRESENTA LA UNIDAD O EQUIPO</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;" >';
            contenido +='<td style="width: 8%;"><b>Nro.</b></td>';
            contenido +='<td style="width: 18%;"><b>Tipo de falla</b></td>';
            contenido +='<td ><b>Descripción</b></td>';
          contenido +='</tr>';
        for (var i = 0; i < res.fallas.length; i++) {
          contenido +='<tr >';
            contenido +='<td style="text-align: center; vertical-align: middle; ">'+(i+1)+'</td>';
            contenido +='<td style="text-align: center; vertical-align: middle; ">'+res.fallas[i].tipo+'</td>';
            contenido +='<td>'+res.fallas[i].nombre+'</td>';
          contenido +='</tr>';
        }
        contenido +='</table>';
        contenido +='<table class="table table-bordered"  style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr >';
            contenido +='<th colspan="4" style="background: #344563; color: white; text-align: center;" >Observaciones</th>';
          contenido +='<tr>';
          contenido +='<tr>';
            contenido +='<td><p>'+res.observacion+'.</p></td>';
          contenido +='</tr>';
        contenido +='</table>';
        if ( res.archivos.length > 0) {
        contenido +='<table class="table table-bordered"  style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr >';
            contenido +='<th colspan="4" style="background: #344563; color: white; text-align: center;" >Archivos</th>';
          contenido +='<tr>';
          contenido +='<tr><td>';
            for (var i = 0; i < res.archivos.length; i++) {
            contenido +='<a href="'+ruta+'/image/archivos/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_externo+'</a><br />';
              }
          contenido +='</td></tr>';
        contenido +='</table>';
        }
        var botones = '<div class="btn-group pull-left"><a href="'+ruta+'/activo-reporte/'+res.id+'/pdf" class="btn btn-primary" title="PDF" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
        botones+='<a href="'+reportes+'/'+res.id+'/edit" class="btn btn-warning" title="Editar"><i class="fa fa-edit"></i></a>';
        botones+='<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" OnClick="DeleteReporte('+res.id+')"><i class="fa fa-remove"></i></button></div>';
        botones+='<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>';
        $('#titulo-modal-archivos').html(titulo);
        $('#contenido-modal-archivos').html(contenido);
        $('#botones_archivos').html(botones);
        $('#modal-archivos').modal('show');
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos del reporte. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

