
var tickets = ruta+'/tickets';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ---------------------------------------------------tabla de tickets de un activo en particular----------------------------------------------------------------------

var tabla_tickets = $("#tabla-tickets").DataTable({
  processing: true,
  serverSide: true,
  ajax: tickets+'/'+id+'/listar',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'empleado', name: 'empleado',
        render: function ( data, type, full, meta ) {
        var text = full.empleado.nombre;
        return text;
      }
    },
    { data: 'salida', name: 'salida',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_salida+' '+full.hora_salida;
        return text;
      }
    },
    { data: 'llegada', name: 'llegada',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_llegada+' '+full.hora_llegada;
        return text;
      }
    },
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text = '<button type="button" class="btn btn-info btn-xs data-toggle="tooltip" data-placement="top" title="Ver" OnClick="Mostrar('+data+')"><i class="fa  fa-eye"></i></button> ';
      return text;
      }
    }
  ],

  //createdRow: function ( row, full, index ) {
    //if (full.estado == 'Sin asignar') {
    // $(row).addClass('danger')
    //}
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 //},

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-tickets').click(function(){
  tabla_tickets.ajax.reload();
})

function Mostrar(id){
  $.ajax({
    url: tickets+'/'+id,
    type: 'GET',
    success: function(res){
      var titulo;
      var contenido;
        titulo = 'Ticket Nro. '+res.id;
        contenido= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="4" style="background: #344563; color: white; text-align: center;" >DETALLES DEL EQUIPO O UNIDAD</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Nro. de Activo</b></td>';
            contenido +='<td ><b>Placa</b></td>';
            contenido +='<td ><b>Marca</b></td>';
            contenido +='<td ><b>Modelo</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.nro_activo+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.placa+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.marca+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.modelo+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="3" style="background: #344563; color: white; text-align: center;" >DETALLES DEL TICKET</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Fecha y hora de salida</b></td>';
            contenido +='<td ><b>Fecha y hora de llegada</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.sf+' '+res.sh+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.lf+' '+res.lh+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>Responsable</b></th>';
            contenido +='<th style="text-align: center;"><b>C.I.</b> </th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.empleado.nombre+'</td>';
            contenido +='<td>'+res.empleado.cedula+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>PASAJEROS</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.pasajeros+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>RUTA</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.ruta+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
                contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>NOTAS/DESCRIPCION</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.descripcion+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido +='<br/> Creado por: '+res.creador;
        if (res.modificador != null ) { 
          contenido +='<br/> Modificado por: ' +res.modificador;
        }
        var botones = '<a href="'+tickets+'/'+res.id+'/pdf" class="btn btn-primary pull-left" title="PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
        botones+='<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>';
        $('#titulo-modal').html(titulo);
        $('#contenido-modal').html(contenido);
        $('#botones').html(botones);
        $('#modal-ver').modal('show');

    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}
