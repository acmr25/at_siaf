
var archivos = ruta+'/activo-archivo';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

// ---------------------------------------------------tabla de archivos de un activo en particular----------------------------------------------------------------------

var tabla_archivos = $("#tabla-archivos").DataTable({
  processing: true,
  serverSide: true,
  ajax: archivos+'/'+id+'/listar',
  columns: [

    { data: 'nombre_externo', name: 'nombre_externo','className': 'text-center',
      render: function ( data, type, full, meta ) {
        var text ='<a href="'+ruta+'/image/archivos/'+full.nombre_interno+'" target="_back">'+data+'</a>';
        return text;
        }
    },
    { data: 'descripcion', name: 'descripcion','className': 'text-center'},
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
        var text = '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="DeleteArchivo('+data+')""><i class="fa fa-remove"></i></button>';
        return text;
        }
    }

  ],

  //createdRow: function ( row, full, index ) {
    //if (full.pivot.estado == 'PENDIENTE' ) {
    // $(row).addClass('danger')
    //}
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 //},'<a href="'+ruta+'/image/archivos/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_externo+'</a>'

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-archivos').click(function(){
  tabla_archivos.ajax.reload();
})

// ------------------------------------------- BORRAR REPORTE----------------------------------------------------------------------

function DeleteArchivo(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = archivos+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            $('#modal-archivos').modal('hide');
            tabla_archivos.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar el archivo. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El archivo se ha eliminado exitosamente',
      'success'
      );
  });
}

