
var RUTA_VIAJES = ruta+'/viajes';

// -----------------------------------------------------------------------------------------------------------------------
var tabla_solicitudes = $("#tabla-solicitudes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/solicitudes_listar/3',
  search: { "caseInsensitive": true },
  columns: [
    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'motivo', name: 'motivo',
    render: function ( data, type, full, meta ) {
        var text ='<b>Solicitante: </b>'+full.solicitante.nombre+' '+full.solicitante.cargo+' de '+full.solicitante.departamento.nombre+'</br>';
        text +='<b>Motivo: </b>'+full.motivo+'</br>';
        text +='<b>Nro. de Pasajeros: </b>'+full.nro_pasajeros+'</br>';
        text +='<b>Supervisor: </b>'+full.supervisor.nombre+'</br>';
        text +='<b>Fecha y hora de salida:</b>'+full.fecha_salida+' '+full.hora_salida;
        if (full.fecha_regreso != null) {
          text +='</br><b>Fecha y hora de regreso:</b>'+full.fecha_regreso+' '+full.hora_regreso;
        }
        if (full.dt_aprobado != null) {
          text +='</br><b>Aprobado el: </b>'+full.dt_aprobado;
        }
        return text
      }
    },
    { data: 'viajes_asociados', name: 'viajes_asociados',
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.viajes.length > 0) {
          for (var i = 0; i < full.viajes.length; i++) {            
            text +='<b>ID.</b>'+full.viajes[i].id;
            if (full.chofer) { text += ' - <b>Chofer: </b>'+full.chofer.nombre+'</a><br />'} ;
          }
        }
        return text
      }
    },
    { data: 'estado', name: 'estado'},
    { data: 'solicitante.nombre', name: 'solicitante.nombre', "visible": false},
    { data: 'supervisor.nombre', name: 'supervisor.nombre', "visible": false},
    { data: 'motivo', name: 'motivo', "visible": false},
    { data: 'origen', name: 'origen', "visible": false},
    { data: 'destino', name: 'destino', "visible": false},
    { data: 'fecha_salida', name: 'fecha_salida', "visible": false},
    { data: 'fecha_regreso', name: 'fecha_regreso', "visible": false},

  ],
  language: leng,
  order: [[ 0, "desc" ]],
  "bAutoWidth": false,
  "scrollX": true,
  responsive: true,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-solicitudes').click(function(){
  tabla_solicitudes.ajax.reload();
})

// ------------------------------------------------------------------------------------------------------------------------

var tabla_viajes = $("#tabla-viajes").DataTable({
  processing: true,
  serverSide: true,
  ajax: RUTA_VIAJES+'/listar/2',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'activos_list', name: 'activos_list', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text ='Sin Asignar';
      if (full.activos.length > 0) {
        for (var i = 0; i < full.activos.length; i++) {
          text ='<a href="'+ruta+'/activos/'+full.activos[i].id+'">'+full.activos[i].unidad+' '+full.activos[i].placa+' '+full.activos[i].marca+''+full.activos[i].modelo+'</a><br />';
        }
      }
      return text;
      }
    },
    { data: 'chofer.nombre', name: 'chofer.nombre',
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.chofer ==null) {
          text+='Sin Asignar';
        }
        else {
          text+=data;
        }
        return text;
        }
    },
    { data: 'origen', name: 'origen'},    
    { data: 'destino', name: 'destino'},    
    { data: 'salida', name: 'salida',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_salida+' '+full.hora_salida;
        return text;
      }
    },
    { data: 'llegada', name: 'llegada',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_llegada+' '+full.hora_llegada;
        return text;
      }
    },
    { data: 'solicitudes_asociados', name: 'solicitudes_asociados', "width": "25%",
      render: function ( data, type, full, meta ) {
        var text ='';
        if (full.solicitudes.length > 0) {
          for (var i = 0; i < full.solicitudes.length; i++) {            
            text +='<b>ID.</b>'+full.solicitudes[i].id;
            if (full.solicitante) { text += ' - <b>Solicitante: </b>'+full.solicitante.nombre+'</a><br />'} ;
          }
        }
        return text
      }
    }, 
  ],

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-viajes').click(function(){
  tabla_viajes.ajax.reload();
})

