var RUTA_ROLES = ruta+'/roles';
var tabla_roles = $("#tabla-roles").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_ROLES+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'nombre',name: 'nombre'},
	{ data: 'descripcion', name: 'descripcion'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showRol('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteRol('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});

function loadCatalago(){
	$.getJSON(RUTA_ROLES+'/opciones',function(data){
		$("#rol_padre").fadeIn(1000).html("");
		$("#rol_padre").append('<option value="">Seleccione</option>');
		for (var i = 0; i < data.length ; i++) {
			$("#rol_padre").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}
	});
}


$('#actualizar-roles').click(function(){
	tabla_roles.ajax.reload();
})

function Rol(){
	this.id = $('#rol_id').val();
	this.nombre = $('#rol_nombre').val();
	this.descripcion = $('#rol_descripcion').val();
}

function removeStyleRol(){
	$('#field-nombre-rol').removeClass("has-error");
	$('#field-nombre-rol .msj-error').html("");
	$('#field-descripcion-rol').removeClass("has-error");
	$('#field-descripcion-rol .msj-error').html("");
}

$('#guardar-rol').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Rol();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_ROLES
	}else{
		type = 'PUT';
		route = RUTA_ROLES+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleRol();
			$('#modal-roles').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleRol()
				if(jqXHR.responseJSON.nombre){
					$('#field-nombre-rol').addClass("has-error");
					$('#field-nombre-rol .msj-error').html(jqXHR.responseJSON.nombre)
				}
				if(jqXHR.responseJSON.descripcion){
					$('#field-descripcion-rol').addClass("has-error");
					$('#field-descripcion-rol .msj-error').html(jqXHR.responseJSON.descripcion)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-roles').on('hidden.bs.modal', function (e) 
{
	tabla_roles.ajax.reload();
	$('#form-rol')[0].reset();
	$('#rol_id').val('');
	removeStyleRol();
}); 



function showRol(id){
	$.ajax({
		url: RUTA_ROLES+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#rol_id').val(res.id);
			$('#rol_nombre').val(res.nombre);
			$('#rol_descripcion').val(res.descripcion);
			$('#modal-roles').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos del Rol/Gerencia. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteRol(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_ROLES+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_roles.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar el rol. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Rol se ha eliminado exitosamente',
			'success'
			);
	});
}