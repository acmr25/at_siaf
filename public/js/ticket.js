
var tickets = ruta+'/tickets';

// ---------------------------------------------------tabla de tickets de un activo en particular----------------------------------------------------------------------

var tabla_tickets = $("#tabla-tickets").DataTable({
  processing: true,
  serverSide: true,
  ajax: tickets+'/listar',
  columns: [

    { data: 'id', name: 'id','className': 'text-center'},
    { data: 'activo', name: 'activo',
        render: function ( data, type, full, meta ) {
        var text = '<a href="'+ruta+'/activos/'+full.activo.id+'" style="font-size: 13px"><b> Nro. de Activo '+full.activo.nro_activo+'</b> <br />';
        text +=  full.activo.modelo+' / '+full.activo.marca+'</a>';
        return text;
      }
    },
    { data: 'empleado', name: 'empleado',
        render: function ( data, type, full, meta ) {
        var text = full.empleado.nombre;
        return text;
      }
    },
    { data: 'salida', name: 'salida',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_salida+' '+full.hora_salida;
        return text;
      }
    },
    { data: 'llegada', name: 'llegada',
        render: function ( data, type, full, meta ) {
        var text = full.fecha_llegada+' '+full.hora_llegada;
        return text;
      }
    },    
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text = '<button type="button" class="btn btn-info btn-xs data-toggle="tooltip" data-placement="top" title="Ver" OnClick="Mostrar_Completar('+data+', 3)"><i class="fa  fa-eye"></i></button> ';
        if (full.estado == 'En proceso' || full.estado == 'Pendiente') {
          text += '<button type="button" class="btn btn-warning btn-xs data-toggle="tooltip" data-placement="top" title="Editar" OnClick="Mostrar_Completar('+data+', 1)"><i class="fa fa-edit"></i></button> '
        }        
        if (full.estado == 'En proceso') {
          text += '<button type="button" class="btn btn-success btn-xs data-toggle="tooltip" data-placement="top" title="Cerrar" OnClick="Mostrar_Completar('+data+', 2)"><i class="fa  fa-check"></i></button>';
        }
        return text;
      }
    }
  ],

  //createdRow: function ( row, full, index ) {
    //if (full.estado == 'Sin asignar') {
    // $(row).addClass('danger')
    //}
    //if (full.pivot.estado == 'ACERCANDOSE' ) {
    //  $(row).addClass('warning')
    //}
 //},

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
});

$('#actualizar-tickets').click(function(){
  tabla_tickets.ajax.reload();
})

function Ticket(){
  this.id = $('#ticket_id').val();
  this.activo_id = $('#activo_id').val();
  this.responsable_id = $('#responsable_id').val();
  this.fecha_salida = $('#fecha_salida').val();
  this.hora_salida = $('#hora_salida').val();
  this.fecha_llegada = $('#fecha_llegada').val();
  this.hora_llegada = $('#hora_llegada').val();
  this.pasajeros = $('#pasajeros').val();
  this.ruta = $('#ruta').val();
  this.descripcion = $('#descripcion').val();
  this.horas = $('#horas').val();
  this.kilometros = $('#kilometros').val();
}

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleTicket(){
  $('#field-activo_id').removeClass("has-error");
  $('#field-activo_id .msj-error').html("");

  $('#field-responsable_id').removeClass("has-error");
  $('#field-responsable_id .msj-error').html("");

  $('#field-fecha_salida').removeClass("has-error");
  $('#field-fecha_salida .msj-error').html("");

  $('#field-hora_salida').removeClass("has-error");
  $('#field-hora_salida .msj-error').html("");

  $('#field-fecha_llegada').removeClass("has-error");
  $('#field-fecha_llegada .msj-error').html("");

  $('#field-hora_llegada').removeClass("has-error");
  $('#field-hora_llegada .msj-error').html("");

  $('#field-pasajeros').removeClass("has-error");
  $('#field-pasajeros .msj-error').html("");

  $('#field-ruta').removeClass("has-error");
  $('#field-ruta .msj-error').html("");

  $('#field-descripcion').removeClass("has-error");
  $('#field-descripcion .msj-error').html("");
  
  $('#field-horas').removeClass("has-error");
  $('#field-horas .msj-error').html("");
  $('#field-kilometros').removeClass("has-error");
  $('#field-kilometros .msj-error').html("");
}

function Mostrar_Completar(id,opcion){
  $.ajax({
    url: tickets+'/'+id,
    type: 'GET',
    success: function(res){
      var titulo;
      var contenido;
      if (opcion == 1) {
        $('#ticket_id').val(res.id);
        $('#activo_id').val(res.activo_id).trigger('change');
        $('#responsable_id').val(res.responsable_id).trigger('change');  
        $('#fecha_salida').val(res.fecha_salida);
        $('#hora_salida').val(res.hora_salida);
        $('#fecha_llegada').val(res.fecha_llegada);
        $('#hora_llegada').val(res.hora_llegada);
        $('#pasajeros').val(res.pasajeros);
        $('#ruta').val(res.ruta);
        $('#descripcion').val(res.descripcion);
        $('#modal-ticket').modal('show');        
      }
      if (opcion == 2) {
        titulo = 'Cerrar Ticket Nro. '+res.id;
        contenido = 'Fecha de salida: '+res.sf+'<br/>Hora de la salida: '+res.sh+'<br/><br/>';
        $('#ticket_id').val(res.id);
        if (res.activo.medida != null) {
          $('#horas').val(res.activo.medida.horas);
          $('#kilometros').val(res.activo.medida.kilometros);          
        }
        $('#titulo-completar').html(titulo);
        $('#salida_horarios').html(contenido);
        $('#modal-completar').modal('show');        
      }
      if(opcion == 3){
        titulo = 'Ticket Nro. '+res.id;
        contenido= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="4" style="background: #344563; color: white; text-align: center;" >DETALLES DEL EQUIPO O UNIDAD</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Nro. de Activo</b></td>';
            contenido +='<td ><b>Placa</b></td>';
            contenido +='<td ><b>Marca</b></td>';
            contenido +='<td ><b>Modelo</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.nro_activo+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.placa+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.marca+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.activo.modelo+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr>';
            contenido +='<th colspan="3" style="background: #344563; color: white; text-align: center;" >DETALLES DEL TICKET</th>';
          contenido +='<tr>';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<td ><b>Fecha y hora de salida</b></td>';
            contenido +='<td ><b>Fecha y hora de llegada</b></td>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.sf+' '+res.sh+'</td>';
            contenido +='<td style="vertical-align: middle; text-align: center;">'+res.lf+' '+res.lh+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>Responsable</b></th>';
            contenido +='<th style="text-align: center;"><b>C.I.</b> </th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.empleado.nombre+'</td>';
            contenido +='<td>'+res.empleado.cedula+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>PASAJEROS</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.pasajeros+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>RUTA</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.ruta+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
                contenido+= '<table class="table table-bordered" style="margin-top: 0;margin-bottom: 0;">';
          contenido +='<tr style="background: #EDF2F7; text-align: center;">';
            contenido +='<th style="text-align: center;"><b>NOTAS/DESCRIPCION</th>';
          contenido +='</tr>';
          contenido +='<tr>';
            contenido +='<td>'+res.descripcion+'</td>';
          contenido +='</tr>';
        contenido +='</table>';
        contenido +='<br/> Creado por: '+res.creador;
        if (res.modificador != null ) { 
          contenido +='<br/> Modificado por: ' +res.modificador;
        }
        var botones = '<a href="'+tickets+'/'+res.id+'/pdf" class="btn btn-primary pull-left" title="PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
        botones+='<button type="button" class="btn btn-danger pull-left" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="DeleteReporte('+res.id+')"><i class="fa fa-remove"></i>Eliminar</button>';
        botones+='<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>';
        $('#titulo-modal').html(titulo);
        $('#contenido-modal').html(contenido);
        $('#botones').html(botones);
        $('#modal-ver').modal('show');
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

$('#guardar-ticket').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Ticket();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = tickets;
  }
  else{
    type = 'PUT';
    route = tickets+'/'+data.id;
  }
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyleTicket();
      $('#modal-ticket').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleTicket()
          if(jqXHR.responseJSON.activo_id){         
            $('#field-activo_id').addClass("has-error");
            $('#field-activo_id .msj-error').html(jqXHR.responseJSON.activo_id);
          }          
          if(jqXHR.responseJSON.responsable_id){         
            $('#field-responsable_id').addClass("has-error");
            $('#field-responsable_id .msj-error').html(jqXHR.responseJSON.responsable_id);
          }
          if(jqXHR.responseJSON.fecha_salida){         
            $('#field-fecha_salida').addClass("has-error");
            $('#field-fecha_salida .msj-error').html(jqXHR.responseJSON.fecha_salida);
          }
          if(jqXHR.responseJSON.hora_salida){         
            $('#field-hora_salida').addClass("has-error");
            $('#field-hora_salida .msj-error').html(jqXHR.responseJSON.hora_salida);
          }
          if(jqXHR.responseJSON.fecha_llegada){         
             $('#field-fecha_llegada').addClass("has-error");
            $('#field-fecha_llegada .msj-error').html(jqXHR.responseJSON.fecha_llegada);
          }
          if(jqXHR.responseJSON.hora_llegada){         
            $('#field-hora_llegada').addClass("has-error");
            $('#field-hora_llegada .msj-error').html(jqXHR.responseJSON.hora_llegada);
          }
          if(jqXHR.responseJSON.pasajeros){         
            $('#field-pasajeros').addClass("has-error");
            $('#field-pasajeros .msj-error').html(jqXHR.responseJSON.pasajeros);
          }
          if(jqXHR.responseJSON.ruta){         
            $('#field-ruta').addClass("has-error");
            $('#field-ruta .msj-error').html(jqXHR.responseJSON.ruta);
          }
          if(jqXHR.responseJSON.descripcion){         
            $('#field-descripcion').addClass("has-error");
            $('#field-descripcion .msj-error').html(jqXHR.responseJSON.descripcion);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

$('#completar-ticket').click(function(){
  var btn = this;
  starLoad(btn);
  var data = new Ticket();
  var type = "PUT";
  var route = tickets+'/completar/'+data.id;
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyleTicket();
      $('#modal-completar').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleTicket()
          if(jqXHR.responseJSON.fecha_llegada){         
             $('#field-fecha_llegada').addClass("has-error");
            $('#field-fecha_llegada .msj-error').html(jqXHR.responseJSON.fecha_llegada);
          }
          if(jqXHR.responseJSON.hora_llegada){         
            $('#field-hora_llegada').addClass("has-error");
            $('#field-hora_llegada .msj-error').html(jqXHR.responseJSON.hora_llegada);
          }
          if(jqXHR.responseJSON.horas){         
            $('#field-horas').addClass("has-error");
            $('#field-horas .msj-error').html(jqXHR.responseJSON.horas);
          } 
          if(jqXHR.responseJSON.kilometros){         
            $('#field-kilometros').addClass("has-error");
            $('#field-kilometros .msj-error').html(jqXHR.responseJSON.kilometros);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Verifique si la hora y fecha de llegada son validas. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});


$('#modal-ticket').on('hidden.bs.modal', function (e) 
{ 
  $('#form-ticket')[0].reset();
  $('#ticket_id').val(null);
  $('#activo_id').val(null).trigger('change'); 
  $('#responsable_id').val(null).trigger('change'); 
  $('#fecha_salida').val(null);
  $('#hora_salida').val(null);
  $('#pasajeros').val(null);
  $('#ruta').val(null);
  $('#descripcion').val(null);
  removeStyleTicket();
  tabla_tickets.ajax.reload();
});

$('#modal-completar').on('hidden.bs.modal', function (e) 
{ 
  $('#form-completar')[0].reset();
  $('#ticket_id').val('');
  $('#fecha_llegada').val(null);
  $('#hora_llegada').val(null);
  removeStyleTicket();
  tabla_tickets.ajax.reload();
});