var RUTA_SITIOS = ruta+'/sitios';
var tabla_sitios = $("#tabla-sitios").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_SITIOS+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'nombre',name: 'nombre'},
	{ data: 'padre', name: 'padre'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showSitio('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteSitio('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


loadCatalago();
function loadCatalago(){
	$.getJSON(RUTA_SITIOS+'/padres',function(data){
		$("#sitio_padre").fadeIn(1000).html("");
		$("#sitio_padre").append('<option value="">Seleccione</option>');
		$("#sitio_padre").append('<option value="null">Ninguna</option>');

		for (var i = 0; i < data.length ; i++) {
			$("#sitio_padre").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}
	});
	
}


$('#actualizar-sitios').click(function(){
	tabla_sitios.ajax.reload();
})

function Sitio(){
	this.id = $('#sitio_id').val();
	this.nombre = $('#sitio_nombre').val();
	this.padre = $('#sitio_padre').val();
}

function removeStyleSitio(){
	$('#field-nombre-sitio').removeClass("has-error");
	$('#field-nombre-sitio .msj-error').html("");
	$('#field-diminutivo-sitio').removeClass("has-error");
	$('#field-diminutivo-sitio .msj-error').html("");
}

$('#guardar-sitio').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Sitio();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_SITIOS
	}else{
		type = 'PUT';
		route = RUTA_SITIOS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleSitio();
			loadCatalago();
			$('#modal-sitios').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleSitio()
				if(jqXHR.responseJSON.sitio){
					$('#field-nombre-sitio').addClass("has-error");
					$('#field-nombre-sitio .msj-error').html(jqXHR.responseJSON.sitio)
				}
				if(jqXHR.responseJSON.diminutivo){
					$('#field-diminutivo-sitio').addClass("has-error");
					$('#field-diminutivo-sitio .msj-error').html(jqXHR.responseJSON.diminutivo)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-sitios').on('hidden.bs.modal', function (e) 
{
	tabla_sitios.ajax.reload();
	$('#form-sitio')[0].reset();
	$('#sitio_id').val('');
	removeStyleSitio();
	loadCatalago();
}); 



function showSitio(id){
	$.ajax({
		url: RUTA_SITIOS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#sitio_id').val(res.id);
			$('#sitio_nombre').val(res.nombre);
			$('#sitio_padre').val(res.padre);
			$('#modal-sitios').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos del Departamento/Gerencia. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteSitio(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancelar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_SITIOS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_sitios.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar el Departamento/Gerencia. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Departameno/Gerencia se ha eliminado exitosamente',
			'success'
			);
	});
}