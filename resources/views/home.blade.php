@extends('templates.master')

@section('title', 'SIAF')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('new')
  <div class="pull-right">
  Crear >> <a href="{{route('ordenes.create')}}"> Orden de Trabajo</a> | <a href="{{route('reportes.create')}}"> Reporte de fallas</a> 
  </div><br />
@endsection  

@section('contenido')
{{ csrf_field() }}
<div class="nav-tabs-custom  tab-success">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1" data-toggle="tab">
                  <div class="small-box bg-aqua" style="width: 143px; height: 135px">
                    <div class="inner">
                        <h3>{{$activos->count()}}</h3>
                        Mantenimientos
                        <p style="font-size: 10px;">
                            {{$pendiente }} pendientes | {{$acercandose}} acercandose
                        </p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-android-car"></i>
                    </div>
                  </div>
                </a>
            </li>
            <li>
                <a href="#tab_2" data-toggle="tab">
                    <div class="small-box bg-green" style="width: 143px; height: 135px">
                        <div class="inner">
                          <h3>{{$ordenes->count()}}</h3>

                          Ordenes de Trabajo
                          <p style="font-size: 10px;">
                          {{$ordenes->where('estado_orden','abierta')->count()}} abiertas | 
                          {{$ordenes->where('estado_orden','en progreso')->count()}} en proceso | 
                          {{$ordenes->where('estado_orden','detenida')->count()}} detenidas </p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-social-buffer"></i>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#tab_3" data-toggle="tab">
                    <div class="small-box bg-red" style="width: 143px; height: 135px">
                        <div class="inner">
                          <h3>{{$reportes->count()}}</h3>

                          Reportes de fallas <br /> sin asignar
                        </div>
                        <div class="icon">
                          <i class="ion ion-clipboard"></i>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#tab_5" data-toggle="tab">
                    <div class="small-box bg-maroon" style="width: 143px; height: 135px">
                        <div class="inner">
                          <h3 id="recargar_solicitudes">{{$solicitudes->count()}}</h3>

                          Solicitudes <br />sin atender
                        </div>
                        <div class="icon">
                          <i class="fa fa-pencil-square-o"></i>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#tab_6" data-toggle="tab">
                    <div class="small-box bg-purple" style="width: 143px; height: 135px">
                        <div class="inner">
                          <h3>{{$viajes->count()}}</h3>

                          Viajes
                          <p style="font-size: 10px;" id="count1">
                          {{$viajes->where('estado','Programado')->count()}} Programados | 
                          {{$viajes->where('estado','En proceso')->count()}} En proceso |
                          {{$viajes->where('estado','Sin asignar')->count()}} Sin asignar
                          </p>                          
                        </div>
                        <div class="icon">
                          <i class="fa fa-road"></i>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#tab_4" data-toggle="tab">
                    <div class="small-box bg-yellow" style="width: 143px; height: 135px">
                        <div class="inner">
                          <h3>{{$articulos->count()}}</h3>

                          Inventario con <br />bajo stock
                        </div>
                        <div class="icon">
                          <i class="ion ion-grid"></i>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <table class="table  table-striped" id="tabla_mantenimientos" >
                    <thead >
                        <th style="width: 400px">Unidad/Equipo</th>
                        <th > Prioridad</th>
                        <th > Tareas Pendientes</th>
                        <th style="width: 50px"> </th>
                    </thead>
                    <tbody >
                        @foreach($activos as $activo)
                        <tr id="row.1.{{$activo->id}}">
                            <td style="width: 350px">
                              <a href="{{route('activos.show',$activo->id)}}">
                              {{$activo->unidad}} {{$activo->placa}} {{$activo->marca}} {{$activo->modelo}} { {{$activo->nro_activo}} }</a></td>
                            <td style="width: 50px">{{$activo->prioridad}}</td>
                            <td>
                                <?php $number = 0; ?>
                                <div style="display:none" id="row.2.{{$activo->id}}" onClick="cambiarDisplay('row.2.{{$activo->id}}','row.3.{{$activo->id}}')">
                                    @foreach($activo->tareas as $tarea)
                                        @if($tarea->pivot->estado != null && $tarea->pivot->orden_id == null )
                                            @if($tarea->pivot->estado == 'PENDIENTE')
                                            <span class="text-danger"><i class="fa fa-fw fa-wrench"></i></span>
                                            @else
                                            <span class="text-warning"><i class="fa fa-fw fa-tachometer"></i></span>
                                            @endif - {{$tarea->nombre}}<br /><?php $number++ ?>
                                        @endif
                                    @endforeach                                        
                                </div>
                                <div style="display ;text-align: center;" id="row.3.{{$activo->id}}" onClick="cambiarDisplay('row.2.{{$activo->id}}','row.3.{{$activo->id}}')">
                                    <span class=" label label-lg label-info btn">{{$number}} Tareas</span>
                                </div>
                            </td>
                            <td>
                                <a class="btn btn-default pull-right" href="{{route('activo-orden.nuevo',$activo->id)}}"><span class="text-success"><i class="fa fa-plus"></i></span> Crear Orden</a>
                            </td>
                        </tr >
                        @endforeach                            
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane fade " id="tab_2">
              <table  class="table  table-striped"  id="tabla_ordenes">
                <thead >
                  <tr >
                    <th class="text-center">Nro.</th>
                    <th class="text-center">Fecha de Inicio</th>
                    <th class="text-center">Responsable</th>
                    <th class="text-center">Unidad/Equipo</th>
                    <th class="text-center">Estado</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ordenes as $orden)
                  <tr onClick="ordenVer('{{$orden->id}}')">
                    <td class="text-center">{{$orden->id}}</td>
                    <td class="text-center">{{$orden->inicia}}</td>
                    <td>{{$orden->empleado->nombre}}</td>
                    <td>
                      <a href="{{route('activos.show',$orden->activo->id)}}">
                        {{$orden->activo->unidad}} {{$orden->activo->placa}} {{$orden->activo->marca}} {{$orden->activo->modelo}} { {{$orden->activo->nro_activo}} }
                      </a>
                    </td>
                    <td class="text-center">@if($orden->estado_orden == 'abierta')
                        <span class=" label label-success">Abierta
                        @elseif($orden->estado_orden == 'en progreso')
                        <span class=" label label-primary">En progreso
                        @elseif($orden->estado_orden == 'detenida')
                        <span class=" label label-danger">Detenida
                        @endif </span>
                    </td>
                  </tr >
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane fade " id="tab_3">
              <table  class="table  table-striped"  id="tabla_reportes">
                <thead >
                  <tr >
                    <th class="text-center">ID</th>
                    <th class="text-center">Unidad/Equipo</th>
                    <th class="text-center">Reportado por</th>                  
                    <th class="text-center">Fecha</th>
                    <th class="text-center"> </th>                     
                    <th class="text-center"> </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($reportes as $reporte)
                  <tr>
                    <td class="text-center">{{$reporte->id}}</td> 
                    <td>
                      <a href="{{route('activos.show',$reporte->activo->id)}}">
                        {{$reporte->activo->unidad}} {{$reporte->activo->placa}} {{$reporte->activo->marca}} {{$reporte->activo->modelo}} { {{$reporte->activo->nro_activo}} }
                      </a>
                    </td>
                    <td >{{$reporte->informador}}</td>
                    <td class="text-center">{{$reporte->fecha}}</td>
                    <td class="text-center"><span class="label label-warning label-lg">Asignar Orden<a href="{{route('activo-reporte.orden', $reporte->id)}}" class="btn btn-default btn-xs " title="Asignar" ><i class="fa fa-pencil-square-o"></i></a> </span></td>
                    <td class="text-center">
                      <button type="button" class="btn btn-info btn-xs data-toggle="tooltip" data-placement="top" title="Ver" OnClick="Mostrar({{$reporte->id}})"><i class="fa fa-eye"></i></button>
                    </td>
                  </tr >
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane fade " id="tab_4">
              <table  class="table  table-striped"  id="tabla_lowstock">
                <thead >
                  <tr >
                    <th class="text-center">Nro. de Articulo</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Cantidad</th>
                    <th class="text-center">Categoría</th>
                    <th class="text-center">Fabricante</th>
                  </tr>
                </thead>
                <tbody>
                      @foreach($articulos as $articulo)
                      <tr onClick="BajoStock('{{$articulo->id}}')">
                        <td class="text-center">{{$articulo->nro_activo}}</td>
                        <td class="text-center">{{$articulo->nombre}}</td>
                        <td class="text-center">{{$articulo->cantidad}}</td>
                        <td class="text-center">{{$articulo->categoria}}</td>
                        <td class="text-center">{{$articulo->fabricante}}</td>
                      </tr >
                      @endforeach
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade " id="tab_5">
              <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-solicitudes"><i class="fa fa-repeat"></i></a>
              </br>
              </br>
              <table class="table table-bordered table-hover table-striped" id="tabla-solicitudes" width="100%" >
                <thead style="">
                  <tr>
                    <th>
                        <input name="select_all" value="1" id="select-all-solicitudes" type="checkbox">
                    </th>
                    <th>Nro.</th>
                    <th>Origen</th> 
                    <th>Destino</th> 
                    <th>Solicitante</th> 
                    <th>Aprobado por</th>
                    <th>Detalles</th> 
                  </tr>
                </thead>
              </table>
              <button class="btn btn-info btn-sm" id="btn-tomar-select"><i class="fa fa-plus"></i>NUEVO VIAJE</button>
              <button class="btn btn-warning btn-sm " id="btn-asignar-select"><i class="fa fa-mail-forward"></i> ASIGNAR </button>
              <button class="btn btn-danger btn-sm pull-right" id="btn-rechazar-select"><i class="fa fa-close"></i> RECHAZAR</button>
            </div>
            <div class="tab-pane fade " id="tab_6">
              <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-viajes"><i class="fa fa-repeat"></i></a></br>
              </br>
              <table class="table table-bordered table-hover table-striped" id="tabla-viajes" width="100%" >
                <thead style="">
                  <tr>
                    <th>ID</th>
                    <th>Unidad/Equipo</th>
                    <th>Chofer Asignado</th>
                    <th>Origen</th>
                    <th>Fecha y hora de Salida</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>
              </table> 
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
</div>

@include('dashboard.activos.reportes.archivos')

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
  function starLoad(btn){
    $(btn).button('loading');
    $('.load-ajax').addClass('overlay');
    $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
  } 

  function endLoad(btn){
    $(btn).button('reset');
    $('.load-ajax').removeClass('overlay');
    $('.load-ajax').fadeIn(1000).html("");
  }

  $(document).ready(function() {
      $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
          // var target = $(e.target).attr("href"); // activated tab
          // alert (target);
          $($.fn.dataTable.tables( true ) ).css('width', '100%');
          $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
      } ); 
  }); 

</script>
<script src="{{asset('public/js/home.js')}}"></script>

@endsection

