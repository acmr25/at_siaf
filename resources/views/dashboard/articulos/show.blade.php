@extends('templates.master')

@section('title', 'Inventario')

@section('css')
<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('titulo_modulo', $articulo->nro_activo.' '.$articulo->nombre)

@section('contenido')
 
<div class="box box-success"  id="caja">
  <div class="box-header text-center">
    <p class="box-title"> Detalles de {{$articulo->nombre}}</p>
    <div class="pull-right box-tools">
      <a class="dropdown-toggle text-muted" title="Opciones" data-toggle="dropdown" href="#">
        <i class="fa fa-gear "></i>
      </a>
      <ul class="dropdown-menu">
        <li role="presentation"><a role="menuitem"  data-toggle="modal"  title="Editar" data-target="#modal-edit"> <span class="text-warning"><i class="fa fa-pencil"></i></span> Editar</a></li>
        <li role="presentation"><a role="menuitem"  data-toggle="tooltip" data-placement="top" title="Eliminar" OnClick="deleteArticulo('{{$articulo->id}}')"> <span class="text-danger"><i class="fa fa-remove"></i></span> Eliminar</a></li>
      </ul>
    </div>
  </div>
  <div class="box-body" >
    <dl class=""><br>
      <div class="col-sm-3">
        <dt>Nro de activo</dt>
          <dd>{{$articulo->nro_activo}}.</dd>
        <dt>Nombre</dt>
          <dd>{{$articulo->nombre}}.</dd>
      </div>
      <div class="col-sm-3">
        <dt>Fabricante o Marca</dt>
          <dd>{{$articulo->fabricante}}.</dd>
        <dt>Categoría</dt>
          <dd>{{$articulo->categoria}}.</dd>
      </div>
      <div class="col-sm-2">
        <dt>Cantidad en mano</dt>
          <dd>{{$articulo->cantidad}}  {{$articulo->unidad}}</dd>
        <dt>Punto de pedido</dt>
          <dd>{{$articulo->lowstock}}  {{$articulo->unidad}}</dd>
      </div>
      <div class="col-sm-4">
        <dt>Descripción</dt>
          <dd><p>{{$articulo->descripcion}}.</p></dd>
      </div>  
    </dl>            
  </div>
</div>


    <div class="box box-success">
      <div class="box-header text-center">
        <p class="box-title"> Movimientos de {{$articulo->nombre}}</p>
      </div>
      <div class="box-body" >
        <button type="button" class="btn btn-default pull-left" data-toggle="modal" title="Realizar Movimiento" data-target="#modal-movimiento">
          <span class="text-success"><i class="fa fa-plus"></i></span> Realizar Movimiento
        </button>
        <div class="pull-right"><a href='javascript:void(0)' class="btn btn-default "  id="actualizar-movimientos"><i class="fa fa-repeat"></i></a></div>
        <br>
        <hr>
        <table class="table table-bordered table-hover" id="movimientos" width="100%" align="center">
          <thead style="">
            <tr>
              <th style="width: 50px">Cantidad</th>
              <th style="width: 140px">Fecha</th>
              <th>Razón</th>
              <th>Usuario</th>
              <th>Unidad/Equipo</th>
              <th style="width: 140px">Nro. de Orden</th>   
            </tr>            
          </thead>
        </table>
        <div style="border-style:solid; border-width:1px;border-color: #E5E8E8;  text-align: center; font-family: Lucida Sans Unicode; color: #7B7D7D;">
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-info"><i class="fa fa-plus"></i></span><br>
            Entrada de Articulo            
          </div>
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-danger"><i class="fa fa-minus"></i></span><br>
           Salida de Articulo            
          </div>           
      </div> 
      </div>
    </div>
  


@include('dashboard.articulos.edit')
@include('dashboard.movimientos.movimiento')

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script src="{{asset('public/js/articulo_edit.js')}}"></script>


<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection