<!-- Modal -->
<div class="modal fade" id="modal-edit" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-articulo']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario de Artículos</h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', $articulo->id , ['id'=>'id_articulo']) !!}

        <div class="row">
          <div class="form-group  col-sm-6" id="field-nro_activo-articulo">
            {!! Form::label('nro_activo', 'Nro. de Activo', ['class' => 'control-label']) !!}<span style="color:red;"> *</span>
              {!!Form::text('nro_activo', $articulo->nro_activo , ['id'=>'nro_activo-articulo', 'class'=>'form-control','placeholder'=>'Código o Nro de Activo','required'])!!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group  col-sm-6" id="field-nombre-articulo">
            {!! Form::label('nombre', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!!Form::text('nombre', $articulo->nombre, ['id'=>'nombre-articulo', 'class'=>'form-control','placeholder'=>'Nombre del articulo','required'])!!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-sm-6" id="field-fabricante-articulo">
            {!! Form::label('fabricante', 'Fabricante o Marca:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('fabricante',$fabricantes , $articulo->fabricante, ['id' => 'fabricante-articulo', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-categoria-articulo">
            {!! Form::label('categoria', 'Categoría',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('categoria', $categorias , $articulo->categoria, ['id' => 'categoria-articulo', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-6" id="field-unidad-articulo">
            {!! Form::label('unidad', 'Unidad/Medida:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('unidad',$unidades , $articulo->unidad, ['id' => 'unidad-articulo', 'class' => 'form-control select2','required' => 'required', 'placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-lowstock-articulo">
            {!! Form::label('lowstock', 'Punto de pedido:',['class'=>'control-label']) !!}
            {!! Form::number('lowstock', $articulo->lowstock, ['id' => 'lowstock-articulo','class' => 'form-control','min'=>0]) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>          
        </div>
        <div class="row">
          <div class="form-group col-sm-12" id="field-descripcion-articulo">
            {!! Form::label('descripcion', 'Descripción:',['class'=>'control-label']) !!}
            {!! Form::textarea('descripcion', $articulo->descripcion, ['id' => 'descripcion-articulo','class' => 'form-control ', 'required' => 'required', 'rows'=>3]) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="col-sm-offset-9">
          <span style="color:red;"> *</span><small class="text-muted"> Campos Requeridos </small><span style="color:red;"> *</span>
        </div>
      </div>

      <div class="modal-footer">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-articulo" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

