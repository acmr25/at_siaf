@extends('templates.master')

@section('title', 'Inventario')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">


@endsection

@section('contenido')

<div class="nav-tabs-custom tab-success">
  <ul class="nav nav-tabs ">
    <li class="active"><a href="#tab-articulos" data-toggle="tab"><b>Lista de Artículos</b></a></li>
    <li ><a href="#tab-movimientos" data-toggle="tab"><b>Lista de Movimientos</b></a></li>
  
  </ul>

  <div class="tab-content">
    <div class="tab-pane fade in active " id="tab-articulos"><!-- DETALLES DE LA UNIDAD -->
      <div class="button">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-articulo">
          <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Artículo
        </button>
        <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-articulo"><i class="fa fa-repeat"></i> Actualizar tabla</a>
      </div><hr>
      <div class="table">
        <table class="table table-bordered table-hover" id="articulos" width="100%" align="center">
          <thead class="text-center">
            <tr>
              <th style="text-align: center; width:8%"><span style="color: white">----</span><span class="glyphicon glyphicon-info-sign" style="font-size: 20px"></span> </th>
              <th>ID</th>
              <th>Nombre</th>
              <th>Cantidad</th>
              <th>Categoría</th>
              <th>Fabricante</th>
            </tr>
          </thead>
        </table>        
      </div>
      <div  style="border-style:solid; border-width:1px;border-color: #E5E8E8; text-align: center; font-family: Lucida Sans Unicode; color: #909497; ">
        <div style="margin: 10px; display: inline-block; " >
          <span class="label label-danger"><i class="fa fa-warning"></i></span><br>Bajo Stock           
        </div>          
      </div>       
    </div>
    <!-- /.tab-pane fade in active -->


    <div class="tab-pane fade" id="tab-movimientos"><!-- TAREAS DE LA UNIDAD -->
      <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-movimientos">
        <i class="fa fa-repeat"></i> Actualizar</a><br><hr>
      <table class="table table-bordered table-hover" id="movimientos" width="100%" align="center">
        <thead >
          <tr>
            <th style="width:15%">Nro. de Articulo</th>
            <th >Nombre</th>
            <th class="hidden">Tipo</th> 
            <th >Cantidad</th>
            <th >Fecha</th>
            <th >Usuario</th>
            <th >Razón</th>
            <th >Unidad/Equipo</th>
            <th >Nro. de Orden</th> 
          </tr>
        </thead>
      </table>
      <div style="border-style:solid; border-width:1px;border-color: #E5E8E8;  text-align: center; font-family: Lucida Sans Unicode; color: #7B7D7D;">
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-info"><i class="fa fa-plus"></i></span><br>
            Entrada de Articulo            
          </div>
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-danger"><i class="fa fa-minus"></i></span><br>
           Salida de Articulo            
          </div>           
      </div>
    </div>
    <!-- /.tab-pane fade  -->

    <!-- /.tab-pane fade  -->
  </div>
  <!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->  



@include('dashboard.articulos.modal')


@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/pdfmake.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/vfs_fonts.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script src="{{asset('public/js/articulo.js')}}"></script>


<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection


