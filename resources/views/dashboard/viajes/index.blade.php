@extends('templates.master')

@section('title', 'V.S. Activos')

@section('css')

@section('titulo_modulo', 'Viajes y Solicitudes Activas de Servicio de Logística Liviana')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('contenido')
  {{ csrf_field() }}
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom tab-success">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"> <b>Solicitudes Aprobadas/Pendientes</b></a></li>
              <li><a href="#tab_2" data-toggle="tab"><b>Solicitudes Sin Asignar/Con viaje Programado</b></a></li>
              <li ><a href="#tab_3" data-toggle="tab"><b>Viajes</b></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-solicitudes"><i class="fa fa-repeat"></i></a>
                </br>
                </br>
                <table class="table table-bordered table-hover table-striped" id="tabla-solicitudes" width="100%" >
                  <thead style="">
                    <tr>
                      <th>
                          <input name="select_all" value="1" id="select-all-solicitudes" type="checkbox">
                      </th>
                      <th>Nro.</th>
                      <th>Origen</th> 
                      <th>Destino</th> 
                      <th>Solicitante</th> 
                      <th>Aprobado por</th>
                      <th>Detalles</th>
                    </tr>
                  </thead>
                </table> 
                <div class="box-footer">
                  <button class="btn btn-info btn-sm" id="btn-tomar-select"><i class="fa fa-plus"></i>NUEVO VIAJE</button>
                  <button class="btn btn-warning btn-sm " id="btn-asignar-select"><i class="fa fa-mail-forward"></i> ASIGNAR </button>
                  <button class="btn btn-danger btn-sm pull-right" id="btn-rechazar-select"><i class="fa fa-close"></i> RECHAZAR</button>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-pendientes"><i class="fa fa-repeat"></i></a>
                </br>
                </br>
                <table class="table table-bordered table-hover table-striped" id="tabla-pendientes" width="100%" >
                  <thead style="">
                    <tr>
                      <th>Nro.</th>
                      <th>Origen</th> 
                      <th>Destino</th> 
                      <th>Solicitante</th> 
                      <th>Aprobado por</th>
                      <th>Detalles</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                </table> 
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-viajes"><i class="fa fa-repeat"></i></a>
                </br>
                </br>
                <table class="table table-bordered table-hover table-striped" id="tabla-viajes" width="100%" >
                  <thead style="">
                    <tr>
                      <th>ID</th>
                      <th>Unidad/Equipo</th>
                      <th>Chofer Asignado</th>
                      <th>Origen</th>
                      <th>Fecha y hora de Salida</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                </table> 
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
  });
  

  $.fn.dataTable.ext.errMode = 'throw';
</script>
<script src="{{asset('public/js/vs_activas.js')}}"></script>

@endsection

