@extends('templates.master')

@section('title', 'V.S. Activos')

@section('css')

@endsection

@section('contenido')

<div class="row">
  <div class="col-md-8">
    <div class="box box-success ">
      <div class="box-header with-border">
        <h3 class="box-title">Viaje Nro. {{$viaje->id}}</h3>
      </div>
      <div class="box-body" > 
        {!! Form::open(['method' => 'PUT', 'route' => ['viajes.completar', $viaje->id], 'class' => 'form-horizontal']) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-md-6 {{ $errors->has('activo_id') ? ' has-error' : '' }}">
              {!! Form::label('activo_id', 'Vehículo a asignar:') !!}
              {!! Form::select('activo_id', $a_opciones, (isset($activo)?$activo->id:null), ['id' => 'activo_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'--Seleccione--', 'disabled']) !!}
              <small class="text-danger">{{ $errors->first('activo_id') }}</small>
          </div>          
          <div class="col-md-6 {{ $errors->has('chofer_id') ? ' has-error' : '' }}">
              {!! Form::label('chofer_id', 'Chofer a asignar:') !!}
              {!! Form::select('chofer_id', $e_opciones, (isset($viaje)?$viaje->chofer_id:null), ['id' => 'chofer_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'--Seleccione--', 'disabled']) !!}
              <small class="text-danger">{{ $errors->first('chofer_id') }}</small>
          </div>
        </div>
        <div class="{{ $errors->has('origen') ? ' has-error' : '' }}">
            {!! Form::label('origen', 'Origen de la salida:') !!}
            {!! Form::text('origen', (isset($viaje)?$viaje->origen:null), ['class' => 'form-control', 'required' => 'required', 'disabled']) !!}
            <small class="text-danger">{{ $errors->first('origen') }}</small>
        </div>
        <div class="row">
          <div class=" col-md-6 {{ $errors->has('fecha_salida') ? ' has-error' : '' }}">
              {!! Form::label('fecha_salida', 'Fecha de Salida:') !!}
              {!! Form::date('fecha_salida', (isset($viaje)?$viaje->fecha_salida:\Carbon\Carbon::now()), ['id' => 'fecha_salida','class' => 'form-control', 'required' => 'required', 'disabled']) !!}
              <small class="text-danger">{{ $errors->first('fecha_salida') }}</small>
          </div>
          <div class=" col-md-6{{ $errors->has('hora_salida') ? ' has-error' : '' }}">
             {!! Form::label('hora_salida', 'Hora:') !!}
             {!! Form::time('hora_salida', (isset($viaje)?$viaje->hora_salida:date('H:i', strtotime("07:00:00"))) , ['id' => 'hora_salida','class' => 'form-control', 'required' => 'required', 'disabled']) !!}
              <small class="text-danger">{{ $errors->first('hora_salida') }}</small>
          </div>          
        </div>
        <div class="{{ $errors->has('destino') ? ' has-error' : '' }}">
            {!! Form::label('destino', 'Destino de la salida:') !!}
            {!! Form::text('destino', (isset($viaje)?$viaje->destino:null), ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('destino') }}</small>
        </div>
        <div class="row">
          <div class=" col-md-6 {{ $errors->has('fecha_llegada') ? ' has-error' : '' }}">
              {!! Form::label('fecha_llegada', 'Fecha de llegada:') !!}
              {!! Form::date('fecha_llegada', \Carbon\Carbon::now(), ['id' => 'fecha_llegada','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('fecha_llegada') }}</small>
          </div>
          <div class=" col-md-6{{ $errors->has('hora_llegada') ? ' has-error' : '' }}">
             {!! Form::label('hora_llegada', 'Hora de llegada:') !!}
             {!! Form::time('hora_llegada', \Carbon\Carbon::now()->format('H:i'), ['id' => 'hora_llegada','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('hora_llegada') }}</small>
          </div>          
        </div>
        <div class="{{ $errors->has('observaciones') ? ' has-error' : '' }}">
            {!! Form::label('observaciones', 'Observaciones:') !!}
            {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'required' => 'required', 'rows'=>2]) !!}
            <small class="text-danger">{{ $errors->first('observaciones') }}</small>
        </div>
        @if($activo->medida != null )
        <div class="row">
          @if($activo->medida->horas != null)
            <div class=" col-md-6 {{ $errors->has('horas') ? ' has-error' : '' }}">
                {!! Form::label('horas', 'Horas (lectura actual):') !!}
                {!! Form::number('horas', $activo->medida->horas, ['class' => 'form-control', 'min'=>$activo->medida->horas, 'max'=>999999, 'required' => 'required',]) !!}
                <small class="text-danger">{{ $errors->first('horas') }}</small>
            </div>
          @endif
          @if($activo->medida->kilometros != null)
            <div class=" col-md-6{{ $errors->has('kilometros') ? ' has-error' : '' }}">
               {!! Form::label('kilometros', 'Kilometros (lectura actual):') !!}
               {!! Form::number('kilometros', $activo->medida->kilometros, ['class' => 'form-control', 'min'=>$activo->medida->kilometros , 'max'=>999999, 'required' => 'required',]) !!}
                <small class="text-danger">{{ $errors->first('kilometros') }}</small>
            </div>  
          @endif        
        </div>
        @else
            <span class="text-muted"><center><b> El Vehiculo no posee medidas (horas y kilometros) que actualizar.</b></center></span>       
        @endif        
      </div>  
      <div class="box-footer with-border">
        <a href="{{ route('viajes.index') }}" class="btn btn-info ">Cancelar</a>
        {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}
      </div>       
    </div>      
  </div>
  <div class="col-md-4">
    <div class="box box-success ">
      <div class="box-header with-border">
        <h3 class="box-title">Solicitudes Asociadas</h3>
      </div>
      <div class="box-body" > 
        <table class="table table-bordered table-hover table-striped" width="100%" >
          <thead style="">
            <tr>
              <th>Marcar Pendiente</th>
              <th>Nro.</th>
              <th>Solicitud</th>
            </tr>
          </thead>
          <tbody>
            @foreach($viaje->solicitudes as $solicitud)
            <tr >
              <td style="vertical-align: middle; text-align: center;"><input type="checkbox" value="{{$solicitud->id}}" name="pendiente[]"></td>
              <td style="vertical-align: middle; text-align: center;">{{$solicitud->id}}</td>
              <td >
              <p>
                <b>Solicitante: </b>{{$solicitud->solicitante->nombre}}</br>
                <b>Nro. de Pasajeros: </b>{{$solicitud->nro_pasajeros}}</br>
                <b>Origen: </b>{{$solicitud->origen}}</br>
                <b>Destino: </b>{{$solicitud->destino}}</br>
                <b>Fecha y hora de salida: </b>{{ date('h:i A', strtotime($solicitud->hora_salida))}} {{date('d-m-Y', strtotime($solicitud->fecha_salida))}}
                @if ($solicitud->hora_regreso != null)
                  </br><b>Fecha y hora de retorno: </b>{{ date('h:i A', strtotime($solicitud->hora_regreso))}} {{date('d-m-Y', strtotime($solicitud->fecha_regreso))}}                  
                @endif
              </p>
              </td>
            </tr>
            @endforeach  
          </tbody> 
        </table>
        {!! Form::close() !!}
      </div>  
    </div>      
  </div>
</div>

@endsection

@section('js')

@endsection

