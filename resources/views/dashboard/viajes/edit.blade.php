@extends('templates.master')

@section('title', 'V.S. Activos')

@section('css')
<!-- Select2 -->  
<link rel="stylesheet" href="{{ asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')

<div class="row">
  <div class="col-md-6">
    <div class="box box-success ">
      <div class="box-header with-border">
        <h3 class="box-title">Viaje Nro. {{$viaje->id}}</h3>
        @if ($viaje->estado != 'Completado')
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteViaje('{{$viaje->id}}')"><i class="fa fa-remove"></i></button>        
          </div>
        @endif  
      </div>
      <div class="box-body" > 
        {!! Form::open(['method' => 'PUT', 'route' => ['viajes.update', $viaje->id], 'class' => 'form-horizontal']) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class="{{ $errors->has('activo_id') ? ' has-error' : '' }}">
            {!! Form::label('activo_id', 'Vehículo a asignar:') !!}
            {!! Form::select('activo_id', $a_opciones, (isset($unidad)?$unidad->id:null), ['id' => 'activo_id', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'--Seleccione--']) !!}
            <small class="text-danger">{{ $errors->first('activo_id') }}</small>
        </div>
        
        <div class="{{ $errors->has('chofer_id') ? ' has-error' : '' }}">
            {!! Form::label('chofer_id', 'Chofer a asignar:') !!}
            {!! Form::select('chofer_id', $e_opciones, (isset($viaje)?$viaje->chofer_id:null), ['id' => 'chofer_id', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'--Seleccione--']) !!}
            <small class="text-danger">{{ $errors->first('chofer_id') }}</small>
        </div>
        <div class="{{ $errors->has('origen') ? ' has-error' : '' }}">
            {!! Form::label('origen', 'Origen de la salida:') !!}
            {!! Form::text('origen', (isset($viaje)?$viaje->origen:null), ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('origen') }}</small>
        </div>
        <div class="row">
          <div class=" col-md-6 {{ $errors->has('fecha_salida') ? ' has-error' : '' }}">
              {!! Form::label('fecha_salida', 'Fecha de Salida:') !!}
              {!! Form::date('fecha_salida', (isset($viaje->fecha_salida)?$viaje->fecha_salida:\Carbon\Carbon::now()), ['id' => 'fecha_salida','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('fecha_salida') }}</small>
          </div>
          <div class=" col-md-6{{ $errors->has('hora_salida') ? ' has-error' : '' }}">
             {!! Form::label('hora_salida', 'Hora:') !!}
             {!! Form::time('hora_salida', (isset($viaje->hora_salida)?$viaje->hora_salida:\Carbon\Carbon::now()->format('H:i')) , ['id' => 'hora_salida','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('hora_salida') }}</small>
          </div>          
        </div>
      </div>  
      <div class="box-footer with-border">

            <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>
            {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     

      </div>       
    </div>      
  </div>
  <div class="col-md-6">
    <div class="box box-success ">
      <div class="box-header with-border">
        <h3 class="box-title">Solicitudes Asociadas</h3>
      </div>
      <div class="box-body" > 
        <table class="table table-bordered table-hover table-striped" width="100%" >
          <thead style="">
            <tr>
              <th>Eliminar</th>
              <th>Nro.</th>
              <th>Solicitud</th>
            </tr>
          </thead>
          <tbody>
            @foreach($viaje->solicitudes as $solicitud)
            <tr >
              <td style="vertical-align: middle; text-align: center;"><input type="checkbox" value="{{$solicitud->id}}" name="eliminar[]"></td>
              <td style="vertical-align: middle; text-align: center;">{{$solicitud->id}}</td>
              <td >
              <p>
                <b>Solicitante: </b>{{$solicitud->solicitante->nombre}}</br>
                <b>Nro. de Pasajeros: </b>{{$solicitud->nro_pasajeros}}</br>
                <b>Origen: </b>{{$solicitud->origen}}</br>
                <b>Destino: </b>{{$solicitud->destino}}</br>
                <b>Fecha y hora de salida: </b>{{ date('h:i A', strtotime($solicitud->hora_salida))}} {{date('d-m-Y', strtotime($solicitud->fecha_salida))}}
                @if ($solicitud->hora_regreso != null)
                  </br><b>Fecha y hora de retorno: </b>{{ date('h:i A', strtotime($solicitud->hora_regreso))}} {{date('d-m-Y', strtotime($solicitud->fecha_regreso))}}                  
                @endif
              </p>
              </td>
            </tr>
            @endforeach  
          </tbody> 
        </table>
        {!! Form::close() !!}
      </div>  
    </div>      
  </div>
</div>

@endsection

@section('js')
<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>
<script type="text/javascript">
  $(".select2").select2({
    placeholder: "--Seleccione--",
    width:'100%',
    language: "es",
  })

  function deleteViaje(id){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida! El vehiculo y las solicitudes asociadas tendras sus status anterior.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancalar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta+'/viajes/'+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            window.location.href = ruta+'/viajes';
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar el rol. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El Viaje se ha eliminado exitosamente',
      'success'
      );
  });
}
</script>
@endsection

