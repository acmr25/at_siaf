@extends('templates.master')

@section('title', 'V.S. Finalizadas')

@section('css')

@section('titulo_modulo', 'Viajes y Solicitudes Finalizadas de Servicio de Logística Liviana')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('contenido')
<!-- Custom Tabs -->
<div class="nav-tabs-custom tab-success">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab"> <b>Solicitudes Completadas/Rechazadas</b></a></li>
    <li ><a href="#tab_2" data-toggle="tab"><b>Viajes Completados</b></a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-solicitudes"><i class="fa fa-repeat"></i></a>
      </br>
      </br>
      <table class="table table-bordered table-hover table-striped" id="tabla-solicitudes" width="100%" >
        <thead style="">
          <tr>
            <th>Nro.</th>
            <th>Solicitud</th> 
            <th>Viajes Asociados</th> 
            <th>Status</th>
          </tr>
        </thead>
      </table> 
      <div class="box-footer">
        <button class="btn btn-info btn-sm" id="btn-tomar-select"><i class="fa fa-plus"></i>NUEVO VIAJE</button>
        <button class="btn btn-warning btn-sm " id="btn-asignar-select"><i class="fa fa-mail-forward"></i> ASIGNAR </button>
        <button class="btn btn-danger btn-sm pull-right" id="btn-rechazar-select"><i class="fa fa-close"></i> RECHAZAR</button>
      </div>
    </div>
    <!-- /.tab-pane -->
    <div class="tab-pane" id="tab_2">
      <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-viajes"><i class="fa fa-repeat"></i></a>
      </br>
      </br>
      <table class="table table-bordered table-hover table-striped" id="tabla-viajes" width="100%" >
        <thead style="">
          <tr>
            <th>ID</th>
            <th>Unidad/Equipo</th>
            <th>Chofer Asignado</th>
            <th>Origen</th>
            <th>Destino</th>
            <th>Fecha y hora de Salida</th>
            <th>Fecha y hora de Llegada</th>
            <th>Solicitudes Asociadas</th> 
          </tr>
        </thead>
      </table> 
    </div>
    <!-- /.tab-pane -->
  </div>
  <!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
  });
  

  $.fn.dataTable.ext.errMode = 'throw';
</script>
<script src="{{asset('public/js/vs_inactivas.js')}}"></script>

@endsection

