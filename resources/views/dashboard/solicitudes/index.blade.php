@extends('templates.master')

@section('title', 'Solicitudes')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('contenido')

<!-- Custom Tabs -->
<div class="nav-tabs-custom tab-success">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab"> <b>Mis Solicitudes</b></a></li>
    @if(Auth::user()->rol_id==2)
      <li><a href="#tab_2" data-toggle="tab"><b>Solicitudes de Otros</b></a></li>
    @endif
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <a class="btn btn-default" href='{{ route('solicitudes.create') }}'>
        <span class="text-success"><i class="fa fa-plus"></i></span> Nueva Solicitud
      </a>
      <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-solicitudes"><i class="fa fa-repeat"></i></a>
      </br>
      </br>
      <table class="table table-bordered table-hover table-striped " id="tabla-solicitudes" width="100%" >
        <thead style="">
          <tr>
            <th>Nro.</th>
            <th>Origen</th> 
            <th>Destino</th> 
            <th>Detalles</th> 
            <th>Status</th>
            <th>Calificación</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
    <!-- /.tab-pane -->
    @if(Auth::user()->rol_id==2)
    <div class="tab-pane" id="tab_2">
      <a href='javascript:void(0)' class="btn btn-default pull-right btn-sm"  id="actualizar-solicitudes-supervisor"><i class="fa fa-repeat"></i></a></br></br>
      <table class="table table-bordered table-hover table-striped " id="tabla-solicitudes-supervisor" width="100%" >
        <thead style="">
          <tr>
            <th>Nro.</th>
            <th>Origen</th> 
            <th>Destino</th> 
            <th>Detalles</th> 
            <th>Status</th>
            <th>Calificación</th>
          </tr>
        </thead>
      </table>
    </div>
    <!-- /.tab-pane -->
    @endif
  </div>
  <!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->  

 
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('public/js/solicitudes.js')}}"></script>
@if(Auth::user()->rol_id==2)
  <script src="{{asset('public/js/solicitudes_supervisor.js')}}"></script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
  });

  $.fn.dataTable.ext.errMode = 'throw';
</script>
@endsection

