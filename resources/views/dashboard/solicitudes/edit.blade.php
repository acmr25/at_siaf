@extends('templates.master')

@section('title', 'Solicitud Nro.'.$solicitud->id)

@section('css')
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('titulo_modulo', 'Solicitud Nro.'.$solicitud->id )

@section('new')
  <ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Inicio</a></li>
    <li class="active">Solicitud Nro. {{$solicitud->id }}</li>
  </ol>
@endsection


@section('contenido')

<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <div class="box box-success ">
      <div class="box-header with-border">
        <h3 class="box-title">Formulario de servicio de logística de Unidades Livianas</h3>
      </div>
      <div class="box-body" > 
        {!! Form::open(['method' => 'PUT', 'route' => ['solicitudes.update', $solicitud->id], 'class' => 'form-horizontal']) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        @if(Auth::user()->rol_id == 1) 
        <div class="col-md-6 {{ $errors->has('sup_id') ? ' has-error' : '' }}"  style="padding: 6px 6px">
            {!! Form::label('sup_id', 'Supervisor:') !!}
            {!! Form::select('sup_id',$supervisores, $solicitud->sup_id, ['id' => 'sup_id', 'class' => 'form-control', 'required' => 'required','placeholder'=>'Seleccione']) !!}
            <strong class="text-danger">{{ $errors->first('sup_id') }}</strong>
        </div>
          <div class="col-md-6  {{ $errors->has('nro_pasajeros') ? ' has-error' : '' }}" style="padding: 6px 6px">
        @else
          <div class="col-md-12  {{ $errors->has('nro_pasajeros') ? ' has-error' : '' }}" style="padding: 6px 6px">
        @endif
            {!! Form::label('nro_pasajeros', 'Nro. de pasajeros:') !!}
            {!! Form::number('nro_pasajeros', $solicitud->nro_pasajeros, ['id' => 'nro_pasajeros','class' => 'form-control', 'required' => 'required','min'=>1, 'max'=>4]) !!}
            <strong class="text-danger">{{ $errors->first('nro_pasajeros') }}</strong>
        </div>
        <div class=" {{ $errors->has('ida_vuelta') ? ' has-error' : '' }}" style="padding: 6px 6px">
            <label for="ida_vuelta">
                {!! Form::radio('ida_vuelta', 'ida',  true, ['id' => 'radio_ida','onclick' => 'hide()', 'require'=> 'required']) !!} Ida Solamente
                {!! Form::radio('ida_vuelta', 'vuelta', null, ['id' => 'radio_vuelta','onclick' => 'show()', 'require'=> 'required']) !!} Ida y Vuelta
            </label>
            <strong class="text-danger">{{ $errors->first('ida_vuelta') }}</strong>
        </div>
        <div id="ida" class="col-md-12"  style="padding-bottom: 15px">
          <div class=" col-md-6{{ $errors->has('fecha_salida') ? ' has-error' : '' }}">
              {!! Form::label('fecha_salida', 'Fecha de la Salida:') !!}
              {!! Form::date('fecha_salida', $solicitud->fecha_salida, ['class' => 'form-control', 'required' => 'required']) !!}
              <strong class="text-danger">{{ $errors->first('fecha_salida') }}</strong>
          </div>
          <div class=" col-md-6{{ $errors->has('hora_salida') ? ' has-error' : '' }}">
              {!! Form::label('hora_salida', 'Hora de la Salida') !!}
              {!! Form::time('hora_salida', $solicitud->hora_salida, ['id' => 'hora_salida','class' => 'form-control', 'required' => 'required']) !!}
              <strong class="text-danger">{{ $errors->first('hora_salida') }}</strong>
          </div>
        </div>
        <div id="regreso" hidden="true" class="col-md-6"  style="padding-bottom: 15px">
          <div class=" col-md-6{{ $errors->has('fecha_regreso') ? ' has-error' : '' }}">
              {!! Form::label('fecha_regreso', 'Fecha del Regreso:') !!}
              {!! Form::date('fecha_regreso', $solicitud->fecha_regreso, ['id' => 'fecha_regreso','class' => 'form-control']) !!}
              <strong class="text-danger">{{ $errors->first('fecha_regreso') }}</strong>
          </div>
          <div class=" col-md-6{{ $errors->has('hora_regreso') ? ' has-error' : '' }}">
              {!! Form::label('hora_regreso', 'Hora del Regreso:') !!}
              {!! Form::time('hora_regreso', $solicitud->hora_regreso, ['id' => 'hora_regreso','class' => 'form-control']) !!}
              <strong class="text-danger">{{ $errors->first('hora_regreso') }}</strong>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 {{ $errors->has('origen_padre') ? ' has-error' : '' }}">
              {!! Form::label('origen_padre', 'Estado de Origen:') !!}
              {!! Form::select('origen_padre', $padres, null, ['id' => 'origen_padre', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <strong class="text-danger">{{ $errors->first('origen_padre') }}</strong>
          </div>
          <div class="col-md-3 {{ $errors->has('origen_hijo') ? ' has-error' : '' }}">
              {!! Form::label('origen_hijo', 'Ciudad/Zona de Origen:') !!}
              {!! Form::select('origen_hijo', [], null, ['id' => 'origen_hijo', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <strong class="text-danger">{{ $errors->first('origen_hijo') }}</strong>
          </div>
          <div class=" col-md-6{{ $errors->has('origen') ? ' has-error' : '' }}" >
              {!! Form::label('origen', 'Dirección de Origen:') !!}
              {!! Form::textarea('origen', $solicitud->origen, ['class' => 'form-control', 'required' => 'required','required','rows'=>1, 'style' => 'resize:none']) !!}
              <strong class="text-danger">{{ $errors->first('origen') }}</strong>
          </div>          
        </div>
        <div class="row {{ $errors->has('destino') ? ' has-error' : '' }}">
            <div class="col-md-3 {{ $errors->has('destino_padre') ? ' has-error' : '' }}">
                {!! Form::label('destino_padre', 'Estado de Destino:') !!}
                {!! Form::select('destino_padre', $padres, null, ['id' => 'destino_padre', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
            </div>
            <div class="col-md-3 {{ $errors->has('destino_hijo') ? ' has-error' : '' }}">
                {!! Form::label('destino_hijo', 'Ciudad/Zona de Destino:') !!}
                {!! Form::select('destino_hijo', [], null, ['id' => 'destino_hijo', 'class' => 'form-control select2', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
            </div>          
            <div class="col-md-6" >
                {!! Form::label('destino', 'Dirección de Destino:') !!}
                {!! Form::textarea('destino', $solicitud->destino, ['class' => 'form-control', 'required' => 'required','required','rows'=>1, 'style' => 'resize:none']) !!}
                <strong class="text-danger pull-right">{{ $errors->first('destino') }}</strong> 
            </div>
        </div>
        <div class="{{ $errors->has('motivo') ? ' has-error' : '' }}" style="padding: 6px 6px">
            {!! Form::label('motivo', 'Motivo:') !!}
            {!! Form::textarea('motivo', $solicitud->motivo, ['class' => 'form-control', 'required' => 'required','rows'=>1]) !!}
            <strong class="text-danger">{{ $errors->first('motivo') }}</strong>
        </div>
        <div class="{{ $errors->has('notas') ? ' has-error' : '' }}" style="padding: 6px 6px">
            {!! Form::label('notas', 'Notas:') !!}
            {!! Form::textarea('notas', $solicitud->notas, ['class' => 'form-control', 'required' => 'required','rows'=>1]) !!}
            <strong class="text-danger">{{ $errors->first('notas') }}</strong>
        </div>
      </div>  
      <div class="box-footer with-border">

            <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>
            {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     

      </div>       
        {!! Form::close() !!}
    </div>      
  </div>
</div>
  
@endsection

@section('js')
<script>
  $.getJSON(ruta+"/solicitudes/"+ $('#origen_padre').val() +"/getCiudad", function(jsonData){
  var x = document.getElementById("origen_hijo");
  $.each(jsonData, function(i,data)
    {
    var c = document.createElement("option");
    c.text = data.nombre;
    x.options.add(c, data.id);
  });
});

$.getJSON(ruta+"/solicitudes/"+ $('#destino_padre').val() +"/getCiudad", function(jsonData){
  var x = document.getElementById("destino_hijo");
  $.each(jsonData, function(i,data)
    {
    var c = document.createElement("option");
    c.text = data.nombre;
    x.options.add(c, data.id);
  });
});

$( "#origen_padre" ).change(function() 
{ 
  var select = document.getElementById("origen_hijo"),
  length = select.options.length;
    while(length--){
      select.remove(length);
  }
  $.getJSON(ruta+"/solicitudes/"+ $(this).val() +"/getCiudad", function(jsonData){
    var x = document.getElementById("origen_hijo");
    $.each(jsonData, function(i,data)
      {
      var c = document.createElement("option");
      c.text = data.nombre;
      x.options.add(c, data.id);
    });
  });    
});

$( "#destino_padre" ).change(function() 
{ 
  var select = document.getElementById("destino_hijo"),
  length = select.options.length;
    while(length--){
      select.remove(length);
  }
  $.getJSON(ruta+"/solicitudes/"+ $(this).val() +"/getCiudad", function(jsonData){
    var x = document.getElementById("destino_hijo");
    $.each(jsonData, function(i,data)
      {
      var c = document.createElement("option");
      c.text = data.nombre;
      x.options.add(c, data.id);
    });
  });    
});

if ( $('#fecha_regreso').val() != '') {
  $("#radio_ida").prop("checked", false);
  $("#radio_vuelta").prop("checked", true);
  document.getElementById("ida").classList.remove('col-md-12');
  document.getElementById("ida").classList.add('col-md-6');  
  document.getElementById('regreso').style.display = "block";
  document.getElementById("fecha_regreso").required = true;
  document.getElementById("hora_regreso").required = true;
}

function show(){
  document.getElementById("ida").classList.remove('col-md-12');
  document.getElementById("ida").classList.add('col-md-6');
  document.getElementById('regreso').style.display = "block";
  document.getElementById("fecha_regreso").required = true;
  document.getElementById("hora_regreso").required = true;
}
function hide(){
  $('#fecha_regreso').val(null);
  $('#hora_regreso').val(null);
  document.getElementById("ida").classList.remove('col-md-6');
  document.getElementById("ida").classList.add('col-md-12');
  document.getElementById('regreso').style.display = "none";
  document.getElementById("fecha_regreso").required = false;
  document.getElementById("hora_regreso").required = false;

}
</script>
@endsection