@extends('templates.master')

@section('title', 'Solicitud de Servicio Logística Nro.'. $solicitud->id )

@section('css')

@endsection

@section('titulo_modulo', 'Solicitud de Servicio Logística Nro.'. $solicitud->id )

@section('new')

@endsection


@section('contenido')

<div class="row">
  <div @if(count($solicitud->viajes)>0) class="col-md-8" @else class="col-md-offset-2 col-md-8"@endif>
    <div class="box box-success ">
      <div class="box-header with-border">
        {!! Form::open(['method' => 'DELETE', 'route' => ['solicitudes.destroy', $solicitud->id], 'class' => 'form-horizontal']) !!}
        <h1 class="box-title" style="padding-top: 8px">Solicitud de Servicio Logística Nro. {{$solicitud->id}}</h1>
        @if((Auth::user()->id == $solicitud->solicitante_id && $solicitud->estado=='Por Autorizar') ||
            (Auth::user()->id == $solicitud->solicitante_id && Auth::user()->rol_id==2 && $solicitud->estado=='Aprobado'))
          <div class="pull-right">
            <a href="{{route('solicitudes.edit',$solicitud->id)}}" class="btn btn-warning btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}
          </div>
        {!! Form::close() !!}          
        @elseif(Auth::user()->id == $solicitud->sup_id  && $solicitud->estado=='Por Autorizar')
        <div class="pull-right">        
            <a href="{{route('solicitudes.aprobar',$solicitud->id)}}" class="btn btn-success btn-sm" title="Aprobar"><i class="fa fa-check"></i></a>
            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"  title="Rechazar" data-target="#modal-rechazar">
            <i class="fa fa-remove"></i></button>
        </div>
        @endif
      </div>
      <div class="box-body" >
        @if(Auth::user()->id != $solicitud->solicitante_id)
        <div class="col-md-12" style="padding: 6px 6px;">
          {!! Form::label('solicitante', 'Solicitante:') !!}
          {!! Form::text('solicitante', $solicitud->solicitante->nombre.' '.$solicitud->solicitante->cargo.' de '.$solicitud->solicitante->departamento->nombre, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
        </div> 
        @endif
        @if(Auth::user()->rol_id == 1)
        <div class="col-md-12" style="padding: 6px 6px;">
          {!! Form::label('sup_id', 'Supervisor:') !!}
          {!! Form::text('sup_id', $solicitud->supervisor->nombre.' '.$solicitud->supervisor->cargo.' de '.$solicitud->supervisor->departamento->nombre, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
        </div>
        @endif
        <div class="col-md-6" style="padding: 6px 6px;" id="estado">
          {!! Form::label('estado', 'Estado de la Solicitud:') !!}
          {!! Form::text('estado', $solicitud->estado, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
        </div>
        <div class="col-md-6" style="padding: 6px 6px;">
          {!! Form::label('nro_pasajeros', 'Nro. de pasajeros:') !!}
          {!! Form::number('nro_pasajeros', $solicitud->nro_pasajeros, ['id' => 'nro_pasajeros','class' => 'form-control', 'readonly','min'=>1, 'max'=>4]) !!}
        </div>
        <div class=" col-md-6" style="padding: 6px 6px;">
          {!! Form::label('fecha_salida', 'Fecha de la Salida:') !!}
          {!! Form::date('fecha_salida', $solicitud->fecha_salida, ['class' => 'form-control', 'readonly']) !!}
        </div>
        <div class=" col-md-6" style="padding: 6px 6px;">
          {!! Form::label('hora_salida', 'Hora de la Salida') !!}
          {!! Form::time('hora_salida', $solicitud->hora_salida, ['id' => 'hora_salida','class' => 'form-control', 'readonly']) !!}
        </div>
        @if($solicitud->fecha_regreso != null)
          <div class=" col-md-6" style="padding: 6px 6px;">
            {!! Form::label('fecha_regreso', 'Fecha del Regreso:') !!}
            {!! Form::date('fecha_regreso', $solicitud->fecha_regreso, ['class' => 'form-control', 'readonly']) !!}
          </div>
          <div class=" col-md-6" style="padding: 6px 6px;">
            {!! Form::label('hora_regreso', 'Hora del Regreso:') !!}
            {!! Form::time('hora_regreso', $solicitud->hora_regreso, ['id' => 'hora_regreso','class' => 'form-control', 'readonly']) !!}
          </div>
        @endif          

        <div class="col-md-6" style="padding: 6px 6px;">
          {!! Form::label('origen', 'Origen de la Salida:') !!}
          {!! Form::textarea('origen', $solicitud->origen, ['class' => 'form-control', 'readonly','required','rows'=>1]) !!}            
        </div>
        <div class="col-md-6" style="padding: 6px 6px;">
            {!! Form::label('destino', 'Destino:') !!}
            {!! Form::textarea('destino', $solicitud->destino, ['class' => 'form-control', 'readonly','required','rows'=>1]) !!}
          </div>
        <div style="padding: 6px 6px;">
          {!! Form::label('motivo', 'Motivo:') !!}
          {!! Form::textarea('motivo', $solicitud->motivo, ['class' => 'form-control', 'readonly','rows'=>1]) !!}
        </div>

        <div style="padding: 6px 6px;" id="notas">
          {!! Form::label('notas', 'Notas:') !!}
          {!! Form::textarea('notas', $solicitud->notas, ['class' => 'form-control', 'readonly','rows'=>1]) !!}
        </div>
      </div>  
      <div class="box-footer with-border">
        <a href="{{route('solicitudes.index')}}" class="btn btn-default "><i class="fa fa-fw fa-reply"></i> Regresar</a>
      </div>       
    </div>      
  </div>
  @if(count($solicitud->viajes)>0)
  <div class="col-md-4">
    <div class="box box-success ">
      <div class="box-header with-border">
        Viajes Asignados
      </div>
      <div class="box-body" >
        <table class="table table-bordered table-hover table-striped " id="tabla-solicitudes" width="100%" >
          <thead style="">
            <tr>
              <th>Nro.</th>
              <th>Detalles</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($solicitud->viajes as $viaje)
              @if ($viaje->estado=='Sin asignar')
                <tr><td colspan="2">Sin vehiculo y chofer sin asignar</td></tr>
              @else
                <tr>
                  <td>{{$viaje->id}}</td>
                  <td>Chofer Asignado: {{$viaje->chofer->nombre}} </br>
                    Fecha y hora de Salida: {{$viaje->fecha_salida}} {{date('h:i A', strtotime($viaje->hora_salida))}} </br>
                    Lugar de la salida: {{$viaje->origen}}</br>
                    @if ($viaje->estado == 'Completado')
                      Fecha y hora de llegada: {{$viaje->fecha_llegada}} {{date('h:i A', strtotime($viaje->hora_llegada))}} </br>
                      Lugar de la llegada: {{$viaje->destino}}</br>
                    @endif
                    Condicion del viaje: {{$viaje->estado}}
                  </td>              
                </tr>              
              @endif
            @endforeach
          </tbody>
        </table> 
      </div>
    </div>   

  </div>
  @endif
</div>
@include('dashboard.solicitudes.rechazo')
  
@endsection

@section('js')

<script type="text/javascript">

function Solicitud(){
  this.id = $('#id_solicitud').val();
  this.motivo_rechazo = $('#motivo_rechazo').val();
}
function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 
function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 
// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStylesolicitud(){
  $('#field-observaciones-solicitud').removeClass("has-error");
  $('#field-observaciones-solicitud .msj-error').html("");
}

var RUTA_SOLICITUD = ruta+'/solicitudes/{{$solicitud->id}}';

$('#guardar-solicitud').click(function(){
  var btn = this
  starLoad(btn)
  var data = new Solicitud();
  $.ajax({
    url: RUTA_SOLICITUD+'/rechazar',
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: 'PUT',
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      $('#modal-rechazar').modal('hide');
      setTimeout("location.href= RUTA_SOLICITUD, 2050");
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStylesolicitud()
          if(jqXHR.responseJSON.motivo_rechazo){         
            $('#field-observaciones-solicitud').addClass("has-error");
            $('#field-observaciones-solicitud .msj-error').html(jqXHR.responseJSON.motivo_rechazo);
          }          
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

$('#modal-rechazar').on('hidden.bs.modal', function (e) 
  {
    removeStylesolicitud();
  });



</script>

@endsection