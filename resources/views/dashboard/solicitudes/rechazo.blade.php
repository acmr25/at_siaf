<!-- Modal -->
<div class="modal fade" id="modal-rechazar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open() !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Rechazo de Solicitud de Servicio Logística Nro. {{$solicitud->id}} </h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', $solicitud->id, ['id'=>'id_solicitud']) !!}
        <div class="form-group col-sm-12" id="field-observaciones-solicitud">
          {!! Form::label('motivo_rechazo', 'Motivo del rechazo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
          {!! Form::textarea('motivo_rechazo', null, ['id' => 'motivo_rechazo','class' => 'form-control ', 'required' => 'required', 'rows'=>3]) !!}
          <span>
            <strong class="text-danger msj-error"></strong>
          </span>
        </div>        
        <div class="text-right">
          <span style="color:red;"> *</span><small class="text-muted"> Campos Requeridos </small><span style="color:red;"> *</span>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class=" pull-left btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-solicitud" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>   
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

