<!-- Modal -->
<div class="modal fade" id="modal-empleado" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-empleado']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario de Trabajadores</h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'id_empleado']) !!} 

        <div class="row">
          <div class="form-group  col-sm-6" id="field-cedula-empleado">
            {!! Form::label('cedula', 'Cédula', ['class' => 'control-label']) !!}<span style="color:red;"> *</span>
            {!!Form::text('cedula', null, ['id'=>'cedula-empleado', 'class'=>'form-control numero','placeholder'=>'23.533.432','onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl"])!!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group  col-sm-6" id="field-nombre-empleado">
            {!! Form::label('nombre', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!!Form::text('nombre', null, ['id'=>'nombre-empleado', 'class'=>'form-control','placeholder'=>'Nombre del empleado','required','onkeypress'=>'return soloLetras(event)'])!!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12" id="field-tipo-empleado">
            {!! Form::label('tipo', 'Tipo de empleado:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('tipo', ['Ayudante de Chofer de Equipos Pesados'=>'Ayudante de Chofer de Equipos Pesados','Chofer de Equipos Pesados'=>'Chofer de Equipos Pesados','Chofer de Vehículos Livianos'=>'Chofer de Vehículos Livianos','Mecánico'=>'Mecánico'] , null, ['id' => 'tipo-empleado', 'class' => 'form-control','required' => 'required','placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-6" id="field-fecha-empleado">
            <div class="form-group">
              <label>Fecha de ingreso:</label><span style="color:red;"> *</span>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="fecha" placeholder="dd-mm-aaaa">
                </div>                
                <!-- /.input group -->
            </div>
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-contrato-empleado">
            {!! Form::label('contrato', 'Contrato:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('contrato', ['Condicionado'=>'Condicionado','Fijo'=>'Fijo'] , null, ['id' => 'contrato-empleado', 'class' => 'form-control','required' => 'required', 'placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-6" id="field-condicion-empleado">
            {!! Form::label('condicion', 'Estatus:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('condicion', ['Activo'=>'Activo','Inactivo'=>'Inactivo'] , null, ['id' => 'condicion-empleado', 'class' => 'form-control','required' => 'required', 'placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-telefono-empleado">
            {!! Form::label('telefono', 'Telefono:',['class'=>'control-label']) !!}
            {!!Form::text('telefono', null, ['id'=>'telefono-empleado', 'class'=>'form-control','placeholder'=>'Telefono','data-inputmask'=>"'mask': '9999-999-9999'",'data-mask'])!!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div> 
        </div>
        <div class="col-sm-offset-9">
          <span style="color:red;"> *</span><small class="text-muted"> Campos Requeridos </small><span style="color:red;"> *</span>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-empleado" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>