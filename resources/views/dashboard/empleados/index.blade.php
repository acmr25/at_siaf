@extends('templates.master')

@section('title', 'Trabajadores')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('public/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@endsection

@section('titulo_modulo', 'Trabajadores')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-empleado">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Trabajador
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-empleado"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="empleados" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Cédula</th>
              <th>Nombre</th>
              <th>Estatus</th>
              <th>Tipo</th>
              <th>Telefono</th>
              <th>Inicio</th>
              <th>Contrato</th>
              <th>Acción</th>            
            </tr>
          </thead>
        </table>
        
      </div><!-- /.box-body -->
  </div>
</div>

@include('dashboard.empleados.modal')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/pdfmake.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/vfs_fonts.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>
<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>
<!-- InputMask -->
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('public/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{asset('public/js/validate.js')}}"></script>
<script src="{{asset('public/js/empleado.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>
<script type="text/javascript">
$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
</script>
@endsection

