<!-- Modal -->
<div class="modal fade" id="modal-movimiento" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-movimiento']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Entrada/salida de {{$articulo->nombre}} </h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'id_movimiento']) !!}
        {!! Form::hidden('articulo_id', $articulo->id, ['id'=>'articulo_id']) !!}
        <div class="row">
          <div class="form-group col-sm-4" id="field-tipo">
            {!! Form::label('tipo', 'Tipo de Movimiento:', ['class' => 'control-label']) !!}
            {!! Form::select('tipo', ['ENTRADA'=>'Entrada','SALIDA'=>'Salida'] , null, ['id' => 'tipo', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-sm-4" id="field-activo">
            {!! Form::label('activo', 'Unidad / Equipo:', ['class' => 'control-label']) !!}
            {!! Form::select('activo', $activos , null, ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-sm-4" id="field-cantidad">
            {!! Form::label('cantidad', 'Cantidad:',['class'=>'control-label']) !!}
            {!! Form::number('cantidad', null, ['id' => 'cantidad','class' => 'form-control','min'=>0 ]) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
          
          <div class="form-group" id="field-razon">
            {!! Form::label('razon', 'Razón de uso:',['class'=>'control-label']) !!}
            {!! Form::textarea('razon', null, ['id' => 'razon','class' => 'form-control ', 'required' => 'required', 'rows'=>3]) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>      
      </div>
      <div class="modal-footer">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-movimiento" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>