@extends('templates.master')

@section('title', $activo->unidad.' '.$activo->placa.' '.$activo->marca.' '.$activo->modelo.' {'.$activo->nro_activo.'}')

@section('css')
<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

<style type="text/css">
  .badge-warning {
    background-color: #FF9800;
  }
  .badge-boton {
    background-color: #FFCA28;
  }

</style>

@endsection

@section('titulo_modulo', $activo->unidad.' '.$activo->placa.' '.$activo->marca.' '.$activo->modelo.' {'.$activo->nro_activo.'}')

@section('contenido')

<div class="">
          <div class="nav-tabs-custom tab-success">
            <ul class="nav nav-tabs ">
              <li @if($tabs=='detalles') class="active" @endif><a href="#detalles" data-toggle="tab"><b>Detalles</b></a></li>
              @if($activo->medida != null && ($activo->medida->horas != 0 || $activo->medida->kilometros != 0) )
                <li @if($tabs=='tareas') class="active" @endif><a href="#tareas" data-toggle="tab"><b>Tareas de Mantenimiento</b></a></li>
              @endif
              <li @if($tabs=='reportes') class="active" @endif><a href="#reportes" data-toggle="tab"><b>Reportes de fallas</b></a></li> 
              <li @if($tabs=='ordenes') class="active" @endif><a href="#ordenes" data-toggle="tab"><b>Ordenes</b></a></li>
              <li @if($tabs=='viajes') class="active" @endif><a href="#viajes" data-toggle="tab"><b>Viajes</b></a></li>
              <li @if($tabs=='archivos') class="active" @endif><a href="#archivos" data-toggle="tab"><b>Archivos</b></a></li>
              <li class="dropdown pull-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-gear"></i>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem"  href="{{route('activos.pdf',$activo->id)}}" target="_blank"> <span class="text-info"><i class="fa fa-file-pdf-o"></i></span> Generar PDF</a></li>
                  <li role="presentation"><a role="menuitem"  href="{{route('activos.edit',$activo->id)}}"> <span class="text-warning"><i class="fa fa-pencil"></i></span> Editar</a></li>
                  <li role="presentation"><a role="menuitem"  OnClick="deleteActivo('{{$activo->id}}')" > <span class="text-danger"><i class="fa fa-remove"></i></span> Eliminar</a></li>
                </ul>
              </li>             
            </ul>

            <div class="tab-content">
              <div  @if($tabs=='detalles') class="tab-pane fade in active " @else class="tab-pane fade" @endif  id="detalles"><!-- DETALLES DE LA UNIDAD -->
                <div class="row">
                  <div class="col-sm-4">
                    <dl class="dl-horizontal"  >
                      <dt>Unidad</dt>
                        <dd  >{{$activo->unidad}}</dd>
                      <dt>Nro. de Placa</dt>
                        <dd >{{$activo->placa}}</dd>
                      <dt>Marca</dt>
                        <dd >{{$activo->marca}}</dd>                        
                      <dt>Modelo</dt>
                        <dd >{{$activo->modelo}}</dd>
                      <dt>Serial</dt>
                        <dd >{{$activo->serial}}</dd>
                      <dt>Nro. Activo</dt>
                        <dd  >{{$activo->nro_activo}}</dd>
                      <dt>Status</dt>
                        @if($activo->estatus != 'INOPERATIVO')
                          <dd ><b><span style="color:green">{{$activo->estatus}}</span></b></dd>
                        @else 
                          <dd ><span class="text-muted">{{$activo->estatus}}</span></dd>
                        @endif
                      <dt>Nro. de Activo</dt>
                        <dd  >{{$activo->nro_activo}}</dd>
                      <dt>Prioridad</dt>
                        @if($activo->prioridad == 'Alta')
                          <dd ><b><span style="color:red">{{$activo->prioridad}}</span></b></dd>
                        @else 
                          <dd ><b><span style="color:#FF9900">{{$activo->prioridad}}</span></b></dd>
                        @endif
                      <dt>Clasificación</dt>
                        <dd >{{$activo->clasificacion}}</dd>
                      <dt>Tipo</dt>
                        <dd >{{$activo->tipo}}</dd>                                            
                      <dt> Ubicación </dt>
                        <dd >
                          @if($activo->ubicacion_temporal != null || $activo->ubicacion_temporal != '')
                            {{$activo->ubicacion_temporal}} (temporal)
                          @else
                            {{$activo->ubicacion_fija}}
                          @endif
                        </dd>
                    </dl>
                  </div>
                  <div class="col-sm-4">
                    <dl class="dl-horizontal">
                      <dt>Año</dt>
                        <dd >{{$activo->año}}</dd>
                      <dt>Color</dt>
                        <dd >{{$activo->color}}</dd> 
                      <dt>Tipo de Aceite</dt>
                        <dd >{{$activo->tipo_aceite}}</dd>
                      <dt>Filtro del Aceite</dt>
                        <dd >{{$activo->filtro_aceite}}</dd>
                      <dt>Filtro del Aire</dt>
                        <dd >{{$activo->filtro_aire}}</dd>
                      <dt>Filtro del Combustible</dt>
                        <dd >{{$activo->filtro_combustible}}</dd>
                      <dt>Filtro Sep. del Agua</dt>
                        <dd >{{$activo->filtro_agua}}</dd>
                      <dt>Nro. de Puestos</dt>
                        <dd > @if($activo->puestos > 0){{$activo->puestos}}@endif</dd>                      
                      @if($activo->medida != null )
                        @if($activo->medida->horas != null)
                        <dt>Horas:</dt>
                          <dd >{{$activo->medida->horas}}</dd>
                        @endif
                        @if($activo->medida->kilometros != null)
                        <dt>Kilometros:</dt>
                          <dd >{{$activo->medida->kilometros}}</dd>
                        @endif
                      @else
                          <span class="text-muted"><center><b>No posee medidas (horas y kilometros)</b></center></span>       
                      @endif 
                    </dl>
                  </div>
                  <div class="col-sm-4"><!-- DETALLES DE LA UNIDAD / IMAGEN -->
                    @if($activo->foto != null )
                      <a href="{{asset('public/image/activo/images/'.$activo->foto) }} " target="_blank">
                        <img src="{{asset('public/image/activo/images/'.$activo->foto) }}" class="img-rounded img-responsive" style="width:300px;height:200px;">
                      </a>
                    @else
                        <img src="{{asset('public/image/default.jpg') }}" class="img-rounded img-responsive" style="width:300px;height:200px;">     
                    @endif                                 
                  </div>
                </div> 
              </div>
              <!-- /.tab-pane fade in active -->

              <div @if($activo->medida != null && $tabs=='tareas') class="tab-pane fade in active " @else class="tab-pane fade" @endif id="tareas"><!-- TAREAS DE LA UNIDAD -->
                <div class="text-center">
                  <button type="button" class="btn btn-default pull-left" data-toggle="modal" data-target="#modal-tarea">
                    <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Mantenimiento
                  </button>
                LECTURA ACTUAL: <b> 
                  @if($activo->medida != null && $activo->medida->horas != null) {{$activo->medida->horas}} Hr @endif 
                  @if($activo->medida != null && $activo->medida->kilometros != null) {{$activo->medida->kilometros}} Km @endif
                </b>
                <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-tareas"><i class="fa fa-repeat"></i> Actualizar</a>
                </div>
                <hr>
                
                <table class="table table-bordered table-hover table-striped" id="tabla-tareas" >
                  <thead >
                    <tr>
                      <th style="text-align: center;"><span style="color: white">-----</span><span class="glyphicon glyphicon-info-sign" style="font-size: 20px"></span> </th>
                      <th style="text-align: center;width: 280px">Tarea</th>
                      <th>Prioridad</th> 
                      <th>Realizar cada</th>
                      <th>Preaviso</th>
                      <th>Ultima Realización</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                </table>
                <div  style="border-style:solid; border-width:1px;border-color: #E5E8E8;  text-align: center; font-family: Lucida Sans Unicode; color: #7B7D7D;">
                  <div style="margin: 10px; display: inline-block; " >
                    <span class="label label-danger"><i class="fa fa-fw fa-wrench"></i></span><br>
                    Matenimiento pendiente            
                  </div>
                  <div style="margin: 10px; display: inline-block; " >
                    <span class="label label-warning"><i class="fa fa-fw fa-tachometer"></i></span><br>
                   Matenimiento acercándose            
                  </div>           
                </div>
              </div>

              <!-- /.tab-pane fade  -->

              <div @if($tabs=='reportes') class="tab-pane fade in active " @else class="tab-pane fade" @endif id="reportes">
                <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-reportes"><i class="fa fa-repeat"></i> Actualizar</a>
                <br>
                <hr>
                <table class="table table-bordered table-hover table-striped" id="tabla-reportes" >
                  <thead>
                    <tr>
                      <th style="text-align: center;">ID</th>                      
                      <th style="text-align: center;">Reportado por</th>    
                      <th style="text-align: center;">Fecha</th>                     
                      <th style="text-align: center;">Prioridad</th>
                      <th style="text-align: center;">Orden de trabajo</th>
                      <th style="text-align: center;"></th>

                    </tr>
                  </thead>
                </table>
              </div>
              <!-- /.tab-pane fade  -->

              <div @if($tabs=='ordenes') class="tab-pane fade in active " @else class="tab-pane fade" @endif id="ordenes">
                <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-ordenes"><i class="fa fa-repeat"></i> Actualizar</a>
                <br>
                <hr>
                <table class="table table-bordered table-hover table-striped" id="tabla-ordenes" >
                  <thead>
                    <tr>
                      <th style="text-align: center;">Nro.</th>
                      <th style="text-align: center;">Fecha de Inicio</th>
                      <th style="text-align: center;">Fecha de Fin</th>
                      <th style="text-align: center;">Tareas</th>
                      <th style="text-align: center;">Responsable</th>
                      <th style="text-align: center;">Nro. Reporte de fallas</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <!-- /.tab-pane fade  -->


              <div @if($tabs=='viajes') class="tab-pane fade in active " @else class="tab-pane fade" @endif id="viajes">
                <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-viajes"><i class="fa fa-repeat"></i> Actualizar</a><br/><hr>
              <table class="table table-bordered table-hover table-striped" id="tabla-viajes" width="100%" >
                <thead style="">
                  <tr>
                    <th>ID</th>
                    <th>Chofer Asignado</th>
                    <th>Status</th>
                    <th>Origen</th>
                    <th>Destino</th>
                    <th>Fecha y hora de Salida</th>
                    <th>Fecha y hora de Llegada</th>
                    <th>Solicitudes Asociadas</th> 
                  </tr>
                </thead>
              </table> 
              </div>
              <!-- /.tab-pane fade  -->


              <div @if($tabs=='archivos') class="tab-pane fade in active " @else class="tab-pane fade" @endif id="archivos">
                <a class="btn btn-default" href="{{route('activo-archivo.crear',$activo->id)}}"><span class="text-success"><i class="fa fa-plus"></i></span> Agregar Archivo(s)</a>
                <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-archivos"><i class="fa fa-repeat"></i> Actualizar</a>
                <br /><br />
                <table class="table table-bordered table-hover table-striped" id="tabla-archivos" >
                  <thead >
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                </table>

              </div>
              <!-- /.tab-pane fade  -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
@include('dashboard.tickets.ver_ticket')
@include('dashboard.activos.reportes.archivos')
@include('dashboard.activos.tareas.modal')


@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/pdfmake.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/vfs_fonts.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<!-- fileinput -->
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>

<script src="{{asset('public/js/activo-tarea.js')}}"></script>
<script src="{{asset('public/js/activo-reporte.js')}}"></script>
<script src="{{asset('public/js/activo-archivo.js')}}"></script>
<script src="{{asset('public/js/activo-orden.js')}}"></script>
<script src="{{asset('public/js/activo-viaje.js')}}"></script>

<script>

$(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
}); 

  $.fn.dataTable.ext.errMode = 'throw';

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

// --------------------------------------------------ELIMINAR ACTIVO---------------------------------------
var RUTA_ACTIVOS = ruta+'/activos';

function deleteActivo(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = RUTA_ACTIVOS+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
              showConfirmButton: false,
              timer: 2000
            })
            setTimeout("location.href= RUTA_ACTIVOS", 2050);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al activo. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El activo se ha eliminado exitosamente',
      'success'
      );
  });
}

</script>



@endsection