@extends('templates.master')

@section('title', 'Unidades')

@section('css')

<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/bootstrap-fileinput-master/css/fileinput.css') }}">


@endsection

@section('titulo_modulo')


@section('contenido')

<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <div class="box box-success ">
      <div class="box-header with-border">
          <h3 class="box-title">Subir Archivo(s)</h3>
      </div>
      <div class="box-body" > 

        {!! Form::open(['method' => 'POST', 'route' => 'activo-archivo.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          {!! Form::hidden('activo_id', $activo_id, ['id'=>'activo_id']) !!}

          <div class="{{ $errors->has('descripcion') ? ' has-error' : '' }}">
               {!! Form::label('descripcion', 'Descripción:') !!}
               {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => 'required','rows'=>3]) !!}
               <small class="text-danger">{{ $errors->first('descripcion') }}</small>
          </div> 
          <div class="{{ $errors->has('archivo')?' has-error':'' }}">
            {!! Form::label('archivo', 'Archivo(s):',['class'=>'control-label']) !!}
            <input type="file" name="archivo[]" id='archivo' multiple="">
            <p class="help-block">Si es mas de un archivo, debe seleccionarlos al mismo tiempo (deben estar en la misma ubicación). </p>
            @if($errors->has('archivo'))
            <span class="text-danger">
              <strong>{{ $errors->first('archivo')}}</strong>
            </span>
            @endif
          </div>          
      
      </div>  
      <div class="box-footer with-border">

            <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>
            {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     

      </div>       
        {!! Form::close() !!}
    </div>      
  </div>
</div>


  
@endsection

@section('js')

<!-- fileinput -->
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>


<script>


  $(function() {
    //<!-- fileinput -->
    $('#archivo').fileinput({
    showUpload: false,
    language: 'es',
    browseClass: "btn btn-info",
    maxFileSize: 500,
    maxFileCount: 5,
    overwriteInitial: false,
    })
    //<!-- fileinput -->
  });  

</script>

@endsection