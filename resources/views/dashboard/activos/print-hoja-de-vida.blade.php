<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Hoja de Vida de la Unidad Nro. {{$activo->nro_activo}}</title>
    <link rel="stylesheet" href="{{asset('public/dist/print.css') }}">
    <style type="text/css">

      div.CABEZA {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        width: 100%;
        text-align: left;
        border-collapse: collapse;
      }
      .divTable.CABEZA .divTableCell {
        border: 1px solid #AAAAAA;
        font-size: 10px;
        padding: 6px 6px;
        background: #344563;
        color: white;
      }
     
      div.minimalistBlack {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
      }
      .divTable.minimalistBlack .divTableCell, .divTable.minimalistBlack .divTableHead {
        padding: 10px 10px;
        border: 1px solid #AAAAAAA;
      }
      .divTable.minimalistBlack .divTableBody .divTableCell {
        font-size: 10px;
      }
      /* DivTable.com */
      .divTable{ display: table; }
      .divTableRow { display: table-row; }
      .divTableHeading { display: table-header-group;}
      .divTableCell, .divTableHead { display: table-cell;}
      .divTableHeading { display: table-header-group;}
      .divTableFoot { display: table-footer-group;}
      .divTableBody { display: table-row-group;}
    </style>
  </head>
 <body class="clearfix">
    <script type="text/php">
    if ( isset($pdf) ) {
        $font = $fontMetrics->getFont("arial", "bold");
        $pdf->page_text(550, 18, "{PAGE_NUM} de {PAGE_COUNT}", $font, 8, array(0,0,0));
    }
    </script> 
    <div style="margin-top: 10px;margin-bottom: 20px;white-space: nowrap;">
      <img src="{{asset('public/image/logo_AmazonasTech_RIF.png') }}" class="pull-left" width="199">
        <div class="text-right">
          <b>Departamento de Gestión de Flota</b><br />
          <b>Hoja de Vida<b />
        </div>
    </div>
    <div class="divTable CABEZA">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DE LA UNIDAD</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Unidad</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Marca</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Modelo</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Placa</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Nro. de Activo</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$activo->unidad}}</div>
          <div class="divTableCell">{{$activo->marca}}</div>
          <div class="divTableCell">{{$activo->modelo}}</div>
          <div class="divTableCell">{{$activo->placa}}</div>
          <div class="divTableCell">{{$activo->nro_activo}}</div>
        </div>
        <div class="divTableRow">
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Serial</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Año</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Estatus</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Prioridad</b></div>
          <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Ubicación</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$activo->serial}}</div>
          <div class="divTableCell">{{$activo->año}}</div>
          <div class="divTableCell">{{$activo->estatus}}</div>
          <div class="divTableCell">{{$activo->prioridad}}</div>
          <div class="divTableCell">{{$activo->ubicacion_fija}}</div>
        </div>
        <div class="divTableRow">
            <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Tipo de Aceite</b></div>
            <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Filtro de Aceite</b></div>
            <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Filtro de Aire</b></div>
            <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Filtro de Combustible</b></div>
            <div style=" background: #EDF2F7; text-align: center;" class="divTableCell"><b>Filtro Separador de Agua</b></div>
        </div>
        <div class="divTableRow">
            <div class="divTableCell">{{$activo->tipo_aceite}}</div>
            <div class="divTableCell">{{$activo->filtro_aceite}}</div>
            <div class="divTableCell">{{$activo->filtro_aire}}</div>
            <div class="divTableCell">{{$activo->filtro_combustible}}</div>
            <div class="divTableCell">{{$activo->filtro_agua}}</div>
        </div>
      </div>
    </div><br />
    @if (count($activo->reportes) > 0)    
      <div class="divTable CABEZA">
        <div class="divTableBody">
          <div class="divTableRow">
            <div style="text-align: center;" class="divTableCell"><b>REPORTES DE FALLAS DE LA UNIDAD</b></div>
          </div>
        </div>
      </div>    
      <div class="divTable minimalistBlack">
        <div class="divTableBody" >
          <div class="divTableRow">
            <div style="background:#EDF2F7; text-align: center; vertical-align: middle; " class="divTableCell"><b>N° de Reporte</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Descripción de Fallas</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Reportado por</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Lugar y Fecha</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>OT</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Prioridad</b></div>
          </div>
          @foreach ($activo->reportes as $reporte)                    
              <div class="divTableRow">
                <div style="width: 8%;  text-align: center;" class="divTableCell">{{$reporte->id}}</div>
                <div style=" " class="divTableCell">
                  <?php $i=1; ?>
                  @foreach ($reporte->fallas as $falla)
                   {{$i}}. ({{$falla->tipo}}) {{$falla->nombre}}<br />
                   <?php $i++; ?>
                  @endforeach
                  Observaciones: {{$reporte->observacion}}
                </div>
                <div class="divTableCell">{{$reporte->informador}}<br />{{$reporte->ci_informador}}<br />{{$reporte->cargo_informador}}</div>
                <div class="divTableCell">{{$reporte->lugar}}<br />{{$reporte->fecha}}</div>
                <div class="divTableCell">
                @if($reporte->estado =='Sin asignar') {{$reporte->estado}}
                @else Nro. {{$reporte->orden->id}}
                @endif
                </div>
                <div class="divTableCell">{{$reporte->prioridad}}</div>
              </div>
          @endforeach
        </div>
      </div><br />
    @endif
    @if (count($activo->ordenes) > 0)    
      <div class="divTable CABEZA">
        <div class="divTableBody">
          <div class="divTableRow">
            <div style="text-align: center;" class="divTableCell"><b>ORDENES DE TRABAJO DE LA UNIDAD</b></div>
          </div>
        </div>
      </div>    
      <div class="divTable minimalistBlack">
        <div class="divTableBody" >
          <div class="divTableRow">
            <div style="background:#EDF2F7; text-align: center; vertical-align: middle; " class="divTableCell"><b>N° de OT</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Responsable</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Actividades</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Insumos</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Inicio/Fin</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Reporte</b></div>
          </div>
          @foreach ($activo->ordenes as $orden)                  
              <div class="divTableRow">
                <div style="width: 8%;  text-align: center;" class="divTableCell">{{$orden->id}}</div>
                <div class="divTableCell">{{$orden->empleado->nombre}}</div>
                <div style=" " class="divTableCell">
                  <?php $i=1; ?>
                  @foreach ($orden->tareas as $tarea)
                   {{$i}}. {{$tarea->nombre}}<br />
                  <?php $i++; ?>
                  @endforeach
                </div>
                <div style=" " class="divTableCell">
                  <?php $i=1; ?>
                  @foreach ($orden->movimientos as $movimiento)
                   {{$i}}. ({{$movimiento->cantidad}}) {{$movimiento->articulo->nombre}}-{{$movimiento->razon}}<br />
                  <?php $i++; ?>
                  @endforeach
                </div>
                <div class="divTableCell">{{$orden->inicia}} / {{$orden->termina}}</div>
                <div class="divTableCell"> @if(isset($orden->reporte_id))Nro. {{$orden->reporte_id}} @endif  </div>
              </div>
          @endforeach
        </div>
      </div><br />
    @endif
    @if (count($activo->viajes) > 0)    
      <div class="divTable CABEZA">
        <div class="divTableBody">
          <div class="divTableRow">
            <div style="text-align: center;" class="divTableCell"><b>VIAJES DE LA UNIDAD</b></div>
          </div>
        </div>
      </div>    
      <div class="divTable minimalistBlack">
        <div class="divTableBody" >
          <div class="divTableRow">
            <div style="background:#EDF2F7; text-align: center; vertical-align: middle; " class="divTableCell"><b>N° de Viaje</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Chofer Asignado</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Status</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Origen</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Destino</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Fecha de Salida</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Fecha de Legada</b></div>
            <div style="background:#EDF2F7; vertical-align: middle;" class="divTableCell"><b>Solicitudes Asociadas</b></div>
          </div>
          @foreach ($activo->viajes as $viaje)                  
              <div class="divTableRow">
                <div style="width: 8%;  text-align: center;" class="divTableCell">{{$viaje->id}}</div>
                <div class="divTableCell">{{$viaje->chofer->nombre}}</div>
                <div class="divTableCell">{{$viaje->estado}}</div>
                <div class="divTableCell">{{$viaje->origen}}</div>
                <div class="divTableCell">{{$viaje->destino}}</div>
                <div class="divTableCell">{{$viaje->fecha_salida}}</div>
                <div class="divTableCell">{{$viaje->fecha_llegada}}</div>
                <div style=" " class="divTableCell">
                  @foreach ($viaje->solicitudes as $solicitud)
                   {{$solicitud->id}}. {{$solicitud->solicitante->nombre}}<br />
                  @endforeach
                </div>
              </div>
          @endforeach
        </div>
      </div><br />
    @endif       
  </body>
</html>