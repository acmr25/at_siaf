@extends('templates.master')

@section('title', 'Unidades')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <a class="btn btn-default"  data-toggle="modal" href='{{ route('activos.create') }}'><span class="text-success"><i class="fa fa-plus"></i></span> Agregar Unidad</a>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-activo"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="activos" width="100%" >
          <thead style="">
            <tr>
              <th style="text-align: center;"><span class="glyphicon glyphicon-info-sign" style="font-size: 20px"></span> </th>
              <th>Descripción</th>
              <th>Clasificación</th>
              <th>Estatus</th>
              <th>Ubicación</th>      
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th style="text-align: center;"><span class="glyphicon glyphicon-info-sign" style="font-size: 20px"></span> </th>
              <th>Descripción</th>
              <th>Clasificación</th>
              <th>Estatus</th>
              <th>Ubicación</th>      
            </tr>
          </tfoot>
        </table>
        <div  style="border-style:solid; border-width:1px;border-color: #E5E8E8;  text-align: center; font-family: Lucida Sans Unicode; color: #7B7D7D;">
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-danger"><i class="fa fa-fw fa-wrench"></i></span><br>
            Matenimiento pendiente            
          </div>
          <div style="margin: 10px; display: inline-block; " >
            <span class="label label-warning"><i class="fa fa-fw fa-tachometer"></i></span><br>
           Matenimiento acercándose            
          </div>           
        </div>
      </div><!-- /.box-body -->
  </div>
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>

<script src="{{asset('public/js/activo.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection 