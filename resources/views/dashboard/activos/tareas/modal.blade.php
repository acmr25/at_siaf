<!-- Modal -->
<div class="modal fade" id="modal-tarea" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-tarea']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario de Mantenimientos</h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'id']) !!}
        {!! Form::hidden('activo_id', $activo->id, ['id'=>'activo_id']) !!}

        @if($activo->medida != null && ($activo->medida->horas != 0 ||$activo->medida->kilometros != 0) )
          <div class="row" id="tarea_act">
            <div class="form-group col-sm-7" id="field-tarea-nombre">
              {!! Form::label('nombre', 'Nombre del mantenimiento:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
              {!! Form::select('nombre', $tarea , null, ['id' => 'tarea_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span>
                <strong class="text-danger msj-error"></strong>
              </span>
            </div>
            <div class="form-group col-sm-5" id="field-tarea-prioridad">
              {!! Form::label('prioridad', 'Prioridad',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
              {!! Form::select('prioridad', ['Baja'=>'Baja','Media'=>'Media','Alta'=>'Alta'] , null, ['id' => 'prioridad', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span>
                <strong class="text-danger msj-error"></strong>
              </span>
            </div>
          </div>

          @if($activo->medida->horas > 0)
          <div class="row ">
            <center><b>Seguimiento por horas</b></center><br>
            
            <div class="form-group col-sm-4" id="field-horas">
              {!! Form::label('horas_aviso', 'Realizar cada:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
              {!! Form::text('horas_aviso', null, ['id' => 'horas_aviso','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
            <div class="form-group col-sm-4" id="field-pre_horas">
              {!! Form::label('horas-preaviso', 'Preaviso:',['class'=>'control-label']) !!}
              {!! Form::text('horas-preaviso', null, ['id' => 'horas-preaviso','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
            <div class="form-group col-sm-4" id="field-realizacion_horas">
              {!! Form::label('horas-ultima', 'Ultima realización:',['class'=>'control-label']) !!}
              {!! Form::text('horas-ultima', null, ['id' => 'horas-ultima','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
          </div>         
          @endif
          @if($activo->medida->kilometros > 0)
          <div class="row ">
            <center><b>Seguimiento por kilometros</b></center><br>
            <div class="form-group col-sm-4" id="field-kilometros">
              {!! Form::label('kilometros_aviso', 'Realizar cada:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
              {!! Form::text('kilometros_aviso', null, ['id' => 'kilometros_aviso','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
            <div class="form-group col-sm-4" id="field-pre_kilometros">
              {!! Form::label('kilometros-preaviso', 'Preaviso:',['class'=>'control-label']) !!}
              {!! Form::text('kilometros-preaviso', null, ['id' => 'kilometros-preaviso','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
            <div class="form-group col-sm-4" id="field-realizacion_kilometros">
              {!! Form::label('kilometros-ultima', 'Ultima realización:',['class'=>'control-label']) !!}
              {!! Form::text('kilometros-ultima', null, ['id' => 'kilometros-ultima','class' => 'form-control' ]) !!}
              <span>
                <small class="text-danger msj-error"></small>
              </span>
            </div>
            <div class="col-sm-offset-9">
              <span style="color:red;"> *</span><small class="text-muted"> Campos Requeridos </small><span style="color:red;"> *</span>
            </div>
          </div>
          @endif      
        @else
          <span class="text-muted"><center><h2><b>No posee medidas (horas y kilometros)</b></h2> </center></span><br>
          <span class="text-muted"><center><b>Por lo tanto no puede asignarle tareas de mantenimiento</b></center></span><br>
        @endif 

      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          @if($activo->medida != null)
            <button type="button" id="guardar-tarea" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
          @endif 
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
