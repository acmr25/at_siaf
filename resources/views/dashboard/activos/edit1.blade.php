@extends('templates.master')

@section('title', 'Unidades')

@section('css')

<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

@endsection

@section('titulo_modulo', 'Unidades')


@section('contenido')
<div class="row" >
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
          <h3 class="box-title">Formulario para registro de Unidades</h3>
      </div>
      <div class="box-body" > 
        {!! Form::open(['method' => 'PUT', 'route' => ['activos.update', $activo->id], 'class' => 'form-horizontal', 'files' => true]) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="row">
          <div  class="col-md-4" >
            <div class=" form-group {{ $errors->has('unidad') ? ' has-error' : '' }}">
              {!! Form::label('unidad', 'Unidad:', ['class' => 'col-md-5 control-label']) !!}
              <div class="col-md-7">
                {!!Form::text('unidad', $activo->unidad, ['class'=>'form-control','placeholder'=>'Camioneta','required'=>'required'])!!}
                <strong class="text-danger">{{ $errors->first('unidad') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('placa') ? ' has-error' : '' }}">
                {!! Form::label('placa', 'Placa:', ['class' => 'col-md-5 control-label']) !!}
              <div class="col-md-7">
                  {!! Form::text('placa', $activo->placa, ['class' => 'form-control','placeholder'=> '8455', 'maxlength'=>30]) !!}
                  <strong class="text-danger">{{ $errors->first('placa') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('marca') ? ' has-error' : '' }}">
                {!! Form::label('marca', 'Marca:', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                  {!! Form::select('marca', $marcas , $activo->marca, ['id' => 'marca', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione']) !!}
                  <strong class="text-danger">{{ $errors->first('marca') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('modelo') ? ' has-error' : '' }}">
                {!! Form::label('modelo', 'Modelo:', ['class' => 'col-md-5 control-label']) !!}
              <div class="col-md-7">
                  {!! Form::text('modelo', $activo->modelo, ['class' => 'form-control',  'placeholder'=> 'XD-8569', 'maxlength'=>30]) !!}
                  <strong class="text-danger">{{ $errors->first('modelo') }}</strong>
              </div>
            </div>
            <div class=" form-group {{ $errors->has('serial') ? ' has-error' : '' }}">
              {!! Form::label('serial', 'Serial:', ['class' => 'col-md-5 control-label']) !!}
              <div class="col-md-7">
                {!!Form::text('serial', $activo->serial, ['class'=>'form-control','placeholder'=>'8XGAX16Y8CD018803'])!!}
                <strong class="text-danger">{{ $errors->first('serial') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('estatus') ? ' has-error' : '' }}">
                {!! Form::label('estatus', 'Status:', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                  {!! Form::select('estatus', ['Inoperativo'=>'Inoperativo','Operativo'=>'Operativo','En mantenimiento'=>'En mantemiento','En servicio'=>'En servicio'] , $activo->estatus, ['id' => 'estatus', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione','required'=>'required']) !!}
                  <strong class="text-danger">{{ $errors->first('estatus') }}</strong>
              </div>
            </div>            
            <div class=" form-group {{ $errors->has('nro_activo') ? ' has-error' : '' }}">
              {!! Form::label('nro_activo', 'Nro. de Activo:', ['class' => 'col-md-5 control-label']) !!}
              <div class="col-md-7">
                {!!Form::text('nro_activo', $activo->nro_activo, ['class'=>'form-control','placeholder'=>'9009053010'])!!}
                <strong class="text-danger">{{ $errors->first('nro_activo') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('prioridad') ? ' has-error' : '' }}">
                {!! Form::label('prioridad', 'Prioridad:', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                  {!! Form::select('prioridad', ['Baja'=>'Baja','Media'=>'Media','Alta'=>'Alta'] , $activo->prioridad, ['id' => 'prioridad', 'class' => 'form-control', 'placeholder'=> 'Seleccione','required'=>'required']) !!}
                  <strong class="text-danger">{{ $errors->first('prioridad') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('clasificacion') ? ' has-error' : '' }}" >
                {!! Form::label('clasificacion', 'Clasificación:', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                  {!! Form::select('clasificacion', ['Unidad Pesada'=>'Unidad Pesada','Vehiculo Liviano'=>'Vehiculo Liviano','Vehiculo Pesado'=>'Vehiculo Pesado'] , $activo->clasificacion, ['id' => 'clasificacion', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione']) !!}
                  <strong class="text-danger">{{ $errors->first('clasificacion') }}</strong>
              </div>
            </div>            
            <div class="form-group {{ $errors->has('tipo') ? ' has-error' : '' }}" >
                {!! Form::label('tipo', 'Tipo:', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                  {!! Form::select('tipo', $tipos , $activo->tipo, ['id' => 'tipo', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione']) !!}
                  <strong class="text-danger">{{ $errors->first('tipo') }}</strong>
              </div>
            </div>         
            <div class="form-group{{ $errors->has('ubicacion_fija') ? ' has-error' : '' }}">
                {!! Form::label('ubicacion_fija', 'Ubicación Fija:', ['class' => 'col-sm-5 control-label']) !!}
              <div class="col-sm-7">
                  {!! Form::select('ubicacion_fija', $ub_fija , $activo->ubicacion_fija, ['id' => 'ubicacion_fija', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione','required'=>'required']) !!}
                  <strong class="text-danger">{{ $errors->first('ubicacion_fija') }}</strong>
              </div>
            </div>
          </div>
          <div class="col-md-4">
              <div class="form-group {{ $errors->has('año') ? ' has-error' : '' }}">
                {!! Form::label('año', 'Año:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                {!!Form::text('año', $activo->año, ['class'=>'form-control','placeholder'=>'2018'])!!}
                  <strong class="text-danger">{{ $errors->first('año') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                {!! Form::label('color', 'Color:', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                  {!! Form::select('color', $colores , $activo->color, ['id' => 'color', 'class' => 'form-control select2', 'placeholder'=> 'Seleccione']) !!}
                  <strong class="text-danger">{{ $errors->first('color') }}</strong>
              </div>
            </div>   
            <div class="form-group {{ $errors->has('tipo_aceite') ? ' has-error' : '' }}">
                {!! Form::label('tipo_aceite', 'Tipo de Aceite:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                  {!! Form::text('tipo_aceite', $activo->tipo_aceite, ['class' => 'form-control', 'placeholder'=> 'Tipo de Aceite', 'maxlength'=>30 ]) !!}
                  <strong class="text-danger">{{ $errors->first('tipo_aceite') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('filtro_aceite') ? ' has-error' : '' }}">
                {!! Form::label('filtro_aceite', 'Filtro del Aceite:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                  {!! Form::text('filtro_aceite', $activo->filtro_aceite, ['class' => 'form-control','placeholder'=> 'Filtro del Aceite', 'maxlength'=>30 ]) !!}
                  <strong class="text-danger">{{ $errors->first('filtro_aceite') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('filtro_aire') ? ' has-error' : '' }}">
                {!! Form::label('filtro_aire', 'Filtro del Aire:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                  {!! Form::text('filtro_aire', $activo->filtro_aire, ['class' => 'form-control',  'placeholder'=> 'Filtro del Aire', 'maxlength'=>30]) !!}
                  <strong class="text-danger">{{ $errors->first('filtro_aire') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('filtro_combustible') ? ' has-error' : '' }}">
                {!! Form::label('filtro_combustible', 'Filtro del Combustible:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                  {!! Form::text('filtro_combustible', $activo->filtro_combustible, ['class' => 'form-control', 'placeholder'=> 'Filtro del Combustible', 'maxlength'=>30]) !!}
                  <strong class="text-danger">{{ $errors->first('filtro_combustible') }}</strong>
              </div>
            </div>
            <div class="form-group {{ $errors->has('filtro_agua') ? ' has-error' : '' }}">
                {!! Form::label('filtro_agua', 'Filtro Sep. del Agua:', ['class' => 'col-md-6 control-label']) !!}
              <div class="col-md-6">
                  {!! Form::text('filtro_agua', $activo->filtro_agua, ['class' => 'form-control', 'placeholder'=> 'Filtro Sep. del Agua', 'maxlength'=>30]) !!}
                  <strong class="text-danger">{{ $errors->first('filtro_agua') }}</strong>
              </div>
            </div>
            <div class="form-group{{ $errors->has('puestos') ? ' has-error' : '' }}">
                {!! Form::label('puestos', 'Nro. de Puestos:', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-sm-6">
                  {!! Form::select('puestos',[1=>1,2=>2,3=>3,4=>4] , $activo->puestos, ['id' => 'puestos', 'class' => 'form-control', 'placeholder'=>'Seleccione']) !!}
                  <strong class="text-danger">{{ $errors->first('puestos') }}</strong>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-10">
                  <div class="checkbox{{ $errors->has('check') ? ' has-error' : '' }}">
                      <label for="check">
                      {!! Form::checkbox('check', '5', null, ['id' => 'check', 'onchange'=>'javascript:showContent()']) !!}  El equipo tiene un medidor (por ejemplo, kilómetros u horas)
                      </label>
                  </div>
                  <small class="text-danger">{{ $errors->first('check') }}</small>
              </div>
            </div>
            <div id="content" style="display: none;">
              <div class="form-group{{ $errors->has('horas') ? ' has-error' : '' }}">
                  {!! Form::label('horas', 'Horas (lectura actual):', ['class' => 'col-sm-6 control-label']) !!}
                  <div class="col-sm-6">
                    {!! Form::number('horas', null, ['class' => 'form-control', 'min'=>0, 'max'=>999999]) !!}
                    <small class="text-danger">{{ $errors->first('horas') }}</small>
                </div>
              </div>
              <div class="form-group{{ $errors->has('kilometros') ? ' has-error' : '' }}">
                  {!! Form::label('kilometros', 'Kilometros (lectura actual):', ['class' => 'col-sm-6 control-label']) !!}
                  <div class="col-sm-6">
                    {!! Form::number('kilometros', null, ['class' => 'form-control', 'min'=>0 , 'max'=>999999]) !!}
                    <small class="text-danger">{{ $errors->first('kilometros') }}</small>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class=" {{ $errors->has('foto') ? ' has-error' : '' }}">
              {!! Form::label('foto', 'Imagen de la unidad') !!}
              {!! Form::file('foto', null ,['id'=>'foto', 'data-btnClass'=>'btn-info']) !!}
              <p class="help-block">En formato jpeg ó png</p>
              <strong class="text-danger">{{ $errors->first('foto') }}</strong>
            </div>
          </div>
        </div>  
      </div>
      <div class="box-footer with-border">
          <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>
          {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     
      </div>       
        {!! Form::close() !!}
    </div>  
  </div>
</div>
  
@endsection

@section('js')

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>

<!-- InputMask -->
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>


<!-- SELECT2 -->
<script>
function show(){
  document.getElementById('tipo_campo').style.display = "block";
}
function hide(){
  document.getElementById('tipo_campo').style.display = "none";
}
  function showContent() {
    element = document.getElementById("content");
    check = document.getElementById("check");
    if (check.checked) {
      element.style.display='block';
    }
    else {
            element.style.display='none';
    }
  }
  
  $(function() {
//<!-- fileinput -->
  @if ($activo->foto!=null)
  $('#foto').fileinput({
    showUpload: false,
    language: 'es',
    browseClass: "btn btn-info",
    allowedFileExtensions: ['jpg', 'png'],
    overwriteInitial: true,
    initialPreviewAsData: false,
    initialPreview:[
      "<img class='file-preview-image kv-preview-data' src='{{asset('public/image/activo/images/' .$activo->foto) }}' style='width:240px; height:160px'>"]
    })
  @else
  $('#foto').fileinput({
    showUpload: false,
    language: 'es',
    browseClass: "btn btn-info",
    allowedFileExtensions: ['jpg', 'png']
    })
  @endif    
//<!--END fileinput -->
//<!-- SELECT2 -->
    $('.select2').select2({
      placeholder: "Seleccione",
      tags:true,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true
        }
      },
      templateResult: function (data) {
        var $result = $("<span></span>");
        $result.text(data.text);
        if (data.newOption) {
          $result.append(" <em>(Nuevo)</em>");
        }
        return $result;
      },
      language: "es",
      width:'100%'
    })
    $('#estatus').select2({
      placeholder: "Seleccione",
      language: "es",
      width:'100%'
    })
    $('#prioridad').select2({
      placeholder: "Seleccione",
      language: "es",
      width:'100%'
    })
    $('#puestos').select2({
      placeholder: "Seleccione",
      language: "es",
      width:'100%'
    })

  }); 

 
</script>
<!-- END SELECT2 -->
@endsection