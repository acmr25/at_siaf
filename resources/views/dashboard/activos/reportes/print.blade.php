<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reporte Nro. {{$reporte->id}}</title>
    <link rel="stylesheet" href="{{asset('public/dist/print.css') }}">
    <style type="text/css">

      div.head_table {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        width: 100%;
        text-align: left;
        border-collapse: collapse;
      }
      .divTable.head_table .divTableCell {
        border: 1px solid #AAAAAA;
        padding: 6px 6px;
        font-size: 13px;
        background: #344563;
        color: white;
      }
     
      div.minimalistBlack {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
      }
      .divTable.minimalistBlack .divTableCell, .divTable.minimalistBlack .divTableHead {
        padding: 10px 10px;
        border: 1px solid #AAAAAAA;
      }
      .divTable.minimalistBlack .divTableBody .divTableCell {
        font-size: 13px;
      }
      /* DivTable.com */
      .divTable{ display: table; }
      .divTableRow { display: table-row; }
      .divTableHeading { display: table-header-group;}
      .divTableCell, .divTableHead { display: table-cell;}
      .divTableHeading { display: table-header-group;}
      .divTableFoot { display: table-footer-group;}
      .divTableBody { display: table-row-group;}
    </style>
  </head>
 <body class="clearfix">
    <div style="margin-top: 10px;margin-bottom: 10px;white-space: nowrap;">
      <img src="{{asset('public/image/logo_AmazonasTech_RIF.png') }}" class="pull-left" width="199">
      <b class="pull-right" style="bottom: 0px;">Departamento de Gestión de Flota</b>
    </div> <br /><br /><br /><br />

    <div class="text-right" style="margin-top: 10px;margin-bottom: 10px;">
      <b>REPORTE DE FALLAS <br /> NRO. {{$reporte->id}}</b>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DEL REPORTE</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Fecha</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Prioridad</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 70%;"><b>Lugar</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$reporte->fecha = date('d-m-Y', strtotime($reporte->fecha))}}</div>
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$reporte->prioridad}}</div>
          <div class="divTableCell">{{$reporte->lugar}}</div>
        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 30%;"><b>Reportado por</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>C.I.</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 49%;"><b>Cargo</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$reporte->informador}}</div>
          <div class="divTableCell">{{$reporte->ci_informador}}</div>
          <div class="divTableCell">{{$reporte->cargo_informador}}</div>
        </div>
      </div>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DEL EQUIPO O UNIDAD</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Unidad/Equipo</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Placa</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Marca</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Modelo</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;" ><b>Nro. de Activo</b></div>
        </div>
        <div class="divTableRow">
          <div  class="divTableCell">{{$reporte->activo->unidad}}</div>
          <div  class="divTableCell">{{$reporte->activo->placa}}</div>
          <div  class="divTableCell">{{$reporte->activo->marca}}</div>
          <div  class="divTableCell">{{$reporte->activo->modelo}}</div>
          <div  class="divTableCell">{{$reporte->activo->nro_activo}}</div>
        </div>

      </div>
    </div>  
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>FALLAS QUE PRESENTA LA UNIDAD O EQUIPO</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div style="background:#EDF2F7; text-align: center; font-weight: bold; width: 8%;" class="divTableCell">Nro.</div>
          <div style="background:#EDF2F7; text-align: center; font-weight: bold; width: 15%; " class="divTableCell">Tipo de falla</div>
          <div style="background:#EDF2F7; font-weight: bold;" class="divTableCell">Descripción</div>
        </div><?php $i=1; ?>
        @foreach ($reporte->fallas as $falla)          
            <div class="divTableRow">
              <div style="text-align: center; vertical-align: middle; " class="divTableCell">{{$i}}</div>
              <div style="text-align: center; vertical-align: middle; " class="divTableCell">{{$falla->tipo}}</div>
              <div class="divTableCell">{{$falla->nombre}}</div>
            </div>
          <?php $i++; ?>
        @endforeach
        <?php if($i<5){ for ($i ; $i <=5  ; $i++) { ?>
            <div class="divTableRow" >
              <div style="text-align: center; vertical-align: middle; height: 18px;" class="divTableCell"></div>
              <div style="text-align: center; vertical-align: middle; height: 18px;" class="divTableCell"></div>
              <div style="height: 18px;" class="divTableCell"></div>
            </div>
        <?php } } ?>
      </div>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>OBSERVACIONES</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack"  >
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="height: 100px"><p>{{$reporte->observacion}}</p></div>
        </div>
      </div>
    </div>
    <div></div>
    @if($reporte->archivos)<?php $count=0; ?>
      @foreach ($reporte->archivos as $archivo)
        @if($archivo->tipo == 'imagen')
        <?php $count++; ?>
        @endif           
      @endforeach
      <?php if ($count > 0) { ?><br />
        <div style="page-break-after: always;"></div>
        <div class="divTable head_table">
          <div class="divTableBody">
            <div class="divTableRow">
              <div style="text-align: center;" class="divTableCell"><b>ANEXOS</b></div>
            </div>
          </div>
        </div>    
        <div class="divTable minimalistBlack">
          <div class="divTableBody" >
            @foreach ($reporte->archivos as $archivo)
              @if($archivo->tipo == 'imagen')
                <div class="divTableRow">
                  <div class="divTableCell">
                    <center>
                      <img src="{{ asset('public/image/archivos/'.$archivo->nombre_interno) }}" style="width:250px;height:150px; margin-top: 10px;" ><br />
                      {{$archivo->nombre_externo}}
                    </center>
                  </div>
                </div>
              @endif          
            @endforeach      
          </div>
        </div>        
      <?php } ?>      
    @endif
  </body>
</html>