
<div class="modal fade" id="modal-archivos">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header" style="text-align: center;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="titulo-modal-archivos"></h4>
			</div>
			<div class="modal-body" id="contenido-modal-archivos">
				
			</div>
			<div class="modal-footer " id="botones_archivos">
				
			</div>
		</div>
	</div>
</div>