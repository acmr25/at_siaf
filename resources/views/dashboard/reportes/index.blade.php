@extends('templates.master')

@section('title', 'Reportes')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

<style type="text/css">
  .badge-warning {
    background-color: #FF9800;
  }
  .badge-boton {
    background-color: #FFCA28;
  }

</style>
@endsection

@section('contenido')
{!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
<div class="row" >
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
        <a class="btn btn-default" href="{{route('reportes.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span> Agregar Reporte</a>
        <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-reportes"><i class="fa fa-repeat"></i> Actualizar</a>
        <br>
        <hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-reportes" >
          <thead>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">Unidad/Equipo</th>
              <th style="text-align: center;">Reportado por</th>                  
              <th style="text-align: center;">Fecha</th>
              <th style="text-align: center;">Prioridad</th>                     
              <th style="text-align: center;">Orden de trabajo</th>
              <th style="text-align: center;"></th>

            </tr>
          </thead>
        </table>
      </div><!-- /.box-body -->
  </div>
</div>

@include('dashboard.activos.reportes.archivos')

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('public/js/reporte.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection

