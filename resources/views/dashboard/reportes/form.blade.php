@extends('templates.master')

@section('title', 'Reportes')

@section('css')
<!-- fileinput --> 
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">


@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Reportes de fallas</h3>
    </div>
    <div class="box-body" >

      @if(isset($reporte))
      {!! Form::open(['method' => 'PUT','route' => ['reportes.update', $reporte->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
      {!! Form::hidden('id', $reporte->id) !!}  
      @else
      {!! Form::open(['method' => 'POST', 'route' => 'reportes.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @endif

        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class=" form-group {{ $errors->has('activo_id') ? ' has-error' : '' }}">
            {!! Form::label('activo_id', 'Unidad / Equipo:', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
            {!! Form::select('activo_id', $activos , (isset($reporte)?$reporte->activo_id:null), ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
            <small class="text-danger">{{ $errors->first('activo_id') }}</small>
            </div>
        </div>
        <div class="  form-group {{ $errors->has('fecha') ? ' has-error' : '' }}">
            {!! Form::label('fecha', 'Fecha:', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                {!! Form::date('fecha', (isset($reporte)?$reporte->fecha:null), ['class' => 'form-control', 'required' => 'required']) !!}
              </div>          
              <small class="text-danger">{{ $errors->first('fecha') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('prioridad') ? ' has-error' : '' }}">
            {!! Form::label('prioridad', 'Prioridad:', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::select('prioridad', ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'], (isset($reporte)?$reporte->prioridad:null), ['id' => 'prioridad', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('prioridad') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('lugar') ? ' has-error' : '' }}">
            {!! Form::label('lugar', 'Lugar/Ubicación:', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::text('lugar', (isset($reporte)?$reporte->lugar:null), ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('lugar') }}</small>
            </div>
        </div>
        <div class=" form-group{{ $errors->has('informador') ? ' has-error' : '' }}">
          {!! Form::label('informador', 'Reportado por:', ['class' => 'col-sm-4 control-label']) !!}
          <div class="col-sm-7">
            {!! Form::text('informador', (isset($reporte)?$reporte->informador:null), ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Nombre Completo']) !!}
            <small class="text-danger">{{ $errors->first('informador') }}</small> 
          </div>
        </div>
        <div class="form-group{{ $errors->has('ci_informador') ? ' has-error' : '' }}">
          {!! Form::label('ci_informador', 'C.I.:', ['class' => 'col-sm-4 control-label']) !!}
          <div class="col-sm-7">
            {!! Form::text('ci_informador', (isset($reporte)?$reporte->ci_informador:null), ['class' => 'form-control', 'placeholder'=>'23.533.432','required']) !!}
            <small class="text-danger">{{ $errors->first('ci_informador') }}</small>    
          </div>                
        </div>
        <div class="form-group{{ $errors->has('cargo_informador') ? ' has-error' : '' }}">
            {!! Form::label('cargo_informador', 'Cargo:',['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::text('cargo_informador', (isset($reporte)?$reporte->cargo_informador:null), ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Cargo desempeñado']) !!}
              <small class="text-danger">{{ $errors->first('cargo_informador') }}</small>  
            </div>           
        </div>
        <div class="pull-right">
          <button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Agregar" id="nuevo" OnClick="agregar()">Agregar Falla</button>

          <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" id="borrar" OnClick="coño()">Eliminar Falla</button>
        </div><br /> <br />
        @if(isset($reporte))
          @for($i = 0; $i < $reporte->fallas->count(); $i++)
            <div id="cel" class="clonedcel">
              <div class=" form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                {!! Form::label('nombre[]', 'Falla y Tipo: ',['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-4">
                  {!! Form::text('nombre[]', $reporte->fallas[$i]->nombre, ['id' => 'nombre[]', 'class' => 'form-control','maxlength'=>200,'minlength'=>5 ]) !!}
                  <small class="text-danger">{{ $errors->first('nombre') }}</small>                  
                </div>
                <div class="col-sm-3">
                  {!! Form::select('tipo[]', ['Combustible'=>'Combustible','Electrica'=>'Electrica','Hidraulica'=>'Hidraulica','Mecanica'=>'Mecanica', 'Neumatica'=>'Neumatica' ],  $reporte->fallas[$i]->tipo, ['id' => 'tipo[]', 'class' => 'form-control','placeholder'=>'Seleccione']) !!}
                </div>
              </div>
            </div>         
          @endfor 
          <div id="input1" class="clonedInput">
            <div class=" form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
              {!! Form::label('nombre[]', 'Falla y Tipo: ',['class' => 'col-sm-4 control-label']) !!}
              <div class="col-sm-4">
                {!! Form::text('nombre[]', null, ['id' => 'nombre[]', 'class' => 'form-control','maxlength'=>200,'minlength'=>5 ]) !!}
                <small class="text-danger">{{ $errors->first('nombre') }}</small>                  
              </div>
              <div class="col-sm-3">
                {!! Form::select('tipo[]', ['Combustible'=>'Combustible','Electrica'=>'Electrica','Hidraulica'=>'Hidraulica','Mecanica'=>'Mecanica', 'Neumatica'=>'Neumatica' ], null, ['id' => 'tipo[]', 'class' => 'form-control', 'placeholder'=>'Seleccione']) !!}
              </div>
            </div>
          </div>
        @else
        <div id="input1" class="clonedInput">
          <div class=" form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
            {!! Form::label('nombre[]', 'Falla y Tipo: ',['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::text('nombre[]', null, ['id' => 'nombre[]', 'class' => 'form-control', 'maxlength'=>200,'minlength'=>5, 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('nombre') }}</small>                  
            </div>
            <div class="col-sm-3">
              {!! Form::select('tipo[]', ['Combustible'=>'Combustible','Electrica'=>'Electrica','Hidraulica'=>'Hidraulica','Mecanica'=>'Mecanica', 'Neumatica'=>'Neumatica' ], null, ['id' => 'tipo[]', 'class' => 'form-control', 'placeholder'=>'Seleccione', 'required' => 'required']) !!}
            </div>
          </div>
        </div>
        @endif 
        <div class=" form-group {{ $errors->has('observacion') ? ' has-error' : '' }}">
            {!! Form::label('observacion', 'Observaciones:',['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::textarea('observacion', (isset($reporte)?$reporte->observacion:null), ['class' => 'form-control', 'required' => 'required','rows'=>3]) !!}
              <small class="text-danger">{{ $errors->first('observacion') }}</small>
            </div>
        </div> 
        @if(isset($reporte))
          @if($reporte->archivos->count() >0 )
          <div class="row">
            <div class="col-sm-4">
              <b class="pull-right">Archivos actuales:</b>
            </div>
            <div class="col-sm-7">
              <table class="table table-responsive " cellspacing="0">
                <thead  style="color:gray; ">
                  <tr>
                    <th style="text-align: center; width:10%">Eliminar</th>
                    <th style="text-align: center; width:30%">Nombre del archivo</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($reporte->archivos as $archivo)
                  <tr >
                    <td style="text-align: center;"><input type="checkbox" value="{{ $archivo->id}}" name="eliminar[]"></td>
                    <td >
                      <a href="{{asset('public/image/archivos/'.$archivo->nombre_interno) }} " target="_blank">
                        {{ $archivo->nombre_externo}}
                      </a>
                    </td>
                  </tr>
                  @endforeach  
                </tbody> 
              </table>            
            </div>
          </div>
          @endif 
        @endif
        <div class=" form-group {{ $errors->has('archivo')?' has-error':'' }}">
          {!! Form::label('archivo', 'Archivo(s):',['class' => 'col-sm-4 control-label']) !!}
          <div class="col-sm-7">
            <input type="file" name="archivo[]" id='archivo' multiple="">
            <p class="help-block">Si es mas de un archivo, debe seleccionarlos al mismo tiempo (deben estar en la misma ubicación). Recuerde que si quiere visualizar las imagenes en los reportes impresos, estas deben estar en formato jpg o png.</p>
            @if($errors->has('archivo'))
            <span class="text-danger">
              <strong>{{ $errors->first('archivo')}}</strong>
            </span>
            @endif
          </div>
        </div>          
    </div>  
    <div class="box-footer with-border">
        <div class="col-md-4 col-md-offset-5">
          <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>
          {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}     
        </div>
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

<!-- fileinput -->
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script>


  $(function() {
    //<!-- fileinput -->
    $('#archivo').fileinput({
    showUpload: false,
    language: 'es',
    browseClass: "btn btn-info",
    maxFileSize: 500,
    maxFileCount: 5,
    overwriteInitial: false,
    })
    //<!-- fileinput -->
  });  

  $("#activo_id").select2({
  placeholder: "Seleccione",
  width:'100%',
  language: "es",
  })

</script>



<script type="text/javascript">
        
function agregar(){

    var num     = $('.clonedInput').length;
    var newNum  = new Number(num + 1);

    var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);

    $('#input' + num).after(newElem);
    $('#borrar').attr('disabled',false);

    if (newNum == 5)
        $('#nuevo').attr('disabled','disabled');
};


function coño(){
    var num = $('.clonedInput').length;

    $('#input' + num).remove();
    $('#nuevo').attr('disabled',false);

    if (num-1 == 1)
        $('#borrar').attr('disabled','disabled');
};

$('#borrar').attr('disabled','');
   
</script>

@endsection