<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Ticket de Entrada/Salida Nro. {{$ticket->id}}</title>
    <link rel="stylesheet" href="{{asset('public/dist/print.css') }}">
    <style type="text/css">

      div.head_table {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        width: 100%;
        text-align: left;
        border-collapse: collapse;
      }
      .divTable.head_table .divTableCell {
        border: 1px solid #AAAAAA;
        padding: 6px 6px;
        font-size: 13px;
        background: #344563;
        color: white;
      }
     
      div.minimalistBlack {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
      }
      .divTable.minimalistBlack .divTableCell, .divTable.minimalistBlack .divTableHead {
        padding: 10px 10px;
        border: 1px solid #AAAAAAA;
      }
      .divTable.minimalistBlack .divTableBody .divTableCell {
        font-size: 13px;
      }
      /* DivTable.com */
      .divTable{ display: table; }
      .divTableRow { display: table-row; }
      .divTableHeading { display: table-header-group;}
      .divTableCell, .divTableHead { display: table-cell;}
      .divTableHeading { display: table-header-group;}
      .divTableFoot { display: table-footer-group;}
      .divTableBody { display: table-row-group;}
    </style>
  </head>
 <body class="clearfix">
    <div style="margin-top: 10px;margin-bottom: 10px;white-space: nowrap;">
      <img src="{{ asset('public/image/logo_AmazonasTech_RIF.png') }}" class="pull-left" width="199">
      <b class="pull-right" style="bottom: 0px;">Departamento de Gestión de Flota</b>
    </div> <br /><br /><br /><br />

    <div class="text-right" style="margin-top: 10px;margin-bottom: 10px;">
      <b>Ticket de Entrada/Salida <br /> NRO. {{$ticket->id}}</b>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DEL EQUIPO O UNIDAD</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%;" ><b>Nro. de Activo</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%;"><b>Placa</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%;"><b>Marca</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%;"><b>Modelo</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$ticket->activo->nro_activo}}</div>
          <div class="divTableCell">{{$ticket->activo->placa}}</div>
          <div class="divTableCell">{{$ticket->activo->marca}}</div>
          <div class="divTableCell">{{$ticket->activo->modelo}}</div>
        </div>
      </div>
    </div> 
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DEL TICKET</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%;"><b>Fecha y hora de salida</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%;"><b>Fecha y hora de llegada</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$ticket->fecha_salida}} {{$ticket->hora_salida}}</div>
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$ticket->fecha_llegada}} {{$ticket->hora_llegada}}</div>
        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%;"><b>Responsable</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%;"><b>C.I.</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$ticket->empleado->nombre}}</div>
          <div class="divTableCell">{{$ticket->empleado->cedula}}</div>
        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Pasajeros</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$ticket->pasajeros}}</div>
        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Ruta</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$ticket->ruta}}</div>
        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Notas / Descripción</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$ticket->descripcion}}</div>
        </div>
      </div>
    </div>
  </body>
</html>