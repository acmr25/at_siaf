<!-- Modal -->
<div class="modal fade" id="modal-ver" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulo-modal"></h4>
      </div>
      <div class="modal-body" id="contenido-modal">
     
      </div>
      <div class="modal-footer" id="botones">     
      </div>      
    </div>
  </div>
</div>

