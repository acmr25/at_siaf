<!-- Modal -->
<div class="modal fade" id="modal-completar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulo-completar"></h4>
      </div>
      <div class="modal-body" id="contenido-completar">
        <div id="salida_horarios"></div>
        {!! Form::open(['id'=>'form-completar']) !!}
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'ticket_id']) !!}
        <div class="row">
          <div class="form-group col-md-6" id="field-fecha_llegada">
              {!! Form::label('fecha_llegada', 'Fecha de llegada:') !!}
              {!! Form::date('fecha_llegada', null, ['id' => 'fecha_llegada','class' => 'form-control', 'required' => 'required']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="field-hora_llegada">
             {!! Form::label('hora_llegada', 'Hora:') !!}
             {!! Form::time('hora_llegada', null, ['id' => 'hora_llegada','class' => 'form-control', 'required' => 'required']) !!}
            <span><strong class="text-danger msj-error"></strong></span>
          </div>       
        </div>
        <div class="row">
          <div class="form-group col-md-6" id="field-horas">
              {!! Form::label('horas', 'Horometraje Actual:') !!}
              {!! Form::number('horas', null, ['id' => 'horas','class' => 'form-control', 'required' => 'required']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="field-kilometros">
             {!! Form::label('kilometros', 'Kilometraje Actual:') !!}
             {!! Form::number('kilometros', null, ['id' => 'kilometros','class' => 'form-control', 'required' => 'required']) !!}
            <span><strong class="text-danger msj-error"></strong></span>
          </div>       
        </div>   
      </div>
      <div class="modal-footer" id="botones">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="completar-ticket" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>
        {!! Form::close() !!}      
      </div>      
    </div>
  </div>
</div>

