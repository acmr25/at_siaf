<!-- Modal -->
<div class="modal fade" id="modal-ticket" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulo-modal">Formulario de Ticket</h4>
      </div>
      <div class="modal-body" id="contenido-modal">
        {!! Form::open(['id'=>'form-ticket']) !!}
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'ticket_id']) !!}
        <div class="form-group" id="field-activo_id">
            {!! Form::label('activo_id', 'Unidad / Equipo:', ['class' => 'control-label']) !!}
            {!! Form::select('activo_id', $activos_opciones , null, ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-responsable_id">
            {!! Form::label('responsable_id', 'Responsable:', ['class' => 'control-label']) !!}
            {!! Form::select('responsable_id', $empleados_opciones , null, ['id' => 'responsable_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="row">
          <div class="form-group col-md-6" id="field-fecha_salida">
              {!! Form::label('fecha_salida', 'Fecha de Salida:') !!}
              {!! Form::date('fecha_salida', null, ['id' => 'fecha_salida','class' => 'form-control', 'required' => 'required']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="field-hora_salida">
             {!! Form::label('hora_salida', 'Hora:') !!}
             {!! Form::time('hora_salida', null, ['id' => 'hora_salida','class' => 'form-control', 'required' => 'required']) !!}
            <span><strong class="text-danger msj-error"></strong></span>
          </div>          
        </div>        
        <div class="form-group"  id="field-pasajeros">
            {!! Form::label('pasajeros', 'Pasajero(s):') !!}
            {!! Form::text('pasajeros', null, ['id' => 'pasajeros','class' => 'form-control', 'required' => 'required']) !!}
            <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-ruta">
            {!! Form::label('ruta', 'Ruta/Recorrido') !!}
            {!! Form::textarea('ruta', null, ['id' => 'ruta', 'class' => 'form-control', 'required' => 'required','rows'=>1]) !!}
            <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-descripcion">
            {!! Form::label('descripcion', 'Notas/descripción:') !!}
            {!! Form::textarea('descripcion', null, ['id' => 'descripcion', 'class' => 'form-control', 'required' => 'required','rows'=>1]) !!}
            <span><strong class="text-danger msj-error"></strong></span>
        </div>
     
      </div>
      <div class="modal-footer" id="botones">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-ticket" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>
        {!! Form::close() !!}      
      </div>      
    </div>
  </div>
</div>

