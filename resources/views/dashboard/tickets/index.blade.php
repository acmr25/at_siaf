@extends('templates.master')

@section('title', 'Tickets')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{ asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-ticket">
            <span class="text-success"><i class="fa fa-plus"></i></span> Crear Ticket
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-tickets"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-tickets" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Unidad/Equipo</th>
              <th>Responsable</th>
              <th>Fecha y hora de Salida</th>
              <th>Fecha y hora de Llegada</th>
              <th></th>
            </tr>
          </thead>
        </table>        
      </div><!-- /.box-body -->
  </div>
</div>
@include('dashboard.tickets.ver_ticket')
@include('dashboard.tickets.completar')
@include('dashboard.tickets.modal_ticket')

  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script src="{{asset('public/js/ticket.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';

  $(".select2").select2({
    placeholder: "Seleccione",
    width:'100%',
    language: "es",
  })
</script>



@endsection

