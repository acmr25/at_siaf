<!-- Modal -->
<div class="modal fade" id="modal-proveedores" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-proveedor']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario de Proveedores</h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'id_proveedor']) !!} 

        <div class="row">
          <div class="form-group  col-sm-6" id="field-rif-proveedor">
            {!! Form::label('rif', 'RIF:', ['class' => 'control-label']) !!}<span style="color:red;"> *</span>
            {!!Form::text('rif', null, ['id'=>'rif-proveedor', 'class'=>'form-control','placeholder'=>'J-31109522-5'])!!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group  col-sm-6" id="field-nombre-proveedor">
            {!! Form::label('nombre', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!!Form::text('nombre', null, ['id'=>'nombre-proveedor', 'class'=>'form-control','placeholder'=>'Nombre del proveedor','required'])!!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-6" id="field-direccion-proveedor">
            {!! Form::label('direccion', 'Dirección:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::textarea('direccion', null, ['id' => 'direccion-proveedor','class' => 'form-control', 'required' => 'required', 'placeholder'=> 'Dirección','rows'=>1]) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-telefono-proveedor">
            {!! Form::label('telefono', 'Telefono:',['class'=>'control-label']) !!}
            {!!Form::text('telefono', null, ['id'=>'telefono-proveedor', 'class'=>'form-control','placeholder'=>'Telefono','data-inputmask'=>"'mask': '9999-999-9999'",'data-mask'])!!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div> 
        </div>
        <div class="row">
          <div class="form-group col-sm-6" id="field-tipo-proveedor">
            {!! Form::label('tipo', 'Tipo de proveedor:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('tipo', ['Proveedor Externo'=>'Proveedor Externo'] , 'Proveedor Externo', ['id' => 'tipo-proveedor', 'class' => 'form-control','required' => 'required']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
          <div class="form-group col-sm-6" id="field-condicion-proveedor">
            {!! Form::label('condicion', 'Estatus:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
            {!! Form::select('condicion', ['Activo'=>'Activo','Inactivo'=>'Inactivo'] , null, ['id' => 'condicion-proveedor', 'class' => 'form-control','required' => 'required', 'placeholder'=> 'Seleccione']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
          </div>
        </div>
        <div class="col-sm-offset-9">
          <span style="color:red;"> *</span><small class="text-muted"> Campos Requeridos </small><span style="color:red;"> *</span>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-proveedor" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>