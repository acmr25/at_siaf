@extends('templates.master')

@section('title', 'Proveedores')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('public/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@endsection

@section('titulo_modulo', 'Proveedores')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-proveedores">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Proveedor
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-proveedores"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="proveedores" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>RIF</th>
              <th>Nombre</th>
              <th>Dirección</th>              
              <th>Telefono</th>
              <th>Estatus</th>
              <th>Acción</th>            
            </tr>
          </thead>
        </table>
        
      </div><!-- /.box-body -->
  </div>
</div>

@include('dashboard.proveedores.modal')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{asset('public/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{asset('public/js/proveedor.js')}}"></script>



<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection

