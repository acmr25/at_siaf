<!-- Modal -->
<div class="modal fade" id="modal-departamentos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-departamento']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Departamentos</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('departamento_id', null, ['id'=>'departamento_id']) !!}
        <div class="form-group" id="field-nombre-departamento">
          {!! Form::label('departamento_nombre', 'Nombre:') !!}
          {!! Form::text('departamento_nombre', null, ['id' => 'departamento_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div> </br>
        <div class="form-group" id="field-descripcion-departamento">
          {!! Form::label('departamento_descripcion', 'Descripción:') !!}
          {!! Form::textarea('departamento_descripcion', null, ['id' => 'departamento_descripcion','class' => 'form-control col-sm-4', 'required' => 'required', 'rows'=>3]) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div> </br>
        <div class="form-group" id="field-padre-departamento">
            {!! Form::label('departamento_padre', 'Gerencia:') !!}
            {!! Form::select('departamento_padre', [], null, ['id' => 'departamento_padre', 'class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('departamento_padre') }}</small>
        </div> </br>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-departamento" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>