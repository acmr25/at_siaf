<!-- Modal -->
<div class="modal fade" id="modal-sitios" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-area']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Areas</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('sitio_id', null, ['id'=>'sitio_id']) !!}
        <div class="form-group" id="field-nombre-sitio">
          {!! Form::label('sitio_nombre', 'Nombre:') !!}
          {!! Form::text('sitio_nombre', null, ['id' => 'sitio_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div> </br>
        <div class="form-group" id="field-padre-sitio">
            {!! Form::label('sitio_padre', 'Gerencia a la cual perteece:') !!}
            {!! Form::select('sitio_padre', [], null, ['id' => 'sitio_padre', 'class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('sitio_padre') }}</small>
        </div> </br>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-sitio" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>