<!-- Modal -->
<div class="modal fade" id="modal-roles" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-rol']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Roles</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('rol_id', null, ['id'=>'rol_id']) !!}
        <div class="form-group" id="field-nombre-rol">
          {!! Form::label('rol_nombre', 'Nombre:') !!}
          {!! Form::text('rol_nombre', null, ['id' => 'rol_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
        </br></br>  
        <div class="form-group" id="field-descripcion-rol">
          {!! Form::label('rol_descripcion', 'Descripción:') !!}
          {!! Form::textarea('rol_descripcion', null, ['id' => 'rol_descripcion','class' => 'form-control col-sm-4', 'required' => 'required', 'rows'=>3]) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
        </br>  
        </br>
        </br> 
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-rol" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>