@extends('templates.master')

@section('title', 'Mantenimiento')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('contenido')
<div class="row" >
  <div class="col-md-6">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-roles">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Rol
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-roles"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-roles" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Descripción</th>
              <th></th>
            </tr>
          </thead>
        </table>        
      </div><!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-6">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-departamentos">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Departamentos
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-departamentos"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-departamentos" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Descripción</th>
              <th>Gerencia</th>
              <th></th>
            </tr>
          </thead>
        </table>        
      </div><!-- /.box-body -->
    </div>
  </div>
</div>
<div class="row" >
  <div class="col-md-6">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-sitios">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Sitio
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-sitios"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-sitios" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Zona/Ciudad</th>
              <th>Padre</th>
              <th></th>
            </tr>
          </thead>
        </table>        
      </div><!-- /.box-body -->
    </div>
  </div>
</div>

@include('dashboard.mantenimiento.sitio')
@include('dashboard.mantenimiento.rol')
@include('dashboard.mantenimiento.departamento')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/validate.js')}}"></script>
<script type="text/javascript">
function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 
</script>
<script src="{{asset('public/js/sitio.js')}}"></script>
<script src="{{asset('public/js/departamento.js')}}"></script>
<script src="{{asset('public/js/rol.js')}}"></script>
@endsection

