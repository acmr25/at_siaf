@extends('templates.master')

@section('title', 'Usuarios')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-user">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Usuario
          </button>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-user"><i class="fa fa-repeat"></i> Actualizar</a><hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-usuarios" width="100%" >
          <thead style="">
            <tr>
              <th>ID</th>
              <th>Cédula</th>
              <th>Nombre</th>
              <th>Cargo</th>
              <th>Gerencia/Departamento</th>
              <th>Correo</th>
              <th>Usuario</th>
              <th>Estado</th>
              <th>Rol</th>
              <th></th>
            </tr>
          </thead>
        </table>        
      </div><!-- /.box-body -->
  </div>
</div>
@include('dashboard.usuarios.modal')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>
<script src="{{asset('public/js/validate.js')}}"></script>

<script src="{{asset('public/js/user.js')}}"></script>
<script type="text/javascript">
$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
</script>
@endsection

