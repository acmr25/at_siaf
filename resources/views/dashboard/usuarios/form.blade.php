@extends('templates.master')

@section('title', 'Perfil')

@section('css')

@endsection

@section('titulo_modulo', 'Perfil')


@section('contenido')
<div class="row" >
  <div class="col-md-6 col-md-offset-3">
    <div class="box box-success">
      <div class="box-header with-border">
          <h3 class="box-title">{{$user->nombre}}</h3>
      </div>
      <div class="box-body" > 
        {!! Form::open(['method' => 'PUT', 'route' => ['perfil.update', $user->id], 'class' => 'form-horizontal', 'files' => true]) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="col-md-12">
        <div class="row form-group">
            <div class=" col-md-6 {{ $errors->has('nombre') ? ' has-error' : '' }}">
              {!! Form::label('nombre', 'Nombre Completo:') !!}
              {!! Form::text('nombre', $user->nombre , ['id' => 'nombre','class' => 'form-control', 'required' => 'required', 'onkeypress'=>'return soloLetras(event)']) !!}
              <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
            </div>
            <div class=" col-md-6 {{ $errors->has('cedula') ? ' has-error' : '' }}">
                {!! Form::label('cedula', 'Cédula de Identidad:') !!}
                {!! Form::text('cedula', $user->cedula , ['id' => 'cedula','class' => 'form-control numero', 'required' => 'required','onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl"]) !!}
                <strong class="text-danger">{{ $errors->first('cedula') }}</strong>
            </div>
        </div>
        <div class="row form-group">
          <div class=" col-md-6 {{ $errors->has('cargo') ? ' has-error' : '' }}">
              {!! Form::label('cargo', 'Cargo:') !!}
              {!! Form::text('cargo', $user->cargo , ['id' => 'cargo','class' => 'form-control', 'required' => 'required','onkeypress'=>'return soloLetras(event)']) !!}
              <strong class="text-danger">{{ $errors->first('cargo') }}</strong>
          </div>
          <div class=" col-md-6 {{ $errors->has('departamento_id') ? ' has-error' : '' }}">
              {!! Form::label('departamento_id', 'Gerencia/Departamento:') !!}
              {!! Form::select('departamento_id', $departamentos, $user->departamento_id , ['id' => 'departamento_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <strong class="text-danger">{{ $errors->first('departamento_id') }}</strong>
          </div>  
        </div>
        <div class="row form-group">
          <div class=" col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Correo Electronico:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                {!! Form::email('email', $user->email , ['id' => 'email','class'=>'form-control', 'required' => 'required','placeholder'=>'pasante.ait@amazonastech.com.ve']) !!}
              </div>
              <strong class="text-danger">{{ $errors->first('email') }}</strong>
          </div>
          <div class="col-md-6 {{ $errors->has('usuario') ? ' has-error' : '' }}">
            {!! Form::label('usuario', 'Nombre de usuario:') !!}
            {!! Form::text('usuario',  $user->usuario, ['id' => 'usuario','class' => 'form-control', 'required' => 'required', 'readonly']) !!}
            <strong class="text-danger">{{ $errors->first('usuario') }}</strong>
          </div>       
        </div>
        <div class="row form-group">
          <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('password', 'Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password', ['id' => 'password','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <strong class="text-danger">{{ $errors->first('password') }}</strong>
          </div>
          <div class="col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              {!! Form::label('password_confirmation', 'Confirme Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password_confirmation', ['id' => 'password_confirmation','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
          </div>
        </div>
        </div>
      </div>
      <div class="box-footer with-border">
          <a href="{{ url('') }}" class="btn btn-info ">Cancelar</a>
          {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     
      </div>       
      {!! Form::close() !!}
    </div>  
  </div>
</div>
  
@endsection

@section('js')
<script src="{{asset('public/js/validate.js')}}"></script>
<script type="text/javascript">
$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
</script>

@endsection