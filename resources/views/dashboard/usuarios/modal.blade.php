<!-- Modal -->
<div class="modal fade" id="modal-user" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-user']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario de Usuario</h4>
      </div>
      <div class="modal-body">

        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'user_id']) !!}
        <div class="row">
            <div class=" form-group col-md-6" id="field-nombre">
              {!! Form::label('nombre', 'Nombre Completo:') !!}
              {!! Form::text('nombre', null, ['id' => 'nombre','class' => 'form-control', 'required' => 'required', 'placeholder' => 'Angela Montilla','onkeypress'=>'return soloLetras(event)']) !!}
              <span><strong class="text-danger msj-error"></strong></span>   
            </div>
            <div class=" form-group col-md-6" id="field-cedula">
              {!! Form::label('cedula', 'Cédula de Identidad:') !!}
              {!! Form::text('cedula', null, ['id' => 'cedula','class' => 'form-control numero','placeholder'=>'23.533.432','required' => 'required','onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl"]) !!}
              <span><strong class="text-danger msj-error"></strong></span>     
            </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6" id="field-cargo">
              {!! Form::label('cargo', 'Cargo:') !!}
              {!! Form::text('cargo', null, ['id' => 'cargo','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Pasante de Transporte y Servicios','onkeypress'=>'return soloLetras(event)']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="field-departamento_id">
              {!! Form::label('departamento_id', 'Gerencia/Departamento:') !!}
              {!! Form::select('departamento_id',$departamentos, null, ['id' => 'departamento_id','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione',]) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>   
        </div>        
        <div class="row">
          <div class="form-group col-md-6" id="field-email">
              {!! Form::label('email', 'Correo Electronico:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                {!! Form::email('email', null, ['id' => 'email','class'=>'form-control', 'required' => 'required','placeholder'=>'pasante.ait@amazonastech.com.ve']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="field-usuario">
              {!! Form::label('usuario', 'Nombre de Usuario:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"  style="font-size: 12px"></i></span>
                {!! Form::text('usuario', null, ['id' => 'usuario','class'=>'form-control', 'required' => 'required','placeholder'=>'acmontilla','onkeypress'=>'return soloLetras(event)']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
        </div>       
        <div class="row">
          <div class="form-group col-md-6" id="field-password">
              {!! Form::label('password', 'Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password', ['id' => 'password','class' => 'form-control', 'required' => 'required', 'placeholder'=>'********']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>        
          <div class="form-group col-md-6" id="field-password_confirmation">
              {!! Form::label('password_confirmation', 'Confirme Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password_confirmation', ['id' => 'password_confirmation','class' => 'form-control', 'required' => 'required', 'placeholder'=>'********']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6" id="CAMPO_ROL">
              {!! Form::label('rol_id','Tipo de Usuario') !!}
              {!! Form::select('rol_id',$roles, null, ['id' => 'rol_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="CAMPO_ESTADO">
              {!! Form::label('estado', 'Estatus de Usuarios') !!}
              {!! Form::select('estado',['ACTIVO'=>'Activo','INACTIVO'=>'Inactivo'], null, ['id' => 'estado', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
        </div>       
      </div>
      <div class="modal-footer">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-usuario" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

