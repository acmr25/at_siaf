<!-- Modal -->
<div class="modal fade" id="modal-compleado" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Completar Orden de trabajo</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'form-estado']) !!}
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('activo_id', $orden->activo->id, ['id'=>'activo_id']) !!}
        {!! Form::hidden('orden_id', $orden->id, ['id'=>'orden_id']) !!}

        <div class=" col-md-12 form-group" id="field-termina">
            {!! Form::label('termina', 'Fecha de Completación', ['class' => 'col-sm-5 control-label text-right']) !!}
            <div class="col-sm-6">
              {!! Form::date('termina', null, ['id'=>'termina','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('termina') }}</small>
            </div>
        </div>
        @if($orden->activo->medida != null)
          @if($orden->activo->medida->horas > 0 || $orden->activo->medida->horas != null)
          <div class=" col-md-12 form-group " id="field-horas">
              {!! Form::label('horas', 'Horas', ['class' => 'col-sm-5 control-label text-right']) !!}
              <div class="col-sm-6">
                {!! Form::number('horas', $orden->activo->medida->horas, ['id'=>'horas', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('horas') }}</small>
            </div>
          </div>
          @endif
          @if($orden->activo->medida->kilometros > 0 || $orden->activo->medida->kilometros != 0)
          <div class=" col-md-12 form-group" id="field-kilometros">
              {!! Form::label('kilometros', 'Kilometros', ['class' => 'col-sm-5 control-label text-right']) !!}
              <div class="col-sm-6">
                {!! Form::number('kilometros', $orden->activo->medida->kilometros, ['id'=>'kilometros', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('kilometros') }}</small>
            </div>
          </div>
          @endif
        @endif
        <br /><br /><br /><br /><br /><br /><br />     
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-completar" class="btn btn-success" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
          {!! Form::close() !!}
        </div>      
      </div>
    </div>
  </div>
</div>