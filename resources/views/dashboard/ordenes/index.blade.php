@extends('templates.master')

@section('title', 'Ordenes')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('contenido')
{!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
<div class="row" >
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
        <a class="btn btn-default" href="{{route('ordenes.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span> Crear Orden</a>
        <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-ordenes"><i class="fa fa-repeat"></i> Actualizar</a>
        <br>
        <hr>
        <table class="table table-bordered table-hover table-striped" id="tabla-ordenes" >
          <thead>
            <tr>
              <th style="text-align: center;">Nro.</th>
              <th style="text-align: center;">Fecha de Inicio</th>
              <th style="text-align: center;">Fecha de Fin</th>
              <th style="text-align: center;">Responsable</th>
              <th style="text-align: center;">Unidad/Equipo</th>
              <th style="text-align: center;">Estado</th>
            </tr>
          </thead>
        </table>
      </div><!-- /.box-body -->
  </div>
</div>

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('public/js/orden.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>

@endsection

