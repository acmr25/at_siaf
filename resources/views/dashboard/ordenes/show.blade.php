@extends('templates.master')

@section('title', 'Ordenes')

@section('css')
<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style type="text/css">
.custom {
    width: 100px !important;
    height: 50px !important;
    margin-right: 5px !important;
}

</style>
@endsection


@section('contenido')
 
<div class="box box-success" >
  <div class="box-header"  id="botones"><br />
    <div class="row">
      <div class="col-md-3">
        <b style="font-size: 20px">Orden de Trabajo Nro. {{$orden->id}}</b>
      </div>
      <div class="col-md-3">
        <div class="btn-group">
          @if($orden->estado_orden != 'completada')
          <a class="btn btn-default" href="{{route('ordenes.edit', $orden->id)}}" title="Eliminar"><i class="fa fa-fw fa-pencil"></i></a>
          @endif
          <a href="{{route('ordenes.pdf', $orden->id)}}" class="btn btn-default" title="PDF" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
          <a class="btn btn-default" OnClick="delete_Orden('{{$orden->id}}')" title="Eliminar" ><i class="fa fa-fw fa-trash"></i></a>
        </div>
      </div>
      <div class="col-md-6">
        <button type="button" @if($orden->estado_orden == 'abierta') class="btn btn-success custom " disabled="true" @else class="btn btn-default custom " @endif style="font-size: 12px" onclick="cambiarEstado('{{$orden->id}}','abierta')"><b>ABIERTA</b></button>
        <button type="button" @if($orden->estado_orden == 'en progreso') class="btn btn-warning custom " disabled="true" @else class="btn btn-default custom " @endif style="font-size: 12px" onclick="cambiarEstado('{{$orden->id}}',3)"><b>EN PROCESO</b></button>
        <button type="button" @if($orden->estado_orden == 'detenida') class="btn btn-danger custom " disabled="true" @else class="btn btn-default custom " @endif style="font-size: 12px" onclick="cambiarEstado('{{$orden->id}}','detenida')"><b>DETENIDA</b></button>
        <button type="button" @if($orden->estado_orden == 'completada') class="btn btn-info custom " disabled="true" @else class="btn btn-default custom " @endif style="font-size: 12px"  data-toggle="modal" data-target="#modal-compleado"><b>COMPLETADA</b></button>
      </div>
    </div>
  </div>
  <div class="box-body" >
    <div id="detalles" class="row">
      <div id="detalles_orden" class="col-md-5">
        <table class="table" style="border:hidden; ">
          <tr style="border:hidden;">
            <td style="color: #605F5F">Unidad/Equipo:</td><td>
              <a href="{{route('activos.show',$orden->activo->id)}}">
                {{$orden->activo->unidad}} {{$orden->activo->placa}} {{$orden->activo->marca}} {{$orden->activo->modelo}} { {{$orden->activo->nro_activo}} }
              </a>
            </td>
          </tr>          
          <tr style="border:hidden;">
            <td style="color: #605F5F">Responsable:</td><td>{{$orden->empleado->nombre}}</td>
          </tr>
          <tr style="border:hidden;">
            <td style="color: #605F5F">Prioridad:</td><td  style="color: @if($orden->Alta == 'Alta') red @else orange @endif">{{$orden->prioridad}}</td>
          </tr>
          <tr style="border:hidden;">
            <td style="color: #605F5F">Fecha de Inicio:</td><td>{{$orden->inicia}}</td>
          </tr>
          @if($orden->estado_orden == 'completada')
          <tr style="border:hidden;">
            <td style="color: #605F5F">Fecha de Fin:</td><td>{{$orden->termina}}</td>
          </tr>
          @endif
        </table>
      </div>
      @if($orden->reporte)
      <div id="detalles_reporte" class="col-md-7">
        <table class="table" style="border:hidden; ">
          <tr style="border:hidden;">
            <td style="color: #605F5F">Reporte de fallas:</td><td>
              <a href="{{route('activo-reporte.pdf',$orden->reporte->id)}}"  target="_blank">
                Nro .{{$orden->reporte->id}}
              </a>
            </td>
          </tr>          
          <tr style="border:hidden;">
            <td style="color: #605F5F">Reportado por:</td><td>{{$orden->reporte->informador}} {{$orden->reporte->cargo_informador}}</td>
          </tr>
          <tr style="border:hidden;">
            <td style="color: #605F5F">Tipo y descripcion de la fallas:</td>
              <td>
                @foreach ($orden->reporte->fallas as $falla)
                {{$falla->tipo}} - {{$falla->nombre}}.<br />
                @endforeach
              </td>
          </tr>
        </table>
      </div>
      @endif      
    </div>    
    <div >
      <table class="table table-hover" style="border:hidden; " id="tareas">
        <tr style="background-color: #E5E7E9">
          <td colspan="3" >TAREAS ASIGNADAS</td>
        </tr>
        @foreach ($orden->tareas as $tarea)
          <tr>
            <td class="text-center" width="10%"> 
              @if($tarea->pivot->status == 'completa')<i class="fa fa-fw fa-check text-success"></i> 
              @else <i class="fa fa-fw fa-clock-o text-warning"></i> 
              @endif
            </td>
            <td>{{$tarea->nombre}}</td>
            <td class="text-center" width="10%"> 
              @if($orden->estado_orden != 'completada')
                <?php if( count($orden->tareas) > 1) { ?>
                <button type="button" class="btn btn-danger btn-xs" onclick="delete_Tarea({{$orden->id}},{{$tarea->id}})">
                    <i class="fa fa-fw fa-trash"> </i>
                </button>   
                <?php }?>
              @endif
            </td>
          </tr>
        @endforeach
      </table>

      <div style="display:none" id="div1" >

        {!! Form::open(['id'=>'form-tarea']) !!}

          {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
          {!! Form::hidden('activo_id', $orden->activo->id, ['id'=>'activo_id']) !!}
          {!! Form::hidden('orden_id', $orden->id, ['id'=>'orden_id']) !!}
          
          
            <div class="col-md-6" id="select_tarea">
              {!! Form::select('tarea_id', [] , null, ['id' => 'tarea_id', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}             
            </div> 
            
              <button type="button" id="guardar-tarea" class="btn btn-success " data-loading-text="Guardando..." autocomplete="off"><i class="fa fa-fw fa-save"></i></button>
              <button type="button" class="btn btn-default " onClick="cambiarDisplay('div1','div2')" ><i class="fa fa-fw fa-close"></i></button>
                  
        {!! Form::close() !!}
      <br />  
      </div>
      
      <div style="display;" id="div2" >
        @if($orden->estado_orden != 'completada')
        <button type="button" class="btn btn-default" data-toggle="modal" onClick="cambiarDisplay('div1','div2')">
          <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Tarea
        </button>
        <br />
        <br />
        @endif
      </div>
      
    </div>

    
    <div >
      <table class="table table-hover " style="border:hidden; " id="articulos">
        <tr style="background-color: #E5E7E9">
          <td colspan="5" >ARTICULOS/INSUMOS ASIGNADOS</td>
        </tr>
        <tr style="background-color: #F5F8FA" class="text-center">
          <td style="width: 110px">Nro. de activo </td>
          <td >Articulo/Insumo</td>
          <td style="width: 110px">Cantidad</td>
          <td>Uso</td>
          <td style="width: 110px"></td>
        </tr>        
        @if(isset($orden->movimientos))
          @foreach ($orden->movimientos as $movimiento)
          <tr class="text-center">
            <td ><a href="{{route('articulos.edit',$movimiento->articulo->id)}}"> {{$movimiento->articulo->nro_activo}}</a></td>
            <td>{{$movimiento->articulo->nombre}}</td>
            <td>{{$movimiento->cantidad}}</td>
            <td class="text-justify">{{$movimiento->razon}}</td>
            <td>
              @if($orden->estado_orden != 'completada')
              <button type="button" class="btn btn-danger btn-xs" onclick="delete_Movimiento({{$movimiento->id}})">
                <i class="fa fa-fw fa-trash"> </i>
              </button>
              @endif
            </td>
          </tr>
          @endforeach
        @endif        
      </table>
      <div style="display:none" id="div3" >

        {!! Form::open(['id'=>'form-movimiento']) !!}

          {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
          {!! Form::hidden('tipo', 'SALIDA', ['id'=>'tipo']) !!}
          {!! Form::hidden('activo_id', $orden->activo->id, ['id'=>'activo_id']) !!}
          {!! Form::hidden('orden_id', $orden->id, ['id'=>'orden_id']) !!}

          <div class="row">
            <div class="col-md-4">
              {!! Form::select('articulo_id', [] , null, ['id' => 'articulo_id', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}             
            </div>
            <div class="col-md-2" id="field-cantidad">
               {!! Form::number('cantidad', null, ['id' => 'cantidad','class' => 'form-control','min'=>0 ,'placeholder'=>'Cantidad']) !!}
            <span>
              <strong class="text-danger msj-error"></strong>
            </span>
            </div>
            <div class="col-md-4" id="field-razon">
              {!! Form::text('razon', null, ['id' => 'razon', 'class' => 'form-control', 'required' => 'required' ,'placeholder'=>'Razón de uso']) !!}
              <span>
                <strong class="text-danger msj-error"></strong>
              </span>
            </div>
            <div class="col-md-2">
              <button type="button" id="guardar-movimiento" class="btn btn-success" data-loading-text="..." autocomplete="off"><i class="fa fa-fw fa-save"></i></button>
              <button type="button" class="btn btn-default" onClick="cambiarDisplay('div3','div4')" ><i class="fa fa-fw fa-close"></i></button>
            </div>              
          </div>    
        {!! Form::close() !!}
      <br />  
      </div>
      
      <div style="display;" id="div4" >
        @if($orden->estado_orden != 'completada')
        <button type="button" class="btn btn-default" data-toggle="modal" onClick="cambiarDisplay('div3','div4')">
          <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Articulo
        </button>
        <br />
        <br />
        @endif
      </div>            
    </div>
    <div >
      <table class="table table-hover " style="border:hidden; " id="notas">
        <tr style="background-color: #E5E7E9">
          <td >NOTAS</td>
        </tr>
        <tr >
          <td>{{$orden->notas}}</td>
        </tr>               
      </table>     
    </div>
  </div>

</div>

@include('dashboard.ordenes.complete')

@endsection

@section('js')

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>
<script src="{{asset('public/js/orden_show.js')}}"></script>

@endsection