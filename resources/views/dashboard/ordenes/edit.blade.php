@extends('templates.master')

@section('title', 'Ordenes de Trabajo')

@section('css')
<!-- fileinput --> 
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')

<div class="row">
  @if(isset($orden->reporte))
  <div class=" col-md-8">
  @else
  <div class="col-md-offset-2 col-md-8">
  @endif
    @if($errors->has())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          
             @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
      </div>
    @endif  
    <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Ordenes de trabajo</h3>
    </div>
    <div class="box-body" >

      {!! Form::open(['method' => 'PUT', 'route' => ['ordenes.update', $orden->id], 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 

        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class=" form-group {{ $errors->has('activo_id') ? ' has-error' : '' }}">
            {!! Form::label('activo_id', 'Unidad / Equipo:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
            {!! Form::select('activo_id', $activos , $orden->activo_id, ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione' , 'disabled']) !!}
            <small class="text-danger">{{ $errors->first('activo_id') }}</small>
            </div>
        </div>        

        <div class="form-group{{ $errors->has('responsable_id') ? ' has-error' : '' }}">
            {!! Form::label('responsable_id', 'Responsable:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::select('responsable_id', $empleados, $orden->responsable_id, ['id' => 'responsable_id', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('responsable_id') }}</small>
          </div>
        </div>
        <div class="form-group{{ $errors->has('inicia') ? ' has-error' : '' }}">
            {!! Form::label('inicia', 'Fecha de Inicio', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::date('inicia', $orden->inicia, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('inicia') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('prioridad') ? ' has-error' : '' }}">
            {!! Form::label('prioridad', 'Prioridad:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::select('prioridad', ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'], $orden->prioridad, ['id' => 'prioridad', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('prioridad') }}</small>
          </div>
        </div>
        <div class="form-group{{ $errors->has('notas') ? ' has-error' : '' }}">
            {!! Form::label('notas', 'Notas:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::textarea('notas', $orden->notas, ['class' => 'form-control', 'rows'=>1]) !!}
              <small class="text-danger">{{ $errors->first('notas') }}</small>
          </div>
        </div>

    </div>  
    <div class="box-footer with-border">
        <div class="pull-right">

          <a href="{{ URL::previous() }}" class="btn btn-info ">Cancelar</a>

          {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}     
        </div>
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  @if(isset($orden->reporte))
  <div class="col-md-4">
    <div class="box">
      <div class="box-body">
        Orden de trabajo realizada para el reporte de fallas Nro. {{ $orden->reporte->id }} de la unidad {{ $orden->activo->nro_activo }}.
        <br /><br />
        Con las siguientes fallas:
        <table class="table table-responsive " cellspacing="0">
          <thead  style="color:gray; ">
            <tr>
              <th style="text-align: center; width:10%">Tipo</th>
              <th style="text-align: center; width:30%">Descripción</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($orden->reporte->fallas as $falla)
            <tr >
              <td >{{ $falla->tipo }}</td>
              <td >{{ $falla->nombre }}</td>
            </tr>
            @endforeach  
          </tbody> 
        </table>
      </div>
    </div>
  </div>
  @endif

</div>
  
  
@endsection

@section('js')

<!-- fileinput -->
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script>

$(function () {
//ACTIVO
 $(".select2").select2({
  placeholder: "Seleccione",
  width:'100%',
  language: "es",
})
})
</script>

@endsection