<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Orden de Trabajo Nro. {{$orden->id}}</title>
    <link rel="stylesheet" href="{{asset('public/dist/print.css') }}">
    <style type="text/css">

      div.head_table {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        width: 100%;
        text-align: left;
        border-collapse: collapse;
      }
      .divTable.head_table .divTableCell {
        border: 1px solid #AAAAAA;
        padding: 6px 6px;
        font-size: 13px;
        background: #344563;
        color: white;
      }
     
      div.minimalistBlack {
        font-family: Arial, Helvetica, sans-serif;
        border: 0px solid #F2F2F2;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
      }
      .divTable.minimalistBlack .divTableCell, .divTable.minimalistBlack .divTableHead {
        padding: 10px 10px;
        border: 1px solid #AAAAAAA;
      }
      .divTable.minimalistBlack .divTableBody .divTableCell {
        font-size: 13px;
      }
      /* DivTable.com */
      .divTable{ display: table; }
      .divTableRow { display: table-row; }
      .divTableHeading { display: table-header-group;}
      .divTableCell, .divTableHead { display: table-cell;}
      .divTableHeading { display: table-header-group;}
      .divTableFoot { display: table-footer-group;}
      .divTableBody { display: table-row-group;}
    </style>
  </head>
 <body class="clearfix">
    <div style="margin-top: 10px;margin-bottom: 10px;white-space: nowrap;">
      <img src="{{asset('public/image/logo_AmazonasTech_RIF.png') }}" class="pull-left" width="199">
      <b class="pull-right" style="bottom: 0px;">Departamento de Gestión de Flota</b>
    </div> <br /><br /><br /><br />

    <div class="text-right" style="margin-top: 10px;margin-bottom: 10px;">
      <b>Orden de Trabajo <br /> Nro. {{$orden->id}}</b>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DE LA ORDEN</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%"><b>Fecha de Inicio</b></div>          
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%"><b>Fecha de Fin</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%"><b>Prioridad</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 25%"><b>Estado</b></div>

        </div>
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$orden->inicia = date('d-m-Y', strtotime($orden->inicia))}}</div>
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$orden->termina }}</div>
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$orden->prioridad}}</div>
          <div class="divTableCell" style="text-align: center;vertical-align: middle; ">{{$orden->estado_orden}}</div>

        </div>
      </div>
    </div>
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%"><b>Responsable</b></div>
          <div class="divTableCell" style="text-align: center; background:#EDF2F7;width: 50%"><b>C.I.</b></div>
        </div>
        <div class="divTableRow">
          <div class="divTableCell">{{$orden->empleado->nombre}}</div>
          <div class="divTableCell">{{$orden->empleado->cedula}}</div>
        </div>
      </div>
    </div>
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>DETALLES DEL EQUIPO O UNIDAD</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Unidad</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Placa</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Marca</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;"><b>Modelo</b></div>
          <div  class="divTableCell" style="text-align: center; background:#EDF2F7;" ><b>Nro. de Activo</b></div>
        </div>
        <div class="divTableRow">
          <div  class="divTableCell">{{$orden->activo->unidad}}</div>
          <div  class="divTableCell">{{$orden->activo->placa}}</div>
          <div  class="divTableCell">{{$orden->activo->marca}}</div>
          <div  class="divTableCell">{{$orden->activo->modelo}}</div>
          <div  class="divTableCell">{{$orden->activo->nro_activo}}</div>
        </div>
      </div>
    </div>  
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>TAREAS / ACTIVIDADES</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div style="background:#EDF2F7; text-align: center; font-weight: bold; width: 8%;" class="divTableCell">Nro.</div>
          <div style="background:#EDF2F7; font-weight: bold;" class="divTableCell">Descripción</div>
        </div><?php $i=1; ?>
        @foreach ($orden->tareas as $tarea)          
            <div class="divTableRow">
              <div style="text-align: center; vertical-align: middle; " class="divTableCell">{{$i}}</div>
              <div class="divTableCell">{{$tarea->nombre}}</div>
            </div>
          <?php $i++; ?>
        @endforeach
      </div>
    </div>
    @if($orden->movimientos->count() > 0)
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>ARTICULOS / INSUMOS</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack">
      <div class="divTableBody" >
        <div class="divTableRow">
          <div style="background:#EDF2F7; text-align: center; font-weight: bold; width: 10%;" class="divTableCell">Nro.</div>
          <div style="background:#EDF2F7; font-weight: bold;" class="divTableCell">Nombre</div>
          <div style="background:#EDF2F7; font-weight: bold; width: 40px; text-align: center;" class="divTableCell">Cantidad</div>
          <div style="background:#EDF2F7; font-weight: bold; width: 300px" class="divTableCell">Razón</div>
        </div>
        @foreach ($orden->movimientos as $movimiento)          
            <div class="divTableRow">
              <div style="text-align: center; vertical-align: middle; " class="divTableCell">{{$movimiento->articulo->nro_activo}}</div>
              <div class="divTableCell">{{$movimiento->articulo->nombre}}</div>
              <div style="text-align: center; vertical-align: middle;" class="divTableCell">{{$movimiento->cantidad}}</div>
              <div class="divTableCell">{{$movimiento->razon}}</div>
            </div>
        @endforeach
      </div>
    </div>
    @endif
    <div class="divTable head_table">
      <div class="divTableBody">
        <div class="divTableRow">
          <div style="text-align: center;" class="divTableCell"><b>NOTAS</b></div>
        </div>
      </div>
    </div>    
    <div class="divTable minimalistBlack"  >
      <div class="divTableBody" >
        <div class="divTableRow">
          <div class="divTableCell" style="height: 100px"><p>{{$orden->notas}}</p></div>
        </div>
      </div>
    </div>
    <div></div>
  </body>
</html>