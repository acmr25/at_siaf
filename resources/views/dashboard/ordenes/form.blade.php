@extends('templates.master')

@section('title', 'Ordenes de Trabajo')

@section('css')
<!-- fileinput --> 
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

<!-- Select2 -->  
<link rel="stylesheet" href="{{asset('public/plugins/select2/dist/css/select2.min.css') }}">

@endsection

@section('contenido')

<div class="row">
  @if(isset($reporte))
  <div class=" col-md-8">
  @else
  <div class="col-md-offset-2 col-md-8">
  @endif
    @if($errors->has())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          
             @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
      </div>
    @endif  
    <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Ordenes de trabajo</h3>
    </div>
    <div class="box-body" >

      {!! Form::open(['method' => 'POST', 'route' => 'ordenes.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 

        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        @if(isset($activo))
          <div class=" form-group {{ $errors->has('activo_id') ? ' has-error' : '' }}">
              {!! Form::label('activo_id', 'Unidad / Equipo:', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-7">
              {!! Form::select('activo_id', $activos , (isset($activo)?$activo->id:null), ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('activo_id') }}</small>
              </div>
          </div>
        @else
        <div class=" form-group {{ $errors->has('activo_id') ? ' has-error' : '' }}">
            {!! Form::label('activo_id', 'Unidad / Equipo:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
            {!! Form::select('activo_id', $activos , (isset($reporte)?$reporte->activo_id:null), ['id' => 'activo_id', 'class' => 'form-control select2','required' => 'required','placeholder'=> 'Seleccione' ,  (isset($reporte)? 'disabled':null)]) !!}
            <small class="text-danger">{{ $errors->first('activo_id') }}</small>
            </div>
        </div>
        @endif

        @if(isset($reporte))
        {!! Form::hidden('activo_id', $reporte->activo_id) !!}
        {!! Form::hidden('reporte_id', $reporte->id) !!}
        @endif        

        <div class="form-group{{ $errors->has('responsable_id') ? ' has-error' : '' }}">
            {!! Form::label('responsable_id', 'Responsable:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::select('responsable_id', $empleados, null, ['id' => 'responsable_id', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('responsable_id') }}</small>
          </div>
        </div>
        <div class="form-group{{ $errors->has('inicia') ? ' has-error' : '' }}">
            {!! Form::label('inicia', 'Fecha de Inicio', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::date('inicia', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('inicia') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('prioridad') ? ' has-error' : '' }}">
            {!! Form::label('prioridad', 'Prioridad:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::select('prioridad', ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'], null, ['id' => 'prioridad', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('prioridad') }}</small>
          </div>
        </div>
        <div class="form-group{{ $errors->has('notas') ? ' has-error' : '' }}">
            {!! Form::label('notas', 'Notas:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-7">
              {!! Form::textarea('notas', null, ['class' => 'form-control', 'rows'=>1]) !!}
              <small class="text-danger">{{ $errors->first('notas') }}</small>
          </div>
        </div>

      <div style="border-top: solid;border-top-color: #E5E7E9;border-top-width: 2px" id="selectores">
        <br />
        <p> {!! Form::select('task', [] , null, ['id' => 'task', 'class' => 'form-control select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}</p>
        <div id="dynamicDiv">

        </div>
        <a class="btn btn-link" href="javascript:void(0)" id="addInput">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          Agregar Tarea
        </a>
      </div><br />
    </div>  
    <div class="box-footer with-border">
      <a href="{{ URL::previous() }}" class="btn btn-info">Cancelar</a>
      {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  @if(isset($reporte))
  <div class="col-md-4">
    <div class="box">
      <div class="box-body">
        Creando orden de trabajo para el reporte de fallas Nro. {{ $reporte->id }} de la unidad {{$reporte->activo->unidad}} {{$reporte->activo->placa}} {{$reporte->activo->marca}} {{$reporte->activo->modelo}} ({{$reporte->activo->nro_activo}}).
        <br /><br />
        Con las siguientes fallas:
        <table class="table table-responsive " cellspacing="0">
          <thead  style="color:gray; ">
            <tr>
              <th style="text-align: center; width:10%">Tipo</th>
              <th style="text-align: center; width:30%">Descripción</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($reporte->fallas as $falla)
            <tr >
              <td >{{ $falla->tipo }}</td>
              <td >{{ $falla->nombre }}</td>
            </tr>
            @endforeach  
          </tbody> 
        </table>
      </div>
    </div>
  </div>
  @endif
  @if(isset($orden->reporte))
  <div class="col-md-4">
    <div class="box">
      <div class="box-body">
        Creando orden de trabajo para el reporte de fallas Nro. {{ $orden->reporte->id }} de la unidad {{ $orden->activo->nro_activo }}.
        <br /><br />
        Con las siguientes fallas:
        <table class="table table-responsive " cellspacing="0">
          <thead  style="color:gray; ">
            <tr>
              <th style="text-align: center; width:10%">Tipo</th>
              <th style="text-align: center; width:30%">Descripción</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($orden->reporte->fallas as $falla)
            <tr >
              <td >{{ $falla->tipo }}</td>
              <td >{{ $falla->nombre }}</td>
            </tr>
            @endforeach  
          </tbody> 
        </table>
      </div>
    </div>
  </div>
  @endif

</div>
  
  
@endsection

@section('js')

<!-- fileinput -->
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('public/plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>

<!-- Select2 -->  
<script src="{{asset('public/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<script>

$(function () {
//ACTIVO
 $(".select2").select2({
  placeholder: "Seleccione",
  width:'100%',
  language: "es",
})
if ($('#reporte_id').val() != null) {
  $('#activo_id').trigger('change').prop('disabled',true);
}
//TAREAS
//si se trae a un  activo en especifico se mostrara esto

$.getJSON(ruta+"/ordenes/"+ $('#activo_id').val() +"/getTareas", function(taras){

    taras = $.map(taras, function(item) {
            return { id: item.id, text: item.nombre }; 
        });

  $("#task").select2({
  placeholder: "Seleccione",
  tags:true,
  data: taras,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true }
    },
  templateResult: function (data) {
    var $result = $("<span></span>");
    $result.text(data.text);
    if (data.newOption) {
      $result.append(" <em>(Nuevo)</em>");
    }
    return $result;
  },
  width:'92%',
  language: "es",
  })
});

$(document).on('click', '#addInput', function () {
  

  $.getJSON(ruta+"/ordenes/"+ $('#activo_id').val() +"/getTareas", function(taras){
  var scntDiv = $('#dynamicDiv');  
  taras = $.map(taras, function(item) {
      return { id: item.id, text: item.nombre }; 
  });
  var $prueba=$('<p>'+
        '{!! Form::select('tarea[]', [], null, ['id' => 'tarea[]', 'class' => 'form-control tar select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}'+
        '<a class="btn btn-danger pull-right" href="javascript:void(0)" id="remInput">'+
        '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>'+
        '</a>'+
    '</p>');
  $prueba.appendTo(scntDiv);
  $prueba.find('.tar').select2({
    placeholder: "Seleccione",
    tags:true,
    data: taras,
    createTag: function (params) {
      return {
        id: params.term,
        text: params.term,
        newOption: true }
      },
    templateResult: function (data) {
      var $result = $("<span></span>");
      $result.text(data.text);
      if (data.newOption) {
        $result.append(" <em>(Nuevo)</em>");
      }
      return $result;
    },
    width:'92%',
    language: "es",
    })
    return false;

    });
  });

  $(document).on('click', '#remInput', function () {
        $(this).parents('p').remove();
      return false;
  });

//SI SE SELECCIONA OTRO ACTIVO

  $( "#activo_id" ).change(function() 
  {
    $.getJSON(ruta+"/ordenes/"+ $(this).val() +"/getTareas", function(jsonData){

    jsonData = $.map(jsonData, function(item) {
            return { id: item.id, text: item.nombre }; 
        });

    var $prueba=$(' <br /> <p>'+
    '{!! Form::select('tarea[]', [] ,null, ['id' => 'tarea[]', 'class' => 'form-control tar select2', 'required' => 'required','placeholder'=> 'Seleccione']) !!}'+
    '</p><div id="dynamicDiv"></div><a class="btn btn-link" href="javascript:void(0)" id="addInput"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Tarea</a></div><br />');

    $prueba.find('.tar').select2({
      placeholder: "Seleccione",
      tags:true,
      data: jsonData,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true }
        },
      templateResult: function (data) {
        var $result = $("<span></span>");
        $result.text(data.text);
        if (data.newOption) {
          $result.append(" <em>(Nuevo)</em>");
        }
        return $result;
      },
      width:'92%',
      language: "es",
    })    
    $("#selectores").html($prueba);
    });    
  });

});

</script>

@endsection