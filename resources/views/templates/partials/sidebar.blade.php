
<aside class="main-sidebar" >

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <li class="header"><a href="{{ url('') }}"><i class="fa fa-home"></i><span>INICIO</span></a></li>

        <li class="header">MODULOS</li>
        @if(Auth::user()->rol_id == 3)
        <!-- Optionally, you can add icons to the links -->
        <li  @if($li=='activos') class="active" @endif><a href="{{route('activos.index')}}"><i class="fa fa-truck text-red"></i><span>Unidades</span></a></li>
        <li  @if($li=='ordenes') class="active" @endif><a href="{{route('ordenes.index')}}"><i class="fa fa-clone text-green"></i><span>Órdenes</span></a></li>
        <li @if($li=='viajes_activo' || $li=='viajes_inactivo') class="active treeview" @else class="treeview"  @endif>
          <a href="#"><i class="fa fa-road text-blue"></i><span>Viajes y solicitudes</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li  @if($li=='viajes_activo') class="active" @endif><a href="{{route('viajes.index')}}"><i class="fa fa-circle-o"></i> <span>V.S. Activas</span></a></li>
            <li  @if($li=='viajes_inactivo') class="active" @endif><a href="{{ route('viajes.cerrados') }}"><i class="fa fa-circle-o"></i> <span>V.S. Finalizadas</span></a></li>          
          </ul>
        </li>
        <li  @if($li=='reportes') class="active" @endif><a href="{{route('reportes.index')}}"><i class="fa fa-book text-orange"></i><span >Reportes</span></a></li>
        <li  @if($li=='articulos') class="active" @endif><a href="{{route('articulos.index')}}"><i class="fa fa-cubes text-maroon"></i><span>Inventario</span></a></li>
        <li  @if($li=='empleados') class="active" @endif><a href="{{route('empleados.index')}}"><i class="fa fa-users text-purple"></i><span>Trabajadores</span></a></li>
        <!-- /.<li  @if($li=='proveedores') class="active" @endif><a href="{{route('proveedores.index')}}"><i class="fa fa-industry text-navy"></i><span>Proveedores</span></a></li>-->
        @endif
        @if(Auth::user()->rol_id == 4 || Auth::user()->rol_id == 1 || Auth::user()->rol_id == 2)
        <li  @if($li=='solicitudes') class="active" @endif><a href="{{route('solicitudes.index')}}"><i class="fa fa-book text-orange"></i><span>Solicitudes</span></a></li>
        @endif
        @if(Auth::user()->rol_id == 4)
        <li  @if($li=='usuarios') class="active" @endif><a href="{{route('usuarios.index')}}"><i class="fa fa-users text-red"></i><span>Usuarios</span></a></li>
        <li  @if($li=='mantenimiento') class="active" @endif><a href="{{ url('mantenimiento') }}"><i class="fa fa-wrench text-navy"></i><span>Mantenimiento</span></a></li>
        @endif
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>