
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="{!! asset('public/image/mini.png') !!}" />

  <title>@yield('title', 'Amazonas Tech')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!--por defecto -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">

    <!-- sweealert -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">
    
    @yield('css')

<!-- CSS PARA CENTRAR EL CONTENIDO DE LAS TABLAS VERTICALMENTE -->
<style type="text/css">
table.dataTable tbody td { 
  vertical-align: middle; 
}
table.dataTable thead th { 
  vertical-align: middle; 
}
</style>
    
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/skins/_all-skins.min.css') }}">
  <!--por defecto -->
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-green-light   sidebar-mini">
  <div class="wrapper" > 
    @include('templates.partials.sidebar')
    @include('templates.partials.header') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
      <section class="content-header">        
        <h1>@yield('titulo_modulo')</h1>
        @yield('new')        
      </section>
       <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <section class="content">
        @include('flash::message')        
        @yield('contenido')
      </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
     <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; {{date('Y')}}  <a href="{{ url('') }}"> SIAF </a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('public/adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminlte/js/adminlte.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('public/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script type="text/javascript">
  $('#flash-overlay-modal').modal();
  //FLASH TIEMPO DE VISTA
  $('div.alert').not('.alert-important').delay(4000).fadeOut(1250);
  //LENGUAJE ESPAÑOL PARA LAS DATATABLES
  var leng = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
    "sFirst":    "Primero",
    "sLast":     "Último",
    "sNext":     "Siguiente",
    "sPrevious": "Anterior"
    },
    "oAria": {
    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  };

  var ruta = "{{ url('') }}";

</script>

@yield('js')

</body>
</html>