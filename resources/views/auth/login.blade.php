
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="{!! asset('public/image/mini.png') !!}" />
  <title>@yield('title', 'SIAF | Login')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!--por defecto -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
   
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/skins/_all-skins.min.css') }}">
  <!--por defecto -->
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>

<body class="hold-transition login-page" style="background-image: url('{{ asset('public/image/fondo.jpg') }}');background-size: cover;">

  <div class="col-md-8 col-md-offset-2">
    <div class="login-box">
      <div class="login-logo">
        <a href="{{ url('') }}">
          
        <center><img src="{{ asset('public/image/siaf.png') }}" class="img-responsive" alt="Image" width="300"></center> 
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
       <center>Login</center><hr />
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

              <div class="{{ $errors->has('usuario') ? ' has-error' : '' }}">

                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"  style="font-size: 12px"></i></span>
                  {!! Form::text('usuario', null, ['class'=>'form-control', 'id'=>'usuario', 'name'=>'usuario','required' => 'required','placeholder'=>'Username']) !!}
                </div>
                @if($errors->has('usuario'))
                  <i class="text-danger">
                     <strong><i class="fa fa-times-circle-o"></i>
                        @if($errors->first('usuario') == "These credentials do not match our records.")
                        Los datos ingresados no coinciden con nuestros registros
                        @else
                        {{ $errors->first('usuario')}}
                        @endif
                    </strong>
                  </i>
                @endif
              </div><br />

              <div class="{{ $errors->has('password') ? ' has-error' : '' }}">

                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                  {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                </div>
                @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div> 
              <br />
              <div class="row">
                  <div class="col-md-6">
                    <a class="btn btn-link" href="{{ url('/register') }}">Registrarme</a>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-flat pull-right">
                        <i class="fa fa-btn fa-sign-in"></i> Entrar
                    </button>                     
                  </div>                  
              </div>
          </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->  
  </div>


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('public/adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminlte/js/adminlte.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>

</body>
</html>