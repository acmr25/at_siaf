
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="{!! asset('public/image/mini.png') !!}" />

  <title>@yield('title', 'SIAF | Registro')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!--por defecto -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/css/skins/_all-skins.min.css') }}">
  <!--por defecto -->
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>

<body class="hold-transition login-page" style="background-image: url('{{ asset('public/image/fondo.jpg') }}');background-size: cover;">

  <div class="col-md-12">
    <div class="login-box">
      <div class="login-logo">
        <a href="{{ url('') }}">          
        <center><img src="{{ asset('public/image/siaf.png') }}" class="img-responsive" alt="Image" width="300"></center> 
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <center>Registro de Usuario</center><hr />
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-6">
                    <div class="{{ $errors->has('nombre') ? ' has-error' : '' }}">
                        {!! Form::label('nombre', 'Nombre Completo:') !!}
                        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Angela Montilla']) !!}
                        <small class="text-danger">{{ $errors->first('nombre') }}</small>
                    </div>    
                </div>
                <div class="col-md-6">
                    <div class="{{ $errors->has('cedula') ? ' has-error' : '' }}">
                        {!! Form::label('cedula', 'C.I.:') !!}
                        {!! Form::text('cedula', null, ['class' => 'form-control','placeholder'=>'23.533.432','required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('cedula') }}</small>
                    </div>      
                </div>
            </div><br />
              <div class="{{ $errors->has('cargo') ? ' has-error' : '' }}">
                  {!! Form::label('cargo', 'Cargo:') !!}
                  {!! Form::text('cargo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Pasante de Transporte y Servicios',]) !!}
                  <small class="text-danger">{{ $errors->first('cargo') }}</small>
              </div>
            <br />
              <div class="{{ $errors->has('departamento_id') ? ' has-error' : '' }}">
                  {!! Form::label('departamento_id', 'Departamento:') !!}
                  {!! Form::select('departamento_id', $areas, null, ['id' => 'departamento_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                  <small class="text-danger">{{ $errors->first('departamento_id') }}</small>
              </div>
            <br />
            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Correo Electronico:') !!}
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                  {!! Form::email('email', null, ['class'=>'form-control', 'id'=>'email', 'name'=>'email','required' => 'required','placeholder'=>'Ej: jane_doe@amazonastech.com.ve']) !!}
                </div>
                <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>
            <br />
            <div class="{{ $errors->has('usuario') ? ' has-error' : '' }}">
                {!! Form::label('usuario', 'Nombre de Usuario:') !!}
                {!! Form::text('usuario', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Acmontilla',]) !!}
                <small class="text-danger">{{ $errors->first('usuario') }}</small>
            </div> <br />           
            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Contraseña:') !!}
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                  {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                </div>
                <small class="text-danger">{{ $errors->first('password') }}</small>
            </div>
            <br />
            <div class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                {!! Form::label('password_confirmation', 'Confirme Contraseña:') !!}
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                  {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                </div>
                <small class="text-danger">{{ $errors->first('password') }}</small>
            </div>
            <br />
            <button type="submit" class="btn btn-primary btn-block btn-flat">
                    <i class="fa fa-btn fa-user"></i> Registrar
            </button> 
        </form>
        <br />
        <a class="btn" href="{{ url('/login') }}">Ya estoy registrad@</a>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->  
  </div>


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('public/adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminlte/js/adminlte.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
</body>
</html>