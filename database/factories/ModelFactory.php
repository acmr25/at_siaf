<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'nombre' => 'Angela Montilla',
        'cedula' => '23.533.432',
        'cargo' => 'Pasante AIT',
        'estado' => 'ACTIVO',
        'rol' => 'Admin_user',
        'departamento_id' => 1,
        'email' => 'pasanteait@amazonastech.com.ve',
        'password' => bcrypt(1234),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Empleado::class, function (Faker\Generator $faker) {
    return [
    	'cedula' => $faker->unique()->numberBetween($min = 1000, $max = 5000),
        'nombre' => $faker->name,
        'tipo' => $faker->randomElement($array = array ('Chofer','Mecánico','Operador')),
        'cargo'  => $faker->randomElement($array = array ('Chofer administrativo','Chofer de equipos pesados','Mecánico','Ayudante de Mecánico','Operador de equipos pesados','Ayudante de opérador')),
        'condicion' => $faker->randomElement(['Activo','Inactivo']),
        'telefono' => $faker->phoneNumber,
        'fecha_ingreso'=>$faker->date($format = 'Y-m-d', $max = 'now'), // '1979-06-09'
        'contrato'=> $faker->randomElement(['Fijo','Condicionado']),

    ];
});

$factory->define(App\Activo::class, function (Faker\Generator $faker) {
    return [
        'nro_activo'=> $faker->unique()->numberBetween($min = 1000, $max = 5000),
        'placa'=> $faker->unique()->numberBetween($min = 1000, $max =5000),
        'modelo'=> $faker->unique()->shuffle('123-ABC'),
        'marca'=> $faker->randomElement(['Ford','Kia','Volkswagen','Jeep','Toyota','Freeway','JAC', 'MACK']),
        'año'=> $faker->numberBetween($min = 2000, $max = 2018),
        'tipo'=> $faker->randomElement(['Vehiculo liviano','Vehiculo Pesado','Chuto','Autobus','Retroexcavadora']),
        'color'=> $faker->safeColorName,
        'estatus'=> $faker->randomElement($array = array ('Operativo','Inoperativo')),
        'prioridad'=> $faker->randomElement(['Media','Alta']),
        'tipo_aceite'=> $faker->shuffle('123-ABC'),
        'filtro_aceite'=> $faker->shuffle('123-ABC'),
        'filtro_aire'=> $faker->shuffle('123-ABC'),
        'filtro_combustible'=> $faker->shuffle('123-ABC'),
        'filtro_agua'=> $faker->shuffle('123-ABC'),
        'foto'        => $faker->imageUrl($width = 640, $height = 480),
    ];
});

$factory->define(App\Articulo::class, function (Faker\Generator $faker) {
    return [
        'nro_activo'=> $faker->unique()->numberBetween($min = 1000, $max = 5000),
        'nombre'=> $faker->text($maxNbChars = 10),
        'categoria'=> $faker->randomElement(['Neumáticos','Motores','Cadenas','Aceites','Lubricantes','Baterias','Mechas']),
        'descripcion'=> $faker->shuffle('123-ABC'),
        'fabricante'=> $faker->randomElement(['Ford','Kia','Volkswagen','Jeep','Toyota','Freeway','JAC', 'MACK']),
        'unidad'=>  $faker->randomElement(['Unidades','Gramos','Kilogramos','Toneladas','Litros']),
        'cantidad'=> $faker->numberBetween($min = 0, $max = 10),
        'lowstock'=> $faker->numberBetween($min = 0, $max = 10)
    ];
});

$factory->define(App\Tarea::class, function (Faker\Generator $faker) {
    return [
        'nombre'=> $faker->unique()->randomElement([
            'REEMPLAZO DE FILTRO DE AIRE DE CABINA',
            'AJUSTE Y COMPLETACION DE FLUIDOS',
            'LIMPIEZA INTERNA GENERAL',
            'REEMPLAZO DE REFRIGERANTE',
            'MANTENIMIENTO MAYOR SISTEMA DE FRENO'
        ]),
        'mantenimiento'=> 'Preventivo',
        'descripcion'=> $faker->shuffle('123-ABC'),

    ];
});