<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula')->unique()->nullable();
            $table->string('nombre');
            $table->string('tipo'); //Ayudante de Chofer de Equipos Pesados,Chofer de Equipos Pesados,Chofer de Vehículos Livianos,Mecánico, Electricista, Proveedor Externo
            $table->string('condicion');
            $table->string('telefono')->nullable();
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_salida')->nullable();
            $table->string('contrato')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleados');
    }
}
