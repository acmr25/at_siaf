<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->foreign('solicitante_id')->references('id')->on('users')->onUpdate('cascade');
            $table->integer('sup_id')->unsigned()->nullable();
            $table->foreign('sup_id')->references('id')->on('users')->onUpdate('cascade');

            $table->enum('tipo', ['LIVIANA','PESADA']);
            $table->integer('nro_pasajeros')->default(1);
            $table->string('motivo');

            $table->string('origen');
            $table->date('fecha_salida');
            $table->time('hora_salida');
            $table->string('destino');
            $table->date('fecha_regreso')->nullable();
            $table->time('hora_regreso')->nullable();

            $table->string('notas')->nullable();
            $table->string('pesado_solicitud')->nullable();

            $table->enum('estado', ['Por Autorizar','Aprobado','Rechazado','Sin Asignar','Pendiente','Con viaje Programado','Completada', 'Cerrada']);
            $table->string('motivo_rechazo')->nullable();
            $table->string('comentarios')->nullable();

            $table->enum('calificacion', ['Excelente','Bueno','Regular','Malo'])->nullable();
            $table->dateTime('dt_aprobado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitudes');
    }
}
