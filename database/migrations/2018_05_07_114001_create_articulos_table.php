<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nro_activo')->unique()->nullable();
            $table->string('nombre');
            $table->string('categoria');
            $table->string('descripcion')->nullable();
            $table->string('fabricante');
            $table->string('unidad');
            $table->integer('cantidad')->nullable();
            $table->integer('lowstock')->nullable();
            $table->string('estado')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos');
    }
}
