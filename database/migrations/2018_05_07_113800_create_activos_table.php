<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('unidad');
            $table->string('placa')->nullable();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('serial')->nullable();
            $table->string('estatus')->nullable();//['Inoperativo','Operativo',En mantemiento','En servicio']
            $table->string('nro_activo')->unique()->nullable();
            $table->integer('puestos')->nullable();
            $table->string('prioridad')->nullable();
            $table->string('clasificacion')->nullable();
            //$table->enum('clasificacion', ['Unidad Pesada','Vehiculo Liviano', 'Vehiculo Pesado']);
            $table->string('tipo')->nullable();
            $table->string('año')->nullable();            
            $table->string('color')->nullable();

            $table->string('tipo_aceite')->nullable();
            $table->string('filtro_aceite')->nullable();
            $table->string('filtro_aire')->nullable();
            $table->string('filtro_combustible')->nullable();
            $table->string('filtro_agua')->nullable();  

            $table->string('foto')->nullable(); 
            $table->string('mantenimiento')->nullable();
            $table->string('ubicacion_fija')->nullable();
            $table->string('ubicacion_temporal')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activos');
    }
}
