<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activo_id')->unsigned();
            $table->foreign('activo_id')->references('id')->on('activos')->onUpdate('cascade');
            
            $table->integer('responsable_id')->unsigned()->nullable();
            $table->foreign('responsable_id')->references('id')->on('empleados')->onUpdate('cascade');

            $table->integer('creador_id')->unsigned();
            $table->foreign('creador_id')->references('id')->on('users')->onUpdate('cascade');
            
            $table->integer('modificador_id')->unsigned()->nullable();
            $table->foreign('modificador_id')->references('id')->on('users')->onUpdate('cascade');
                        
            $table->integer('reporte_id')->unsigned()->nullable();
            $table->foreign('reporte_id')->references('id')->on('reportes')->onUpdate('cascade');
 

            $table->enum('estado_orden', ['abierta', 'en progreso','detenida','completada']);

            $table->date('inicia');

            $table->string('prioridad');
            
            $table->date('termina')->nullable();

            $table->string('notas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordenes');
    }
}
