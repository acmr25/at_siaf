<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creador_id')->unsigned();
            $table->foreign('creador_id')->references('id')->on('users')->onUpdate('cascade');
            
            $table->integer('chofer_id')->unsigned()->nullable();
            $table->foreign('chofer_id')->references('id')->on('empleados')->onUpdate('cascade');
            $table->integer('ayudante_id')->unsigned()->nullable();
            $table->foreign('ayudante_id')->references('id')->on('empleados')->onUpdate('cascade');

            $table->string('origen')->nullable();
            $table->date('fecha_salida')->nullable();
            $table->time('hora_salida')->nullable();

            $table->string('destino');
            $table->date('fecha_llegada')->nullable();
            $table->time('hora_llegada')->nullable();

            $table->string('observaciones')->nullable();

            $table->enum('estado', ['Sin asignar','Programado','En proceso', 'Completado']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajes');
    }
}
