<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('activo_id')->unsigned();
            $table->foreign('activo_id')->references('id')->on('activos')->onUpdate('cascade');

            $table->integer('creador_id')->unsigned();
            $table->foreign('creador_id')->references('id')->on('users')->onUpdate('cascade');
            $table->integer('modificador_id')->unsigned()->nullable();
            $table->foreign('modificador_id')->references('id')->on('users')->onUpdate('cascade');
            
            $table->date('fecha');
            $table->string('lugar');

            $table->string('informador');
            $table->string('ci_informador');
            $table->string('cargo_informador');

            $table->string('observacion');

            $table->enum('prioridad', ['Baja', 'Media','Alta']);
            
            $table->enum('estado', ['Sin asignar', 'Asignado']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reportes');
    }
}
