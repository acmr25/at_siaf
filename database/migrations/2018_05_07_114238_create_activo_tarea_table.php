<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivoTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activo_tarea', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('activo_id')->unsigned();
            $table->foreign('activo_id')->references('id')->on('activos')->onUpdate('cascade');

            $table->integer('tarea_id')->unsigned();
            $table->foreign('tarea_id')->references('id')->on('tareas')->onUpdate('cascade');

            $table->integer('horas')->nullable();
            $table->integer('pre_horas')->nullable();
            $table->integer('realizacion_horas')->nullable();

            $table->integer('kilometros')->nullable();
            $table->integer('pre_kilometros')->nullable();
            $table->integer('realizacion_kilometros')->nullable();

            $table->string('prioridad');

            $table->string('estado')->nullable();

            $table->integer('orden_id')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activo_tarea');
    }
}
