<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articulo_id')->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos')->onUpdate('cascade');

            $table->integer('activo_id')->unsigned()->nullable();
            $table->foreign('activo_id')->references('id')->on('activos')->onUpdate('cascade');

            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')->on('ordenes')->onDelete('cascade')->onUpdate('cascade');

            $table->enum('tipo', ['ENTRADA', 'SALIDA']);
            $table->integer('cantidad');
            $table->string('razon');

            $table->integer('creador_id')->unsigned();
            $table->foreign('creador_id')->references('id')->on('users')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimientos');
    }
}
