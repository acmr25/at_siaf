<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Sitio;

class SitiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sitio::create([
            'nombre' => 'Monagas',
            'padre' => null,
        ]);
        Sitio::create([
            'nombre' => 'Anzoategui',
            'padre' => null,
        ]);
        Sitio::create([
            'nombre' => 'Maturín',
            'padre' => 1,
        ]);
        Sitio::create([
            'nombre' => 'El Tigre',
            'padre' => 2,
        ]);
        Sitio::create([
            'nombre' => 'Puerto la Cruz',
            'padre' => 2,
        ]);
    }
}
