<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Empleado;

class TrabajadoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empleado::create([
            'nombre' => 'Joel Leal',
            'cedula' => '23.533.432',
            'tipo' => 'Chofer de Vehículos Livianos',
            'condicion' => 'Activo',
            'fecha_ingreso' => date('Y/m/d'),
            'fecha_salida' => date('Y/m/d'),
            'contrato' => 'Fijo'
        ]);
        Empleado::create([
            'nombre' => 'Carlos Sanchez',
            'cedula' => '23.533.435',
            'tipo' => 'Mecánico',
            'condicion' => 'Activo',
            'fecha_ingreso' => date('Y/m/d'),
            'fecha_salida' => date('Y/m/d'),
            'contrato' => 'Fijo'
        ]);
    }
}
