<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nombre' => 'Angela Montilla',
            'cedula' => '23.533.432',
            'cargo' => 'Pasante AIT',
            'estado' => 'ACTIVO',
            'rol_id' => 4,
            'departamento_id' => 1,
            'email' => 'pasanteait@amazonastech.com.ve',
            'usuario' => 'acmontilla',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10)
        ]);
        User::create([
            'nombre' => 'Angela Montilla',
            'cedula' => '12.123.123',
            'cargo' => 'Pasante AIT',
            'estado' => 'ACTIVO',
            'rol_id' => 1,
            'departamento_id' => 3,
            'email' => 'acmrbi@gmail.com',
            'usuario' => 'acmr',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10)
        ]);
        User::create([
            'nombre' => 'Daniel Porras',
            'cedula' => '12.123.131',
            'cargo' => 'Supervisor AIT',
            'estado' => 'ACTIVO',
            'rol_id' => 2,
            'departamento_id' => 3,
            'email' => 'dporras@gmail.com',
            'usuario' => 'dporras',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10)
        ]);
        User::create([
            'nombre' => 'Juan Arcia',
            'cedula' => '9.125.123',
            'cargo' => 'Supervisor de Mantenimiento',
            'estado' => 'ACTIVO',
            'rol_id' => 3,
            'departamento_id' => 2,
            'email' => 'jarcia@gmail.com',
            'usuario' => 'jarcia',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10)
        ]);
    }
}
