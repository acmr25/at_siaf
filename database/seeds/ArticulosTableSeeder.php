<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Articulo;
use App\Movimiento;

class ArticulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Articulo::create(['id' => '1','nro_activo' => '0000000001','nombre' => 'Aceite 20w50 Mineral','categoria' => 'LUBRICANTES','descripcion' => 'Aceite 20w50 Mineral Multigrado Sellado','fabricante' => 'PROTEC OIL','unidad' => 'LITROS','cantidad' => '65','lowstock' => '75','estado' => 'BAJO STOCK','created_at' => '2018-05-23 11:25:12','updated_at' => '2018-05-23 14:59:21']);
		Movimiento::create(['articulo_id'=>1, 'tipo'=> 'ENTRADA', 'cantidad'=>'65', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '2','nro_activo' => '0000000002','nombre' => 'BUJIAS FORD','categoria' => 'MOTORES','descripcion' => 'Sellos Bujia Ford Fusion Escape 3.0 6v 2006 2009 ','fabricante' => 'FORD','unidad' => 'UNIDAD','cantidad' => '15','lowstock' => '20','estado' => 'BAJO STOCK','created_at' => '2018-05-23 11:29:44','updated_at' => '2018-05-23 11:30:26']);
		Movimiento::create(['articulo_id'=>2, 'tipo'=> 'ENTRADA', 'cantidad'=>'15', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '3','nro_activo' => '0000000003','nombre' => 'Aceite Motor Mineral 20w50 ','categoria' => 'LUBRICANTES','descripcion' => 'Aceite Motor Mineral 20w50 ','fabricante' => 'UNDER GROUND','unidad' => 'LITROS','cantidad' => '650','lowstock' => '500','estado' => '','created_at' => '2018-05-23 11:31:44','updated_at' => '2018-05-24 08:22:51']);
		Movimiento::create(['articulo_id'=>3, 'tipo'=> 'ENTRADA', 'cantidad'=>'650', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '4','nro_activo' => '0000000004','nombre' => 'GOMA DE VALVULAS','categoria' => 'MOTORES','descripcion' => '','fabricante' => 'MOPAR','unidad' => 'UNIDAD','cantidad' => '10','lowstock' => '20','estado' => 'BAJO STOCK','created_at' => '2018-05-23 11:35:04','updated_at' => '2018-05-23 11:35:04']);
		Movimiento::create(['articulo_id'=>4, 'tipo'=> 'ENTRADA', 'cantidad'=>'10', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '5','nro_activo' => '0000000005','nombre' => 'EMPACADURA','categoria' => 'MOTORES','descripcion' => 'Empacadura Tapa Valvula ','fabricante' => 'FORD','unidad' => 'UNIDAD','cantidad' => '13','lowstock' => '10','estado' => '','created_at' => '2018-05-23 11:36:04','updated_at' => '2018-05-23 14:57:53']);
		Movimiento::create(['articulo_id'=>5, 'tipo'=> 'ENTRADA', 'cantidad'=>'13', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '6','nro_activo' => '00000000006','nombre' => 'Rotor Distribuidor','categoria' => 'DISTRIBUIDORES','descripcion' => 'Rotor Distribuidor Mazda 323 626 Ford Laser','fabricante' => 'ENZO','unidad' => 'UNIDAD','cantidad' => '5','lowstock' => '10','estado' => 'BAJO STOCK','created_at' => '2018-05-23 11:38:28','updated_at' => '2018-05-23 11:38:28']);
		Movimiento::create(['articulo_id'=>6, 'tipo'=> 'ENTRADA', 'cantidad'=>'5', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '7','nro_activo' => '0000000007','nombre' => 'Manguera Superior Radiador','categoria' => 'MANGUERAS','descripcion' => 'Manguera Superior Radiador Grand Cherokee 2011 2012 2013 14','fabricante' => 'MOPAR','unidad' => 'UNIDAD','cantidad' => '10','lowstock' => '5','estado' => '','created_at' => '2018-05-23 11:40:54','updated_at' => '2018-05-23 11:40:54']);
		Movimiento::create(['articulo_id'=>7, 'tipo'=> 'ENTRADA', 'cantidad'=>'10S', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '8','nro_activo' => '00000000008','nombre' => 'Manguera Gasolina','categoria' => 'MANGUERAS','descripcion' => 'Manguera Gasolina Ford Focus // Ecosport // Fiesta Power','fabricante' => 'SALVA FOCUS','unidad' => 'UNIDAD','cantidad' => '110','lowstock' => '111','estado' => 'BAJO STOCK','created_at' => '2018-05-23 11:43:38','updated_at' => '2018-05-23 11:43:38']);
		Movimiento::create(['articulo_id'=>8, 'tipo'=> 'ENTRADA', 'cantidad'=>'110', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '9','nro_activo' => '0000000009','nombre' => 'SELLO','categoria' => 'SELLOS','descripcion' => 'Sello Estopera Bujia Tapa Valvula Tucson Getz Elantra Accent','fabricante' => 'SALVA FOCUS','unidad' => 'UNIDAD','cantidad' => '5','lowstock' => '2','estado' => '','created_at' => '2018-05-23 14:01:27','updated_at' => '2018-05-23 14:01:27']);
		Movimiento::create(['articulo_id'=>9, 'tipo'=> 'ENTRADA', 'cantidad'=>'5', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

		Articulo::create(['id' => '10','nro_activo' => '0000000010','nombre' => 'EMPACADURA','categoria' => 'EMPACADURAS','descripcion' => 'Empacadura Camara Original Isuzunpr 4hg1 4hf1 Chevrolet','fabricante' => 'UNDER GROUND','unidad' => 'UNIDAD','cantidad' => '15','lowstock' => '20','estado' => 'BAJO STOCK','created_at' => '2018-05-23 14:06:09','updated_at' => '2018-05-23 14:06:09']);
		Movimiento::create(['articulo_id'=>10, 'tipo'=> 'ENTRADA', 'cantidad'=>'15', 'razon'=>'Stock Inicial', 'creador_id'=> 4]);

    }
}
