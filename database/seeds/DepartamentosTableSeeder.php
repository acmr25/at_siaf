<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Departamento;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Departamento::create(['nombre'=>'AIT', 'descripcion' => 'departamento amazonas tech']);
        Departamento::create(['nombre'=>'Transporte y Logística', 'descripcion' => 'departamento amazonas tech']);
        Departamento::create(['nombre'=>'GTH', 'descripcion' => 'departamento amazonas tech']);
    }
}
