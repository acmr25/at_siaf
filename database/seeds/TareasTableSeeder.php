<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Tarea;

class TareasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tarea::create([
	        'nombre' => 'REEMPLAZO DE FILTRO DE AIRE DE CABINA',
	        'mantenimiento' => 'Preventivo'
    	]);
    	Tarea::create([
	        'nombre' => 'AJUSTE Y COMPLETACION DE FLUIDOS',
	        'mantenimiento' => 'Preventivo'
    	]);
    	Tarea::create([
	        'nombre' => 'LIMPIEZA INTERNA GENERAL',
	        'mantenimiento' => 'Preventivo'
    	]);
    	Tarea::create([
	        'nombre' => 'REEMPLAZO DE REFRIGERANTE',
	        'mantenimiento' => 'Preventivo'
    	]);
    	Tarea::create([
	        'nombre' => 'MANTENIMIENTO MAYOR SISTEMA DE FRENO',
	        'mantenimiento' => 'Preventivo'
    	]);
    }
}
