<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Activo;

class ActivosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activo::create([
			'unidad' => 'CHUTO',
			'placa' => 'A12CL5D',
			'marca' => 'JAC',
			'modelo' => '4253 TRACTOR',
			'serial' => '8XR3RBL6XEU000096',
			'estatus' => 'Operativo',
			'nro_activo' => '9010003006',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Pesado',
			'tipo' => 'CHUTO',
			'año' => '2014',
			'color' => 'ROJO',
    		'ubicacion_fija'=>'SEMCA'
    		]);
        Activo::create([
			'unidad' => 'CHUTO',
			'placa' => 'A65BI0E',
			'marca' => 'JAC',
			'modelo' => '4253 TRACTOR',
			'serial' => '8XR3RBL61EU000035',
			'estatus' => 'Inoperativo',
			'nro_activo' => '9010003011',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Pesado',
			'tipo' => 'CHUTO',
			'año' => '2014',
			'color' => 'ROJO',
    		'ubicacion_fija'=>'SEMCA',
    		'ubicacion_temporal'=>'TALLER DJ'
    		]);
        Activo::create([
			'unidad' => 'CHUTO',
			'placa' => 'A65BI1E',
			'marca' => 'JAC',
			'modelo' => '4253 TRACTOR',
			'serial' => '8XR3RBL63EU000005',
			'estatus' => 'Operativo',
			'nro_activo' => '9010003010',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Pesado',
			'tipo' => 'CHUTO',
			'año' => '2014',
			'color' => 'ROJO',
    		'ubicacion_fija'=>'SEMCA'
    		]);
        Activo::create([
			'unidad' => 'CHUTO',
			'placa' => 'A66BI3E',
			'marca' => 'JAC',
			'modelo' => '4253 TRACTOR',
			'serial' => '8XR3RBL66EU000094',
			'estatus' => 'Inoperativo',
			'nro_activo' => '9010003007',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Pesado',
			'tipo' => 'CHUTO',
			'año' => '2014',
			'color' => 'ROJO',
    		'ubicacion_fija'=>'SEMCA'
    		]);

        Activo::create([
			'unidad' => 'CAMIONETA',
			'placa' => 'A39BK9K',
			'marca' => 'MAZDA',
			'modelo' => 'BT-50',
			'serial' => '8LFUNY069CMM04531',
			'estatus' => 'Operativo',
			'nro_activo' => '9010001009',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Liviano',
			'tipo' => 'Camioneta',
			'año' => '2012',
			'color' => 'GRIS',
    		'ubicacion_fija'=>'Amazonas'
    		]);
        Activo::create([
			'unidad' => 'CAMIONETA',
			'placa' => 'A45AG3R',
			'marca' => 'FORD',
			'modelo' => 'F-150',
			'serial' => '3FTRF17W47MA34178',
			'estatus' => 'Inoperativo',
			'nro_activo' => '9010001012',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Liviano',
			'tipo' => 'CAMIONETA',
			'año' => '2007',
			'color' => 'BLANCO',
    		'ubicacion_fija'=>'SEMCA'
    		]);
        Activo::create([
			'unidad' => 'CAMIONETA',
			'placa' => 'A50AN4N',
			'marca' => 'CHEVROLET',
			'modelo' => 'SILVERADO',
			'serial' => '8ZCNCRE00BV332923',
			'estatus' => 'Operativo',
			'nro_activo' => '9010001019',
			'prioridad' => 'Alta',
			'clasificacion' => 'Vehiculo Liviano',
			'tipo' => 'CAMIONETA',
			'año' => '2011',
			'color' => 'BLANCO',
    		'ubicacion_fija'=>'SEMCA'
    		]);
    }
}
