<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(SitiosTableSeeder::class);
        $this->call(DepartamentosTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ActivosTableSeeder::class);
        $this->call(TareasTableSeeder::class);
        $this->call(TrabajadoresTableSeeder::class);
        $this->call(ArticulosTableSeeder::class);
        $this->call(SolicitudesTableSeeder::class);
    }
}
