<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Rol;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
			'nombre' => 'Solicitante',
			'descripcion' => 'Solo puede realizar solicitudes.'
    	]);
        Rol::create([
			'nombre' => 'Supervisor',
			'descripcion' => 'Puede realizar solicitudes y aprobarlas.'
    	]);
    	Rol::create([
			'nombre' => 'Admintrador de app',
			'descripcion' => 'Usuario general para usar los demas modulos de la app, re-aprueba las solicitudes si fueron aprobadas por un supervisor.'
    	]);
    	Rol::create([
			'nombre' => 'Admintrador de mantenimiento',
			'descripcion' => 'Puede realizar solicitudes,encargado del modulo de mantenimiento de la app y usuarios.'
    	]);
    }
}
