<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $table = 'archivos';
    protected $fillable = ['activo_id',
                            'reporte_id',
                            'nombre_externo',
                            'nombre_interno',
                            'tipo',
                            'descripcion'];
    
    public function activo()
    {
    	return $this->belongsTo('App\Activo','activo_id');
    }
    
    public function reporte()
    {
    	return $this->belongsTo('App\Reporte','reporte_id');
    }
}