<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sitio extends Model
{
    protected $table = 'sitios';
    protected $fillable = [
                            'nombre','padre'
    						];
    						
    public function padreData()
    {
    	return $this->belongsTo('App\Sitio', 'padre');
    }
}
