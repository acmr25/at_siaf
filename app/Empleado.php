<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'empleados';
    protected $fillable = [
							'cedula',
							'nombre',
							'tipo',
							'condicion',
							'telefono',
                            'fecha_ingreso',
                            'fecha_salida',
                            'contrato'
    						];
public function ordenes()
    {
    	return $this->hasMany('App\Orden');
    }
public function viajes()
    {
    	return $this->hasMany('App\Viajes');
    }
}
