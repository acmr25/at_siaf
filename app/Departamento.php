<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
	protected $table = "departamentos";

	protected $fillable = ['nombre', 'descripcion', 'padre'];

	public function padreData()
    {
    	return $this->belongsTo('App\Departamento', 'padre');
    }
	public function usuarios()
    {
        return $this->hasMany('App\User', 'departamento_id', 'id');
    }
}
