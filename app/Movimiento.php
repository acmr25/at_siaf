<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'movimientos';
    protected $fillable = [
    						'articulo_id',
    						'activo_id',
    						'orden_id',
    						'tipo',
    						'cantidad',
							'razon',
							'created_at',
							'creador_id'];
							
	public function articulo()
	    {
	    	return $this->belongsTo('App\Articulo','articulo_id');
	    }
	public function activo()
	    {
	    	return $this->belongsTo('App\Activo','activo_id');
	    }
	public function orden()
	    {
	    	return $this->belongsTo('App\Orden','orden_id');
	    }
	public function creador()
        {
            return $this->belongsTo('App\User','creador_id');
        }
}


