<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = 'reportes';
    protected $fillable = [
    						'activo_id',
                            'creador_id',
                            'modificador_id',
                            'fecha',
                            'lugar',
                            'informador',
                            'ci_informador',
                            'cargo_informador',
							'observacion',			
							'estado',
                            'prioridad',
                            'created_at'
                        ];
							
	public function activo()
	    {
	    	return $this->belongsTo('App\Activo','activo_id');
	    }
    public function creador()
        {
            return $this->belongsTo('App\User','creador_id');
        }
    public function modificador()
        {
            return $this->belongsTo('App\User','modificador_id');
        }
	public function orden()
    	{
        	return $this->hasOne('App\Orden', 'reporte_id');
    	}
    public function fallas()
    {
        return $this->hasMany('App\Falla', 'reporte_id');
    }
    public function archivos()
    {
        return $this->hasMany('App\Archivo', 'reporte_id');
    }
}
