<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Falla extends Model
{
    protected $table = 'fallas';
    protected $fillable = [
    						'reporte_id',
							'nombre',
							'tipo'];
							
	public function reporte()
	    {
	    	return $this->belongsTo('App\Reporte','reporte_id');
	    }
}
