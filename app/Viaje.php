<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viaje extends Model
{
    protected $table = 'viajes';
    protected $fillable = [
							'creador_id',
							'chofer_id',
                            'ayudante_id',
                            'origen',
                            'fecha_salida',
                            'hora_salida',
                            'destino',
                            'fecha_llegada',
                            'hora_llegada',
                            'observaciones',
							'estado'
    						];
    public function chofer()
    {
        return $this->belongsTo('App\Empleado','chofer_id');
    }
    public function ayudante()
    {
        return $this->belongsTo('App\Empleado','ayudante_id');
    }
    public function creador()
    {
        return $this->belongsTo('App\User','creador_id');
    }
    public function activos()
    {
        return $this->belongsToMany('App\Activo', 'activo_viaje')
                     ->withPivot('id');
    }
    public function solicitudes()
    {
        return $this->belongsToMany('App\Solicitud', 'solicitud_viaje');
    }
}
