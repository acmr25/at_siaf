<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'articulos';
    protected $fillable = [
    						'nro_activo',
    						'nombre',
							'categoria',
							'descripcion',
							'fabricante',
							'unidad',
							'cantidad',
							'lowstock',
							'estado'
    						];
	public function movimientos()
	    {
	    	return $this->hasMany('App\Movimiento');
	    }
}
