<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table = 'medidas';
    protected $fillable = [
    						'activo_id',
							'horas',
							'kilometros',
						];
	 public function activo()
    {
    	return $this->belongsTo('App\Activo');
    }
}


