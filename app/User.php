<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'cedula',
        'cargo',
        'estado',
        'rol_id',
        'departamento_id',
        'email',
        'usuario',
        'password',
        'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function reportes()
    {
        return $this->hasMany('App\Reporte');
    }
    public function ordenes()
    {
        return $this->hasMany('App\Orden');
    }
    public function viajes()
    {
        return $this->hasMany('App\Viaje');
    }
    public function departamento()
    {
        return $this->belongsTo('App\Departamento','departamento_id');
    }
    public function rol()
    {
        return $this->belongsTo('App\Rol','rol_id');
    }
    public function movimientos()
    {
        return $this->hasMany('App\Movimiento');
    }
}
