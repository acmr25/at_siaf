<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpleadoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return 
        [
            'cedula'=>'required|unique:empleados,cedula',
            'nombre'=>'required|min:5|max:45',
            'condicion'=>'required',
            'tipo'=>'required',
            'fecha_ingreso'=>'required',
            'contrato'=>'required'           
        ];           
    }

    public function messages()
    {
        return
        [
            'cedula.required'=>'Se requiere este campo.',                      
            'cedula.unique'=> 'Ya existe un empleado con esa cédula.  ',

            'nombre.required'=>'Se requiere este campo.',
            'nombre.min'=>'El nro. de caracteres debe estar entre 5 a 45.  ',
            'nombre.max'=>'El nro. de caracteres debe estar entre 5 a 45.  ',

            'condicion.required'=>'Se requiere este campo.  ',
            'tipo.required'=>'Se requiere este campo.  ',
            'fecha_ingreso.required'=>'Se requiere este campo.',
            'contrato.required'=>'Se requiere este campo.'
        ];
    }
}
