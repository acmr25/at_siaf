<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ActivoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'unidad' =>'required|min:3|max:100',
            'placa' =>'min:3|max:100',
            'marca' =>'max:100',
            'modelo' =>'max:100',
            'serial' =>'min:3|max:100',
            'estatus' =>'min:3|max:100',
            'nro_activo' =>'digits:10|unique:activos,nro_activo',
            'prioridad' =>'min:3|max:100',
            'clasificacion' =>'min:3|max:100',
            'tipo' =>'min:3|max:100',
            'año' =>'digits:4',
            'color' =>'min:3|max:100',
            'tipo_aceite' =>'min:3|max:100',
            'filtro_aceite' =>'min:3|max:100',
            'filtro_aire' =>'min:3|max:100',
            'filtro_combustible' =>'min:3|max:100',
            'filtro_agua' =>'min:3|max:100',
            'foto' =>'image',
            'ubicacion_fija' =>'min:3|max:200',
            'horas' => 'numeric|between:0,9999999',
            'kilometros' => 'numeric|between:0,9999999'
        ];
    }
}
