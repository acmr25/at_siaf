<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticuloRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nro_activo' =>'required|min:4|max:30|unique:articulos,nro_activo',
            'nombre' =>'required|min:4|max:30',
            'categoria' =>'required|min:4|max:30',
            'descripcion' =>'min:4|max:200',
            'fabricante' =>'required|max:30',
            'unidad' =>'required|min:4|max:30',
            'cantidad' =>'numeric|between:0,5000',
            'lowstock' =>'numeric|between:0,5000'            
        ];
    }
    public function messages()
    {
        return
        [
            'nro_activo.required'=>'Se requiere este campo.',
            'nombre.required'=>'Se requiere este campo.',
            'categoria.required'=>'Se requiere este campo.',
            'fabricante.required'=>'Se requiere este campo.',
            'unidad.required'=>'Se requiere este campo.',
            
            'nro_activo.min'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'nombre.min'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'categoria.min'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'descripcion.min'=>'El nro. de caracteres debe estar entre 4 a 200.',
            'unidad.min'=>'El nro. de caracteres debe estar entre 4 a 30.',
            
            'nro_activo.max'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'nombre.max'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'categoria.max'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'descripcion.max'=>'El nro. de caracteres debe estar entre 4 a 200.',
            'fabricante.max'=>'El nro. de caracteres debe estar entre 4 a 30.',
            'unidad.max'=>'El nro. de caracteres debe estar entre 4 a 30.',
            
            'cantidad.numeric'=>'Este campo es numérico.',
            'lowstock.numeric'=>'Este campo es numérico.',
            'cantidad.between'=>'Solo se aceptan valores entre 0 y 5000',
            'lowstock.between'=>'Solo se aceptan valores entre 0 y 5000',

            'nro_activo.unique'=> 'Ya esta en uso.'
        ];
            
    }
}
