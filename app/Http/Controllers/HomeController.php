<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Activo;
use App\Reporte;
use App\Articulo;
use App\Orden;
use App\Empleado;
use App\Solicitud;
use App\Viaje;
use DB;
use Laracasts\Flash\Flash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $activos=Activo::where('mantenimiento','!=',null)
                        ->whereHas('tareas', function($query)
                        {
                            $query->where('orden_id',null);
                        })
                        ->orderBy('prioridad')->get();

        $activos->each(function($activos){
            $activos->tareas;
            return $activos;
        });                

        $pendiente=Activo::where('mantenimiento','!=',null)
                        ->whereHas('tareas', function($query)
                        {
                            $query->where('orden_id',null)->where('estado','PENDIENTE');
                        })
                        ->count();

        $acercandose=Activo::where('mantenimiento','!=',null)
                        ->whereHas('tareas', function($query)
                        {
                            $query->where('orden_id',null)->where('estado','ACERCANDOSE');
                        })
                        ->count();
                        
        $ordenes=Orden::where('estado_orden','!=', 'completada')->get();
            $ordenes->each(function($ordenes){
                $ordenes->inicia = date('d-m-Y', strtotime($ordenes->inicia)); 
                $ordenes->tareas;
                $ordenes->empleado;
                $ordenes->activo;
                return $ordenes;
            });

        $reportes=Reporte::where('estado','Sin asignar')->get(); 
            $reportes->each(function($reportes){
                $reportes->fecha = date('d-m-Y', strtotime($reportes->fecha)); 
                return $reportes;
            });

        $articulos=Articulo::where('estado', 'BAJO STOCK')->orderBy('nombre')->get();

        $viajes = Viaje::where('estado','!=', 'Completado')->get(); 
        $ahora_fecha= date('Y-m-d');
        $hora= date('H:i:s');
        foreach ($viajes as $viaje)
        {   
            if ($viaje->fecha_salida!=null) {
                if ( ($ahora_fecha < $viaje->fecha_salida) || ($ahora_fecha == $viaje->fecha_salida &&  $hora < $viaje->hora_salida) ) {
                        $viaje->estado = 'Programado';$viaje->save();
                    }
                if ( ($ahora_fecha > $viaje->fecha_salida) || ($ahora_fecha == $viaje->fecha_salida && $hora >= $viaje->hora_salida) ) {
                        $viaje->estado = 'En proceso';$viaje->save();    
                    } 
            }            
        }
        $solicitudes = Solicitud::where('tipo','LIVIANA')->whereIn('estado', ['Aprobado', 'Pendiente'])->get();


        //$tickets = Ticket::where('estado','!=', 'Completada')->get(); 
        //$ahora_fecha= date('Y-m-d');
        //$hora= date('H:i:s');
        //foreach ($tickets as $t)
        //{   
        //    if ( ($ahora_fecha < $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida &&  $hora < $t->hora_salida) ) {
        //            $t->estado = 'Pendiente';$t->save();
        //        }
       //     if ( ($ahora_fecha > $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida && $hora >= $t->hora_salida) ) {
        //            $t->estado = 'En proceso';$t->save();    
        //        }              
        //}
        //$tickets->each(function($tickets){
        //    $tickets->fecha_salida = date('d-m-Y', strtotime($tickets->fecha_salida));
        //    $tickets->empleado;
        //    $tickets->activo;
        //    return $tickets;
        //});  

        $li='';
        $hoy= date('d-m-Y');
        $last_login= date('d-m-Y', strtotime(Auth::user()->last_login)); 
        if ($hoy > $last_login){
            $usuario = User::findOrFail(Auth::user()->id);
            $usuario->last_login=date('Y-m-d H:i:s.u');      
            $usuario->save();
            Flash::message('<div class="text-center"><b>Hola '.Auth::user()->nombre.'.</br> Bienvenid@ al Sistema Integral de Administración Flota de Amazonas Tech C.A</b></div>')->important();
        }
        if (Auth::user()->rol_id == 1 || Auth::user()->rol_id == 2) {
            return redirect()->route('solicitudes.index');                     
        //return view('dashboard.solicitudes.index')->with('li',$li);
        }
        if (Auth::user()->rol_id == 4) {                        
            return redirect()->route('usuarios.index');           
        }        
        else {            
            return view('home')->with('li',$li)->with('activos',$activos)->with('articulos',$articulos)->with('pendiente',$pendiente)->with('acercandose',$acercandose)->with('ordenes',$ordenes)->with('reportes',$reportes)->with('viajes',$viajes)->with('solicitudes',$solicitudes);
        }
    }
}