<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ArticuloRequest;

use App\Articulo;
use App\Movimiento;
use App\Activo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

class ArticulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias=Articulo::orderBy('categoria')->lists('categoria','categoria')->unique();
        
        $fabricantes=Articulo::orderBy('fabricante')->lists('fabricante','fabricante')->unique();

        $li='articulos';

        $unidades=Articulo::orderBy('unidad')->lists('unidad','unidad')->unique();
        return view('dashboard.articulos.index')->with('categorias',$categorias)->with('fabricantes',$fabricantes)->with('unidades',$unidades)->with('li',$li);

    }

    public function listar()
    {
        try {
            $articulos = Articulo::get();

            foreach ($articulos as $articulo) {
                if ($articulo->cantidad <= $articulo->lowstock) {
                    $articulo->estado = 'BAJO STOCK';
                    $articulo->save();
                }
            }

            return Datatables::of($articulos)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en articulosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticuloRequest $request)
    {
        DB::beginTransaction();
        try {            
            $articulo = new Articulo($request->all());

            if ($request->cantidad <= $request->lowstock) {
                $articulo->estado = 'BAJO STOCK';                
            }
            $articulo->save();

            if ($request->cantidad > 0) {
                $m= new Movimiento;
                $m->articulo_id=$articulo->id;
                $m->cantidad=$request->cantidad;
                $m->tipo='ENTRADA';
                $m->razon='Stock Inicial';
                $m->creador_id=Auth::user()->id;
                $m->save();
            }

            DB::commit();
            return response()->json($articulo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ArticuloController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
         
        //$a= new Articulo($request->all());
        //$a->save();
        //Flash::success("Se ha agregado ". $a->nombre." ". $a->fabricante."!!");
        //return redirect()->route('articulos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $articulo = Articulo::findOrFail($id);
            return response()->json($articulo);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en AritculosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articulo=Articulo::find($id);
        $movimientos = $articulo->movimientos;
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');      
        $li='articulos';
        $categorias=Articulo::orderBy('categoria')->lists('categoria','categoria')->unique();
        $fabricantes=Articulo::orderBy('fabricante')->lists('fabricante','fabricante')->unique();
        $unidades=Articulo::orderBy('unidad')->lists('unidad','unidad')->unique();
        return view('dashboard.articulos.show')->with('activos',$activos)
                                            ->with('articulo',$articulo)
                                            ->with('categorias',$categorias)
                                            ->with('fabricantes',$fabricantes)
                                            ->with('unidades',$unidades)
                                            ->with('li',$li);;
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'nro_activo' =>'required|min:4|max:30|unique:articulos,nro_activo,'.$id,
                'nombre' =>'required|min:4|max:30',
                'categoria' =>'required|max:30',
                'descripcion' =>'min:4|max:200',
                'fabricante' =>'required|max:30',
                'unidad' =>'required|min:4|max:30',
                'cantidad' =>'numeric|between:0,5000',
                'lowstock' =>'numeric|between:0,5000']);

         DB::beginTransaction();
        try {
            $articulo = Articulo::findOrFail($id);
            $articulo->fill($request->all());
            $articulo->save();
            DB::commit();
            return response()->json($articulo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ArticulosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $articulo = Articulo::findOrFail($id);
            $articulo->movimientos;
            if ($articulo->movimientos) {
                foreach ($articulo->movimientos as $movimiento) {
                    $m = Movimiento::findOrFail($movimiento->id);
                    $m->delete();
                }    
            }

            $articulo->delete();
            DB::commit();
            return response()->json($articulo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en articulosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El articulo debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
}
