<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use App\Activo;
use App\Empleado;
use App\Medida;
use App\Solicitud;
use App\Viaje;

use Auth;
use DB;
use Log;
use Exception;

class ViajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='viajes_activo';
        return view('dashboard.viajes.index')->with('li',$li);
    }

    public function cerrados()
    {
        $li='viajes_inactivo';
        return view('dashboard.viajes.inactivas')->with('li',$li);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function nuevo(Request $request)
    {
        DB::beginTransaction();
        try {                   
                $puestos=0;
                foreach ($request->ids as $id) {
                    $solicitud = Solicitud::findOrFail($id);
                    if ($solicitud->estado == 'Aprobado') {
                        $puestos= $puestos + $solicitud->nro_pasajeros;
                    }
                }
                if ($puestos <= 4) {
                    $viaje = new Viaje([
                    'creador_id' => Auth::user()->id,
                    'estado' => 'Sin asignar',
                    ]);
                    $viaje->save();
                    foreach ($request->ids as $id) {
                        $solicitud = Solicitud::findOrFail($id);
                        if ($solicitud->estado == 'Aprobado' || $solicitud->estado == 'Pendiente' ) {
                            $solicitud->viajes()->attach($viaje->id);
                            $solicitud->estado = 'Sin Asignar';
                            $solicitud->save();
                        }
                    }
                    $viaje->puestos= $puestos;
                    $puestos = $viaje;             
                }

                DB::commit();
                return response()->json($puestos);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ViajesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $li='viajes_activo';        
        $viaje = Viaje::findOrFail($id);
        //if ($viaje->estado=='Sin asignar') {
        $unidad=$viaje->activos()->first();            
        $viaje->chofer;
        $viaje->ayudante;
        $viaje->solicitudes;
        $viaje->creador;

        $a_opciones=Activo::where('clasificacion','Vehiculo Liviano')->where('estatus','Operativo')->select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        $e_opciones= Empleado::where('tipo','Chofer de Vehículos Livianos')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

        if ($viaje->estado == 'Completado') {    
            $li='viajes_inactivo';
            return view('dashboard.viajes.cerrado')->with('viaje',$viaje)->with('li',$li)->with('a_opciones',$a_opciones)->with('e_opciones',$e_opciones)->with('unidad',$unidad);
        }

        return view('dashboard.viajes.show')->with('viaje',$viaje)->with('li',$li)->with('a_opciones',$a_opciones)->with('e_opciones',$e_opciones)->with('unidad',$unidad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $li='viajes_activo';        
        $viaje = Viaje::findOrFail($id);
        //if ($viaje->estado=='Sin asignar') {
            $unidad=$viaje->activos()->first();            
            $viaje->chofer;
            $viaje->ayudante;
            $viaje->solicitudes;
            $viaje->creador;

            $a_opciones=Activo::where('clasificacion','Vehiculo Liviano')->where('estatus','Operativo')->select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
            $e_opciones= Empleado::where('tipo','Chofer de Vehículos Livianos')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

            return view('dashboard.viajes.edit')->with('viaje',$viaje)->with('li',$li)->with('a_opciones',$a_opciones)->with('e_opciones',$e_opciones)->with('unidad',$unidad);
        //}
        //else {
        //    Flash::error("El viaje Nro. ".$viaje->id." no puede modificarse.");
        //    return redirect()->back();
        //}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $viaje = Viaje::findOrFail($id);
        $viaje->fill($request->all());

        $unidad=$viaje->activos()->first();
        if ($unidad != null && $unidad->id != $request->activo_id) {
            $viaje->activos()->detach($unidad->id);
            $viaje->activos()->attach($request->activo_id);                    
        }
        if ($unidad == null) {
            $viaje->activos()->attach($request->activo_id);
        }
        
        if ($request->eliminar!= null) {
          foreach ($request->eliminar as $eliminar) {
                $viaje->solicitudes()->detach($eliminar);
                $solicitud = Solicitud::findOrFail($eliminar);
                $solicitud->estado = 'Aprobado';
                $solicitud->save();
            }  
        }

        $viaje->estado = 'Programado';
        $solicitudes_actuales = Viaje::find($viaje->id)->solicitudes()->get();
        foreach ($solicitudes_actuales as $solicitud) {
            $solicitud->estado = 'Con viaje Programado';
            $solicitud->save();
        }
        $viaje->save();
        Flash::success("Se ha estructurado el Viaje Nro.". $viaje->id." correctamente!!");
        return redirect()->route('viajes.show', $viaje->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $viaje = Viaje::findOrFail($id);
            $solicitudes = Viaje::find($viaje->id)->solicitudes()->get();
            foreach ($solicitudes as $solicitud) {
                $solicitud->estado = 'Aprobado';
                $solicitud->save();
                $solicitud->viajes()->detach($viaje->id);
            }
            $unidad=$viaje->activos()->first();            
            if ($unidad) {
                $viaje->activos()->detach($unidad->id);
            }
            $viaje->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ViajesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function solicitudes_listar($option = 1)
    {
        try {
            switch ($option) {
                case 1:
                    $solicitudes = Solicitud::whereIn('estado', ['Aprobado', 'Pendiente'])->where('tipo','LIVIANA')->get();
                break;
                
                case 2:
                    $solicitudes = Solicitud::whereIn('estado', ['Sin Asignar', 'Con viaje Programado'])->where('tipo','LIVIANA')->get();
                break;

                case 3:
                    $solicitudes = Solicitud::whereIn('estado', ['Rechazado', 'Completada'])->where('tipo','LIVIANA')->get();
                break;
            }

            $solicitudes->each(function($solicitudes){
                $solicitudes->solicitante->departamento;
                $solicitudes->supervisor->departamento;
                $solicitudes->viajes;
                $solicitudes->fecha_salida= date('d-m-Y', strtotime($solicitudes->fecha_salida));           
                $solicitudes->hora_salida= date('h:i A', strtotime($solicitudes->hora_salida));
                $solicitudes->viajes;
                if ($solicitudes->estado == 'Con viaje Programado' || $solicitudes->estado == 'Pendiente' || $solicitudes->estado == 'Completada') {
                    foreach ($solicitudes->viajes as $viaje) {
                        if ( $viaje->estado = 'Programado' ||  $viaje->estado = 'Completado') {
                            $solicitudes->chofer= $viaje->chofer;
                        }
                    }
                }
                if ($solicitudes->fecha_regreso != null) {
                    $solicitudes->fecha_regreso= date('d-m-Y', strtotime($solicitudes->fecha_regreso));           
                    $solicitudes->hora_regreso= date('h:i A', strtotime($solicitudes->hora_regreso));
                }
                return $solicitudes;
            });  
   
            return Datatables::of($solicitudes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ViajesController'.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function listar($option = 1)
    {
        try {
            $ahora_fecha= date('Y-m-d');
            $hora= date('H:i:s');
            switch ($option) {
                case 1:
                    $viajes = Viaje::where('estado','!=', 'Completado')->whereDoesntHave('solicitudes', function($q){ $q->where('tipo', 'PESADA'); })->get();
                    foreach ($viajes as $viaje)
                    {   
                        if ($viaje->estado!='Sin asignar' ) {
                            if ($viaje->fecha_salida!=null) {
                                if ( ($ahora_fecha < $viaje->fecha_salida) || ($ahora_fecha == $viaje->fecha_salida &&  $hora < $viaje->hora_salida) ) {
                                        $viaje->estado = 'Programado';$viaje->save();
                                    }
                                if ( ($ahora_fecha > $viaje->fecha_salida) || ($ahora_fecha == $viaje->fecha_salida && $hora >= $viaje->hora_salida) ) {
                                        $viaje->estado = 'En proceso';$viaje->save();
                                    }
                            }     
                        }                                    
                    }
                break;
                
                case 2:
                    $viajes = Viaje::where('estado','Completado')->whereDoesntHave('solicitudes', function($q){ $q->where('tipo', 'PESADA'); })->get();
                break;
            }
            $viajes->each(function($viajes){
                $viajes->solicitudes;
                $viajes->activos;
                $viajes->chofer;
                $viajes->ayudante;
                $viajes->fecha_salida = date('d-m-Y', strtotime($viajes->fecha_salida));
                $viajes->hora_salida = date('h:i A', strtotime($viajes->hora_salida));
                if ($viajes->estado == 'Sin asignar') {
                    $viajes->fecha_salida = ''; 
                    $viajes->hora_salida = '';
                }
                if ($viajes->estado == 'Completado') {
                    $viajes->fecha_llegada = date('d-m-Y', strtotime($viajes->fecha_llegada));    
                    $viajes->hora_llegada = date('h:i A', strtotime($viajes->hora_llegada));
                }
                else {
                    $viajes->fecha_llegada = '';  
                    $viajes->hora_llegada = '';
                }
                foreach ($viajes->solicitudes as $solicitud) {
                        $viajes->solicitante= $solicitud->solicitante;
                }
                return $viajes;
            });          
            return Datatables::of($viajes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ViajesController'.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    } 

    public function por_unidad($id)
    {
        try {
            $viajes = Activo::find($id)->viajes()->whereDoesntHave('solicitudes', function($q){ $q->where('tipo', 'PESADA'); })->get();
            $viajes->each(function($viajes){
                $viajes->solicitudes;
                $viajes->chofer;
                $viajes->ayudante;
                $viajes->fecha_salida = date('d-m-Y', strtotime($viajes->fecha_salida));
                $viajes->hora_salida = date('h:i A', strtotime($viajes->hora_salida));
                if ($viajes->estado == 'Sin asignar') {
                    $viajes->fecha_salida = ''; 
                    $viajes->hora_salida = '';
                }
                if ($viajes->estado == 'Completado') {
                    $viajes->fecha_llegada = date('d-m-Y', strtotime($viajes->fecha_llegada));    
                    $viajes->hora_llegada = date('h:i A', strtotime($viajes->hora_llegada));

                }                else {
                    $viajes->fecha_llegada = '';  
                    $viajes->hora_llegada = '';
                }
                foreach ($viajes->solicitudes as $solicitud) {
                        $viajes->solicitante= $solicitud->solicitante;
                    }
                return $viajes;
            });         
            return Datatables::of($viajes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoOrdenController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function solicitudes_rechazar(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $solicitud = Solicitud::findOrFail($id);
                if ($solicitud->estado == 'Aprobado') {               
                    $solicitud->estado = 'Rechazado';
                    $solicitud->motivo_rechazo = $request->motivo_rechazo;
                }
                $solicitud->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ViajesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function opciones()
    {
        try {
            $viajes = Viaje::where('estado','!=', 'Completado')->whereDoesntHave('solicitudes', function($q){ $q->where('tipo', 'PESADA'); })->get();
            foreach ($viajes as $viaje) {
                $puestos=0;
                $solicitudes_actuales = Viaje::find($viaje->id)->solicitudes()->get();
                foreach ($solicitudes_actuales as $solicitud) {
                    $puestos= $puestos + $solicitud->nro_pasajeros;
                }
                $disp= 4 - $puestos;
                $viaje->nombre='ID '.$viaje->id.' (PUESTOS DISPONIBLES: '.$disp.')';
            }

            return response()->json($viajes);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function asignar(Request $request)
    {
        DB::beginTransaction();
        try {
            $viaje = Viaje::findOrFail($request->viaje_id);
            
            $puestos=0;
            
            foreach ($request->ids as $id) { //LAS SOLICITUDES A ASIGNAR AL VIAJE CONTAR PASAJEROS
                $solicitud = Solicitud::findOrFail($id);
                if ($solicitud->estado == 'Aprobado') {
                    $puestos= $puestos + $solicitud->nro_pasajeros;
                }
            }

            $solicitudes_actuales = Viaje::find($viaje->id)->solicitudes()->get(); //LAS SOLICITUDES QUE YA ESTAN ASIGNADAS AL VIAJE CONTAR PASAJEROS
            foreach ($solicitudes_actuales as $solicitud) {
                $puestos= $puestos + $solicitud->nro_pasajeros;
            }

            if ($puestos <= 4) {
                foreach ($request->ids as $id) {
                    $solicitud = Solicitud::findOrFail($id);
                    if ($solicitud->estado == 'Aprobado') {
                        $solicitud->viajes()->attach($viaje->id);
                        if ($viaje->estado == 'Sin asignar') {
                            $solicitud->estado = 'Sin Asignar';
                        }
                        if ($viaje->estado == 'Programado' || $viaje->estado == 'En proceso') {
                            $solicitud->estado = 'Con viaje Programado';
                        }
                        $solicitud->save();
                    }
                }                
            }

            DB::commit();
            return response()->json($puestos);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ViajesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function cerrar($id)
    {
        $li='viajes_activo';        
        $viaje = Viaje::findOrFail($id);
        //if ($viaje->estado=='Sin asignar') {
            $activo=$viaje->activos()->first();    
            $activo->each(function($activo){
                $activo->medida;
                return $activo;
            });        
            $viaje->chofer;
            $viaje->ayudante;
            $viaje->solicitudes;
            $viaje->creador;

            $a_opciones=Activo::where('clasificacion','Vehiculo Liviano')->where('estatus','Operativo')->select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
            $e_opciones= Empleado::where('tipo','Chofer de Vehículos Livianos')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

            return view('dashboard.viajes.cierre')->with('viaje',$viaje)->with('li',$li)->with('a_opciones',$a_opciones)->with('e_opciones',$e_opciones)->with('activo',$activo);
    }

    public function completar(Request $request, $id)
    {   
        $viaje = Viaje::findOrFail($id);
        $activo=$viaje->activos()->first(); 
        $m= Medida::where('activo_id',$activo->id)->first();

        if ( $request->fecha_llegada == $viaje->fecha_salida) {
            $this->validate($request,['hora_llegada'=>'after:'.$viaje->hora_salida ],['hora_llegada.after'=>':attribute debe ser una hora posterior a :date.']);
            }
        else {
            $this->validate($request,['fecha_llegada'=>'after:'.$viaje->fecha_salida ]);  
        }

        $viaje->destino=$request->destino;
        $viaje->observaciones=$request->observaciones;
        $viaje->fecha_llegada=$request->fecha_llegada;
        $viaje->hora_llegada=$request->hora_llegada;

        if ($m != null) {
            if ($m->horas != null && $m->kilometros == null) {
                $m->horas=$request->horas;
                $m->kilometros=null;
                $m->save(); 
            }
            if ($m->horas == null && $m->kilometros != null ) {
                $m->kilometros=$request->kilometros;
                $m->horas=null;
                $m->save(); 
            }
            if ($m->horas != null && $m->kilometros != null) {
                $m->horas=$request->horas;
                $m->kilometros=$request->kilometros;
                $m->save(); 
            }
        }
        
        if ($request->pendiente != null) {
            foreach ($request->pendiente as $pendiente) {
                $solicitud = Solicitud::findOrFail($pendiente);
                $solicitud->estado = 'Pendiente';
                $solicitud->save();
            }  
        }

        $viaje->estado = 'Completado';
        $viaje->save();

        $solicitudes_actuales = Viaje::find($viaje->id)->solicitudes()->get();
        foreach ($solicitudes_actuales as $solicitud) {
            if ($solicitud->estado == 'Con viaje Programado') {
                $solicitud->estado = 'Completada';
                $solicitud->save();                
            }
        }
        
        Flash::success("Se ha cerrado el Viaje Nro.". $viaje->id." correctamente!!");
        return redirect()->route('viajes.show', $viaje->id);
    }

}
