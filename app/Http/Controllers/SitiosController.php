<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Sitio;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class SitiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [
                'nombre'=>'required|min:3|max:100|unique:sitios,nombre']);
        DB::beginTransaction();
        try {
            $sitio = new Sitio($request->all());
            if($request->padre == 0 && $request->padre == NULL){
                $padre = null; 
            }else{
                $padre = $request->padre;
            }
            $sitio->padre = $padre;            
            $sitio->save();
            DB::commit();
            return response()->json($sitio);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $sitio = Sitio::findOrFail($id);
            return response()->json($sitio);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'nombre'=>'required|min:3|max:100|unique:sitios,nombre,'.$id,]);
        DB::beginTransaction();
        try {
            $sitio = Sitio::findOrFail($id);
            if($sitio->id == $request->padre){
                $padre = $sitio->padre; 
            }
            else{
                if ($request->padre == '' && $request->padre == 0 && $request->padre == NULL) {
                    $padre=null;
                }
                else{
                    $padre = $request->padre;
                }
            }
            $sitio->fill($request->all());
            $sitio->padre = $padre;
            $sitio->save();
            DB::commit();
            return response()->json($sitio);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $sitio = Sitio::findOrFail($id);
            Sitio::where('padre', $id)->update(['padre'=>'']);
            $sitio->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function listar()
    {
        try {
            $sitios = Sitio::get();
            $sitios->each(function($sitios){
                if($sitios->padreData != null){
                    $sitios->padre = $sitios->padreData->nombre;
                }else{
                    $sitios->padre = "";
                }
                return $sitios;
            });
            return Datatables::of($sitios)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function padres()
    {
        try {
            $sitios = Sitio::where('padre',NULL)->orderBy('nombre')->get();
            return response()->json($sitios);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en SitiosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

}
