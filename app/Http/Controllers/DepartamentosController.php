<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Departamento;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [
                'nombre'=>'required|min:3|max:100|unique:departamentos,nombre',
                'descripcion'=>'min:5|max:100']);
        DB::beginTransaction();
        try {
            $departamento = new Departamento($request->all());
            $departamento->save();
            DB::commit();
            return response()->json($departamento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $departamento = Departamento::findOrFail($id);
            return response()->json($departamento);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'nombre'=>'required|min:3|max:100|unique:departamentos,nombre,'.$id,
                'descripcion'=>'required|min:5|max:100']);
        DB::beginTransaction();
        try {
            $departamento = Departamento::findOrFail($id);
            if($departamento->id == $request->padre){
                $padre = $departamento->padre; 
            }
            else{
                if ($request->padre == '') {
                    $padre=null;
                }
                else{
                    $padre = $request->padre;
                }
            }
            $departamento->fill($request->all());
            $departamento->padre = $padre;
            $departamento->save();
            DB::commit();
            return response()->json($departamento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $departamento = Departamento::findOrFail($id);
            Departamento::where('padre', $id)->update(['padre'=>'']);
            $departamento->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function listar()
    {
        try {
            $departamentos = Departamento::get();
            $departamentos->each(function($departamentos){
                if($departamentos->padreData != null){
                    $departamentos->padre = $departamentos->padreData->nombre;
                }else{
                    $departamentos->padre = "";
                }
                return $departamentos;
            });
            return Datatables::of($departamentos)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function opciones()
    {
        try {
            $departamentos = Departamento::where('padre',NULL)->orderBy('nombre')->get();

            return response()->json($departamentos);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }
}
