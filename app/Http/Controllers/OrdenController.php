<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Reporte;
use App\Falla;
use App\Archivo;
use App\Empleado;
use App\Tarea;
use App\Articulo;
use App\Orden;
use App\Movimiento;
use App\Medida;
use App\User;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;
use File;

use PDF;

class OrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='ordenes';
        return view('dashboard.ordenes.index')->with('li',$li);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='ordenes';
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        $empleados= Empleado::whereIn('tipo', ['Mecánico', 'Proveedor Externo'])->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

        return view ('dashboard.ordenes.form')->with('li',$li)->with('activos',$activos)->with('empleados',$empleados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $orden= new Orden($request->all());//all me toma todas las columas del form CREO Xd
        $orden->inicia = date('Y/m/d', strtotime($request->inicia));
        $orden->estado_orden = 'abierta';
        $orden->creador_id = Auth::user()->id;
        $orden->save();

        if (isset($request->task)) {
            $task=Tarea::find($request->task);
            if ($task==null) {
                    $task = new Tarea;
                    $task->nombre=$request->task;
                    $task->mantenimiento='Correctivo'; 
                    $task->save();
            }
            $relacion = Orden::find($orden->id)->tareas()->get()->contains($task->id);
            if ($relacion == false) {
                $pivote=$task->ordenes()->attach($orden->id, 
                [
                    'status' =>'incompleta', 
                ]); 
                $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($task->id);
                if ($relacion_activo == true) {
                    $task_activo=$task->activos()->updateExistingPivot($orden->activo_id, 
                    [
                        'orden_id' => $orden->id, 
                    ]);
                }
            }    
        }

        if (isset($request->reporte_id)) {
            $reporte=Reporte::find($request->reporte_id);
            $reporte->estado = 'Asignado';
            $reporte->save();

        }

        $cout=count($request->tarea);
        for ($i = 0; $i < $cout; $i++) {
            $tarea=Tarea::find($request->tarea[$i]);
            if ($tarea==null) {
                $tarea = new Tarea;
                $tarea->nombre=$request->tarea[$i];
                $tarea->mantenimiento='Correctivo'; 
                $tarea->save();
            }
            $rela = Orden::find($orden->id)->tareas()->get()->contains($tarea->id);
            if ($rela == false) {
                $pivote=$tarea->ordenes()->attach($orden->id, 
                [
                    'status' =>'incompleta', 
                ]); 
                $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($tarea->id);
                if ($relacion_activo == true) {
                    $tarea_activo=$tarea->activos()->updateExistingPivot($orden->activo_id, 
                    [
                        'orden_id' => $orden->id, 
                    ]);
                }
            }
        }

        Flash::success("Se ha creado con exito la Orden de Trabajo Nro. ". $orden->id."!!");
        return redirect()->route('ordenes.show', $orden->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orden = Orden::findOrFail($id);
        $orden->activo;
        $orden->empleado;
        $orden->reporte;
        $orden->tareas;
        $orden->movimientos;
        $orden->archivos;            
        $orden->inicia = date('d-m-Y', strtotime($orden->inicia));
        $orden->termina = date('d-m-Y', strtotime($orden->termina));

        $li='ordenes';
        return view ('dashboard.ordenes.show') ->with ('orden',$orden)->with('li',$li);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orden = Orden::findOrFail($id);
        $orden->reporte;

        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        $empleados= Empleado::where('tipo','Mecánico')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

        $li='ordenes';
        return view ('dashboard.ordenes.edit') ->with ('orden',$orden)->with('li',$li)->with('activos',$activos)->with('empleados',$empleados);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Orden = Orden::findOrFail($id);
        $Orden->fill($request->all());
        $Orden->inicia = date('Y/m/d', strtotime($request->inicia));
        $Orden->modificador_id = Auth::user()->id;
        $Orden->save();

        Flash::success("Se ha actualizado con exito la Orden de Trabajo Nro. ". $Orden->id."!!");
        return redirect()->route('ordenes.show', $Orden->id);
    }

    public function cambiar($id, $estado)
    {
        DB::beginTransaction();
        try {

            $orden = Orden::findOrFail($id);
            if ($estado == 3) {
                 $orden->estado_orden ='en progreso';
            }
            else{
                $orden->estado_orden = $estado;                
            }
            $orden->termina = null;
            $orden->save();
            DB::commit();
            return response()->json($orden);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function completar(Request $request, $id)
    {   
        $orden = Orden::findOrFail($id);
        $m= Medida::where('activo_id',$orden->activo_id)->first();

        if ($m != null) {
            if ($m->horas != null && $m->kilometros == null) {
                $this->validate($request, 
                    [   
                        'termina' => 'required',
                        'horas' => 'required|numeric|between:0,9999999',
                    ]); 
            }
            if ($m->horas == null && $m->kilometros != null) {
                $this->validate($request, 
                    [   
                        'termina' => 'required',
                        'kilometros' => 'required|numeric|between:0,9999999',
                    ]); 
            }
            if ($m->horas != null && $m->kilometros != null) {
                $this->validate($request, 
                    [   
                        'termina' => 'required',
                        'horas' => 'required|numeric|between:0,9999999',
                        'kilometros' => 'required|numeric|between:0,9999999'
                    ]); 
            }           
        }
        
        DB::beginTransaction();
        try {
            $orden->termina = date('Y/m/d', strtotime($request->termina));
            $orden->estado_orden='completada';

            $orden->save();

            if ($m != null) {
                if ($request->horas != null && $request->kilometros == null ) {
                    $m->horas=$request->horas;
                    $m->kilometros=null;
                    $m->save(); 
                }
                if ($request->horas == null && $request->kilometros != null ) {
                    $m->kilometros=$request->kilometros;
                    $m->horas=null;
                    $m->save(); 
                }
                if ($request->horas != null && $request->kilometros != null ) {
                    $m->fill($request->all());
                    $m->save(); 
                }
            }

            foreach ($orden->tareas as $tarea) {
                $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($tarea->id);
                if ($relacion_activo) {
                    if ($request->horas != null && $request->kilometros == null) {
                        $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                        ['realizacion_horas' => $request->horas,
                        'orden_id' => null,
                    ]);
                    }
                    if ($request->horas == null && $request->kilometros != null) {
                        $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                        ['realizacion_kilometros' => $request->kilometros,
                        'orden_id' => null,
                    ]);
                    }
                    if ($request->horas != null && $request->kilometros != null) {

                        $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                        ['realizacion_horas' => $request->horas,
                        'realizacion_kilometros' => $request->kilometros,
                        'orden_id' => null,
                    ]);
                    }
                }   
            }

            $a=Activo::find($orden->activo_id);
            $a->each(function($a){
                $a->medida;
                $a->tareas;
                return $a;
            });
            if ($a->medida != null) {
                $pendiente =0;
                $acercandose =0;
                foreach ($a->tareas as $tareas) {
                    if ($tareas->pivot->horas != null && $tareas->pivot->kilometros == null) {
                        $ht= $tareas->pivot->horas + $tareas->pivot->realizacion_horas;
                        $ha= $tareas->pivot->pre_horas + $tareas->pivot->realizacion_horas;
                        if ($ha > 0  && $a->medida->horas >= $ha ) {
                            if ($a->medida->horas >= $ht) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->horas >= $ht) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }
                    if ($tareas->pivot->horas == null && $tareas->pivot->kilometros != null) {
                        $kt= $tareas->pivot->kilometros + $tareas->pivot->realizacion_kilometros;
                        $ka= $tareas->pivot->pre_kilometros + $tareas->pivot->realizacion_kilometros;
                        if ($ka > 0  && $a->medida->kilometros >= $ka) {
                            if ($a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }
                    if ($tareas->pivot->horas != null && $tareas->pivot->kilometros != null) {
                        $ht= $tareas->pivot->horas + $tareas->pivot->realizacion_horas;
                        $ha= $tareas->pivot->pre_horas + $tareas->pivot->realizacion_horas;
                        $kt= $tareas->pivot->kilometros + $tareas->pivot->realizacion_kilometros;
                        $ka= $tareas->pivot->pre_kilometros + $tareas->pivot->realizacion_kilometros;
                        if (($ha > 0  && $a->medida->horas >= $ha ) || ($ka > 0  && $a->medida->kilometros >= $ka)) {
                            if ($a->medida->horas >= $ht || $a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->horas >= $ht || $a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }                    
                }
                if ($acercandose > 0 && $pendiente > 0) {
                    $a->mantenimiento= 'PENDIENTE Y ACERCANDOSE';
                    $a->save();
                }
                if ($acercandose > 0 && $pendiente == 0) {
                    $a->mantenimiento= 'ACERCANDOSE';
                    $a->save();
                }
                if ($acercandose == 0 && $pendiente > 0) {
                    $a->mantenimiento= 'PENDIENTE';
                    $a->save();
                }
                if ($acercandose == 0 && $pendiente == 0) {
                    $a->mantenimiento= null;
                    $a->save();
                }
            }

            DB::commit();
            return response()->json($orden);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en OrdenController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $orden = Orden::findOrFail($id);

            if ($orden->reporte_id != null) {
                $reporte = Reporte::findOrFail($orden->reporte_id);
                $reporte->estado = 'Sin asignar';
                $reporte->save();   
            }

            $orden->movimientos;
            if ($orden->movimientos) {
                foreach ($orden->movimientos as $movimiento) {
                    $a = Articulo::findOrFail($movimiento->articulo_id);
                    $a->cantidad = $a->cantidad + $movimiento->cantidad;
                    $a->save();
                }    
            }
            $orden->tareas;
            if ($orden->tareas) {
                foreach ($orden->tareas as $tarea) {
                    $t = Tarea::findOrFail($tarea->id);
                    $orden->tareas()->detach($t->id);
                    if ($t->mantenimiento != 'Preventivo') {
                        $t->delete();
                    }
                    else {
                        $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($t->id);
                        if ($relacion_activo == true) {
                            $task_activo= $t->activos()->updateExistingPivot($orden->activo_id, 
                            [
                                'orden_id' => null, 
                            ]);
                        }    
                    }                                        
                }    
            }

            $orden->delete();

            DB::commit();
            return response()->json($orden);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function getTareas($id)
    {
        $activo =Activo::find($id);
        //mostrara las tareas relacionadas con el activo ($id) y que no tienen ordenes de trabajo
        $tareas = Tarea::wherehas('activos', function($q) use ($id){ $q->where('activo_id', $id); })->
                        whereDoesntHave('ordenes', function($q) use ($activo){ 
                                $q
                                ->where('activo_id', $activo->id)
                                ->where('estado_orden', 'abierta')
                                ->orWhere('estado_orden', 'en progreso')
                                ->orWhere('estado_orden', 'detenida'); 
                        })
                        ->orderBy('nombre')->select('id','nombre')->get();
        return response()->json($tareas); 
    }

    public function getArticulos()
    {
        $articulos=Articulo::where('cantidad', '>', 0)->orderBy('nombre')->select(DB::raw("CONCAT(nro_activo, ' - ', nombre, ' - ', cantidad, ' ',unidad) as full"),'id')->get();;

        return response()->json($articulos); 
    }

    public function listar()
    {
        try {
            $ordenes = Orden::get();

            $ordenes->each(function($ordenes){
                $ordenes->inicia = date('d-m-Y', strtotime($ordenes->inicia));
                if ($ordenes->termina != null) {
                    $ordenes->termina = date('d-m-Y', strtotime($ordenes->termina));    
                } 
                $ordenes->tareas;
                $ordenes->user;
                $ordenes->reporte;
                $ordenes->empleado;
                $ordenes->activo;
                $ordenes->unidad= $ordenes->activo->unidad.' '.$ordenes->activo->placa.' '.$ordenes->activo->marca.' '.$ordenes->activo->modelo;
                
                return $ordenes;
            });

            return Datatables::of($ordenes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoOrdenController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function borrar_tarea($o,$t)
    {
        DB::beginTransaction();
        try {
            $orden = Orden::findOrFail($o);
            $tarea = Tarea::findOrFail($t);

            $orden->tareas()->detach($tarea->id);
            if ($tarea->mantenimiento != 'Preventivo') {
                $tarea->delete();
            }
            else {
                $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($tarea->id);
                if ($relacion_activo == true) {
                    $task_activo= $tarea->activos()->updateExistingPivot($orden->activo_id, 
                    [
                        'orden_id' => null, 
                    ]);
                }    
            }
            DB::commit();
            return response()->json($orden);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function borrar_movimiento($id)
    {
        DB::beginTransaction();
        try {
            $movimiento = Movimiento::findOrFail($id);
            $a = Articulo::findOrFail($movimiento->articulo_id);
            $a->cantidad = $a->cantidad + $movimiento->cantidad;
            $a->save();
            $movimiento->delete();

            DB::commit();
            return response()->json($movimiento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function agregar_tarea(Request $request)
    {
        $tarea=Tarea::find($request->tarea_id);

        if ($tarea == null) {
            $tarea = new Tarea;
            $tarea->nombre=$request->tarea_id;
            $tarea->mantenimiento='Correctivo'; 
            $tarea->save();
        }

        $orden=Orden::find($request->orden_id);

        DB::beginTransaction();
        try {

            $pivote=$tarea->ordenes()->attach($orden->id, 
            [
                'status' =>'incompleta', 
            ]); 

            $relacion_activo = Activo::find($orden->activo_id)->tareas()->get()->contains($tarea->id);
            if ($relacion_activo == true) {
                $tarea_activo=$tarea->activos()->updateExistingPivot($orden->activo_id, 
                [
                    'orden_id' => $orden->id, 
                ]);
            }

            DB::commit();
            return response()->json($pivote);

        }
        catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en OrdenController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function pdf($id)
    {   
        $orden = Orden::findOrFail($id);
        $orden->activo;
        $orden->empleado;
        $orden->tareas;
        $orden->movimientos;
        $orden->archivos;            
        $orden->inicia = date('d-m-Y', strtotime($orden->inicia));
        if ($orden->termina != null) {
            $orden->termina = date('d-m-Y', strtotime($orden->termina));    
        }else{ $orden->termina = '';}
        

        set_time_limit(150);
        return PDF::loadView('dashboard.ordenes.print',array('orden' => $orden))
        ->stream('Orden de Trabajo Nro. '.$orden->id.'.pdf',array('Attachment'=>0));
        //->download('orden Nro. '.$orden->id.'.pdf'); 
    }
}
