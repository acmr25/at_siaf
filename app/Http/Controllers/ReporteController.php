<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Reporte;
use App\Falla;
use App\Archivo;
use App\User;
use Auth;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;
use File;

use PDF;

class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='reportes';
        return view('dashboard.reportes.index')->with('li',$li);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $li='reportes';
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        return view('dashboard.reportes.form')->with('li',$li)->with('activos',$activos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reporte= new Reporte($request->all());//all me toma todas las columas del form CREO Xd
        $reporte->fecha = date('Y/m/d', strtotime($request->fecha));
        $reporte->estado = 'Sin asignar';
        $reporte->creador_id = Auth::user()->id;
        $reporte->save();

        $reporte->each(function($reporte){
                $reporte->activo;
                return $reporte;
            });

        $cout=count($request->nombre);

        for ($i = 0; $i < $cout; $i++) {

            $falla= new Falla;
            $falla->reporte_id=$reporte->id;
            $falla->nombre=$request->nombre[$i];
            $falla->tipo=$request->tipo[$i];
            $falla->save();
        }

        foreach ($request->archivo as $i) {
            if($i != null){
                $name = $i->getClientOriginalName();
                $nombre = pathinfo($name, PATHINFO_FILENAME); // file
                $extension = pathinfo($name, PATHINFO_EXTENSION); // jpg
                $nombre_externo= $nombre.'.'.$extension;
                $nombre_interno= $nombre.'-'.time().'.'.$extension;
                $path_file = public_path().'/image/archivos/';

                if ($extension == 'png' || $extension == 'jpg' || $extension == 'PNG' || $extension == 'JPG') {
                    $tipo = 'imagen';    
                }
                else{
                    $tipo = 'otros';
                }

                $archivo = new Archivo([
                    'nombre_externo' => $nombre_externo,
                    'nombre_interno' => $nombre_interno,
                    'reporte_id' =>  $reporte->id,
                    'activo_id' =>  $reporte->activo_id,
                    'tipo' =>  $tipo,
                    'descripcion' => 'Este archivo pertenece al reporte Nro.'.$reporte->id.' de la unidad Nro.'.$reporte->activo->nro_activo,
                    ]);
                $archivo->save();
                $i->move($path_file,$nombre_interno);
            }
        }  
        
        Flash::success("Se ha agregado el Reporte Nro.". $reporte->id."!!");
        return redirect()->route('reportes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $reporte = Reporte::findOrFail($id);
            
                $reporte->fecha = date('d-m-Y', strtotime($reporte->fecha));
                $reporte->archivos;
                $reporte->nro_archivos = count($reporte->archivos);
                $reporte->fallas;
                $reporte->activo;
             
            return response()->json($reporte);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoReporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reporte = Reporte::findOrFail($id);

        $reporte->each(function($reporte){
            $reporte->fecha = date('d-m-Y', strtotime($reporte->fecha));
            $reporte->archivos;
            $reporte->fallas;
            $reporte->activo;
            return $reporte;
        });

        $prioridad = ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'];

        $li='reportes';
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');

        return view ('dashboard.reportes.form')->with('reporte',$reporte)->with('li',$li)->with('activos',$activos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $reporte = Reporte::findOrFail($id);
        $reporte->fill($request->all());
        $reporte->fecha = date('Y/m/d', strtotime($request->fecha));
        $reporte->modificador_id = Auth::user()->id;
        $reporte->save();

        $reporte->each(function($reporte){
            $reporte->activo;
            $reporte->fallas;
            return $reporte;
        });
    
        $fallas = Falla::where('reporte_id', $reporte->id)->delete(); 

        $cout=count($request->nombre);

        for ($i = 0; $i < $cout; $i++) {
            if ($request->nombre[$i] != null && $request->tipo[$i]) {
                $falla= new Falla;
                $falla->reporte_id=$reporte->id;
                $falla->nombre=$request->nombre[$i];
                $falla->tipo=$request->tipo[$i];
                $falla->save();  
            }            
        }

        if ($request->eliminar!= null) {
          foreach ($request->eliminar as $eliminar) {
                $archivo_borrar = Archivo::findOrFail($eliminar);
                $file= $archivo_borrar->nombre_interno;
                $filename = public_path().'/image/archivos/'.$file;
                File::delete($filename);
                $archivo_borrar->delete();
            }  
        }
        

        foreach ($request->archivo as $i) {
            if($i != null){
                $name = $i->getClientOriginalName();
                $nombre = pathinfo($name, PATHINFO_FILENAME); // file
                $extension = pathinfo($name, PATHINFO_EXTENSION); // jpg
                $nombre_externo= $nombre.'.'.$extension;
                $nombre_interno= $nombre.'-'.time().'.'.$extension;
                $path_file = public_path().'/image/archivos/';

                if ($extension == 'png' || $extension == 'jpg' || $extension == 'PNG' || $extension == 'JPG') {
                    $tipo = 'imagen';    
                }
                else{
                    $tipo = 'otros';
                }
                $archivo = new Archivo([
                    'nombre_externo' => $nombre_externo,
                    'nombre_interno' => $nombre_interno,
                    'reporte_id' =>  $reporte->id,
                    'activo_id' =>  $reporte->activo_id,
                    'tipo' =>  $tipo,
                    'descripcion' => 'Este archivo pertenece al reporte Nro.'.$reporte->id.' de la unidad Nro.'.$reporte->activo->nro_activo,
                    ]);
                $archivo->save();
                $i->move($path_file,$nombre_interno);
            }
        }  
        
        Flash::success("Se ha actualizado el Reporte Nro.". $reporte->id."!!");
        return redirect()->route('reportes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $reporte = Reporte::findOrFail($id);

            $reporte->each(function($reporte){
                $reporte->archivos;
                return $reporte;
            });

            if ($reporte->archivos != null) {
                foreach ($reporte->archivos as $archivo) {
                    $file= $archivo->nombre_interno;
                    $filename = public_path().'/image/archivos/'.$file;
                    File::delete($filename);
                }
            }

            $reporte->delete();
            DB::commit();
            return response()->json($reporte);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ReporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El reporte debe estar relacionado en alguna operación.'
                ], 500);
        }
    }

    public function listar()
    {
        try {
            $reportes = Reporte::get();

            $reportes->each(function($reportes){
                $reportes->fecha = date('d-m-Y', strtotime($reportes->fecha));
                $reportes->archivos;
                $reportes->nro_archivos = count($reportes->archivos);
                $reportes->fallas;
                $reportes->nro_fallas = count($reportes->fallas);
                $reportes->orden;
                $reportes->activo;
                $reportes->unidad= $reportes->activo->unidad.' '.$reportes->activo->placa.' '.$reportes->activo->marca.' '.$reportes->activo->modelo;
                return $reportes;
            });
          
           return Datatables::of($reportes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ReportesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
}
