<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Movimiento;
use App\Articulo;
use App\Activo;
use App\Orden;


use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use Auth;

use DB;
use Log;
use Exception;

class MovimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

     public function movimientos($id)
    {
        try {
            $movimiento = Movimiento::where('articulo_id',$id)->get();
            $movimiento->each(function($movimiento){
                $movimiento->creador;
                $movimiento->activo;
                if ($movimiento->activo != null) {
                    $movimiento->equipo= $movimiento->activo->unidad.' '.$movimiento->activo->placa.' '.$movimiento->activo->marca.' '.$movimiento->activo->modelo;                    
                }
                $movimiento->fecha = date('h:i A d-m-Y ',  strtotime($movimiento->created_at));
                return $movimiento;
            });
            return Datatables::of($movimiento)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en MovimientoController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    public function listar()
    {
        try {
            $movimiento = Movimiento::get();
            $movimiento->each(function($movimiento){
                $movimiento->creador;
                $movimiento->articulo;
                $movimiento->orden;
                $movimiento->fecha = date('h:i d-m-Y ',  strtotime($movimiento->created_at));
                $movimiento->activo;
                if ($movimiento->activo != null) {
                    $movimiento->equipo= $movimiento->activo->unidad.' '.$movimiento->activo->placa.' '.$movimiento->activo->marca.' '.$movimiento->activo->modelo;                    
                }
                return $movimiento;
            });

            $collection = collect($movimiento);
            return Datatables::of($collection)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en MovimientoController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
            if ($request->tipo=='SALIDA') {
            $this->validate($request, 
                [
                    'activo_id' =>'required',
                    'tipo' =>'required',
                    'cantidad' =>'required|numeric|between:1,5000',
                    'razon' =>'required|min:10|max:500',
                ]);    
            }
            else{
                $this->validate($request, 
                [
                    'tipo' =>'required',
                    'cantidad' =>'required|numeric|between:1,5000',
                    'razon' =>'required|min:10|max:500',
                ]);
            }    

        DB::beginTransaction();
        try {
            
            $articulo = Articulo::findOrFail($request->articulo_id);
            
            if ($request->tipo == 'SALIDA' || $request->orden_id != null) {
                if ( $articulo->cantidad >= $request->cantidad ) {
                    $movimiento = new Movimiento($request->all());
                    $movimiento->creador_id=Auth::user()->id;
                    $movimiento->save();

                    $articulo->cantidad = $articulo->cantidad - $movimiento->cantidad;
                    if ($articulo->cantidad <= $articulo->lowstock) {
                        $articulo->estado = 'BAJO STOCK';
                    }
                    $articulo->save();                      
                }
            }  
            if ($request->tipo == 'ENTRADA'){
                $movimiento = new Movimiento($request->all());
                $movimiento->creador_id=Auth::user()->id;
                $movimiento->activo_id=null;
                $movimiento->save();
                $articulo->cantidad= $articulo->cantidad + $movimiento->cantidad;
                if ($articulo->cantidad > $articulo->lowstock) {
                    $articulo->estado = null;
                }
                else {
                    $articulo->estado = 'BAJO STOCK';
                }
                $articulo->save();
            }
            
            DB::commit();
            return response()->json($movimiento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en MovimientoController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
