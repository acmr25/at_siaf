<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Archivo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;
use File;

class ActivoArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear($id)
    {
        $activo_id =$id;
        $li='activos';
        return view('dashboard.activos.archivos.sub')->with('activo_id',$activo_id)->with('li',$li);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->archivo as $i) {
            if($i != null){
                $name = $i->getClientOriginalName();
                $nombre = pathinfo($name, PATHINFO_FILENAME); // file
                $extension = pathinfo($name, PATHINFO_EXTENSION); // jpg
                $nombre_externo= $nombre.'.'.$extension;
                $nombre_interno= $nombre.'-'.time().'.'.$extension;
                $path_file = public_path().'/image/archivos/';

                $archivo = new Archivo([
                    'nombre_externo' => $nombre_externo,
                    'nombre_interno' => $nombre_interno,
                    'activo_id' =>  $request->activo_id,
                    'descripcion' => $request->descripcion,
                    ]);
                $archivo->save();
                $i->move($path_file,$nombre_interno);
            }
        } 

            Flash::success("Se han subido los archivos con exito!!");
            return redirect()->route('activos.show', $request->activo_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $archivo = Archivo::findOrFail($id);

            $file= $archivo->nombre_interno;
            $filename = public_path().'/image/archivos/'.$file;
            File::delete($filename);

            $archivo->delete();
            DB::commit();
            return response()->json($archivo);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivoArchivoController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El archivo debe estar relacionado en alguna operación.'
                ], 500);
        }
    }

    public function listar($id)
    {
        try {

            $activo = Activo::find($id)->archivos()->get();
            return Datatables::of($activo)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

}
