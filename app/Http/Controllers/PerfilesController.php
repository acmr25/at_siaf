<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\Departamento;
use App\Rol;
use App\User;


class PerfilesController extends Controller
{
    public function index()
    {
        $li='';
        return view('dashboard.usuarios.form')
        ->with('user', Auth::user())
        ->with('li', $li)
        ->with('departamentos', Departamento::lists('nombre', 'id'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'cedula'=>'required|unique:users,cedula,'.$id,
                'email'=>'required|email',
                'nombre'=>'required|min:5|max:45',
                'cargo'=>'required|min:5|max:30',
                'password' => 'min:6|confirmed',
                'password_confirmation' => 'min:6',
                'departamento_id' => 'required']);

         DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $pass_last = $usuario->password;
            $usuario->fill($request->all());
            if($request->password != null || !empty($request->password)){
                $usuario->password = bcrypt($request->password);
            }else{
                $usuario->password = $pass_last;
            }
            $usuario->save();
            DB::commit();
            Flash::success("Se han actualizados sus datos  exitosamente!");
            return redirect()->route('perfil.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            Flash::error("Ha ocurrido un error al intentar guardar los datos, vuelva a intentar más tarde.");
            return redirect()->route('perfil.index');
        }
    }
}
