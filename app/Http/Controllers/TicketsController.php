<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use App\Activo;
use App\Archivo;
use App\Empleado;
use App\Medida;
use App\Ticket;

use Auth;
use DB;
use Log;
use Exception;
use File;
use PDF;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='tickets';
        $activos_opciones=Activo::select(DB::raw("CONCAT(marca, ' / ', modelo, ' / ', placa) as full"),'id')->lists('full','id');
        $empleados_opciones= Empleado::select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');
        return view('dashboard.tickets.index')->with('li',$li)->with('activos_opciones',$activos_opciones)->with('empleados_opciones',$empleados_opciones);
    }

    public function listar()
    {
        try {
            $tickets = Ticket::get();
            $ahora_fecha= date('Y-m-d');
            $hora= date('H:i:s');
            foreach ($tickets as $t)
            {   
            	if ($t->estado != 'Completada') {
	                if ( ($ahora_fecha < $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida &&  $hora < $t->hora_salida) ) {
	                        $t->estado = 'Pendiente';$t->save();
	                    }
	                if ( ($ahora_fecha > $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida && $hora >= $t->hora_salida) ) {
	                        $t->estado = 'En proceso';$t->save();    
	                    }            		
            	}
            }
            $tickets->each(function($tickets){
                $tickets->fecha_salida = date('d-m-Y', strtotime($tickets->fecha_salida));
                $tickets->hora_salida = date('h:i A', strtotime($tickets->hora_salida));
                if ($tickets->estado == 'Completada') {
                    $tickets->fecha_llegada = date('d-m-Y', strtotime($tickets->fecha_llegada));    
                    $tickets->hora_llegada = date('h:i A', strtotime($tickets->hora_llegada));
                }
                else {
                    $tickets->fecha_llegada = '';  
                    $tickets->hora_llegada = '';
                }
                $tickets->empleado;
                $tickets->activo;
                return $tickets;
            });         
            return Datatables::of($tickets)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function listar_pendientes()
    {
        try {
            $tickets = Ticket::where('estado','!=', 'Completada')->get();
            $ahora_fecha= date('Y-m-d');
            $hora= date('H:i:s');
            foreach ($tickets as $t)
            {   
                if ( ($ahora_fecha < $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida &&  $hora < $t->hora_salida) ) {
                        $t->estado = 'Pendiente';$t->save();
                    }
                if ( ($ahora_fecha > $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida && $hora >= $t->hora_salida) ) {
                        $t->estado = 'En proceso';$t->save();    
                    }              
            } 
            $tickets->each(function($tickets){
                $tickets->fecha_salida = date('d-m-Y', strtotime($tickets->fecha_salida));
                $tickets->hora_salida = date('h:i A', strtotime($tickets->hora_salida));
                $tickets->empleado;
                $tickets->activo;
                return $tickets;
            });         
            return Datatables::of($tickets)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, 
        [
            'activo_id'=>'required',
            'responsable_id'=>'required',
            'fecha_salida'=>'required',
            'hora_salida'=>'required',
            'pasajeros'=>'required',
            'ruta'=>'required',
            'descripcion'=>'required',],
        [
            'activo_id.required'=>'Seleccione una unidad/equipo',
            'responsable_id.required'=>'Seleccione al Responsable',
            'fecha_salida.required'=>'Llene este campo.',
            'hora_salida.required'=>'Llene este campo.',
            'pasajeros.required'=>'Llene este campo.',
            'ruta.required'=>'Llene este campo.',                       
            'descripcion.required'=>'Llene este campo.']);
        DB::beginTransaction();
        try {
            $ticket = new Ticket($request->all());
            $ticket->creador = Auth::user()->nombre;
            $ticket->fecha_llegada=null;
            $ticket->hora_llegada=null;

            $ahora_fecha= date('Y-m-d'); $hora= date('H:i:s');
            if ( ($ahora_fecha < $ticket->fecha_salida) || ($ahora_fecha == $ticket->fecha_salida &&  $hora < $ticket->hora_salida) ) {
                    $ticket->estado = 'Pendiente';
                }
            if ( ($ahora_fecha > $ticket->fecha_salida) || ($ahora_fecha == $ticket->fecha_salida && $hora >= $ticket->hora_salida) ) {
                    $ticket->estado = 'En proceso';
                }
            $ticket->save();
            DB::commit();
            return response()->json($ticket);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $ticket = Ticket::findOrFail($id);

            $ticket->activo;
            $ticket->activo->medida;
            $ticket->empleado;

            $ticket->sf= date('d-m-Y', strtotime($ticket->fecha_salida));
            $ticket->sh= date('h:i A', strtotime($ticket->hora_salida));

            if ($ticket->estado == 'Completada') {
                $ticket->lf= date('d-m-Y', strtotime($ticket->fecha_llegada));
                $ticket->lh= date('h:i A', strtotime($ticket->hora_llegada)); 
            }
            else{
                $ticket->lf= '';
                $ticket->lh= '';
            }

            return response()->json($ticket);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
        [
            'activo_id'=>'required',
            'responsable_id'=>'required',
            'fecha_salida'=>'required',
            'hora_salida'=>'required',
            'pasajeros'=>'required',
            'ruta'=>'required',
            'descripcion'=>'required',],
        [
            'activo_id.required'=>'Seleccione una unidad/equipo',
            'responsable_id.required'=>'Seleccione al Responsable',
            'fecha_salida.required'=>'Llene este campo.',
            'hora_salida.required'=>'Llene este campo.',
            'pasajeros.required'=>'Llene este campo.',
            'ruta.required'=>'Llene este campo.',                       
            'descripcion.required'=>'Llene este campo.']);

        DB::beginTransaction();
        try {
            $ticket = Ticket::findOrFail($id);
            $ticket->fill($request->all());
            $ticket->modificador = Auth::user()->nombre;

            $ticket->save();
            DB::commit();
            return response()->json($ticket);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function completar(Request $request, $id)
    {   
        $ticket = Ticket::findOrFail($id);
        $m= Medida::where('activo_id',$ticket->activo_id)->first();

        if ($m != null) {
            if ($m->horas != null && $m->kilometros == null) {
                $this->validate($request, 
                    [   
                        'fecha_llegada'=>'required',
                        'hora_llegada'=>'required' ,
                        'horas' => 'required|numeric|between:0,9999999',
                    ]); 
            }
            if ($m->horas == null && $m->kilometros != null) {
                $this->validate($request, 
                    [   
                        'fecha_llegada'=>'required',
                        'hora_llegada'=>'required' ,
                        'kilometros' => 'required|numeric|between:0,9999999',
                    ]); 
            }
            if ($m->horas != null && $m->kilometros != null) {
                $this->validate($request, 
                    [   
                        'fecha_llegada'=>'required',
                        'hora_llegada'=>'required' ,
                        'horas' => 'required|numeric|between:0,9999999',
                        'kilometros' => 'required|numeric|between:0,9999999'
                    ]); 
            }           
        }
        else {
            $this->validate($request, 
            [
                'fecha_llegada'=>'required',
                'hora_llegada'=>'required' ]);            
        }

        DB::beginTransaction();
        try {            

            if ( ($request->fecha_llegada > $ticket->fecha_salida) || ($request->fecha_llegada == $ticket->fecha_salida && $request->hora_llegada > $ticket->hora_salida) ) {
                if ($m != null) {
                    if ($m->horas != null && $m->kilometros == null) {
                        $m->horas=$request->horas;
                        $m->kilometros=null;
                        $m->save(); 
                    }
                    if ($m->horas == null && $m->kilometros != null ) {
                        $m->kilometros=$request->kilometros;
                        $m->horas=null;
                        $m->save(); 
                    }
                    if ($m->horas != null && $m->kilometros != null) {
                        $m->horas=$request->horas;
                        $m->kilometros=$request->kilometros;
                        $m->save(); 
                    }
                }
                $ticket->fecha_llegada=$request->fecha_llegada;
                $ticket->hora_llegada=$request->hora_llegada;
                $ticket->modificador = Auth::user()->nombre;
                $ticket->estado = 'Completada';
                $ticket->save();
                $bueno='bueno';
            }
            DB::commit();
            return response()->json($bueno);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $usuario = Ticket::findOrFail($id);
            $usuario->delete();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El usuario debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
    
    public function pdf($id)
    {   
        $ticket = Ticket::findOrFail($id);
        $ticket->activo;
        $ticket->empleado;           
        $ticket->fecha_salida = date('d-m-Y', strtotime($ticket->fecha_salida));
        $ticket->hora_salida = date('h:i A', strtotime($ticket->hora_salida));
        if ($ticket->fecha_llegada != null && $ticket->hora_llegada != null) {
            $ticket->fecha_llegada = date('d-m-Y', strtotime($ticket->fecha_llegada)); 
            $ticket->hora_llegada = date('h:i A', strtotime($ticket->hora_llegada));
        }else{
            $ticket->fecha_llegada = '';
            $ticket->hora_llegada = '';
        }        

        set_time_limit(150);
        return PDF::loadView('dashboard.tickets.print',array('ticket' => $ticket))
        ->stream('Ticket de Entrada/Salida Nro. '.$ticket->id.'.pdf',array('Attachment'=>0));
        //->download('orden Nro. '.$orden->id.'.pdf'); 
    }

    public function unidad_listar($id)
    {
        try {
            $tickets = Activo::find($id)->tickets()->get();
            $ahora_fecha= date('Y-m-d');
            $hora= date('H:i:s');
            foreach ($tickets as $t)
            {   
                if ($t->estado != 'Completada') {
                    if ( ($ahora_fecha < $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida &&  $hora < $t->hora_salida) ) {
                            $t->estado = 'Pendiente';$t->save();
                        }
                    if ( ($ahora_fecha > $t->fecha_salida) || ($ahora_fecha == $t->fecha_salida && $hora >= $t->hora_salida) ) {
                            $t->estado = 'En proceso';$t->save();    
                        }                   
                }
            }
            $tickets->each(function($tickets){
                $tickets->fecha_salida = date('d-m-Y', strtotime($tickets->fecha_salida));
                $tickets->hora_salida = date('h:i A', strtotime($tickets->hora_salida));
                if ($tickets->estado == 'Completada') {
                    $tickets->fecha_llegada = date('d-m-Y', strtotime($tickets->fecha_llegada));    
                    $tickets->hora_llegada = date('h:i A', strtotime($tickets->hora_llegada));
                }
                else {
                    $tickets->fecha_llegada = '';  
                    $tickets->hora_llegada = '';
                }
                $tickets->empleado;
                $tickets->activo;
                return $tickets;
            });         
            return Datatables::of($tickets)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en TicketsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
}
