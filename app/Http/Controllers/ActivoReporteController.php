<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Reporte;
use App\Falla;
use App\Archivo;
use App\Empleado;
use App\Tarea;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;
use File;

use PDF;

class ActivoReporteController extends Controller
{

    public function show($id)
    {
        try {
            $reporte = Reporte::findOrFail($id);

            
                $reporte->fecha = date('d-m-Y', strtotime($reporte->fecha));
                $reporte->archivos;
                $reporte->nro_archivos = count($reporte->archivos);
                $reporte->fallas;
                $reporte->activo;
             
            return response()->json($reporte);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoReporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }
    public function listar($id)
    {
        try {
            $reportes = Activo::find($id)->reportes()->get();

            $reportes->each(function($reportes){
                $reportes->fecha = date('d-m-Y', strtotime($reportes->fecha));
                $reportes->archivos;
                $reportes->nro_archivos = count($reportes->archivos);
                $reportes->fallas;
                $reportes->nro_fallas = count($reportes->fallas);
                $reportes->orden;
                return $reportes;
            });

            return Datatables::of($reportes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivosReporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }


    public function pdf($id)
    {   
        $reporte = Reporte::findOrFail($id);

        $reporte->each(function($reporte){
            $reporte->fecha = date('d-m-Y', strtotime($reporte->fecha));
            $reporte->activo;
            $reporte->fallas;
            $reporte->archivos;
            return $reporte;
        });
        set_time_limit(150);
        return PDF::loadView('dashboard.activos.reportes.print',array('reporte' => $reporte))
        ->stream('Reporte de Fallas Nro. '.$reporte->id.'.pdf',array('Attachment'=>0));
        //->download('Reporte Nro. '.$reporte->id.'.pdf'); 
    }

    public function orden($id)
    {
        $reporte = Reporte::findOrFail($id);

        $reporte->each(function($reporte){
            $reporte->fecha = date('d-m-Y', strtotime($reporte->fecha));
            $reporte->fallas; $reporte->nro_fallas = count( $reporte->fallas);
            $reporte->activo;

            return $reporte;
        });

        $activo =Activo::find($reporte->activo->id);

        $prioridad = ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'];

        $li='ordenes';
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        $empleados= Empleado::where('tipo','Mecánico')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->lists('full','id');

        //mostrara las tareas relacionadas con el activo ($id) y que no tienen ordenes de trabajo
        $tareas = Tarea::wherehas('activos', function($q) use ($activo){ $q->where('activo_id', $activo->id); })->
                        whereDoesntHave('ordenes', function($q) use ($activo){ 
                                $q
                                ->where('activo_id', $activo->id)
                                ->where('estado_orden', 'abierta')
                                ->orWhere('estado_orden', 'en progreso')
                                ->orWhere('estado_orden', 'detenida'); 
                        })
                        ->orderBy('nombre')->lists('nombre','id');

        return view ('dashboard.ordenes.form')->with('reporte',$reporte)->with('li',$li)->with('activos',$activos)->with('empleados',$empleados)->with('tareas',$tareas);
    }

}
