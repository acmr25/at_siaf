<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Medida;
use App\Tarea;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;


class ActivoTareaController extends Controller
{
    public function listar($id)
    {
        try {            

            $a=Activo::find($id);

            $a->each(function($a){
                $a->medida;
                $a->tareas;
                return $a;
            });

            if ($a->medida != null) {

                $pendiente =0;
                $acercandose =0;
                foreach ($a->tareas as $tareas) {

                    if ($tareas->pivot->horas != null && $tareas->pivot->kilometros == null) {

                        $ht= $tareas->pivot->horas + $tareas->pivot->realizacion_horas;
                        $ha= $tareas->pivot->pre_horas + $tareas->pivot->realizacion_horas;

                        if ($ha > 0  && $a->medida->horas >= $ha ) {

                            if ($a->medida->horas >= $ht) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->horas >= $ht) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }
                    if ($tareas->pivot->horas == null && $tareas->pivot->kilometros != null) {

                        $kt= $tareas->pivot->kilometros + $tareas->pivot->realizacion_kilometros;
                        $ka= $tareas->pivot->pre_kilometros + $tareas->pivot->realizacion_kilometros;

                        if ($ka > 0  && $a->medida->kilometros >= $ka) {

                            if ($a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }
                    if ($tareas->pivot->horas != null && $tareas->pivot->kilometros != null) {

                        $ht= $tareas->pivot->horas + $tareas->pivot->realizacion_horas;
                        $ha= $tareas->pivot->pre_horas + $tareas->pivot->realizacion_horas;
                        $kt= $tareas->pivot->kilometros + $tareas->pivot->realizacion_kilometros;
                        $ka= $tareas->pivot->pre_kilometros + $tareas->pivot->realizacion_kilometros;

                        if (($ha > 0  && $a->medida->horas >= $ha ) || ($ka > 0  && $a->medida->kilometros >= $ka)) {

                            if ($a->medida->horas >= $ht || $a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = 'ACERCANDOSE';   
                                $tareas->pivot->save();
                                $acercandose ++;
                            }                    
                        }
                        else {
                            if ($a->medida->horas >= $ht || $a->medida->kilometros >= $kt) {
                                $tareas->pivot->estado = 'PENDIENTE';   
                                $tareas->pivot->save();
                                $pendiente ++;
                            }
                            else {
                                $tareas->pivot->estado = null;   
                                $tareas->pivot->save();
                            }
                        }
                    }                    
                }
                if ($acercandose > 0 && $pendiente > 0) {
                    $a->mantenimiento= 'PENDIENTE Y ACERCANDOSE';
                    $a->save();
                }
                if ($acercandose > 0 && $pendiente == 0) {
                    $a->mantenimiento= 'ACERCANDOSE';
                    $a->save();
                }
                if ($acercandose == 0 && $pendiente > 0) {
                    $a->mantenimiento= 'PENDIENTE';
                    $a->save();
                }
                if ($acercandose == 0 && $pendiente == 0) {
                    $a->mantenimiento= null;
                    $a->save();
                }

            }

            $activo = Activo::find($id)->tareas()->where('mantenimiento','Preventivo')->get();
            return Datatables::of($activo)->make(true);


        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    public function store(Request $request)
    {
        $tarea=Tarea::find($request->tarea_id);

        if ($tarea == null) {
            $this->validate($request, 
            [ 
                'tarea_id' =>'required|min:10|max:200|unique:tareas,nombre',
                'horas' =>'integer|min:0',
                'pre_horas' =>'integer|min:0',
                'realizacion_horas' =>'integer|min:0',

                'kilometros' =>'integer|min:0',
                'pre_kilometros' =>'integer|min:0',
                'realizacion_kilometros' =>'integer|min:0',

                'prioridad' =>'required'
            ]);

            $tarea = new Tarea;
            $tarea->nombre=$request->tarea_id;
            $tarea->mantenimiento='Preventivo'; 
            $tarea->save();
        }
        else{            
        $this->validate($request, 
            [                 
                'prioridad' =>'required',

                'horas' =>'integer|min:0',
                'pre_horas' =>'integer|min:0',
                'realizacion_horas' =>'integer|min:0',

                'kilometros' =>'integer|min:0',
                'pre_kilometros' =>'integer|min:0',
                'realizacion_kilometros' =>'integer|min:0'
            ]);
        }

        DB::beginTransaction();
        try {
            //VERIFICAR SI YA EXISTE UNA RELACION ENTRE EL ACTIVO Y LA TAREA
            $activo = Activo::find($request->activo_id)->tareas()->get()->contains($tarea->id);
           
            if ($activo == false) {

                if ($request->horas != null && $request->kilometros == null) {
                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'prioridad' => $request->prioridad,
                    ]);
                }
                if ($request->horas == null && $request->kilometros != null) {
                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'kilometros' => $request->kilometros, 
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
                if ($request->horas != null && $request->kilometros != null) {

                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'kilometros' => $request->kilometros,
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
            }                
            DB::commit();
            return response()->json($listo);

        }
        catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivoTareaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos. La tarea ya puede estar relacionada o se requieren campos.'
                ], 500);
        }
    }
    
    public function show($id)
    {

        try {
            $result = DB::table('activo_tarea')->where('id', $id)->get(); 
            return response()->json($result);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoTareaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $tarea=Tarea::find($request->tarea_id);

        if ($tarea == null) {
            $this->validate($request, 
            [ 
                'tarea_id' =>'required|min:10|max:200|unique:tareas,nombre',
                'prioridad' =>'required',
                'horas' =>'integer|min:0',
                'pre_horas' =>'integer|min:0',
                'realizacion_horas' =>'integer|min:0',

                'kilometros' =>'integer|min:0',
                'pre_kilometros' =>'integer|min:0',
                'realizacion_kilometros' =>'integer|min:0'
            ]);

            $tarea = new Tarea;
            $tarea->nombre=$request->tarea_id;
            $tarea->mantenimiento='Preventivo'; 
            $tarea->save();
        }

        else{            
        $this->validate($request, 
            [   'prioridad' =>'required',
            
                'horas' =>'integer|min:0',
                'pre_horas' =>'integer|min:0',
                'realizacion_horas' =>'integer|min:0',

                'kilometros' =>'integer|min:0',
                'pre_kilometros' =>'integer|min:0',
                'realizacion_kilometros' =>'integer|min:0'
            ]);
        }

        DB::beginTransaction();
        try {
            //VERIFICAR SI YA EXISTE UNA RELACION ENTRE EL ACTIVO Y LA TAREA
            $activo = Activo::find($request->activo_id)->tareas()->get()->contains($tarea->id);
           
            if ($activo){

                if ($request->horas != null && $request->kilometros == null) {
                    $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'prioridad' => $request->prioridad,
                    ]);
                }
                if ($request->horas == null && $request->kilometros != null) {
                    $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                    [
                        'kilometros' => $request->kilometros, 
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
                if ($request->horas != null && $request->kilometros != null) {

                    $listo=$tarea->activos()->updateExistingPivot($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'kilometros' => $request->kilometros,
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
            }

            if ($activo == false) {

                if ($request->horas != null && $request->kilometros == null) {
                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'prioridad' => $request->prioridad,

                    ]);
                }
                if ($request->horas == null && $request->kilometros != null) {
                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'kilometros' => $request->kilometros, 
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
                if ($request->horas != null && $request->kilometros != null) {

                    $listo=$tarea->activos()->attach($request->activo_id, 
                    [
                        'horas' => $request->horas, 
                        'pre_horas' => $request->pre_horas,
                        'realizacion_horas' => $request->realizacion_horas,
                        'kilometros' => $request->kilometros,
                        'pre_kilometros' => $request->pre_kilometros,
                        'realizacion_kilometros' => $request->realizacion_kilometros,
                        'prioridad' => $request->prioridad,
                    ]);
                }
            }                
            DB::commit();
            return response()->json($listo);

        }
        catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivoTareaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos. La tarea ya puede estar relacionada o se requieren campos.'
                ], 500);
        }
    }

    public function destroy($activo,$tarea)
    {
       DB::beginTransaction();
        try {

            $joder=Tarea::find($tarea);
            $joder->activos()->detach($activo);
            
            DB::commit();
            return response()->json($joder);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en EmpleadosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El empleado debe estar relacionado en alguna operación.'
                ], 500);
        }
    } 
}
