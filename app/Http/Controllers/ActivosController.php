<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ActivoRequest;

use App\Activo;
use App\Medida;
use App\Tarea;
use App\Empleado;
use File;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

use PDF;

class ActivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$activos=Activo::all();
        //$medidas=Medida::all();
        $li='activos';

        return view('dashboard.activos.index')->with('li',$li);
    }

    public function listar()
    {
        try {
            $activo = Activo::get();
            $activo->each(function($activo){
                $activo->medida;
                if ($activo->ubicacion_temporal != null || $activo->ubicacion_temporal != '') {
                    $activo->ubicacion=$activo->ubicacion_temporal.' (Temporal)';   
                }else{
                    $activo->ubicacion=$activo->ubicacion_fija;
                }
                $activo->descripcion = $activo->unidad.' '.$activo->placa.' '.$activo->marca.' '.$activo->modelo.' {'.$activo->nro_activo.'}';                
                return $activo;
            });
            return Datatables::of($activo)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='activos';
        $ub_fija=Activo::orderBy('ubicacion_fija')->lists('ubicacion_fija','ubicacion_fija')->unique();
        $clasificaciones=Activo::orderBy('clasificacion')->lists('clasificacion','clasificacion')->unique();
        $tipos=Activo::orderBy('tipo')->lists('tipo','tipo')->unique();
        $marcas=Activo::orderBy('marca')->lists('marca','marca')->unique();
        $colores=Activo::orderBy('color')->lists('color','color')->unique();
        return view('dashboard.activos.new')->with('tipos',$tipos)->with('clasificaciones',$clasificaciones)->with('marcas',$marcas)->with('colores',$colores)->with('li',$li)->with('ub_fija',$ub_fija);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivoRequest $request)
    {
        $a= new Activo($request->all());//all me toma todas las columas del form CREO Xd
        $a->save();

        if ($request -> file('foto')) {
            $extension = $request -> file('foto')->getClientOriginalExtension();
            $file_name = $a->id.'.'.$extension;
            $path_file = public_path().'/image/activo/images/';
            $request->file('foto')->move($path_file,$file_name);
            $a->foto=$file_name;
            $a->save();
        }    

        if ($request->horas != null && $request->kilometros == null ) {
            $m= new Medida;
            $m->horas=$request->horas;
            $m->activo_id=$a->id;
            $m->save(); 
        }
        if ($request->horas == null && $request->kilometros != null ) {
            $m= new Medida;
            $m->horas=$request->kilometros;
            $m->activo_id=$a->id;
            $m->save(); 
        }
        if ($request->horas != null && $request->kilometros != null ) {
            $m= new Medida($request->all());
            $m->activo_id=$a->id;
            $m->save(); 
        }

        Flash::success("Se ha agregado ". $a->modelo." ". $a->placa."!!");
        return redirect()->route('activos.show', $a->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activo =Activo::find($id);
        $activo->each(function($activo){
                $activo->medida;
                $activo->tareas;
                $activo->reportes;
                $activo->archivos;
                return $activo;
            });
        
        $ahora= date('d-m-Y h:i A ');
        //SI EL ACTIVO SE CREO EN ESTE MOMENTO Y TIENE MEDIDAS ASOCIADAS SE IRA DIRECTAMENTE A LA PESTAÑA DE MANTENIMIENTO
        $fecha1= date('d-m-Y h:i A ',  strtotime($activo->created_at));
        if ($fecha1 == $ahora && $activo->medida != null) {
            $tabs='tareas'; 
        }
        else {
            $tabs='detalles';
        }
        foreach ($activo->archivos as $archivo) {
            $fecha2=date('d-m-Y h:i A ',  strtotime($archivo->created_at));
            if ($fecha2 == $ahora) {
                $tabs='archivos';   
            }
        }
        //si no hay niguna de las anteriores se va directamente a la pestaña de detalles        
        $li='activos';


        //ENLISTAR las tareas de mantenimiento que no estan asociadas a un activo en especifico.
        //$tarea = Tarea::where('mantenimiento','Preventivo')->whereDoesntHave('activos', function($q) use ($id){ $q->where('activo_id', $id); })->orderBy('nombre')->lists('nombre','id');
        $tarea = Tarea::where('mantenimiento','Preventivo')->orderBy('nombre')->lists('nombre','id');
        return view ('dashboard.activos.show') ->with ('activo',$activo)->with ('tarea',$tarea)->with('li',$li)->with('tabs',$tabs);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activo=Activo::find($id);
        $ub_fija=Activo::orderBy('ubicacion_fija')->lists('ubicacion_fija','ubicacion_fija')->unique();
        $clasificaciones=Activo::orderBy('clasificacion')->lists('clasificacion','clasificacion')->unique();
        $tipos=Activo::orderBy('tipo')->lists('tipo','tipo')->unique();
        $marcas=Activo::orderBy('marca')->lists('marca','marca')->unique();
        $colores=Activo::orderBy('color')->lists('color','color')->unique();
        $li='activos';

        if ($activo->medida == null) {
            return view ('dashboard.activos.edit1') ->with ('activo',$activo)->with('tipos',$tipos)->with('marcas',$marcas)->with('colores',$colores)->with('clasificaciones',$clasificaciones)->with('li',$li)->with('ub_fija',$ub_fija);
        }
        else {
            return view ('dashboard.activos.edit') ->with ('activo',$activo)->with('tipos',$tipos)->with('marcas',$marcas)->with('colores',$colores)->with('clasificaciones',$clasificaciones)->with('li',$li)->with('ub_fija',$ub_fija);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'unidad' =>'required|min:3|max:100',
                'placa' =>'min:3|max:100',
                'marca' =>'max:100',
                'modelo' =>'max:100',
                'serial' =>'min:3|max:100',
                'estatus' =>'min:3|max:100',
                'nro_activo' =>'digits:10|unique:activos,nro_activo,'.$id,
                'prioridad' =>'min:3|max:100',
                'clasificacion' =>'min:3|max:100',
                'tipo' =>'min:3|max:100',
                'año' =>'digits:4',
                'color' =>'min:3|max:100',
                'tipo_aceite' =>'min:3|max:100',
                'filtro_aceite' =>'min:3|max:100',
                'filtro_aire' =>'min:3|max:100',
                'filtro_combustible' =>'min:3|max:100',
                'filtro_agua' =>'min:3|max:100',
                'foto' =>'image',
                'ubicacion_fija' =>'min:3|max:200',
                'horas' => 'numeric|between:0,9999999',
                'kilometros' => 'numeric|between:0,9999999'                
            ]);

        $s= Activo::find($id);
        $s->fill($request->all());

        if ($request->file('foto')) {
            $extension = $request -> file('foto')->getClientOriginalExtension();
            $file_name = $s->id.'.'.$extension;
            $path_file = public_path().'/image/activo/images/';
            $request->file('foto')->move($path_file,$file_name);
            $s->foto=$file_name;
           
        }
        if ($s->foto != null && $request->file('foto')==null) {
            $file= $s->foto;
            $filename = public_path().'/image/activo/images/'.$file;
            File::delete($filename);
            $s->foto=null;
        } 
        $s->save();   

        if ($s->medida==null) {
            if ($request->horas != null && $request->kilometros == null ) {
                $m= new Medida;
                $m->horas=$request->horas;
                $m->activo_id=$s->id;
                $m->save(); 
            }
            if ($request->horas == null && $request->kilometros != null ) {
                $m= new Medida;
                $m->kilometros=$request->kilometros;
                $m->activo_id=$s->id;
                $m->save(); 
            }
            if ($request->horas != null && $request->kilometros != null ) {
                $m= new Medida($request->all());
                $m->activo_id=$s->id;
                $m->save(); 
            }                
        }
        else {
            $m= Medida::where('activo_id',$id)->first();
            if ($request->horas != null && $request->kilometros == null ) {
                $m->horas=$request->horas;
                $m->kilometros=null;
                $m->save(); 
            }
            if ($request->horas == null && $request->kilometros != null ) {
                $m->kilometros=$request->kilometros;
                $m->horas=null;
                $m->save(); 
            }
            if ($request->horas != null && $request->kilometros != null ) {
                $m->fill($request->all());
                $m->save(); 
            }  
        }


        Flash::success("Se ha actualizado la unidad ".$s->modelo. " ".$s->placa. " con éxito!!");
        return redirect()->route('activos.show',  $s->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $activo = Activo::findOrFail($id);

            $activo->tareas()->detach();

            $file= $activo->foto;
            $filename = public_path().'/image/activo/images/'.$file;
            File::delete($filename);
            $activo->delete();
            DB::commit();
            return response()->json($activo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActivosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function pdf($id)
    {   
        $activo = Activo::findOrFail($id);

        $activo->each(function($activo){
            $activo->reportes;
            $activo->ordenes;            
            $activo->viajes;            
            return $activo;
        });
        set_time_limit(150);
        $pdf = PDF::loadView('dashboard.activos.print-hoja-de-vida',array('activo' => $activo))->stream('Hoja de Vida de la Unidad Nro. '.$activo->nro_activo.'.pdf',array('Attachment'=>0));
        return $pdf; 
    }
}
