<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use Auth;
use App\User;
use App\Rol;
use App\Departamento;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $li='usuarios';
        $roles=Rol::orderBy('nombre')->lists('nombre','id');
        $departamentos=Departamento::orderBy('nombre')->lists('nombre','id');
        return view('dashboard.usuarios.index')->with('li',$li)->with('departamentos',$departamentos)->with('roles',$roles);
    }

    public function listar()
    {
        try {
            $id=Auth::user()->id;
            $usuario = User::where('id','!=', $id)->get();
            $usuario->each(function($usuario){
                $usuario->departamento;
                $usuario->rol;
                return $usuario;
            });          
            return Datatables::of($usuario)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('dashboard.usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
        [
            'cedula'=>'required|unique:users,cedula',
            'usuario'=>'required|unique:users,usuario',
            'email'=>'required|email',
            'nombre'=>'required|min:5|max:45',
            'cargo'=>'required|min:5|max:30',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'rol_id' => 'required',
            'departamento_id' => 'required',
            'estado' => 'required']);
        DB::beginTransaction();
        try {
            $usuario = new User($request->all());
            $usuario->password = bcrypt($request->password);
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UserController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }

        //$e= new User($request->all());//all me toma todas las columas del form CREO Xd
        //$e->save();
        //Flash::success("Se ha agregado ". $e->nombre." ". $e->apellido."!!");
        //return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {
            $usuario = User::findOrFail($id);
            return response()->json($usuario);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$usuario=User::find($id);
        //$o= ['Chofer'=>'Chofer','Mecánico'=>'Mecánico','Operador'=>'Operador' ];
        //return view ('dashboard.usuarios.edit') ->with ('usuario',$usuario) ->with ('o', $o);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'cedula'=>'required|unique:users,cedula,'.$id,
                'usuario'=>'required|unique:users,usuario,'.$id,
                'email'=>'required|email',
                'nombre'=>'required|min:5|max:45',
                'cargo'=>'required|min:5|max:30',
                'password' => 'min:6|confirmed',
                'password_confirmation' => 'min:6',
                'rol_id' => 'required',
                'departamento_id' => 'required',
                'estado' => 'required',]);

         DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $pass_last = $usuario->password;
            $usuario->fill($request->all());
            if($request->password != null || !empty($request->password)){
                $usuario->password = bcrypt($request->password);
            }else{
                $usuario->password = $pass_last;
            }
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }


        //$s= User::find($id);
        //$s->fill($request->all());
        //$s->save();
        //Flash::success("Se ha actualizado al usuario ".$s->nombre. " ".$s->apellido. " con éxito!!");
        //return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $usuario->delete();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El usuario debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
}
