<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Activo;
use App\Reporte;
use App\Falla;
use App\Archivo;
use App\Empleado;
use App\Tarea;
use App\Articulo;
use App\Orden;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;
use File;

use PDF;

class ActivoOrdenController extends Controller
{
    public function listar($id)
    {
        try {
            $ordenes = Activo::find($id)->ordenes()->get();

            $ordenes->each(function($ordenes){
                $ordenes->inicia = date('d-m-Y', strtotime($ordenes->inicia));
                if ($ordenes->termina != null) {
                    $ordenes->termina = date('d-m-Y', strtotime($ordenes->termina));    
                }                
                $ordenes->tareas;
                $ordenes->user;
                $ordenes->reporte;
                $ordenes->empleado;
                return $ordenes;
            });

            return Datatables::of($ordenes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActivoOrdenController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    public function nuevo($id)
    {
        
        $activo =Activo::find($id);

        $prioridad = ['Baja'=>'Baja','Media'=>'Media', 'Alta'=>'Alta'];

        $li='ordenes';
        $activos=Activo::select(DB::raw("CONCAT(unidad,' ', placa,' ',marca,' ', modelo) as full"),'id')->lists('full','id');
        $empleados= Empleado::where('tipo','Mecánico')->where('condicion','Activo')->select(DB::raw("CONCAT(nombre,' - #', id) as full"),'id')->get();


        //mostrara las tareas relacionadas con el activo ($id) y que no tienen ordenes de trabajo
        $tareas = Tarea::wherehas('activos', function($q) use ($id){ $q->where('activo_id', $id); })->
                        whereDoesntHave('ordenes', function($q) use ($activo){ 
                                $q
                                ->where('activo_id', $activo->id)
                                ->where('estado_orden', 'abierta')
                                ->orWhere('estado_orden', 'en progreso')
                                ->orWhere('estado_orden', 'detenida'); 
                        })
                        ->orderBy('nombre')->select('id','nombre')->get();

        return view ('dashboard.ordenes.form')->with('activo',$activo)->with('li',$li)->with('activos',$activos)->with('empleados',$empleados)->with('tareas',$tareas);
    }
}
