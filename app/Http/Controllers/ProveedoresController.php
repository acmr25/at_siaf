<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\EmpleadoRequest;

use App\Empleado;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='proveedores';
        return view('dashboard.proveedores.index')->with('li',$li);
    }

    public function listar()
    {
        try {
            $proovedores = Empleado::where('tipo','Proveedor Externo')->get();           
            return Datatables::of($proovedores)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ProveedoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        DB::beginTransaction();
        try {
            $proveedor = new Empleado($request->all());
            $proveedor->save();
            DB::commit();
            return response()->json($proveedor);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ProveedoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $proveedor = Empleado::findOrFail($id);
            return response()->json($proveedor);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ProveedoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         DB::beginTransaction();
        try {
            $proveedor = Empleado::findOrFail($id);
            $proveedor->fill($request->all());
            $proveedor->save();
            DB::commit();
            return response()->json($proveedor);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ProveedoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::beginTransaction();
        try {
            $proveedor = Empleado::findOrFail($id);
            $proveedor->delete();
            DB::commit();
            return response()->json($proveedor);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ProveedoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El empleado debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
}
