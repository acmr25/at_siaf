<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Solicitud;
use App\Sitio;
use App\User;
use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class SolicitudesLivianosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $li='solicitudes';
        return view('dashboard.solicitudes.index')->with('li',$li);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
        $li='solicitudes';    	
    	$padres=Sitio::where('padre', null)->orderBy('nombre')->pluck('nombre','id');
    	$supervisores= User::where('rol_id', 2)->orderBy('nombre')->pluck('nombre','id');
        return view('dashboard.solicitudes.form')->with('supervisores',$supervisores)->with('padres',$padres)->with('li',$li);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
        $li='solicitudes';     
    	$origen_padre=Sitio::find($request->origen_padre);
    	$destino_padre=Sitio::find($request->destino_padre);
    	$origen=  $origen_padre->nombre.' - '. $request->origen_hijo.' - '. $request->origen;
    	$destino=  $destino_padre->nombre.' - '. $request->destino_hijo.' - '. $request->destino;

    	if ( $request->origen_padre==$request->destino_padre && $request->origen_hijo==$request->destino_hijo) {
	    	if (isset($request->fecha_regreso)) {
				if ($request->fecha_salida == $request->fecha_regreso ) {
	        	$this->validate($request, 
	            ['destino' =>'different:origen',
	        	 'hora_regreso' =>'after:hora_salida'],
	            ['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida',
	        	 'hora_regreso.after'=> 'La hora de regreso debe ser una hora posterior a la hora de salida']); 			
				}
		    	else {
	        	$this->validate($request, 
	            ['destino' =>'different:origen',
	        	 'fecha_regreso' =>'after:fecha_salida'],
	            ['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida',
	        	 'fecha_regreso.after'=> 'La fecha de regreso debe ser una fecha posterior a la fecha de salida']); 	    		
		    	}    		
	    	}
	    	else {
	        	$this->validate($request, 
	            ['destino' =>'different:origen'],['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida']);	    		
	    	}  		
    	}
    	if (isset($request->fecha_regreso)) {
	    	if ($request->fecha_salida == $request->fecha_regreso ) {
	        	$this->validate($request, 
	            ['hora_regreso' =>'after:hora_salida'],['hora_regreso.after'=> 'La hora de regreso debe ser una hora posterior a la hora de salida']);   		
	    	}
	    	else {
	        	$this->validate($request, 
	            ['fecha_regreso' =>'after:fecha_salida'],['fecha_regreso.after'=> 'La fecha de regreso debe ser una fecha posterior a la fecha de salida']);     		
	    	}    		
    	}

        $solicitud= new Solicitud($request->all());
        $solicitud->origen=$origen;
        $solicitud->destino=$destino;
        if ($request->fecha_regreso==null) {
			 $solicitud->fecha_regreso=null;
			 $solicitud->hora_regreso=null;
		}
        if ($solicitud->estado =='Aprobado') {
            $solicitud->dt_aprobado= date('Y-m-d H:i:s.u');
        }
        $solicitud->save();

        Flash::success("Se ha agregado la solicitud nro.".$solicitud->id." con exito!!");
        return redirect()->route('solicitudes.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $li='solicitudes';
        $solicitud = Solicitud::findOrFail($id);
        if ($solicitud->solicitante_id==Auth::user()->id || $solicitud->sup_id==Auth::user()->id ) {
            $solicitud->solicitante;
            $solicitud->supervisor;
            $solicitud->viajes;
            return view('dashboard.solicitudes.show')->with('solicitud',$solicitud)->with('li',$li);
        }

        else {
            Flash::error("Usted no ha creado ni es el supervisor seleccionado de la solicitud nro. ".$solicitud->id);
            return redirect()->route('solicitudes.index');   
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $li='solicitudes';

        $solicitud = Solicitud::findOrFail($id);
        if ($solicitud->solicitante_id==Auth::user()->id) {
            $padres=Sitio::where('padre', null)->orderBy('nombre')->pluck('nombre','id');
            $supervisores= User::where('rol_id', 2)->orderBy('nombre')->pluck('nombre','id');
            return view('dashboard.solicitudes.edit')->with('solicitud',$solicitud)->with('supervisores',$supervisores)->with('padres',$padres)->with('li',$li);    
        }
        else {
            Flash::error("Usted no ha creado la solicitud nro. ".$solicitud->id);
            return redirect()->route('solicitudes.index');   
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $origen_padre=Sitio::find($request->origen_padre);
        $destino_padre=Sitio::find($request->destino_padre);
        $origen=  $origen_padre->nombre.' - '. $request->origen_hijo.' - '. $request->origen;
        $destino=  $destino_padre->nombre.' - '. $request->destino_hijo.' - '. $request->destino;

        if ( $request->origen_padre==$request->destino_padre && $request->origen_hijo==$request->destino_hijo) {
            if (isset($request->fecha_regreso)) {
                if ($request->fecha_salida == $request->fecha_regreso ) {
                $this->validate($request, 
                ['destino' =>'different:origen',
                 'hora_regreso' =>'after:hora_salida'],
                ['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida',
                 'hora_regreso.after'=> 'La hora de regreso debe ser una hora posterior a la hora de salida']);             
                }
                else {
                $this->validate($request, 
                ['destino' =>'different:origen',
                 'fecha_regreso' =>'after:fecha_salida'],
                ['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida',
                 'fecha_regreso.after'=> 'La fecha de regreso debe ser una fecha posterior a la fecha de salida']);                 
                }           
            }
            else {
                $this->validate($request, 
                ['destino' =>'different:origen'],['destino.different'=> 'El destino de la salida tiene que ser diferente al origen de la salida']);             
            }       
        }
        if (isset($request->fecha_regreso)) {
            if ($request->fecha_salida == $request->fecha_regreso ) {
                $this->validate($request, 
                ['hora_regreso' =>'after:hora_salida'],['hora_regreso.after'=> 'La hora de regreso debe ser una hora posterior a la hora de salida']);          
            }
            else {
                $this->validate($request, 
                ['fecha_regreso' =>'after:fecha_salida'],['fecha_regreso.after'=> 'La fecha de regreso debe ser una fecha posterior a la fecha de salida']);            
            }           
        }

        $solicitud= Solicitud::find($id);
        $solicitud->fill($request->all());
        $solicitud->origen=$origen;
        $solicitud->destino=$destino;
        if ($request->fecha_regreso==null) {
             $solicitud->fecha_regreso=null;
             $solicitud->hora_regreso=null;
        }
        $solicitud->save();

        Flash::success("Se ha actualizado la solicitud nro.".$solicitud->id." con exito!!");
        return redirect()->route('solicitudes.show',  $solicitud->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitud = Solicitud::findOrFail($id);
        $solicitud->delete();
        Flash::error("Se ha eliminado la solicitud nro.".$solicitud->id." con exito!!");
        return redirect()->route('solicitudes.index');
    }

    public function getCiudad($id)
    {
        $sitios=Sitio::where('padre',$id)->orderBy('nombre')->select('id', 'nombre')->get();
        return response()->json($sitios); 
    }

    public function listar($option = 1)
    {
        try {
            switch ($option) {
                case 1:

                    $solicitudes = Solicitud::where('solicitante_id',Auth::user()->id)->get();
                break;
                
                case 2:
                    $solicitudes = Solicitud::where('solicitante_id','!=',Auth::user()->id)->where('sup_id',Auth::user()->id)->get();
                break;
            }
            $solicitudes->each(function($solicitudes){
                $solicitudes->solicitante->departamento;
                $solicitudes->supervisor->departamento;
                $solicitudes->viajes;
                $solicitudes->fecha_salida= date('d-m-Y', strtotime($solicitudes->fecha_salida));           
                $solicitudes->hora_salida= date('h:i A', strtotime($solicitudes->hora_salida));
                if ($solicitudes->fecha_regreso != null) {
                    $solicitudes->fecha_regreso= date('d-m-Y', strtotime($solicitudes->fecha_regreso));           
                    $solicitudes->hora_regreso= date('h:i A', strtotime($solicitudes->hora_regreso));
                }
                if ($solicitudes->estado == 'Rechazado') {
                    $solicitudes->estado = '<b class="text-danger">'.$solicitudes->estado.'</b></br><p class="text-justify">MOTIVO: '.$solicitudes->motivo_rechazo."</p>";
                }
                if ($solicitudes->estado == 'Aprobado') {
                    $solicitudes->estado = '<b class="text-success">'.$solicitudes->estado."</b>";
                    $solicitudes->dt_aprobado= date('d-m-Y h:i A',strtotime($solicitudes->dt_aprobado));
                }
                if ($solicitudes->estado == 'Pendiente' || $solicitudes->estado == 'Con viaje Programado') {
                    $solicitudes->estado = '<b class="text-success">'.$solicitudes->estado."</b>";
                }
                return $solicitudes;
            });          
            return Datatables::of($solicitudes)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en SolicitudesLivianosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    public function aprobar($id)
    {
        $solicitud= Solicitud::findOrFail($id);
        if ($solicitud->estado=='Por Autorizar' || $solicitud->estado=='Rechazado') {
            if ($solicitud->sup_id == Auth::user()->id) {
                $solicitud->estado="Aprobado";
                $solicitud->dt_aprobado= date('Y-m-d H:i:s.u');
                $solicitud->save();
                Flash::success("Se ha APROBADO la solicitud nro.".$solicitud->id." con exito!!");
                return redirect()->route('solicitudes.show',  $solicitud->id);
            }
            else {
                Flash::error("Usted no es el supervisor seleccionado de la solicitud nro. ".$solicitud->id);
                return redirect()->route('solicitudes.show',  $solicitud->id);
            }            
        }
        else {
            Flash::error('ESA OPERACIÓN NO PUEDE REALIZARSE');
            return redirect()->route('solicitudes.show',  $solicitud->id);
        }

    }
    public function rechazar(Request $request, $id)
    {   
        $this->validate($request, 
        ['motivo_rechazo' => 'required']);
        DB::beginTransaction();
        try {
            $solicitud= Solicitud::findOrFail($id);
            if ($solicitud->sup_id == Auth::user()->id) {
                $solicitud->estado="Rechazado";
                $solicitud->motivo_rechazo= $request->motivo_rechazo;
                $solicitud->save();
            }
            DB::commit();
            return response()->json($solicitud);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en SolicitudesLivianosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }
}
