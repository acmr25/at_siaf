<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\EmpleadoRequest;

use App\Empleado;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $li='empleados';
        $tipos=Empleado::orderBy('tipo')->lists('tipo','tipo')->unique();
        $condiciones=Empleado::orderBy('condicion')->lists('condicion','condicion')->unique();
        $contratos=Empleado::orderBy('contrato')->lists('contrato','contrato')->unique();
        return view('dashboard.empleados.index')->with('tipos',$tipos)->with('condiciones',$condiciones)->with('contratos',$contratos)->with('li',$li);
    }

    public function listar()
    {
        try {
            $empleado = Empleado::where('tipo','!=','Proveedor Externo')->get();
            $empleado->each(function($empleado){
                 $empleado->fecha_ingreso = date('d-m-Y', strtotime($empleado->fecha_ingreso));
                return $empleado;
            });
           
            return Datatables::of($empleado)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en EmpleadosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('dashboard.empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        DB::beginTransaction();
        try {
            $empleado = new Empleado($request->all());
            $empleado->fecha_ingreso = date('Y/m/d', strtotime($empleado->fecha_ingreso));
            $empleado->save();
            DB::commit();
            return response()->json($empleado);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en EmpleadoController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }

        //$e= new Empleado($request->all());//all me toma todas las columas del form CREO Xd
        //$e->save();
        //Flash::success("Se ha agregado ". $e->nombre." ". $e->apellido."!!");
        //return redirect()->route('empleados.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {
            $empleado = Empleado::findOrFail($id);
            $empleado->fecha_ingreso = date('d-m-Y', strtotime($empleado->fecha_ingreso));
            return response()->json($empleado);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en EmpleadosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$empleado=Empleado::find($id);
        //$o= ['Chofer'=>'Chofer','Mecánico'=>'Mecánico','Operador'=>'Operador' ];
        //return view ('dashboard.empleados.edit') ->with ('empleado',$empleado) ->with ('o', $o);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'cedula'=>'required|unique:empleados,cedula,'.$id,
                'nombre'=>'required|min:5|max:45',
                'condicion'=>'required',
                'tipo'=>'required',
                'fecha_ingreso'=>'required',
                'contrato'=>'required' ],
            [
                'cedula.required'=>'Llene este campo',
                'cedula.unique'=> 'Ya existe un empleado con esa cédula.',

                'nombre.required'=>'Llene este campo.',
                'nombre.min'=>'El nro. de caracteres debe estar entre 5 a 45.',
                'nombre.max'=>'El nro. de caracteres debe estar entre 5 a 45.',

                'condicion.required'=>'Se requiere este campo.',
                'tipo.required'=>'Se requiere este campo.',

                'cargo.required'=>'Llene este campo.',
                'cargo.min'=>'El nro. de caracteres debe estar entre 5 a 30.',
                'cargo.max'=>'El nro. de caracteres debe estar entre 5 a 30.',
                'fecha_ingreso.required'=>'Se requiere este campo.',
                'contrato.required'=>'Se requiere este campo.']);

         DB::beginTransaction();
        try {
            $empleado = Empleado::findOrFail($id);
            $empleado->fill($request->all());
            $empleado->fecha_ingreso = date('Y/m/d', strtotime($empleado->fecha_ingreso));
            $empleado->save();
            DB::commit();
            return response()->json($empleado);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en EmpleadosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }


//$s= Empleado::find($id);
//$s->fill($request->all());
//$s->save();
//Flash::success("Se ha actualizado al empleado ".$s->nombre. " ".$s->apellido. " con éxito!!");
//return redirect()->route('empleados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::beginTransaction();
        try {
            $empleado = Empleado::findOrFail($id);
            $empleado->delete();
            DB::commit();
            return response()->json($empleado);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en EmpleadosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El empleado debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
}
