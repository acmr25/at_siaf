<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'],function(){

	Route::group(['middleware'=>'activo'],function(){

		Route::get('/inicio', ['as' =>'home.index' , 'uses' => 'HomeController@index']);

		Route::get('/', function () {
			return redirect()->route('home.index');
		});

		Route::get('perfil/index',['uses' => 'PerfilesController@index','as' => 'perfil.index']);
		Route::put('perfil/update/{user}',['uses' => 'PerfilesController@update','as' => 'perfil.update']);
		
		Route::get('solicitudes/{solicitud}/aprobar', ['as' =>'solicitudes.aprobar' , 'uses' => 'SolicitudesLivianosController@aprobar']);
		Route::put('solicitudes/{solicitud}/rechazar', ['as' =>'solicitudes.rechazar' , 'uses' => 'SolicitudesLivianosController@rechazar']);
		Route::get('solicitudes/{estado}/getCiudad', ['as' =>'solicitudes.getCiudad' , 'uses' => 'SolicitudesLivianosController@getCiudad']);
		Route::get('solicitudes/listar/{option?}', ['as' =>'solicitudes.listar' , 'uses' => 'SolicitudesLivianosController@listar']);
		Route::get('solicitudes/supervisor', ['as' =>'solicitudes.supervisor' , 'uses' => 'SolicitudesLivianosController@supervisor']);
		Route::resource('solicitudes', 'SolicitudesLivianosController');

		Route::get('sitios/padres', 'SitiosController@padres');
		Route::get('sitios/listar', ['as' =>'sitios.listar' , 'uses' => 'SitiosController@listar']);
		Route::resource('sitios', 'SitiosController');

		Route::group(['middleware'=>'user_admin'],function(){
			Route::get('usuarios/listar', ['as' =>'usuarios.listar' , 'uses' => 'UsuariosController@listar']);
			Route::get('usuarios', ['as' =>'usuarios.index' , 'uses' => 'UsuariosController@index']);
			Route::post('usuarios', ['as' =>'usuarios.store' , 'uses' => 'UsuariosController@store']);
			Route::delete('usuarios/{usuario}', ['as' =>'usuarios.destroy' , 'uses' => 'UsuariosController@destroy']);
			Route::put('usuarios/{usuario}', ['as' =>'usuarios.update' , 'uses' => 'UsuariosController@update']);	
			Route::get('usuarios/{usuario}', ['as' =>'usuarios.show' , 'uses' => 'UsuariosController@show']);
			Route::get('departamentos/opciones', 'DepartamentosController@opciones');
			Route::get('departamentos/listar', ['as' =>'departamentos.listar' , 'uses' => 'DepartamentosController@listar']);
			Route::resource('departamentos', 'DepartamentosController');
			Route::get('roles/listar', ['as' =>'roles.listar' , 'uses' => 'RolesController@listar']);
			Route::resource('roles', 'RolesController');
			Route::get('/mantenimiento', function () {
				$li='mantenimiento';
				return view('dashboard.mantenimiento.index')->with('li',$li);
				});		
		});

		Route::group(['middleware'=>'app_admin'],function(){
			Route::get('empleados/listar', ['as' =>'empleados.listar' , 'uses' => 'EmpleadosController@listar']);
			Route::resource('empleados', 'EmpleadosController');

			Route::get('proveedores/listar', ['as' =>'proveedores.listar' , 'uses' => 'ProveedoresController@listar']);
			Route::resource('proveedores', 'ProveedoresController');

			Route::get('proveedores/listar', ['as' =>'proveedores.listar' , 'uses' => 'ProveedoresController@listar']);
			Route::resource('proveedores', 'ProveedoresController');

			Route::get('viajes/{viaje}/por_unidad', ['as' =>'viajes.por_unidad' , 'uses' => 'ViajesController@por_unidad']);
			Route::get('viajes/cerrados', ['as' =>'viajes.cerrados' , 'uses' => 'ViajesController@cerrados']);
			Route::put('viajes/completar/{viaje}', ['as' =>'viajes.completar' , 'uses' => 'ViajesController@completar']);
			Route::get('viajes/{viaje}/cerrar', ['as' =>'viajes.cerrar' , 'uses' => 'ViajesController@cerrar']);
			Route::put('viajes/asignar', ['as' =>'viajes.asignar' , 'uses' => 'ViajesController@asignar']);
			Route::get('viajes/opciones', 'ViajesController@opciones');
			Route::post('viajes/nuevo', ['as' =>'viajes.nuevo' , 'uses' => 'ViajesController@nuevo']);
			Route::put('viajes/solicitudes_rechazar', ['as' =>'viajes.solicitudes_rechazar' , 'uses' => 'ViajesController@solicitudes_rechazar']);
			Route::get('viajes/solicitudes_listar/{option?}', ['as' =>'viajes.solicitudes_listar' , 'uses' => 'ViajesController@solicitudes_listar']);
			Route::get('viajes/listar/{option?}', ['as' =>'viajes.listar' , 'uses' => 'ViajesController@listar']);
			Route::resource('viajes', 'ViajesController');		

			Route::get('movimientos/listar', ['as' =>'movimientos.listar' , 'uses' => 'MovimientosController@listar']);
			Route::get('movimientos/{articulo}/movimientos', ['as' =>'movimientos.movimientos' , 'uses' => 'MovimientosController@movimientos']);
			Route::resource('movimientos', 'MovimientosController');

			Route::get('articulos/listar', ['as' =>'articulos.listar' , 'uses' => 'ArticulosController@listar']);
			Route::resource('articulos', 'ArticulosController');

			Route::get('activos/{activo}/pdf', ['as' =>'activos.pdf' , 'uses' => 'ActivosController@pdf']);
			Route::get('activos/listar', ['as' =>'activos.listar' , 'uses' => 'ActivosController@listar']);
			Route::resource('activos', 'ActivosController');

			Route::get('reportes/listar', ['as' =>'reportes.listar' , 'uses' => 'ReporteController@listar']);
			Route::resource('reportes', 'ReporteController');

			Route::get('ordenes/{orden}/pdf', ['as' =>'ordenes.pdf' , 'uses' => 'OrdenController@pdf']);
			Route::put('ordenes/completar/{orden}', ['as' =>'ordenes.completar' , 'uses' => 'OrdenController@completar']);
			Route::get('ordenes/{orden}/cambiar/{estado}', ['as' =>'ordenes.cambiar' , 'uses' => 'OrdenController@cambiar']);
			Route::delete('ordenes/borrar_movimiento/{movimiento}', ['as' =>'ordenes.borrar_movimiento' , 'uses' => 'OrdenController@borrar_movimiento']);
			Route::get('ordenes/getArticulos', ['as' =>'ordenes.getArticulos' , 'uses' => 'OrdenController@getArticulos']);
			Route::post('ordenes/agregar_tarea', ['as' =>'ordenes.agregar_tarea' , 'uses' => 'OrdenController@agregar_tarea']);
			Route::delete('ordenes/{orden}/borrar_tarea/{tarea}', ['as' =>'ordenes.borrar_tarea' , 'uses' => 'OrdenController@borrar_tarea']);
			Route::get('ordenes/{activo}/getTareas', ['as' =>'ordenes.getTareas' , 'uses' => 'OrdenController@getTareas']);
			Route::get('ordenes/listar', ['as' =>'ordenes.listar' , 'uses' => 'OrdenController@listar']);
			Route::resource('ordenes', 'OrdenController');

			//RUTAS DE LAS TAREAS CON ACTIVO ENLAZADO DE UNA VEZ
			Route::post('activo-tarea', ['as' =>'activo-tarea.store' , 'uses' => 'ActivoTareaController@store']);
			Route::get('activo-tarea/{pivot}', ['as' =>'activo-tarea.show' , 'uses' => 'ActivoTareaController@show']);
			Route::put('activo-tarea/{pivot}', ['as' =>'activo-tarea.update' , 'uses' => 'ActivoTareaController@update']);
			Route::delete('activo-tarea/{activo}/{tarea}', ['as' =>'activo-tarea.destroy' , 'uses' => 'ActivoTareaController@destroy']);
			Route::get('activo-tarea/{activo}/listar', ['as' =>'activo-tarea.listar' , 'uses' => 'ActivoTareaController@listar']);

			//RUTAS DE LOS REPORTES CON ACTIVO ENLAZADO DE UNA VEZ
			Route::get('activo-reporte/{reporte}/orden', ['as' =>'activo-reporte.orden' , 'uses' => 'ActivoReporteController@orden']);
			Route::get('activo-reporte/{reporte}/pdf', ['as' =>'activo-reporte.pdf' , 'uses' => 'ActivoReporteController@pdf']);
			Route::get('activo-reporte/{reporte}', ['as' =>'activo-reporte.show' , 'uses' => 'ActivoReporteController@show']);
			Route::get('activo-reporte/{activo}/listar', ['as' =>'activo-reporte.listar' , 'uses' => 'ActivoReporteController@listar']);

			//RUTAS DE LOS ARCHIVOS CON ACTIVO ENLAZADO DE UNA VEZ
			Route::get('activo-archivo/{activo}/nuevo', ['as' =>'activo-archivo.crear' , 'uses' => 'ActivoArchivoController@crear']);
			Route::get('activo-archivo/{activo}/listar', ['as' =>'activo-archivo.listar' , 'uses' => 'ActivoArchivoController@listar']);
			Route::resource('activo-archivo', 'ActivoArchivoController');

			//RUTAS DE LAS ORDENES CON ACTIVO Y/O ENLAZADO DE UNA VEZ
			Route::get('activo-orden/{activo}/nuevo', ['as' =>'activo-orden.nuevo' , 'uses' => 'ActivoOrdenController@nuevo']);
			Route::get('activo-orden/{activo}/listar', ['as' =>'activo-orden.listar' , 'uses' => 'ActivoOrdenController@listar']);			
		});
	});
});


Route::auth();

