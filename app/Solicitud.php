<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitudes';
    protected $fillable = [
                            'solicitante_id',
                            'sup_id',
                            'tipo',
                            'nro_pasajeros',
                            'origen',
                            'fecha_salida',
                            'hora_salida',
							'destino',
                            'fecha_regreso',
                            'hora_regreso',
                            'notas',
                            'pesado_solicitud',
                            'motivo',
                            'estado',
                            'motivo_rechazo',
                            'comentarios',
                            'calificacion',
                            'dt_aprobado'
    						];
    public function solicitante()
    {
        return $this->belongsTo('App\User','solicitante_id');
    }
    public function supervisor()
    {
        return $this->belongsTo('App\User','sup_id');
    }
    public function viajes()
    {
        return $this->belongsToMany('App\Viaje', 'solicitud_viaje');
    }
}
