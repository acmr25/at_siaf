<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
    protected $table = 'activos';
    protected $fillable = [
                            'unidad',
							'placa',
                            'marca',
                            'modelo',
							'serial',
                            'estatus',
                            'nro_activo',
                            'puestos',
                            'prioridad',
                            'clasificacion',
                            'tipo',
                            'año',
							'color',
							'tipo_aceite',
							'filtro_aceite',
							'filtro_aire',
							'filtro_combustible',
							'filtro_agua',
                            'foto',
                            'mantenimiento',
                            'ubicacion_fija',
                            'ubicacion_temporal',
    							];
    public function medida()
    {
    	return $this->hasOne('App\Medida', 'activo_id');
    }
    public function tareas()
    {
    	return $this->belongsToMany('App\Tarea', 'activo_tarea')
                     ->withPivot('id','estado',
                    'horas','pre_horas','realizacion_horas',
                    'kilometros','pre_kilometros','realizacion_kilometros','prioridad','orden_id');
    }
    public function movimientos()
    {
        return $this->hasMany('App\Movimiento','activo_id');
    }
    public function reportes()
    {
        return $this->hasMany('App\Reporte', 'activo_id');
    }
    public function archivos()
    {
    	return $this->hasMany('App\Archivo', 'activo_id');
    }

    public function ordenes()
    {
        return $this->hasMany('App\Orden', 'activo_id');
    }
    public function viajes()
    {
        return $this->belongsToMany('App\Viaje', 'activo_viaje')
                     ->withPivot('id');
    }
}
