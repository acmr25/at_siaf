<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'ordenes';
    protected $fillable = [
							'activo_id',
							'responsable_id',
                            'creador_id',
                            'modificador_id',
                            'reporte_id',
							'estado_orden',
                            'prioridad',
							'inicia',
							'termina',
							'notas'

    						];
    public function activo()
    {
    	return $this->belongsTo('App\Activo','activo_id');
    }
    public function empleado()
    {
    	return $this->belongsTo('App\Empleado','responsable_id');
    }    
    public function movimientos()
    {
    	return $this->hasMany('App\Movimiento','orden_id');
    }
    public function reporte()
    {
        return $this->belongsTo('App\Reporte','reporte_id');
    }    
    public function tareas()
    {
        return $this->belongsToMany('App\Tarea', 'orden_tarea')
                     ->withPivot('id','status');
    }
    public function creador()
        {
            return $this->belongsTo('App\User','creador_id');
        }
    public function modificador()
        {
            return $this->belongsTo('App\User','modificador_id');
        }

}

