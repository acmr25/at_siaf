<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    
    protected $table = 'tareas';
    protected $fillable = ['nombre','mantenimiento'];

    public function activos()
    {
    	return $this->belongsToMany('App\Activo', 'activo_tarea')
                    ->withPivot('id','estado',
                    'horas','pre_horas','realizacion_horas',
                    'kilometros','pre_kilometros','realizacion_kilometros', 'prioridad','orden_id');
    }
    public function ordenes()
    {
        return $this->belongsToMany('App\Orden', 'orden_tarea')
                     ->withPivot('id','status');
    }
}
